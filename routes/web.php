<?php

use App\Http\Controllers\AMemberController;
use App\Http\Controllers\AutoresponderController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PageReportController;
use App\Http\Controllers\SupportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AMemberController::class, 'index'])
    ->name('index');

Route::get('/login', [AMemberController::class, 'login'])
    ->name('login');

Route::get('policy', [SupportController::class, 'showPolicy'])
    ->name('policy');

Route::middleware('auth')->group(function() {

    Route::get('/dashboard', [DashboardController::class, 'index'])
        ->name('index');

    Route::get('/logout', [AMemberController::class, 'logOut'])
        ->name('logout');

    Route::get('support', [SupportController::class, 'index'])
        ->name('support');

    Route::get('support/videos', [SupportController::class, 'showSupportVideos'])
        ->name('support.videos');
});
