<?php

use Carbon\Carbon;
use SendGrid\Mail\Mail;
use SendGrid\Mail\TypeException;
use SendGrid\Response;

if (!function_exists('sendMail')) {
    /**
     * String subject
     * View $template
     * User $user
     * @param $subject
     * @param $template
     * @param $user
     * @return Response
     * @throws TypeException
     */
    function sendMail
    (
        $subject,
        $template,
        $user
    )
    {
        $activationEmail = new Mail();
        $activationEmail->setFrom(
            config('mail.mailers.sendgrid.fromemail'),
            config('mail.mailers.sendgrid.fromname')
        );

        $activationEmail->setSubject($subject);
        $activationEmail->addTo($user->email, config('mail.mailers.sendgrid.fromname'));
        $activationEmail->addContent(
            "text/html",
            $template
        );

        $sendGrid = new \SendGrid(config('mail.mailers.sendgrid.sendgridapikey'));
        return $sendGrid->send($activationEmail);
    }
}

if (!function_exists('assignIfValueIsSet')) {
    /**
     * String subject
     * View $template
     * User $user
     * @param $var
     * @param $value
     * @return void
     */
    function assignIfValueIsSet(&$var, $value)
    {
        if (isset($value)) {
           $var = $value;
        }
    }
}

if (!function_exists('getDatesByMonthFromRange')) {
    /**
     * @param $last_month
     * @param $end
     * @param string $format
     * @return array
     * @author
     * @since 11 Jan 2021
     * @Desc To get an date sequence array from the starting date to the end date with date format
     */
    function getDatesFromRangeByMonth($last_month, $end, $format = 'Y-m')
    {
        $array = array();
        $realEnd  =   Carbon::parse($end);
        $start = Carbon::now()->subMonth($last_month)->startOfMonth();

        for($date = $start->copy(); $date->lte($realEnd); $date->addMonth()) {
            $array[] = $date->format('Y-m');
        }

        return $array;
    }
}

if (!function_exists('getTimeDiff')) {
    /**
     * @param $date
     * @return string
     */
    function getTimeDiff($date)
    {
        $fromDate = strtotime($date);
        $toDate = time();
        $difference = (int) (($toDate - $fromDate)/60) ;

        if ($difference < 1) {
            return "Just now";
        } else if (($difference = ((int) $difference)) < 60) {
            return $difference . " min ago";
        } else if (($difference = ((int) ($difference/60))) < 24) {
            return $difference . " hours ago";
        } else if (($difference = ((int) ($difference/24))) < 31) {
            return $difference . " days ago";
        } else if (($difference = ((int) ($difference/31))) < 12) {
            return $difference . " months ago";
        } else {
            $difference = (int) ($difference/12);
            return $difference . " years ago";
            return date_format($date, 'd/m/Y \a\t h:i a');
        }
    }
}

if (!function_exists('storeImage')) {
    /**
     * @param $requestImage
     * @return string
     */
    function storeImage($requestImage)
    {
        $targetDirectory = "storage/images/uploads/";
        $fileName = time() . '-'. $requestImage->getClientOriginalName();
        $requestImage->move(public_path($targetDirectory), $fileName);

        return asset(sprintf('storage/images/uploads/%s', $fileName));
    }
}

if (!function_exists('destroyImage')) {
    /**
     * @param $url
     * @return string
     */
    function destroyImage($url)
    {
        $targetDirectory = "storage/images/uploads/";
        $fileName = pathinfo($url)['basename'];

        unlink(public_path(sprintf("%s%s", $targetDirectory, $fileName)));
    }
}
