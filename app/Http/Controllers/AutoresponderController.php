<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\User\Models\Autoresponder;
use Citrix\Authentication\Direct;
use Citrix\GoToWebinar;
use Ctct\ConstantContact;
use Ctct\Exceptions\CtctException;
use Fungku\HubSpot;
use GetResponse\GetResponse;
use iContact\iContactApi;
use NZTim\Mailchimp\Mailchimp;
use SendyPHP\SendyPHP;
use Symfony\Component\HttpFoundation\Request;

require_once public_path() . '/autoresponders-libraries/drip-php/Drip_API.class.php';
require_once public_path() . '/autoresponders-libraries/AWeber-API-PHP/aweber_api/aweber_api.php';
require_once public_path() . '/autoresponders-libraries/Citrix-GoToWebinar-master/citrix.php';
require_once public_path() . '/autoresponders-libraries/sendgrid-php/sendgrid-php.php';
require_once public_path() . '/autoresponders-libraries/WebinarJam.php';
require_once public_path() . '/autoresponders-libraries/EverWebinar.php';
require_once public_path() . '/autoresponders-libraries/GetResponseAPI3.class.php';

class AutoresponderController extends Controller
{
    public function userCreatedAutoresponders(Request $request)
    {
        $autoresponder = new Autoresponder();

        $maildata = $autoresponder->RetrieveMailchimp();

        $getdata = $autoresponder->RetrieveGetresponse();

        $icondata = $autoresponder->RetrieveIcontact();

        $hubdata = $autoresponder->RetrieveHubspot();

        $ctctdata = $autoresponder->RetrieveCtct();

        $mimidata = $autoresponder->RetrieveMadmimi();

        $senddata = $autoresponder->Retrievesendy();

        $Camodata = $autoresponder->RetrieveCamo();

        $Dripdata = $autoresponder->RetrieveDrip();

        $Aweberdata = $autoresponder->RetrieveAweber();

        $Flutterdata = $autoresponder->RetrieveFlutter();

        $gvoData = $autoresponder->retrieveGvo();

        $sendData = $autoresponder->retrieveSendlane();

        $gotoData = $autoresponder->RetrieveGoToWebinar();

        $sendgridData = $autoresponder->retrieveSendgrid();

        $webinarjamData = $autoresponder->retrieveWebinarjam();

        $everWebinarData = $autoresponder->RetrieveEverwebinar();

        $ontraportData = $autoresponder->retrieveOntraport();

        $customhtmlData = $autoresponder->retrieveCustomHtml();

        $emptyCheck = $autoresponder->selectAutoresponders();

        return view("User::autoresponder", [
            'userInfo' => [
                'firstName' => session('user')['first_name'],
                'lastName'  => session('user')['last_name'],
                'role'      => session('user')['role'],
                'uid'      => session('user')['user_id'],
                'messages' => session('messages')
            ],
            'camoresult' => $Camodata,
            'sendresult' => $senddata,
            'mimiresult' => $mimidata,
            'ctctresult' => $ctctdata,
            'hubresult' => $hubdata,
            'getresult' => $getdata,
            'mailresult' => $maildata,
            'iconresult' => $icondata,
            'dripresult' => $Dripdata,
            'aweberresult' => $Aweberdata,
            'flutterresult' => $Flutterdata,
            'gvoresult' => $gvoData,
            'sendlResult' => $sendData,
            'gotoResult' => $gotoData,
            'sendgridResult' => $sendgridData,
            'webinarjamResult' => $webinarjamData,
            'everwebinarResult' => $everWebinarData,
            'ontraportResult' => $ontraportData,
            'customhtmlResult' => $customhtmlData,
            'emptyCheck' => $emptyCheck]);
    }
//    MAilchimp
    public function connectToMailchimp(Request $request)
    {
//        dd(session('user')['user_id']);
        $apiKey = $request['apikey'];
        $mailChimpObj = new Mailchimp($apiKey);

        $this->mailChimpObj = $mailChimpObj;
        try {
            $mailchimp = new Autoresponder();
            $get = $mailchimp->InsertMailchimp($request);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        } catch (\Mailchimp_Error $e) {
            echo json_encode(['status' => 401]);
        }
    }

//    GetResponse
    public function connecttToGetResponse(Request $request)
    {
        //$api_key = '0fcdf9df82590c1cea3552104415fc17';
        $getresponse = new GetResponse($request->apikey);
        $result = $getresponse->getAccountFromFields();

        if ($result == null) {
            echo json_encode(['status' => 401]);
        } else {
            $result = (array)$result;
            $key = array_keys($result);
//            $accname = $result[$key[0]]->name;
            $insert = new Autoresponder();
            $get = $insert->InsertGetresponse($request);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    iContact
    public function connectToIcontact(Request $request)
    {
//		$app_id = 'NRuxji5PQwsW3cwile5BzrOeRVwooA5t';
//		$apiPassword = 'autodemopunabakaabhinav';
//		$apiUsername = 'punabakaabhinav@globussoft.in';
        iContactApi::getInstance()->setConfig(array(
            'appId' => $request->apikey,
            'apiPassword' => $request->apipassword,
            'apiUsername' => $request->apiusername
        ));
        $oiContact = iContactApi::getInstance();
        try {
            $lists = $oiContact->getLists();
            $Credintials['Id'] = $request->apikey;
            $Credintials['Password'] = $request->apipassword;
            $Credintials['Username'] = $request->apiusername;
            $data = json_encode($Credintials);
            $insert = new Autoresponder();
            $get = $insert->InsertIcontact($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        } catch (\Exception $oException) {
//                dd($oException->getMessage());
//                $errors = $oiContact->getErrors();
//                dd($errors);
            echo json_encode(['status' => 401]);
        }
    }

//    Hubspot
    public function connectToHubspot(Request $request)
    {
//        $hapikey = 'e82fa78a-02cf-45b2-8f8e-d654f4ceee0c';
        $hubspot = new HubSpot($request->apikey);
        $data = $hubspot->lists()->get_lists(array('count' => 10));
        if (isset($data->status)) {
            echo json_encode(['status' => 401]);
        } else {
            $insert = new Autoresponder();
            $get = $insert->InsertHubspot($request);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    Sendy
    public function connectToSendy(Request $request)
    {
        $config = array(
            'api_key' => $request->apikey, //your API key is available in Settings
            'installation_url' => $request->installationurl,  //Your Sendy installation
            'list_id' => $request->listid
        );
        $sendy = new SendyPHP($config);
//        $result= $sendy->getListId();
        $result = $sendy->subcount();
        if ($result['status'] == false) {
            echo json_encode(['status' => 401]);
        } else {
            $insert = new Autoresponders();
            $Credintials['apikey'] = $request->apikey;
            $Credintials['url'] = $request->installationurl;
            $Credintials['listid'] = $request->listid;
            $data = json_encode($Credintials);
            $insert = new Autoresponder();
            $get = $insert->InsertSendy($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    ConstantContact
    public function connectToConstantContact(Request $request)
    {
        $cc = new ConstantContact($request->apikey);
        try {
            $cc->accountService->getAccountInfo($request->apiacctoken);
            $Credintials['apikey'] = $request->apikey;
            $Credintials['accesstoken'] = $request->apiacctoken;
            $data = json_encode($Credintials);
            $insert = new Autoresponder();
            $get = $insert->InsertCtct($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        } catch (CtctException $ex) {
            echo json_encode(['status' => 401]);
        }
    }

//    MadMimi
    public function connectToMadmimi(Request $request)
    {
        $mimi = new \MadMimi($request->email, $request->apikey);
        $lists = $mimi->Lists();
        if ($lists == 'Unable to authenticate') {
            echo json_encode(['status' => 401]);
        } else {
            $madmimi = new Autoresponder();
            $Credentials['email'] = $request->email;
            $Credentials['apikey'] = $request->apikey;
            $data = json_encode($Credentials);
            $get = $madmimi->InsertMadmimi($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    Campaign Monitor
    public function connectToCampaignMonitor(Request $request)
    {
        $auth = array('api_key' => $request->apikey);
        $wrap = new \CS_REST_Clients($request->clientid, $auth);
        $result = $wrap->get_lists();
        if ($result->was_successful()) {
            $credentials['apikey'] = $request->apikey;
            $credentials['clientid'] = $request->clientid;
            $data = json_encode($credentials);
            $insert = new Autoresponder();
            $get = $insert->InsertCampaignMonitor($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        } else {
            echo json_encode(['status' => 401]);
        }
    }

//    Drip
    public function connectToDrip(Request $request)
    {
        $Drip = new \Drip_Api($request->apikey);
        $data = $Drip->get_accounts();
        if ($data == null) {
            echo json_encode(['status' => 401]);
        } else {
            $Credentials['apikey'] = $request->apikey;
            $Credentials['account_id'] = $data[0]['id'];
            $data = json_encode($Credentials);
            $drip = new Autoresponder();
            $get = $drip->InsertDrip($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    Aweber
    public function connectToAweber(Request $request)
    {
        $consumerKey = env('AWEBER_CONSUMER_KEY');
        $consumerSecret = env('AWEBER_CONSUMER_SECRET');

        $aweber = new \AWeberAPI($consumerKey, $consumerSecret);
        if (!$request->accesstoken) {

// Put the callback URL of your app below or set to 'oob' if your app isnt
// a web based application.
            $callbackURL = 'oob';

// get a request token
            list($requestToken, $requestTokenSecret) = $aweber->getRequestToken($callbackURL);
            setcookie('requestTokenSecret', $requestTokenSecret);
            setcookie('requestToken', $requestToken);
            setcookie('callbackUrl', $callbackURL);
            header("Location: {$aweber->getAuthorizeUrl()}");
            exit();
        } else {
            try {
                if (isset($_COOKIE['requestTokenSecret'])) {
                    $aweber->user->tokenSecret = $_COOKIE['requestTokenSecret'];
                    $aweber->user->requestToken = $_COOKIE['requestToken'];
                    $aweber->user->verifier = $request->accesstoken;
                    list($accessToken, $accessTokenSecret) = $aweber->getAccessToken();
                    $Credentials['consumerKey'] = $consumerKey;
                    $Credentials['consumerSecret'] = $consumerSecret;
                    $Credentials['accessToken'] = $accessToken;
                    $Credentials['accessTokenSecret'] = $accessTokenSecret;
                    $data = json_encode($Credentials);
                    $aweber = new Autoresponder();
                    $get = $aweber->InsertAweber($data, $request->accname);
                    if ($get == "present") {
                        echo json_encode(['status' => 402]);
                    } else {
                        echo json_encode(['status' => 200]);
                    }
                } else {
                    echo json_encode(['status' => 401]);
                }
            } catch (\AWeberAPIException $exc) {
                echo json_encode(['status' => 401]);
            }
        }
    }

//    FlutterMail
    public function connectToFlutterMail(Request $requests)
    {
        $apiurl = $requests->apiurl;
        $params = array(
            'api_key' => $requests->apikey, // Replace with your API Key
            'api_action' => 'list_list', // Action to add a subscriber
            'api_output' => 'serialize', // api_output can be xml or json or serialize
        );
// Define the data
        $post = array(
            'ids' => 'all',
            'status[all]' => 1, // 1: active, 2: unsubscribed
            //(REPLACE '1234' WITH ACTUAL LIST ID)
        );
// Converts the input fields to the proper format
        $query = "";
        foreach ($params as $key => $value) $query .= $key . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $data = "";
        foreach ($post as $key => $value) $data .= $key . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

// clean up the url
        $apiurl = rtrim($apiurl, '/ ');

        if (!function_exists('curl_init')) {

        }
//            die('CURL not supported.');

// Final API request using CURL
        $api = $apiurl . '/admin/api.php?' . $query;
//        dd($api);
        $request = curl_init($api);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $data);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);
        $response = (string)curl_exec($request);
        curl_close($request);
        if (!$response) {
//            die('Nothing was returned. Do you have a connection to Fluttermail server?');
            echo json_encode(['status' => 401]);
        } else {
            $result = unserialize($response);
            if ($result['result_message'] == "You are not authorized to access this file") {
                echo json_encode(['status' => 401]);
            } else {
                $Credentials['apikey'] = $requests->apikey;
                $Credentials['apiurl'] = $requests->apiurl;
                $data = json_encode($Credentials);
                $flutter = new Autoresponder();
                $get = $flutter->insertFlutter($data, $requests->accname);
                if ($get == "present") {
                    echo json_encode(['status' => 402]);
                } else {
                    echo json_encode(['status' => 200]);
                }
            }
        }
    }

//    GVO
    public function connectToGvo(Request $request)
    {
        $gvo = new Autoresponder();
        $get = $gvo->insertGvo($request);
        if ($get == "present") {
            echo json_encode(['status' => 402]);
        } else {
            echo json_encode(['status' => 200]);
        }
    }

//    GoToWebinar
    public function connectToGoToWebinar(Request $request)
    {
        try{
            $client = new Direct($request->apikey);
            $client->auth($request->email, $request->password);
    //        $client->auth('shamstabreza05@gmail.com', 'punabakaABHINAV1234!');
            $goToWebinar = new GoToWebinar($client);
            $webinarsInfo = $goToWebinar->getWebinars();
            $lists = (array)$webinarsInfo;
            if ($lists == null || $lists == "") {
                echo json_encode(['status' => 401]);
            } else {
                $credentials['apikey'] = $request->apikey;
                $credentials['email'] = $request->email;
                $credentials['password'] = $request->password;
                $data = json_encode($credentials);
                $goToWebinar = new Autoresponder();
                $get = $goToWebinar->insertGoToWebinar($data, $request->accname);
                if ($get == "present") {
                    echo json_encode(['status' => 402]);
                } else {
                    echo json_encode(['status' => 200]);
                }
            }
        }catch (\Exception $e){
            echo json_encode(['status' => 401]);
        }
    }

//    Sendlane
    public function connectToSendlane(Request $request)
    {
        $integrations_apikey = $request->apiKey;
        $integrations_hash_key = md5($request->email . $request->apiKey);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://' . $request->domainName . '/api/v1/lists');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "api=$integrations_apikey&hash=$integrations_hash_key");

        $result = curl_exec($ch);
        $result = json_decode($result);
        $key = '401';
        if (isset($result->error->$key) == 'Invalid hash key!' || isset($result->error->$key) == 'Invalid API key!' || $result == null) {
            echo json_encode(['status' => 401]);
        } else {
            $sendlane = new Autoresponder();
            $credentials['domainName'] = $request->domainName;
            $credentials['apiKey'] = $request->apiKey;
            $credentials['email'] = $request->email;
            $data = json_encode($credentials);
            $get = $sendlane->insertSendlane($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//   SendGrid
    public function connectToSendGrid(Request $request)
    {
        $sg = new \SendGrid($request->apikey);
        $response = $sg->client->contactdb()->lists()->get();
        $data = json_decode($response->body());
        if (isset($data->errors)) {
            echo json_encode(['status' => 401]);
        } else {
            $insert = new Autoresponder();
            $get = $insert->insertSendgrid($request);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    WebinarJAm
    public function connectToWebinarJam(Request $request)
    {
        $webinar = new \WebinarJam($request->apikey);
        $result = $webinar->getWebinars();
        if ($result == 'Unauthorized') {
            echo json_encode(['status' => 401]);
        } else {
            $insert = new Autoresponder();
            $get = $insert->insertWebinarJam($request);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    EverWebinar
    public function connectToEverWebinar(Request $request)
    {
//      $api_key = '6e87e64a293e25751981b4cb23af42f2bd49d42c2b95c2af9e6391f75e4de606';
        $webinar = new \EverWebinar($request->apikey);
        $result = $webinar->getWebinars();
        if ($result == 'Unauthorized') {
            echo json_encode(['status' => 401]);
        } else {
            $insert = new Autoresponder();
            $get = $insert->insertEverwebinar($request);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }

//    Disconnect Autoresponder
    public function disconnectAutoresponder($autoid)
    {
        $disconnect = new Autoresponder();
        $disconnect->deleteAutoresponder($autoid);
        return redirect()->back();
    }

//    Delete Autoresponders
    public function deleteAutoresponder($autoname)
    {
        $disconnectAll = new Autoresponder();
        $disconnectAll->deleteAllAutoresponder($autoname);
        return redirect()->back();
    }

//    Retrieving Lists
    public function retrieveLists(Request $request)
    {
        $autoid = new Autoresponder();
        $users = $autoid->retrieveAutoresponder($request->autoId);

        if (!empty($users)) {

//        Mailchimp
            if ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'mailchimp') {
                $mailChimpObj = new Mailchimp($users[0]->api_creds_required);
                $this->mailChimpObj = $mailChimpObj;
                $list = $mailChimpObj->getList();
                if ($list) {
                    echo json_encode(['status' => 200, 'lists' => $list]);
                }
            } //        Getresponse
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'getresponse') {
                $getresponse = new GetResponse($users[0]->api_creds_required);
                $result = $getresponse->getCampaigns();
                $result = (array)$result;
                $keys = (array_keys($result));
                if ($keys) {
                    echo json_encode(['status' => 201, 'lists' => $result, 'keys' => $keys]);
                }
            } //        iContact
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'icontact') {
                $Credentials = json_decode($users[0]->api_creds_required);
                iContactApi::getInstance()->setConfig(array(
                    'appId' => $Credentials->Id,
                    'apiPassword' => $Credentials->Password,
                    'apiUsername' => $Credentials->Username
                ));
                $oiContact = iContactApi::getInstance();
                $lists = $oiContact->getLists();
                if ($lists) {
                    echo json_encode(['status' => 204, 'lists' => $lists]);
                }
            } //        Hubspot
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'hubspot') {
                $hubspot = new HubSpot($users[0]->api_creds_required);
                $lists = $hubspot->lists()->get_lists(array());
                $lists = $lists->lists;
//            dd($lists[0]->name);
                if ($lists) {
                    echo json_encode(['status' => 202, 'lists' => $lists]);
                }
            } //        ConstantContact
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'constantcontact') {
                $Credentials = json_decode($users[0]->api_creds_required);
                $cc = new ConstantContact($Credentials->apikey);
                $lists = $cc->listService->getLists($Credentials->accesstoken);
                if ($lists) {
                    echo json_encode(['status' => 200, 'lists' => $lists]);
                }
            } //        CampaignMonitor
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'campaignmonitor') {
                $Credentials = json_decode($users[0]->api_creds_required);
                $auth = array('api_key' => $Credentials->apikey);
                $wrap = new \CS_REST_Clients($Credentials->clientid, $auth);
                $result = $wrap->get_lists();
                if ($result->response) {
                    echo json_encode(['status' => 207, 'lists' => $result->response]);
                }
            } //        Drip
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'drip') {
                $Credentials = json_decode($users[0]->api_creds_required);
                $Drip = new \Drip_Api($Credentials->apikey);
                $data['account_id'] = $Credentials->account_id;
                $lists = $Drip->get_campaigns($data);
                if ($lists) {
                    echo json_encode(['status' => 200, 'lists' => $lists]);
                }
            } //        Aweber
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'aweber') {
                $Credentials = json_decode($users[0]->api_creds_required);
                $aweber = new \AWeberAPI($Credentials->consumerKey, $Credentials->consumerSecret);
                $account = $aweber->getAccount($Credentials->accessToken, $Credentials->accessTokenSecret);
                $lists = $account->lists->find(array());
//                dd($lists->data['entries']);
                if ($lists) {
                    echo json_encode(['status' => 200, 'lists' => $lists->data['entries']]);
                }
            } //        Madmimi
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'madmimi') {
                $Credentials = json_decode($users[0]->api_creds_required);
                $mimi = new \MadMimi($Credentials->email, $Credentials->apikey);
                $lists = $mimi->Lists();
                $data = json_decode(json_encode(simplexml_load_string($lists)), true);
//            dd($data['list'][0]['@attributes']['id']);
                if ($data['list']) {
                    echo json_encode(['status' => 203, 'lists' => $data['list']]);
                }
            } //        Sendy
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'sendy') {
                $Credentials = json_decode($users[0]->api_creds_required);
                $config = array(
                    'api_key' => $Credentials->apikey, //your API key is available in Settings
                    'installation_url' => $Credentials->url,  //Your Sendy installation
                    'list_id' => $Credentials->listid
                );
                $sendy = new SendyPHP($config);
                $result = $sendy->subcount();
                if ($result) {
                    echo json_encode(['status' => 208, 'lists' => $Credentials, 'listName' => 'Sendy']);
                }
            } //         Fluttermail
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'fluttermail') {
                $Credentials = json_decode($users[0]->api_creds_required);
                $apiurl = $Credentials->apiurl;
                $params = array(
                    'api_key' => $Credentials->apikey,
                    'api_action' => 'list_list',
                    'api_output' => 'serialize',
                );
// Define the data
                $post = array(
                    'ids' => 'all',
                    'status[all]' => 1, // 1: active, 2: unsubscribed

                );
// Converts the input fields to the proper format
                $query = "";
                foreach ($params as $key => $value) $query .= $key . '=' . urlencode($value) . '&';
                $query = rtrim($query, '& ');

                $data = "";
                foreach ($post as $key => $value) $data .= $key . '=' . urlencode($value) . '&';
                $data = rtrim($data, '& ');

// clean up the url
                $apiurl = rtrim($apiurl, '/ ');

                if (!function_exists('curl_init')) {

                }
//            die('CURL not supported.');

// Final API request using CURL
                $api = $apiurl . '/admin/api.php?' . $query;
//        dd($api);
                $request = curl_init($api);
                curl_setopt($request, CURLOPT_HEADER, 0);
                curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($request, CURLOPT_POSTFIELDS, $data);
                curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);
                $response = (string)curl_exec($request);
                curl_close($request);
                $result = unserialize($response);
                if ($result['result_message'] == "Success: Something is returned") {
                    $data = array_except($result, ['result_code', 'result_message', 'result_output']);
                    echo json_encode(['status' => 200, 'lists' => $data]);
                }
            } //        Sendlane
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'sendlane') {

                $credentials = json_decode($users[0]->api_creds_required);
                $integrations_apikey = $credentials->apiKey;
                $integrations_hash_key = md5($credentials->email . $credentials->apiKey);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://' . $credentials->domainName . '/api/v1/lists');

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "api=$integrations_apikey&hash=$integrations_hash_key");

                $result = curl_exec($ch);
                $result = json_decode($result);
                if (isset($result[0])) {
                    echo json_encode(['status' => 205, 'lists' => $result]);
                }
            } //        GoToWebinar
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'gotowebinar') {
                $credentials = json_decode($users[0]->api_creds_required);
                $client = new Direct($credentials->apikey);
                $client->auth($credentials->email, $credentials->password);
                $goToWebinar = new GoToWebinar($client);
                $webinarsInfo = $goToWebinar->getWebinars();
                $lists = (array)$webinarsInfo;
                $listData[] = '';
                for ($i = 0; $i < count($lists); $i++) {
                    $listData[$i] = $lists[$i]->getData();
                }
                if ($lists) {
                    echo json_encode(['status' => 206, 'lists' => $listData]);
                }
            } //        Sendgrid
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'sendgrid') {

                $sg = new \SendGrid($users[0]->api_creds_required);
                $response = $sg->client->contactdb()->lists()->get();
                $data = json_decode($response->body());
                echo json_encode(['status' => 200, 'lists' => $data->lists]);

            } //        WebinarJam
            //        WebinarJam
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'webinarjam') {

                $webinar = new \WebinarJam($users[0]->api_creds_required);
                $result = $webinar->getWebinars();
                $data = array();
                $i = 0;
                foreach($result['webinars'] as $webinars){
                    $j = 0;
                    foreach($webinars['schedules'] as $schedules)
                    {
                        $data[$i]['webinar_id'] = $webinars['webinar_id'];
                        $data[$i]['name'] = $webinars['name'];
                        $data[$i]['schedule'] = $schedules;
                        $data[$i]['scheduleId'] = $j;
                        $i++;
                        $j++;
                    }
                }
                echo json_encode(['status' => 209, 'lists' => $data]);

            }

            //        EverWebinar
            elseif ($users[0]->autoresponders_id == $request->autoId && $users[0]->autoresponders_type == 'everwebinar') {
                $webinar = new \EverWebinar($users[0]->api_creds_required);
                $result = $webinar->getWebinars();
                $data = array();
                $i = 0;
                foreach($result['webinars'] as $webinars){
                    $j = 0;
                    foreach($webinars['schedules'] as $schedules)
                    {
                        $data[$i]['webinar_id'] = $webinars['webinar_id'];
                        $data[$i]['name'] = $webinars['name'];
                        $data[$i]['schedule'] = $schedules;
                        $data[$i]['scheduleId'] = $j;
                        $i++;
                        $j++;
                    }
                }
                echo json_encode(['status' => 209, 'lists' => $data]);
            }
            print_a("dsfhsbjf");
        } else {
            echo json_encode(['status' => 210, 'lists' => 'something went wrong.']);
        }
    }


    //    Ontraport
    public function connectToOntraport(Request $request)
    {
        $appid = $request->appId;
        $key = $request->appKey;
//        $appid = '2_7861_X3YeBz0j1';
//        $key = 'NXhYJvz2AsywN80';
        $reqType = "fetch_sequences";
        $postargs = "appid=" . $appid . "&key=" . $key . "&reqType=" . $reqType;
        $endpoint = "http://api.ontraport.com/cdata.php";
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postargs);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $xmlParserObject = xml_parser_create();
        xml_parse_into_struct($xmlParserObject, $response, $xmlData);
        xml_parser_free($xmlParserObject);
        if(isset($xmlData[1]['value']) == 'Invalid Application Key' || $xmlData == null || $xmlData == '')
        {
            echo json_encode(['status' => 401]);
        }
        else{
            $ontraport = new Autoresponder();
            $credentials['appid'] = $appid;
            $credentials['appkey'] = $key;
            $data = json_encode($credentials);
            $get = $ontraport->insertOntraport($data, $request->accname);
            if ($get == "present") {
                echo json_encode(['status' => 402]);
            } else {
                echo json_encode(['status' => 200]);
            }
        }
    }
}
