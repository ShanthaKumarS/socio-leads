<?php
namespace App\Http\Controllers;

//require_once public_path() . '/amember/library/Am/Lite.php';

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class AMemberController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function index()
    {
//        return Redirect::to('/amember/login');
        return Redirect::to('/login');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function login(Request $request)
    {
//        $user = \Am_Lite::getInstance()->getUser();
        $user['name_f'] = "Shanth";
        $user['name_l'] = "Kumar";
        $user['user_id'] = 1;

        $request->session()->put('user', $user);

        return Redirect::to('dashboard');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logOut(Request $request)
    {
        $request->session()->forget('user');
        $request->session()->forget('pages');

        return Redirect::to('/amember/logout');
    }
}
