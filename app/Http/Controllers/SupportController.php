<?php

namespace App\Http\Controllers;


use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

/**
 * Class SupportController
 * @package App\Http\Controllers
 */
class SupportController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $user = session()->get('user');
        return view('support.support-main', [
            'user' => $user,
        ]);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function showSupportVideos(Request $request)
    {
        $user = session()->get('user');
        return view('support.support', [
            'user' => $user,
        ]);
    }

    /**
     * @return Factory|View
     */
    public function showPolicy()
    {
        return view('support.policy');
    }
}
