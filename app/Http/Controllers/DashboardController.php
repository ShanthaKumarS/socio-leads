<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Facebook\Repositories\FacebookPagesRepository;
use Modules\Facebook\Repositories\FacebookReportRepository;
use Modules\Facebook\Repositories\PageReportRepository;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    const ONE_DAY = 1;
    const ONE_WEEK = 7;
    const ONE_MONTH = 30;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param FacebookReportRepository $facebookReportRepository
     * @param PageReportRepository $pageReportRepository
     * @return Renderable
     */
    public function index(
        Request $request,
        FacebookReportRepository $facebookReportRepository,
        PageReportRepository $pageReportRepository
    )
    {
        try {
            $user = session()->get('user');
            $pages = FacebookPagesRepository::getAllPages($user['user_id']);
            $request->session()->put('pages', $pages);

            $likesCounts = [
                'day' => $facebookReportRepository->setLastDays(self::ONE_DAY)->getLikesCount(),
                'week' => $facebookReportRepository->setLastDays(self::ONE_WEEK)->getLikesCount(),
                'month' => $facebookReportRepository->setLastDays(self::ONE_MONTH)->getLikesCount(),
                'total' => $facebookReportRepository->setLastDays(null)->getLikesCount()
            ];


            $messagesCounts = [
                'day' => $facebookReportRepository->setLastDays(self::ONE_DAY)->getMessagesCount(),
                'week' => $facebookReportRepository->setLastDays(self::ONE_WEEK)->getMessagesCount(),
                'month' => $facebookReportRepository->setLastDays(self::ONE_MONTH)->getMessagesCount(),
                'total' => $facebookReportRepository->setLastDays(null)->getMessagesCount()
            ];

            $commentsCounts = [
                'day' => $facebookReportRepository->setLastDays(self::ONE_DAY)->getCommentsCount(),
                'week' => $facebookReportRepository->setLastDays(self::ONE_WEEK)->getCommentsCount(),
                'month' => $facebookReportRepository->setLastDays(self::ONE_MONTH)->getCommentsCount(),
                'total' => $facebookReportRepository->setLastDays(null)->getCommentsCount()
            ];

            return view('dashboard', [
                'user' => $user,
                'pages' => $pages,
                'commentsCounts' => $commentsCounts,
                'likesCounts' => $likesCounts,
                'messageCounts' => $messagesCounts,
                'pageReportRepository' => $pageReportRepository
            ]);
        } catch (\Exception $e) {
            return view('dashboard', ['user' => $user, 'pages' => []]);
        }
    }
}
