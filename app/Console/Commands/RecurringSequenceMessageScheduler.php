<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Facebook\Http\Controllers\Cron\RecurringSequenceScheduleController;

class RecurringSequenceMessageScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sequence-recurring:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $scheduleController = new RecurringSequenceScheduleController();
        $scheduleController->scheduleMessage();
    }
}
