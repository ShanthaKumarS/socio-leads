<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Facebook\Http\Controllers\Cron\StepSequenceScheduleController;

class StepSequenceMessageScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sequence-step:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $scheduleController = new StepSequenceScheduleController();
        $scheduleController->scheduleMessage();
    }
}
