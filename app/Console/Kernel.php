<?php

namespace App\Console;

use App\Console\Commands\DateSequenceMessageScheduler;
use App\Console\Commands\PageFansUpdate;
use App\Console\Commands\RecurringSequenceMessageScheduler;
use App\Console\Commands\StepSequenceMessageScheduler;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        DateSequenceMessageScheduler::class,
        RecurringSequenceMessageScheduler::class,
        StepSequenceMessageScheduler::class,
        PageFansUpdate::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sequence-date:schedule')->everyMinute()->withoutOverlapping();
        $schedule->command('sequence-recurring:schedule')->everyMinute()->withoutOverlapping();
        $schedule->command('sequence-step:schedule')->everyMinute()->withoutOverlapping();
        $schedule->command('fans-count:update')->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
