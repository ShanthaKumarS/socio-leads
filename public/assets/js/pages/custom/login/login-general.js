"use strict";

// Class Definition
var KTLogin = function() {
    var _login;

    var _showForm = function(form) {
        var cls = 'login-' + form + '-on';
        var form = 'kt_login_' + form + '_form';

        _login.removeClass('login-forgot-on');
        _login.removeClass('login-signin-on');
        _login.removeClass('login-signup-on');

        _login.addClass(cls);

        KTUtil.animateClass(KTUtil.getById(form), 'animate__animated animate__backInUp');
    }

    var _handleSignInForm = function() {
        var validation;

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			KTUtil.getById('kt_login_signin_form'),
			{
				fields: {
                    email: {
						validators: {
							notEmpty: {
								message: 'email is required'
							},
							emailAddress: {
								message: 'The value is not a valid email address'
							}
						}
					},
					password: {
						validators: {
							notEmpty: {
								message: 'Password is required'
							}
						}
					}
				},
				plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // defaultSubmit: new FormValidation.plugins.DefaultSubmit(), // Uncomment this line to enable normal button submit after form validation
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

        $('#kt_login_signin_submit').on('click', function (e) {
            e.preventDefault();
			$('#form_signin_error').html("");
			$('#form_signin_success').html("");

            validation.validate().then(function(status) {
		        if (status == 'Valid') {
                    $.ajax({
                        type: "POST",
                        url: "/authenticate",
                        data: new FormData($('#kt_login_signin_form')[0]),
                        processData: false,
                        contentType: false,
                        success: function(response) {
                            if (response.status == 200) {
                                swal.fire({
                                    text: "All is cool! Now you submit this form",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn font-weight-bold btn-light-primary"
                                    }
                                }).then(function() {
                                    location.href = '/dashboard';
                                });
                            } else {
                                $('#form_signin_error').html(response.content);
                                KTUtil.scrollTop();
                            }
                        },
                        error: function(response) {
                            var errorMessage = response.responseJSON.errors.email + '<br>' + response.responseJSON.errors.password;
                            Swal.fire('Invalid Data!', errorMessage, 'error');
                        }
                    });
				} else {
                    console.log(response);
                    if (response.status == 422) {
                        $.each( response.responseJSON.errors, function( key, value ) {
                            $('#form_signin_error').append(value[0] + "<br>");
                        });
                    } else {
                        swal.fire({
                            text: "Sorry, looks like there are some errors detected, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(function() {
                            KTUtil.scrollTop();
                        });
                    }
				}
		    });
        });

		$('#kt_login_signin_facebook').on('click', function (e){
			e.preventDefault();
			$('#form_signin_error').html("");
			$('#form_signin_success').html("");
            FB.login(
                function (response) {
                    if (response.status === "connected") {
                        $.ajax({
							type: "GET",
							url: "/facebook/login",
							dataType: "json",
							data: {
								"facebookUserId": response.authResponse.userID,
								"userAccessToken": response.authResponse.accessToken,
							},
							success: function(response) {
								location.href = '/dashboard';
							},
							error: function(response) {
								Swal.fire('Connection Fail!', "Somethign wen wrong", 'error');
							}
						});
                    } else {
                        Swal.fire('Connection Fail!', "Problem in facebook connection, please logout from facebook and try again", 'error');
                    }
                }
            );
        });

        // Handle forgot button
        $('#kt_login_forgot').on('click', function (e) {
            e.preventDefault();
            _showForm('forgot');
        });

        // Handle signup
		$('#kt_login_reg_signup').on('click', function (e) {
            e.preventDefault();
            _showForm('signup');
        });
        $('#kt_login_signup').on('click', function (e) {
            e.preventDefault();
            _showForm('signup');
        });
        // // Handle welcome page to login
        $('.welcome-login').on('click', function (e) {
            e.preventDefault();
            _showForm('signin');
        });

    }

    var _handleSignUpForm = function(e) {
        var validation;
        var form = KTUtil.getById('kt_login_signup_form');

		// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			form,
			{
				fields: {
					first_name: {
						validators: {
							notEmpty: {
								message: 'First Name is required'
							}
						}
					},
					email: {
                        validators: {
							notEmpty: {
								message: 'Email address is required'
							},
                            emailAddress: {
								message: 'The value is not a valid email address'
							}
						}
					},
					phno: {
						validators: {
                            phone: {
								country: 'IN',
								message: 'The value is not a valid'
							}
                        }
					},
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            }
                        }
                    },
                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    agree: {
                        validators: {
                            notEmpty: {
                                message: 'You must accept the terms and conditions'
                            }
                        }
                    },
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

        $('#kt_login_signup_submit').on('click', function (e) {
            e.preventDefault();

			$('#form_signup_error').html("");
			$('#form_signin_error').html("");
			$('#form_signin_success').html("");

            validation.validate().then(function(status) {
		        if (status == 'Valid') {
                    $.ajax({
                        type: "POST",
                        url: "/store",
                        data: new FormData($('#kt_login_signup_form')[0]),
                        processData: false,
                        contentType: false,
                        success: function(response) {
                            if (response.status == 200) {
                                swal.fire({
                                    text: "Registration Successfull.. \n Verification link has been sent to your email",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn font-weight-bold btn-light-primary"
                                    }
                                }).then(function() {
                                    $('#form_signin_success').html(response.content);
                                    _showForm('signin');
                                });
                            } else {
                                KTUtil.scrollTop();
                            }
                        },
                        error: function(response) {
                            console.log(response);
                            if (response.status == 422) {
                                $.each( response.responseJSON.errors, function( key, value ) {
                                    $('#form_signup_error').append(value[0] + "<br>");
                                });
                            } else {
                                swal.fire({
                                    text: "Sorry, looks like there are some errors detected, please try again.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn font-weight-bold btn-light-primary"
                                    }
                                }).then(function() {
                                    KTUtil.scrollTop();
                                });
                            }
                        }
                    });
				} else {
					swal.fire({
		                text: "Sorry, looks like there are some errors detected, please try again.",
		                icon: "error",
		                buttonsStyling: false,
		                confirmButtonText: "Ok, got it!",
                        customClass: {
    						confirmButton: "btn font-weight-bold btn-light-primary"
    					}
		            }).then(function() {
						KTUtil.scrollTop();
					});
				}
		    });
        });

        // Handle cancel button
        $('#kt_login_signup_cancel').on('click', function (e) {
            e.preventDefault();

            _showForm('signin');
        });

		$('#kt_login_signup_signin').on('click', function (e) {
            e.preventDefault();

            _showForm('signin');
        });
    }

    var _handleForgotForm = function(e) {
        var validation;

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			KTUtil.getById('kt_login_forgot_form'),
			{
				fields: {
					email: {
						validators: {
							notEmpty: {
								message: 'Email address is required'
							},
                            emailAddress: {
								message: 'The value is not a valid email address'
							}
						}
					}
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

        // Handle submit button
        $('#kt_login_forgot_submit').on('click', function (e) {
            e.preventDefault();

            validation.validate().then(function(status) {
		        if (status == 'Valid') {
					var email = $('#kt_login_forgot_form input[name ="email"]').val();

					$.ajax({
						type: "POST",
						url: "/user/request/reset-password",
						dataType: "json",
						data: {
							"email" : email,
						},
						success: function(response) {
							if (response.status == 200) {
								$('#form_signin_success').html(response.content);
								_showForm('signin');
							} else {
								$('#form_forgot_error').html(response.content);
								KTUtil.scrollTop();
							}
						},
						error: function(response) {
							$('#form_forgot_error').html(response.content);
							KTUtil.scrollTop();
						}
						});

                    KTUtil.scrollTop();
				} else {
					swal.fire({
		                text: "Sorry, looks like there are some errors detected, please try again.",
		                icon: "error",
		                buttonsStyling: false,
		                confirmButtonText: "Ok, got it!",
                        customClass: {
    						confirmButton: "btn font-weight-bold btn-light-primary"
    					}
		            }).then(function() {
						KTUtil.scrollTop();
					});
				}
		    });
        });

        // Handle cancel button
        $('#kt_login_forgot_cancel').on('click', function (e) {
            e.preventDefault();

            _showForm('signin');
        });
    }

	var _handleResetForm = function(e) {
        var validation;

        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			KTUtil.getById('#kt_login_resetpwd_form'),
			{
				fields: {
					password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            }
                        }
                    },
                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

        // Handle submit button
        $('#kt_login_reset_submit').on('click', function (e) {
            e.preventDefault();

            validation.validate().then(function(status) {
		        if (status == 'Valid') {
					var email = $('#kt_login_reset_form input[name ="email"]').val();

					$.ajax({
						type: "POST",
						url: "/user/request/reset-password",
						dataType: "json",
						data: {
							"email" : email,
						},
						success: function(response) {
							if (response.status == 200) {
								$('#form_signin_success').html(response.content);
								_showForm('signin');
							} else {
								$('#form_reset_error').html(response.content);
								KTUtil.scrollTop();
							}
						},
						error: function(response) {
							$('#form_reset_error').html(response.content);
							KTUtil.scrollTop();
						}
						});

                    KTUtil.scrollTop();
				} else {
					swal.fire({
		                text: "Sorry, looks like there are some errors detected, please try again.",
		                icon: "error",
		                buttonsStyling: false,
		                confirmButtonText: "Ok, got it!",
                        customClass: {
    						confirmButton: "btn font-weight-bold btn-light-primary"
    					}
		            }).then(function() {
						KTUtil.scrollTop();
					});
				}
		    });
        });

        // Handle cancel button
        $('#kt_login_reset_cancel').on('click', function (e) {
            e.preventDefault();

            _showForm('signin');
        });
    }

    // Public Functions
    return {
        // public functions
        init: function() {
            _login = $('#kt_login');

            _handleSignInForm();
            _handleSignUpForm();
            _handleForgotForm();
			_handleResetForm();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTLogin.init();
});


var fbid = $('#fbid').val();
window.fbAsyncInit = function () {
	FB.init({
		appId: fbid,
		xfbml: true,
		cookie: true,
		version: 'v10.0'
	});
};

(function (d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function onSignIn(googleUser) {
	var profile = googleUser.getBasicProfile();

	$.ajax({
		type: "POST",
		url: "/user/google-login",
		dataType: "json",
		data: {
			"id" : profile.getId(),
			"name" : profile.getName(),
			"email" : profile.getEmail(),
			"image" : profile.getImageUrl()
		},
		success: function(response) {
			location.href = "/dashboard";
			gapi.auth2.getAuthInstance().signOut();
		},
		error: function(response) {
			console.log("something went wrong");
		}
	});
}
