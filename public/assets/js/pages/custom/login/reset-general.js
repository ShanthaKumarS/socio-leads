"use strict";

// Class Definition
var KTReset = function() {
	var _login;

    var _showForm = function(form) {
        var cls = 'login-' + form + '-on';
        var form = 'kt_login_' + form + '_form';
        _login.removeClass('login-resetpwd-on');
		
        _login.addClass(cls);
        KTUtil.animateClass(KTUtil.getById(form), 'animate__animated animate__backInUp');
    }

	var _handleResetForm = function(e) {
        var validation;
        var form  = KTUtil.getById('kt_login_resetpwd_form');
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        validation = FormValidation.formValidation(
			KTUtil.getById("kt_login_resetpwd_form"),
			{
				fields: {
					password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            }
                        }
                    },
                    cpassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
				},
				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
					bootstrap: new FormValidation.plugins.Bootstrap()
				}
			}
		);

        // Handle submit button
        $('#kt_login_resetpwd_submit').on('click', function (e) {
            e.preventDefault();
            validation.validate().then(function(status) {
		        if (status == 'Valid') {
		
					var email = $('#kt_login_resetpwd_form input[name ="email"]').val();
					var token = $('#kt_login_resetpwd_form input[name ="token"]').val();
					var password = $('#kt_login_resetpwd_form input[name ="password"]').val();
					
					$.ajax({
						type: "POST",
						url: "reset/password",
						dataType: "json",
						data: {
							"email" : email,
							"token" : token,
							"password" : password
						},
						success: function(response) {
							if (response.status == 200) {
								swal.fire({
									text: response.content,
									icon: "success",
									buttonsStyling: false,
									confirmButtonText: "Sign in",
									customClass: {
										confirmButton: "btn font-weight-bold btn-light-primary"
									}
								}).then(function() {
									window.location = "login";
								});
							} else {
								swal.fire({
									text: response.content,
									icon: "error",
									buttonsStyling: false,
									confirmButtonText: "Ok, got it!",
									customClass: {
										confirmButton: "btn font-weight-bold btn-light-primary"
									}
								}).then(function() {
									KTUtil.scrollTop();
								});
							}
							
						},
						error: function(response) {
							$('h3#reset-password-success').html(response.content);
							_showForm("resetpwd");
						}
					});

                    KTUtil.scrollTop();
				} else {
					swal.fire({
		                text: "Sorry, looks like there are some errors detected, please try again.",
		                icon: "error",
		                buttonsStyling: false,
		                confirmButtonText: "Ok, got it!",
                        customClass: {
    						confirmButton: "btn font-weight-bold btn-light-primary"
    					}
		            }).then(function() {
						KTUtil.scrollTop();
					});
				}
		    });
        });

        // Handle cancel button
        $('#kt_login_reset_cancel').on('click', function (e) {
            e.preventDefault();
        });
    }

    // Public Functions
    return {
        // public functions
        init: function() {
			_login = $('#kt_login');
			_handleResetForm();
		
			_showForm("resetpwd");
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTReset.init();
});

