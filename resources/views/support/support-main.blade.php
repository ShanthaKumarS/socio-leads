<?php
/**
 * Created by Raushan Kumar <raushankumarglobussoft.in>
 * Date: 06/05/2017
 * Time: 04:20 PM
 */
?>
@extends('layouts.master')
@section('title','SocioLeads | Support')
@section('title1',' Support')
@section('style')

@endsection

@section('content')



    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed ">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> Support </h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->


    <!--begin::Content-->
    <div class="content  pt-0  d-flex flex-column flex-column-fluid" id="kt_content">

        <!--begin::Entry-->
        <!--begin::Hero-->
        <div class="d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url('../assets/media/bg/bg-9.jpg')">
            <div class=" container ">

                <div class="d-flex align-items-stretch text-center flex-column">
                    <!--begin::Heading-->
                    <h1 class="text-dark font-weight-bolder mt-15">
                        How can we help?
                    </h1>
                    <!--end::Heading-->


                </div>
            </div>
        </div>
        <!--end::Hero-->

        <!--begin::Section-->
        <div class=" container  py-8">
            <div class="row">
                <!-- <div class="col-lg-4">
                    <div class="card card-custom wave wave-animate-slow wave-primary mb-8 mb-lg-0">
                        <div class="card-body">
                            <div class="d-flex align-items-center p-5">
                                <div class="mr-6">
                                    <i class="fas fa-file-alt text-primary icon-2x"></i> </div>

                                <div class="d-flex flex-column">
                                    <a href="#" class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3">
                                        User Guide
                </a>
                                    <div class="text-dark-75">
                                        Complete Knowledgebase
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-6">
                    <!--begin::Callout-->
                    <div class="card card-custom wave wave-animate-slow wave-danger mb-8 mb-lg-0">
                        <div class="card-body">
                            <div class="d-flex align-items-center p-5">
                                <!--begin::Icon-->
                                <div class="mr-6">
                                    <i class="fas fa-video text-danger icon-3x"></i>
                                </div>
                                <!--end::Icon-->

                                <!--begin::Content-->
                                <div class="d-flex flex-column">
                                    <p class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3">
                                        Help Videos
                                    </p>
                                    <div class="text-dark-75">
                                        <p>We have created many training videos to walk you through using the SocioLeads software. Each video clearly explains how to use the software and the different settings and variables available to you in
                                            your account. Please watch these videos prior to sending a support ticket request.</p>
                                    </div>
                                </div>
                                <!--end::Content-->

                            </div>
                            <a href="/support/videos" type="button" class="btn btn-danger btn-block btn-sm">Watch Help Videos</a>
                        </div>
                    </div>
                    <!--end::Callout-->
                </div>
                <div class="col-lg-6">
                    <!--begin::Callout-->
                    <div class="card card-custom wave wave-animate-slow wave-success mb-8 mb-lg-0">
                        <div class="card-body">
                            <div class="d-flex align-items-center p-5">
                                <!--begin::Icon-->
                                <div class="mr-6">
                                    <i class="fas fa-comment-alt text-success icon-3x"></i></div>
                                <!--end::Icon-->

                                <!--begin::Content-->
                                <div class="d-flex flex-column">
                                    <a href="#" class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3">
                                        Submit a Support Ticket
                                    </a>
                                    <div class="text-dark-75">
                                        <p>Our support team is always happy to help you with anything you need. Please click the button below to submit your support ticket to one of our agents.</p>
                                        <p>We try to respond to all support request in under 48 hours.</p>
                                    </div>
                                </div>
                                <!--end::Content-->
                            </div>
                            <button type="button" class="btn btn-success btn-block btn-sm" data-toggle="modal" data-target="#SupportTicket">Submit Support Ticket</button>
                        </div>
                    </div>
                    <!--end::Callout-->
                </div>
            </div>
        </div>
        <!--end::Section-->

        <!--end::Entry-->
    </div>
    <!--end::Content-->







    <!--begin::Footer-->
    @include('layouts.page-footer')



    <!--begin::Modal Step Sequence-->
    <div class="modal fade" id="SupportTicket" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Contact Us</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="supportsubmit" class="form">
                    <input type="hidden" id="token" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control validate" id="name" type="text" name="name" placeholder="Enter Name" />
                                    <span id="spnError1" style="color: Red; display: none; ">Enter Your Name.*</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Email</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control validate" id="email" type="email" name="email" placeholder="Enter email" />
                                    <span id="spnError2" style="color: Red; display: none; ">Enter Your Email.*</span>
                                    <span id="spnError5" style="color: Red; display: none; ">Enter a valid Email.*</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Message</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <textarea id="msg" name="msg" class="form-control" id="exampleTextarea" rows="3" placeholder="Enter Message"></textarea>
                                <span id="spnError3" style="color: Red; display: none;">Enter Message.*</span>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError4" style="color: darkgreen; display: none">Message has been sent successfully.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button"  name="submit" id="submitContact" class="btn btn-primary mr-2">Send</button>
                        <button type="button" class="btn btn-secondary close-modal" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->






@endsection

@section('script_function')


    <script>
        $(document).ready(function () {
            $('.waves-effect.waves-light.btn.modal-trigger.grey.darken-4').on('click',function () {
                $("#spnError1,#spnError2,#spnError3,#spnError4,#success").css("display", "none");
            });



        $(document).on('click', '#submitContact', function (e) {
                e.preventDefault();
                $("#spnError1,#spnError2,#spnError3,#spnError4,#success").css("display", "none");
                var token = $('#token').val();
                var name = $('#name').val();
                var email = $('#email').val();
                var msg = $('#msg').val();

                $("#ContactUsModal").css("display", "block");
                var status = true;
                if (name == null || name == "") {
                    $("#spnError1").css("display", "block");
                    status = false;
                }else if (email == null || email == "") {
                    $("#spnError2").css("display", "block");
                    status = false;
                }
                else if ($('#email').hasClass('invalid')) {
                    $("#spnError5").css("display", "block");
                    status = false;
                }
                else if (msg == null || msg == "") {
                    $("#spnError3").css("display", "block");
                    status = false;
                }

                if (status == false) {
                    return false;
                }

                $.ajax({
                    url: '/user/sendQuery',
                    data: {
                        _token: token,
                        name: name,
                        email: email,
                        msg: msg
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function (mailresponse) {
                        // mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse.status == 200) {
                            $("#spnError4").html('Message has been sent successfully.').css("display", "block").fadeOut(4100).prev('span').hide();
                            window.setTimeout(function(){
                                $(`#SupportTicket`).find('.close-modal').click();

                                $('#name').val(null);
                                $('#email').val(null);
                                $('#msg').val(null);
                            },4000);
                        }else{
                            $("#spnError4").html('Something went wrong, try after some time.').css({'color': 'red','display': 'block'}).fadeOut(6000).prev('span').hide();
                            window.setTimeout(function(){
                                $(`#SupportTicket`).find('.close-modal').click();
                            },5200);
                        }
                    }
                });
            });





        });



    </script>




@endsection


