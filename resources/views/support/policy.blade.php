<?php
/**
 * Created by Raushan Kumar <raushankumarglobussoft.in>
 * Date: 06/05/2017
 * Time: 04:20 PM
 */
?>
@extends('layouts.master')
@section('title','SocioLeads | Privacy Policy')
@section('title1',' Privacy Policy')
@section('style')

@endsection
@section('content')

    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed ">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> Socio Leads Privacy Policy</h5>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->







    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Accordion-->
                <div class="accordion accordion-solid accordion-panel accordion-svg-toggle mb-10" id="policy">
                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading1">
                            <div class="card-title font-size-h4 text-dark" data-toggle="collapse" data-target="#policy1" aria-expanded="true" aria-controls="policy1" role="button">
                                <div class="card-label">Privacy Policy</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy1" class="collapse show" aria-labelledby="policyHeading1" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>This Privacy Policy applies to your use of http://www.MorrisonPublishing.com, http://www.MorrisonEducation.com, http://anthonymorrison.clickfunnels.com, http://www.anthonymorrison.com or any other URL (the “Site”
                                    or “Website(s)”) owned by Morrison Publishing, LLC its subsidiaries, affiliates and partners (collectively the “Company.”) Your privacy is very important to us. We want to make your experience on the Internet
                                    as enjoyable and rewarding as possible, and we want you to use the Internet’s vast array of information, tools, and opportunities with complete confidence.</p>
                                <p>We have created this Privacy Policy to demonstrate our firm commitment to privacy and security. This Privacy Policy describes how our company collects information from all end users of our products and services
                                    (collectively the “Services”) – those who access some of our Services but do not have accounts (“Visitors”) as well as those who may purchase Products and/or pay a monthly service fee to subscribe to the
                                    Service (“Members”) – what we do with the information we collect, and the choices Visitors and Members have concerning the collection and use of such information. We request that you read this Privacy Policy
                                    carefully.
                                </p>
                                <p>By visiting our Site, you are consenting to our policy of collecting and using your data.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading2">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy2" aria-expanded="false" aria-controls="policy2" role="button">
                                <div class="card-label">Personal Information Our Company Collects and How It Is Used Introduction</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy2" class="collapse" aria-labelledby="policyHeading2" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Our company collects information in different ways from Visitors and Members who access the various parts of our Services and the network of Websites accessible through our Service. We do not specifically target
                                    customers in the EU, however, we have updated our Privacy Policy and will continue to remain compliant and up to date on all policies and requirements. Our commitment is to your privacy and protection of
                                    your data.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading3">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy3" aria-expanded="false" aria-controls="policy3" role="button">
                                <div class="card-label">Registration</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy3" class="collapse" aria-labelledby="policyHeading3" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Members may be asked to provide certain personal information when they sign up for our Products or Services including name, address, telephone number, billing information (such as a credit card number), and
                                    the type of personal computer being used to access the Services. The personal information collected from Members during the registration process is used to manage each Member’s account (such as for billing
                                    purposes).
                                </p>
                                <p>We may also generate non-identifying and aggregate profiles from personal information Members provide during registration (such as the total number, but not the names, of Members). As explained in more detail
                                    below, we may use this aggregated and non-identifying information to sell advertisements that appear on the Services.</p>
                                <p>Our Company collects personal information through forms you complete on the site, as well as but not limited to contests, sweepstakes, text messages, Facebook messages, e-mails, faxes, telephone calls, postal
                                    mail or other communications with the user, as well as from outside sources such as credit card processors and database vendors. For example, when you fill out a form subscribing to our emails, we are sent
                                    data information from our autoresponder company such as your name, email, the website you signed up from, geographical data such as your city, state, Postal code, country, Latitude, Longitude, Timestamp,
                                    Subscription method and IP address. Also, with purchases, we supply our member’s contact information, to our business partners, who then may contact the member by telephone after the member purchase. Sales
                                    proceeds are collected by the business partner company.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading4">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy4" aria-expanded="false" aria-controls="policy4" role="button">
                                <div class="card-label">Our Company Partners and Sponsors</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy4" class="collapse" aria-labelledby="policyHeading4" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Some products and services may be offered to Visitors and Members in conjunction with an affiliate, independent contractor seller or non-affiliated partner. To provide Visitors and Members some of these products
                                    and services, the partner may need to collect and maintain personal information.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading5">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy5" aria-expanded="false" aria-controls="policy5" role="button">
                                <div class="card-label">Socio Leads Software Usage</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy5" class="collapse" aria-labelledby="policyHeading5" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>In SocioLeads we are collecting users Facebook page details such as:</p>
                                <ul>
                                    <li>likes on posts</li>
                                    <li>total followers</li>
                                    <li>Fan Page name</li>
                                    <li>Fan Page access tokens</li>
                                </ul>
                                <p>We use Facebook’s webhook to track users activities such as:</p>
                                <ul>
                                    <li>When a person makes a post on your page</li>
                                    <li>Sharing any links on your page</li>
                                    <li>When a user comments on any post on your page</li>
                                </ul>
                                <p>Users may also subscribe to Facebook Message alerts triggered by our software, but may only do so by following the exact guidelines laid out by Facebook policy for subscribing to any type of messaging bot.</p>
                                <p>Once users on your Fan Page create some activity SocioLeads is notified via the webhook. Our software then uses Facebook products like Graph API and Messenger to perform automated activities on your Facebook
                                    Page or on your behalf such as:</p>
                                <ul>
                                    <li>Liking users comments</li>
                                    <li>Replying to users comments</li>
                                    <li>Sending users Facebook Messages</li>
                                </ul>
                                <p>All automations must be generated content by the owner of the Fan Page from inside of SocioLeads. Under no circumstances do we provide pre-written content to be posted on behalf of our users.</p>
                                <p>SocioLeads stores no information about the Facebook users of any kind.</p>
                                <p>SocioLeads does have built in monitoring to make sure users are not spamming the Facebook platform with any type of comments, likes or messages that are not allowed on Facebook.</p>
                                <p>We reserve the right to deactivate an account at any time based on this type of behavior.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading6">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy6" aria-expanded="false" aria-controls="policy6" role="button">
                                <div class="card-label">Online Shopping</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy6" class="collapse" aria-labelledby="policyHeading6" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>You can purchase products and services or register to receive materials, such as a newsletter, catalog or new product and service updates from our Site. In many cases, you may be asked to provide contact information,
                                    such as your name, address, email address, phone number, and credit/debit card information. If you complete an order for someone else, such as an online gift order sent directly to a recipient, you may be
                                    asked to provide information about the recipient, such as the recipient’s name, address, and phone number. Our Company has no control over the third parties’ use of any personal information you provide when
                                    placing such an order. Please exercise care when doing so. If you order services or products directly from our company, we use the personal information you provide to process that order. We do share this
                                    information with outside parties that we do business with.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading7">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy7" aria-expanded="false" aria-controls="policy7" role="button">
                                <div class="card-label">Online Advertisements</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy7" class="collapse" aria-labelledby="policyHeading7" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Our Company may display online advertisements. In those cases we share information about our Visitors and Members collected through the registration process as well as through online surveys and promotions with
                                    these advertisers. Additionally, in some instances, we use this information to deliver tailored advertisements or joint ventures. For instance, an advertiser or joint venture company tells us the audience
                                    they want to reach and provides us an advertisement tailored to the audience. Based upon the information we have collected, we may then display or send the advertisement to the intended audience. Our company
                                    does share personal information about its Visitors or Members with these advertisers or joint venture companies.</p>
                                <p>By visiting our Company Website, you are consenting to our policy of collecting and using your data. If you do not want to consent to our policy of collecting and using your data you can “opt out” of our Re-Targeting,
                                    Social Network and Facebook.com Website Custom Audience Ads advertising policy by notifying Company in the following manner:</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading8">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy8" aria-expanded="false" aria-controls="policy8" role="button">
                                <div class="card-label">Re-Targeting & Behavioral Targeting</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy8" class="collapse" aria-labelledby="policyHeading8" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Our company partners with a 3rd party ad network to display our advertising on other websites. Our ad network partner uses cookies and web beacons to collect non-personally identifiable information about your
                                    activities on this website as well as others. This information is used to provide you targeted advertising across the Internet, based on your interests. Our Company may utilize categories of information,
                                    including names and email addresses, collected from users on our sites and services, as well as third party sites and services, in connection with the ads that are served. This advertising may include, but
                                    not limited to, contextual advertising, cookies, anonymous cookies, pixels, persistent identifiers, geo-location information, email opt in, search engine terms, behavioral advertising and/or retargeting
                                    advertising. This type of advertising is a form of targeted advertising, to the specific individual who is visiting the Web site. These advertisements appear on websites or other media, including display
                                    ads, pop up ads and ads displayed in mobile browsers.</p>
                                <p>Our Company does not conduct inquiries into the information collection practices of third parties that may collect information from users that leave our Web site. Our Company may share customer information with
                                    third parties to process orders, for third party analytics and for marketing and advertising purposes.</p>
                                <p>If you would like to have this information not used for the purpose of showing you targeted ads, you may opt-out by going to this website (http://preferences-mgr.truste.com/). If you are located in the European
                                    Union you may change your preferences by going to this website (http://www.youronlinechoices.eu/). This does not opt you out of being served advertising it only ensures your information will not be used
                                    by us to serve targeted ads. You will still receive generic advertising across the Internet, which has nothing to do with our company.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading9">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy9" aria-expanded="false" aria-controls="policy9" role="button">
                                <div class="card-label">Social Network and Facebook.com Website Custom Audience Ads (WCA)</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy9" class="collapse" aria-labelledby="policyHeading9" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>A Custom Audience on Facebook.com is a list of people our company would like to show our ads to on Facebook.com, who have shown interest in our products and services, with ads we believe would be of interest
                                    to them. This audience consists of people, whose information and email addresses our Company already has, because they have already subscribed to receiving Company emails. If you no longer have an interest
                                    in our products and services, you can opt-out of seeing Company’s Facebook.com Website Custom Audience Ads. Unsubscribing from our Company’s email list will remove you from Company’s internal email database
                                    list and stop future emails from our Company, but not from being shown Facebook.com Website Custom Audience Ads. The Facebook.com Website Custom Audience Ads external database, is different from our Company’s
                                    internal database and requires the following Opting-out request.</p>
                                <p>Opting-out of Facebook.com Website Custom Audience Ads: To opt out of our Company’s Facebook.com Custom Audience Ads, send an email, from the email address you are opting out, to Company using our email address
                                    provided in Company’s contact information. Put “Opting Out of Facebook.com Website Custom Audience Ads” in the subject line of the email. In the body of the email include your name and email address. Our
                                    Company staff will forward your name and email address to Facebook.com with a request to delete you from all of Company’s Facebook.com Website Custom Audience Ads.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading10">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy10" aria-expanded="false" aria-controls="policy10" role="button">
                                <div class="card-label">Google.com Analytics</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy10" class="collapse" aria-labelledby="policyHeading10" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Our Company may use Analytics tracking code to support Display Advertising, and enable Google Analytics to collect data about your traffic via the DoubleClick cookie in addition to data collected through the
                                    standard Google Analytics implementation. Display Advertising lets us enable features in Analytics that aren’t available through standard implementations, like Remarketing with Google Analytics, Google Display
                                    Network Impression Reporting, the DoubleClick Campaign Manager integration, and Google Analytics Demographics and Interest Reporting.</p>
                                <p>Our Company may use Remarketing with Google Analytics to advertise online: This allows third-party vendors, including Google, to show your ads on sites across the Internet. Our Company and third-party vendors,
                                    including Google, use first-party cookies (such as the Google Analytics cookie) and third-party cookies (such as the DoubleClick cookie) together to inform, optimize, and serve ads based on someone’s past
                                    visits to our website.</p>
                                <p>Our Company may implement Google Display Network Impression Reporting or the DoubleClick Campaign Manager: Our Company and third-party vendors, including Google, use first-party cookies (such as the Google Analytics
                                    cookies) and third-party cookies (such as the DoubleClick cookie) together to report how your ad impressions, other uses of ad services, and interactions with these ad impressions and ad services that are
                                    related to visits to your site.</p>
                                <p>Our Company may implement Google Analytics Demographics and Interest Reporting: The data from Google’s Interest-based advertising or 3rd-party audience data (such as age, gender, and interests) with Google Analytics
                                    allows us to serve ads to you based on this data.</p>
                                <p>Opting-out of Google Analytics’ For The Web and Google Analytics for Display Advertising: Users can opt-out of Google Analytics’ currently available opt-outs for the web at https://tools.google.com/dlpage/gaoptout/</p>
                                <p>Using the Ads Settings, users can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads at https://www.google.com/settings/personalinfo</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading11">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy11" aria-expanded="false" aria-controls="policy11" role="button">
                                <div class="card-label">Responses to Email Inquiries</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy11" class="collapse" aria-labelledby="policyHeading11" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>When Visitors or Members send email inquiries to our company, the return email address is used to answer the email inquiry we receive.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading12">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy12" aria-expanded="false" aria-controls="policy12" role="button">
                                <div class="card-label">Voluntary Customer Surveys</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy12" class="collapse" aria-labelledby="policyHeading12" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>We may periodically conduct both business and individual customer surveys. We encourage our customers to participate in these surveys because they provide us with important information that helps us to improve
                                    the types of products and services we offer and how we provide them to you.</p>
                                <p>We may take the information we receive from individuals responding to our Customer Surveys and combine (or aggregate) it with the responses of other customers we may have, to create broader, generic responses
                                    to the survey questions (such as gender, age, residence, hobbies, education, employment, industry sector, or other demographic information). We then use the aggregated information to improve the quality
                                    of our services to you, and to develop new services and products. This aggregated; non-personally identifying information may be shared with third parties.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading13">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy13" aria-expanded="false" aria-controls="policy13" role="button">
                                <div class="card-label">Special Cases</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy13" class="collapse" aria-labelledby="policyHeading13" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>It is our company’s policy to use or share the personal information about Visitors or Members in ways described herein without additional notice or means to opt-out except as noted herein, or otherwise prohibit
                                    such unrelated uses. Also, we may disclose personal information about Visitors or Members, or information regarding your use of the Services or Web sites accessible through our Services, for any reason if,
                                    in our sole discretion, we believe that it is reasonable to do so, including: credit agencies, collection agencies, merchant database agencies, law enforcement, or to satisfy laws, such as the Electronic
                                    Communications Privacy Act, the Child Online Privacy Act, regulations, or governmental or legal requests for such information; to disclose information that is necessary to identify, contact, or bring legal
                                    action against someone who may be violating our Acceptable Use Policy or Terms Of Service, or other user policies; to operate the Services properly; or to protect our company and our Members.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading14">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy14" aria-expanded="false" aria-controls="policy14" role="button">
                                <div class="card-label">Legal Bases for Processing (for EEA users)</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy14" class="collapse" aria-labelledby="policyHeading14" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>If you are an individual in the European Economic Area (EEA), we collect and process information about you only where we have legal bases for doing so under applicable EU laws. The legal bases depend on the
                                    Services you use and how you use them. This means we collect and use your information only where.</p>
                                <ul>
                                    <li>We need it to provide you the Services, including to operate the Services, provide customer support and personalized features and to protect the safety and security of the Services;</li>
                                    <li>It satisfies a legitimate interest (which is not overridden by your data protection interests), such as for research and development, to market and promote the Services and to protect our legal rights and
                                        interests;
                                    </li>
                                    <li>You give us consent to do so for a specific purpose; or</li>
                                    <li>We need to process your data to comply with a legal obligation.</li>
                                    <li>If you have consented to our use of information about you for a specific purpose, you have the right to change your mind at any time, but this will not affect any processing that has already taken place.
                                        Where we are using your information because we or a third party have a legitimate interest to do so, you have the right to object to that use though, in some cases, this may mean no longer using the
                                        Services.
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading15">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy15" aria-expanded="false" aria-controls="policy15" role="button">
                                <div class="card-label">An Understanding Of How We Use Your Information</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy15" class="collapse" aria-labelledby="policyHeading15" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Information we collect may be used in the following ways</p>
                                <ul>
                                    <li>To notify you of updates to our Site or any Products and Services we offer.</li>
                                    <li>To respond to your request or inquires.</li>
                                    <li>To share offers from other companies with you.</li>
                                    <li>To conduct market research and analysis.</li>
                                    <li> To validate your identity.</li>
                                    <li> To keep records of our interactions with you if you place an order or deal with a customer support representative.</li>
                                    <li>To carry out our obligations and enforce our rights.</li>
                                    <li>To provide you with newsletters, articles or products, Products or Services.</li>
                                    <li>To provide you with alerts and announcements.</li>
                                    <li>To process and complete your transactions for orders, customer service, billing and otherwise to fulfill our contractual obligation to you as a customer.</li>
                                    <li>To analyze purchase history and transactions to improve our products, services and programs.</li>
                                    <li>To prevent, investigate or provide notice of fraud, unlawful or criminal activity, or unauthorized access to or us of personally identifiable information.</li>
                                    <li>To fulfill any other purpose for which you provided your personally identifiable information us.</li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading16">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy16" aria-expanded="false" aria-controls="policy16" role="button">
                                <div class="card-label">Email Marketing</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy16" class="collapse" aria-labelledby="policyHeading16" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>With respect to personal data that we do not maintain or control, you may also may also make your request to access, view, update, delete or port your personal data (where available), or to update your subscription
                                    preferences, by following the links at the bottom of any email communication you receive by clicking the “unsubscribe” link or the “manage subscription” link.</p>
                                <p>For other requests, contacting us at <a href="#">privacy@morrisonpublishing.com</a> with the subject line “Opt Out Of Email Marketing” and we will take care of that request immediately.</p>
                                <p>These are the email marketing platforms you may be subscribed to:</p>
                                <ul>
                                    <li>AWEBER</li>
                                    <li>Ontraport</li>
                                    <li>Active Campaign</li>
                                    <li>SendLane</li>
                                    <li> Get Response</li>
                                </ul>
                                <p>If you make a request to delete your personal data and that data is necessary for the products or services you have purchased, the request will be honored only to the extent it is no longer necessary for any
                                    Services purchased or required for our legitimate business purposes or legal or contractual record keeping requirements. If you make a request to obtain a copy of or port your personal data, to the extent
                                    we have any, we will respond within a reasonable time frame.</p>
                                <p>We utilize 5 different email-marketing platforms due to the fact that each one has features and functionality that the other does not. We use each platform for a specific type of correspondence with you; however,
                                    you are able to opt-out at the bottom of any and all emails that we send to you. If you’d like us to remove you from every platform at once you can send that request to privacy@morrisonpublishing.com with
                                    the subject line “Opt Out Of Email Marketing” and we will take care of that request immediately.</p>
                                <p>To our knowledge all of our data controllers have sent out updates that they are now GDRP compliant. We will continue to monitor and make sure that we only utilize companies who are in compliance with all regulatory
                                    requirements.
                                </p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading17">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy17" aria-expanded="false" aria-controls="policy17" role="button">
                                <div class="card-label">Your Rights</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy17" class="collapse" aria-labelledby="policyHeading16" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>You have the right to request any personally identifiable information we have on file about you. If you would like a copy of this information you can contact us at privacy@morrisonpublishing.com to request it.
                                    We will require proof of your identity before sharing this information. If you find that our information is out of date you may ask us to update that information.</p>

                                <p>In the event that your information is within one of our 3rd party Email Marketing programs listed above you can always change or update that information as well as your settings by following the links at the
                                    bottom of any emails sent to you by our company.</p>
                                <p>You may ask us to delete all of your information. It may not be possible for us to delete all of the information we hold about you where we are fulfilling a transaction or have a legal basis to retain the information,
                                    however, feel free to contact us and we can assist you with your request.</p>
                                <p>If you are receiving emails from us and no longer wish to have them to sent to you all you need to do is click the ‘manage subscription’ or ‘unsubscribe’ buttons at the bottom of any and all emails. This does
                                    not apply to transactional based emails where are required to send you proof of purchase and fulfillment of our contractual obligations to deliver purchased products to you.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading18">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy18" aria-expanded="false" aria-controls="policy18" role="button">
                                <div class="card-label">“Cookies” and How Our Company Uses Them</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy18" class="collapse" aria-labelledby="policyHeading18" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>A “cookie” is a small data file that can be placed on your hard drive when you visit certain Web sites. Our company may use cookies to collect, store, and sometimes track information for purposes stated herein
                                    as well as for statistical purposes to improve the products and services we provide and to manage our telecommunications networks.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading19">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy19" aria-expanded="false" aria-controls="policy19" role="button">
                                <div class="card-label">Your Consent</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy19" class="collapse" aria-labelledby="policyHeading19" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>By continuing to use and navigate our Sites and use our Services you are agreeing to our use of cookies, web beacons and similar technologies as described herein.</p>
                                <p>Cookies are essentially small data files placed on your computer, tablet, mobile phone or other device (“collectively, a “device”) that allows us to record information when you visit or interact with our websites,
                                    service, applications, messaging, and other tools. Though often these technologies are generically referred to as “Cookies,” each functions slightly differently, and is better explained below:</p>
                                <p><b>Cookies:</b> These are small text files (typically made up of letters and numbers) placed in the memory of your browser or device when you visit a website or view a message. Cookies allow a website to recognize
                                    a particular device or browser. There are several types of cookies:</p>
                                <ul>
                                    <li>Session cookies expire at the end of your browser session and allow us to link your actions during that particular browser session.</li>
                                    <li>Persistent cookies are stored on your device in between browser sessions, allowing us to remember your preferences or actions across multiple sites.</li>
                                    <li>First-party cookies are those set by a website that is being visited by the user at the time in order to preserve your settings (e.g., while on our site).</li>
                                    <li>Third-party cookies are placed in your browser by a website, or domain, that is not the website or domain that you are currently visiting. If a user visits a website and another entity sets a cookie through
                                        that website this would be a third-party cookie.</li>
                                    <li>When you enter a website using cookies, you may be asked to fill out a form providing personal information; like your name, e-mail address, and interests. This information is packaged into a cookie and sent
                                        to your browser (Chrome, Firefox, etc.), which then stores the information for later use. The next time you go to the same website, your browser will send the cookie to the server. The message is sent
                                        back to the server each time the browser requests a page from the server.</li>
                                    <li><b>Web Beacons:</b> small files (also called “pixels”, “image tags”, or “script tags”) that may be loaded on our Sites that may work in concert with cookies to identify our users and provide anonymized data
                                        on their behavior.</li>
                                    <li><b>Similar Technologies:</b> Technologies that store information in your browser or device utilizing local shared objects or local storage, such as flash cookies, HTML 5 cookies, and other web application
                                        software methods. These technologies can operate across all of your browsers, and in some instances may not be fully managed by your browser and may require management directly through your installed
                                        applications or device. We do not use these technologies for storing information to target advertising to you on or off our sites.</li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading20">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy20" aria-expanded="false" aria-controls="policy20" role="button">
                                <div class="card-label">What Types of Cookies, Web Beacons and Similar Technologies Do We Use and Why</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy20" class="collapse" aria-labelledby="policyHeading20" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Our cookies, web beacons and similar technologies serve various purposes, but are generally either necessary or essential to the functioning of our sites, services, applications, tools or messaging, help us
                                    improve the performance of or provide you extra functionality of the same, or help us to serve relevant and targeted advertisements. More specifically:</p>
                                <p><b>Strictly Necessary or Essential:</b>‘Strictly necessary’ or ‘essential’ cookies, web beacons and similar technologies let you move around Site and use our Services. Without these technologies, Services you
                                    have asked for cannot be provided. Please note that these technologies do not gather any information about you that could be used for marketing or remembering where you’ve been on the internet. Accepting
                                    these technologies is a condition of using our sites, services, applications, tools or messaging, so if you prevent these from loading we can’t guarantee your use or how the security therein will perform
                                    during your visit.</p>
                                <p><b>Performance:</b> ‘Performance’ cookies, web beacons and similar technologies collect information about how you use our website e.g. which pages you visit, and if you experience any errors. These cookies do
                                    not collect any information that could identify you and is only used to help us improve how Site, understand what interests our users and measure how effective our content is by providing anonymous statistics
                                    and data regarding how our website is used. Accepting these technologies is a condition of using our Sites and Services, so if you prevent these from loading we can’t guarantee your use or how the security
                                    therein will perform during your visit.</p>
                                <p><b>Functionality:</b> These cookies, web beacons or similar technologies are used to provide Services or to remember settings to improve your visit.</p>
                                <p><b>Advertising:</b> First or third-party cookies and web beacons may be placed by our Sites or Services in order to deliver content, including product related advertisements, relevant to your specific interests
                                    on our sites or third-party sites. These technologies allow us to understand how useful our advertisements are, and improve the relevancy of the content delivered to our users.</p>
                                <p>We also utilize 3rd party service providers to assist us in delivering on the same functions, which means that our authorized service providers may also place cookies, web beacons and similar technologies on
                                    your device via our services (third party cookies). They may also collect information that helps them identify your device, such as IP-address, or other unique or device identifiers.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading21">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy21" aria-expanded="false" aria-controls="policy21" role="button">
                                <div class="card-label">How To Manage, Control and Delete Cookies, Web Beacons and Similar Technologies</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy21" class="collapse" aria-labelledby="policyHeading21" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>You may block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential
                                    cookies) it may limit your use of certain features or functions on our Sites or Services. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies as soon
                                    as you visit our Site.</p>
                                <p>Internet browsers allow you to change your cookie settings. These settings are usually found in the ‘options’ or ‘preferences’ menu of your internet browser. In order to understand these settings, the following
                                    links may be helpful. Otherwise you should use the ‘Help’ option in your internet browser for more details.</p>
                                <ul>
                                    <li><a href="https://support.microsoft.com/en-us/topic/delete-and-manage-cookies-168dab11-0753-043d-7c16-ede5947fc64d" target="_blank">Cookie settings in Internet Explorer</a></li>
                                    <li><a href="https://support.mozilla.org/en-US/kb/enhanced-tracking-protection-firefox-desktop?redirectslug=enable-and-disable-cookies-website-preferences&redirectlocale=en-US" target="_blank">Cookie settings in Firefox</a></li>
                                    <li><a href="https://support.google.com/chrome/answer/95647?co=GENIE.Platform%3DDesktop&hl=en">Cookie settings in Chrome</a></li>
                                    <li><a href="https://support.apple.com/en-in/guide/safari/sfri11471/mac" target="_blank">Cookie settings in Safari</a></li>
                                </ul>
                                <p>If you wish to withdraw your consent at any time, you will need to delete your cookies using your internet browser settings.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading22">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy22" aria-expanded="false" aria-controls="policy22" role="button">
                                <div class="card-label">More Information About Cookies</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy22" class="collapse" aria-labelledby="policyHeading22" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <ul>
                                    <li>Useful information about cookies, including information about deleting or blocking cookies, can be found at: <a href="http://www.allaboutcookies.org/" target="_blank">http://www.allaboutcookies.org</a> </li>
                                    <li>A guide to behavioral advertising and online privacy has been produced by the internet advertising industry which can be found at: <a href="https://www.youronlinechoices.com/" target="_blank"> http://www.youronlinechoices.eu</a></li>
                                    <li>Information on the ICC (UK) UK cookie guide can be found on the ICC website section:
                                        <a href="https://www.international-chamber.co.uk/our-expertise/digitaleconomy/" target="_blank">http://www.international-chamber.co.uk/our-expertise/digitaleconomy</a> </li>
                                </ul>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading23">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy23" aria-expanded="false" aria-controls="policy23" role="button">
                                <div class="card-label">Deleting Cookies</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->

                        <!--begin::Body-->
                        <div id="policy23" class="collapse" aria-labelledby="policyHeading23" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">

                                <p>If you don’t want our Company’s cookies on your computer, to be used for the purposes stated herein, they are easy to delete. Simply go to <a href="https://www.whoishostingthis.com/resources/cookies-guide/"
                                                                                                                                                                                 target="_blank">http://wiht.link/cookiesguide</a> for instructions. </p>
                                <p>Advertisers and partners may also use their own cookies. We do not control use of these cookies and expressly disclaim responsibility for information collected through them.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading24">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy24" aria-expanded="false" aria-controls="policy24" role="button">
                                <div class="card-label">Public Forums</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div id="policy24" class="collapse" aria-labelledby="policyHeading24" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Please remember that any information you may disclose in any Member Directory, or other public areas of our Websites or the Internet, becomes public information. You should exercise caution when deciding to
                                    disclose personal information in these public areas.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading25">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy25" aria-expanded="false" aria-controls="policy25" role="button">
                                <div class="card-label">Our Company’s Commitment to Data Security</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div id="policy25" class="collapse" aria-labelledby="policyHeading25" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Services and Sites we sponsor have security measures in place to protect the loss, misuse, and alteration of the information under our control. We follow generally accepted industry standards to protect any
                                    and all personal identifiable information given to us by our customers.</p>
                                <p>While we make every effort to ensure the integrity and security of our network and systems, we cannot guarantee that our security measures will prevent third-party “hackers” from illegally obtaining this information.</p>
                                <p>When you make a purchase from our company any sensitive information is entered on pages using orders forms that have encrypted information using SSL.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading26">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy26" aria-expanded="false" aria-controls="policy26" role="button">
                                <div class="card-label">Transfer of Personal Data Abroad</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div id="policy26" class="collapse" aria-labelledby="policyHeading26" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>If you utilize our Services from a country other than the country where our servers are located, your communications with us may result in transferring your personal data across international borders. Also,
                                    when you call us or initiate a chat, we may provide you with support from one of our global locations outside your country of origin. In these cases, your personal data is handled according to this Privacy
                                    Policy. You understand that we may transfer information about in in the United States and other countries, which may have different data protection laws than those in the country you reside.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading27">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy27" aria-expanded="false" aria-controls="policy27" role="button">
                                <div class="card-label">Notice to California Residents – Your California Privacy Rights:</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div id="policy27" class="collapse" aria-labelledby="policyHeading27" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Under California Law SB 27, California residents have the right to receive, once a year, information about third parties with whom we have shared information about you or your family for their marketing purposes
                                    during the previous calendar year, and a description of the categories of personal information shared. To make such a request, please send an email to Company, to the email address provided in our contact
                                    information and please include the phrase “California Privacy Request” in the subject line, the domain name of the Web site you are inquiring about, along with your name, address and email address. We will
                                    respond to you within thirty days of receiving such a request.</p>
                                <p><b>IF YOU ARE UNDER EIGHTEEN YEARS OF AGE, YOU ARE NOT PERMITTED TO ACCESS THIS WEB SITE FOR ANY REASON. DUE TO THE AGE RESTRICTIONS FOR USE OF THIS WEB SITE, NO INFORMATION OBTAINED BY THIS WEB SITE, FALLS WITHIN THE CHILDREN’S ONLINE PRIVACY PROTECTION ACT (COPPA) AND IS NOT MONITORED AS DOING SO.</b></p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading28">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy28" aria-expanded="false" aria-controls="policy28" role="button">
                                <div class="card-label">Where to Direct Questions About Our Privacy Policy</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div id="policy28" class="collapse" aria-labelledby="policyHeading28" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>If you have any questions about this Privacy Policy or the practices described herein, you may contact us through the contact information provided on this Web site.</p>
                                <p>We comply with the Privacy Shield Principles for all onward transfers of personal data from the EU and Switzerland, including the onward transfer liability provisions.</p>
                                <p>With respect to personal data received or transferred pursuant to each Privacy Shield Framework, we are subject to the regulatory enforcement powers of the U.S. Federal Trade Commission. In certain situations,
                                    we may be required to disclose personal data in response to lawful requests by public authorities, including to meet national security or law enforcement requirements.</p>
                                <p>To easily access, view, update, delete or port your personal data (where available), or to update your subscription preferences, please sign into your Account and visit “User Profile.” Please Contact Us privacy@morrisonpublishing.com
                                    for additional information and guidance for accessing, updating or deleting data.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->

                    <!--begin::Item-->
                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="policyHeading29">
                            <div class="card-title font-size-h4 text-dark collapsed" data-toggle="collapse" data-target="#policy29" aria-expanded="false" aria-controls="policy29" role="button">
                                <div class="card-label">Revisions to This Policy</div>
                                <span class="svg-icon svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div id="policy29" class="collapse" aria-labelledby="policyHeading29" data-parent="#policy">
                            <div class="card-body pt-3 font-size-h6 font-weight-normal text-dark-50">
                                <p>Our company reserves the right to revise, amend, or modify this policy, our Terms of Service agreement, and our other policies and agreements at any time and in any manner, by updating this posting. Your use
                                    of this site after such changes are implemented constitutes your acknowledgement and acceptance of these changes. Please consult this privacy statement prior to every use for any changes.</p>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->
                </div>
                <!--end::Accordion-->
                <!--begin::Section-->

                <!--end:: content-->
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
        <!--end::Content-->












    @include('layouts.page-footer')



@endsection
@section('script_function')




@endsection
