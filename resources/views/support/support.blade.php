<?php
/**
 * Created by Raushan Kumar <raushankumarglobussoft.in>
 * Date: 06/05/2017
 * Time: 04:20 PM
 */
?>
@extends('layouts.master')
@section('title','SocioLeads | Support')
@section('title1',' Support')
@section('style')

@endsection

@section('content')
    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed ">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">We are Happy To Help</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <a href="/auto-responder/campaign" type="button" class="btn btn-light-primary btn-sm">List Builder</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->






    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <div class="card card-custom gutter-bs h-100">
                    <!--begin::Header-->
                    <h3 class="display-3 text-center my-auto text-dark-25">THIS PAGE WILL BE
                        AVAILABLE SOON</h3>
                </div>
{{--                    <div class="card-header card-header-tabs-line">--}}
{{--                        <div class="card-toolbar">--}}
{{--                            <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x" role="tablist">--}}
{{--                                @if($supportDetails)--}}
{{--                                    <?php $topics = $embedLink = []; ?>--}}
{{--                                    <?php $tab_first=true; ?>--}}
{{--                                @foreach($supportDetails  as $category)--}}
{{--                                            <?php--}}
{{--                                            $topics[$category->id] = array_combine(explode(',', $category->topic_titles), explode(',', $category->embed_links));--}}
{{--                                            $cat[$category->id] = $category->category_name;--}}
{{--                                            ?>--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link {{$tab_first ? 'active' : ''}}" data-toggle="tab" href="#tab{{$category->id}}">--}}
{{--                                        <span class="nav-text font-weight-bold">{{$category->category_name}}</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                                <?php $tab_first=false; ?>--}}
{{--                                        @endforeach--}}
{{--                                @endif--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <!--end::Header-->

                    <!--begin::Body-->
{{--                    <div class="card-body px-0">--}}
{{--                        <div class="tab-content pt-5">--}}

{{--                        @if($supportDetails)--}}
{{--                            @if($topics!=false && $topics!=null)--}}
{{--                                <?php $box_first=true; ?>--}}
{{--                                @foreach($topics as $key=>$topic)--}}


{{--                            <!--begin::Tab Content-->--}}
{{--                            <div class="tab-pane {{ $box_first ? 'active' : ''}}" id="tab{{$key}}" role="tabpanel">--}}
{{--                                @if($topic!=null&&$topic!=false)--}}

{{--                                 @if(count($topic)<2)--}}
{{--                                        @foreach($topic as $title=>$embedLink)--}}
{{--                                <div class="container">--}}
{{--                                    @if($title)--}}
{{--                                    <h3 class="font-size-h6 mb-5">{{$title}}</h3>--}}
{{--                                    <div class="row">--}}
{{--                                        <!--begin::Card-->--}}
{{--                                        <div class="card-body">--}}
{{--                                            <div class="embed-responsive embed-responsive-16by9">--}}
{{--                                                <iframe src="{{$embedLink}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
{{--                                            </div>--}}
{{--                                            <!--end::Card-->--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    @else--}}
{{--                                        <h3 class="font-size-h6 mb-5">There are no Topics available for this Category!</h3>--}}
{{--                                        <div class="row">--}}
{{--                                        </div>--}}
{{--                                    @endif--}}


{{--                                </div>--}}
{{--                                        @endforeach--}}

{{--                                    @else--}}

{{--                                        <div class="container">--}}

{{--                                            <div class="row">--}}

{{--                                                @foreach($topic as $title=>$embedLink)--}}
{{--                                                <div class="col-md-6">--}}
{{--                                                    <!--begin::Card-->--}}
{{--                                                    <div class="card-body">--}}
{{--                                                        @if($title)--}}
{{--                                                        <h3 class="font-size-h6 mb-5">{{$title}}</h3>--}}
{{--                                                        <div class="embed-responsive embed-responsive-16by9">--}}
{{--                                                            <iframe src="{{$embedLink}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
{{--                                                        </div>--}}
{{--                                                        @else--}}
{{--                                                            <h3 class="font-size-h6 mb-5">There are no Topics available for this Category!</h3>--}}
{{--                                                            <div class="embed-responsive embed-responsive-16by9">--}}
{{--                                                            </div>--}}
{{--                                                        @endif--}}
{{--                                                    </div>--}}
{{--                                                    <!--end::Card-->--}}
{{--                                                </div>--}}
{{--                                                @endforeach--}}


{{--                                            </div>--}}






{{--                                        </div>--}}

{{--                                    @endif--}}



{{--                                @endif--}}
{{--                            </div>--}}
{{--                            <!--end::Tab Content-->--}}




{{--                                <?php $box_first=false; ?>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            @endif--}}











{{--                        </div>--}}
{{--                    </div>--}}
                    <!--end::Body-->
{{--                </div>--}}
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->











    <!--begin::Footer-->
    @include('layouts.page-footer')

@endsection




@section('script_function')

    <script type="text/javascript">


    </script>

@endsection
