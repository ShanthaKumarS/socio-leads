@extends('layouts.master')
@section('title','SocioLeads | Reports')
@section('content')
    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">

                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> Account Report in Graph </h5>
                <!--end::Page Title-->

                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <a href="{{(isset($report['url']))?$report['url']:'/dashboard'}}" class="btn btn-light-warning font-weight-bolder btn-sm"> Go Back </a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->
    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                @if(! isset($postId))
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Total Account Report Chart
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <!--begin::Chart-->
                                <div id="chart_4"></div>
                                <!--end::Chart-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                </div>
                    @elseif(isset($postId))
                    <div class="row">
                        <div class="col-lg-12">
                            <!--begin::Card-->
                            <div class="card card-custom gutter-b">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">
                                            Total Account Report Chart
                                        </h3>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin::Chart-->
                                    <div id="chart_4X"></div>
                                    <!--end::Chart-->
                                </div>
                            </div>
                            <!--end::Card-->
                        </div>
                    </div>
                    @endif
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header h-auto">
                                <!--begin::Title-->
                                <div class="card-title py-5">
                                    <h3 class="card-label">
                                        Messages
                                    </h3>
                                </div>
                                <!--end::Title-->
                            </div>
                            <!--end::Header-->
                            <div class="card-body">
                                <!--begin::Chart-->
                                <div id="chart_1"></div>
                                <!--end::Chart-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Comments
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <!--begin::Chart-->
                                <div id="chart_2"></div>
                                <!--end::Chart-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Likes Count
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <!--begin::Chart-->
                                <div id="chart_3"></div>
                                <!--end::Chart-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                </div>
                @if(! isset($postId))
                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Fans count
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <!--begin::Chart-->
                                <div id="chart_5"></div>
                                <!--end::Chart-->
                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                </div>
                @endif
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Footer-->
    @include('layouts.page-footer')
    <!--end::Footer-->
@endsection

@section('script_function')
    <!--begin::Page Scripts(used by this page)-->
    <script src="/assets/js/pages/features/charts/apexcharts.js"></script>
    <!--end::Page Scripts-->

    <script type="text/javascript">
       var url = '';

        @if (isset($postId))
            url = '/facebook/report/graphs/post/{{$postId}}';
        @elseif (isset($pageId))
            url = '/facebook/report/graphs/page/{{$pageId}}';
        @else
            url = '/facebook/report/graphs';
        @endif

        function reportDetails() {
            $.ajax({
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}"
                },
                type: 'post',
                datatype: 'json',
                success: function (response) {
                    var data = response.response;
                    var messageDetails = [];
                    var commentDetails = [];
                    var likeDetails = [];
                    var fanDetails = [];
                    var messageDetailsF5M = [];
                    var commentDetailsF5M = [];
                    var likeDetailsF5M = [];
                    var fanDetailsF5M = [];

                    (data.hasOwnProperty('messageResult')) ? messageDetails = data.messageResult : messageDetails.push({
                        "dateTime": date,
                        "count": 0
                    });
                    (data.hasOwnProperty('commentResult')) ? commentDetails = data.commentResult : commentDetails.push({
                        "dateTime": date,
                        "count": 0
                    });
                    (data.hasOwnProperty('likeResult')) ? likeDetails = data.likeResult : likeDetails.push({
                        "date": date,
                        "count": 0
                    });
                    @if(! isset($postId))
                        (data.hasOwnProperty('fansResult')) ? fanDetails = data.fansResult : fanDetails.push({
                            "dateTime": date,
                            "fan_gained": 0
                        });
                    @endif
                    (data.hasOwnProperty('messageResultF5M')) ? messageDetailsF5M = data.messageResultF5M : messageDetailsF5M.push({
                        "date": date,
                        "count": 0
                    });
                    (data.hasOwnProperty('commentResultF5M')) ? commentDetailsF5M = data.commentResultF5M : commentDetailsF5M.push({
                        "date": date,
                        "count": 0
                    });
                    (data.hasOwnProperty('likeResultF5M')) ? likeDetailsF5M = data.likeResultF5M : likeDetailsF5M.push({
                        "date": date,
                        "count": 0
                    });
                    @if(! isset($postId))
                        (data.hasOwnProperty('fansResultF5M')) ? fanDetailsF5M = data.fansResultF5M : fanDetailsF5M.push({
                            "dateTime": date,
                            "fan_gained": 0
                        });
                    @endif
                    dataToGraph(
                        messageDetails, commentDetails, likeDetails, fanDetails,
                        messageDetailsF5M, commentDetailsF5M, likeDetailsF5M, fanDetailsF5M
                    );
                }
            });
        }
        reportDetails();
    </script>
@endsection
