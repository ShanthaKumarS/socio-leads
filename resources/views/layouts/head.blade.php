<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Facebook Controls" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="author" content="SocioLeads">

{{--<!-- from old Code -->--}}
{{--<link type="text/css" rel="stylesheet" href="/assets/plugins/materialize/css/materialize.css"  media="screen,projection"/>--}}

<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->

<!--begin::Global Theme Styles(used by all pages)-->
{{--<link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />--}}
{{--<link href="{{asset('assets/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css" />--}}
{{--<link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />--}}

<link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
<link href="/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/>
<link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
<!--end::Global Theme Styles-->

<!--begin::Layout Themes(used by all pages)-->
{{--<link href="{{asset('assets/css/themes/layout/header/base/light.css')}}" rel="stylesheet" type="text/css" />--}}
{{--<!-- <link href="../assets/css/themes/layout/header/menu/light.css" rel="stylesheet" type="text/css" /> -->--}}
{{--<!-- <link href="../assets/css/themes/layout/brand/dark.css" rel="stylesheet" type="text/css" /> -->--}}
{{--<link href="{{asset('assets/css/themes/layout/aside/dark.css')}}" rel="stylesheet" type="text/css" />--}}

<link href="/assets/css/themes/layout/header/base/light.css" rel="stylesheet" type="text/css"/>
<link href="/assets/css/themes/layout/aside/dark.css" rel="stylesheet" type="text/css"/>

<link rel="shortcut icon" href="/assets/media/logos/favicon.png" />

<link type="text/css" rel="stylesheet" href="/assets/css/style.css"  media="screen,projection"/>

{{--<link rel="stylesheet" type="text/css" href="/assets/plugins/DataTables/media/css/jquery.dataTables.css">--}}

{{--<link type="text/css" rel="stylesheet" href="/assets/css/custom.css"  media="screen,projection"/>--}}
{{--<!--link type="text/css" rel="stylesheet" href="/assets/css/sidebar.css"  media="screen,projection"/-->--}}

{{--<link rel="stylesheet" type="text/css" href="/assets/plugins/simple-line-icons/simple-line-icons.css">--}}
{{--<link rel="stylesheet" type="text/css" href="/assets/plugins/font-awesome/css/font-awesome.css">--}}
{{--<link rel="stylesheet" type="text/css" href="/assets/plugins/DataTables/media/css/jquery.dataTables.css">--}}

{{--<link rel="stylesheet" type="text/css" href="/assets/plugins/sweetalert/dist/sweetalert.css">--}}
{{--<link rel="stylesheet" type="text/css" href="/assets/plugins/animate.css">--}}
