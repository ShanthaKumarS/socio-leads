<!--begin::Aside-->
<div class="aside aside-left  aside-fixed  d-flex flex-column flex-row-auto" id="kt_aside">
    <!--begin::Brand-->
    <div class="brand flex-column-auto " id="kt_brand">
        <!--begin::Logo-->
{{-- 05.01.2021 SK --}}
        <a href="#" class="brand-logo">
            <img alt="Logo" src="/assets/media/logos/logo-light.png" />
        </a>
        <!--end::Logo-->

        <!--begin::Toggle-->
        <button class="brand-toggle btn btn-sm" id="kt_aside_toggle">
            <div class="mr-4 flex-shrink-0 text-center">
                <i class="text-dark-50 ki ki-double-arrow-back"></i>
            </div>			</button>
        <!--end::Toolbar-->
    </div>
    <!--end::Brand-->

    <!--begin::Aside Menu-->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">

        <!--begin::Menu Container-->
        <div id="kt_aside_menu" class="aside-menu my-4 " data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <!--begin::Menu Nav-->
            <ul class="menu-nav ">
                <li class="menu-item {{ (request()->is('dashboard')) ? 'menu-item-active' : '' }}" aria-haspopup="true"><a href="/dashboard" class="menu-link "><i class="fas fa-tachometer-alt menu-icon"></i><span class="menu-text">Dashboard</span></a></li>
                <li class="menu-item {{ (request()->is('auto-responder/campaign')) ? 'menu-item-active' : '' }}" aria-haspopup="true"><a href="/facebook/report" class="menu-link "><i class="fas fa-user-circle menu-icon"></i><span class="menu-text">Account Report</span></a></li>
                <li class="menu-item {{ (request()->is('user/auto-responder')) ? 'menu-item-active' : '' }}" aria-haspopup="true"><a href="/auto-responder" class="menu-link "><i class="fas fa-comments menu-icon"></i><span class="menu-text">Auto Respond</span></a></li>
                <li class="menu-item {{ (request()->is('auto-responder/campaign')) ? 'menu-item-active' : '' }}" aria-haspopup="true"><a href="/auto-responder/campaign" class="menu-link "><i class="fas fa-list menu-icon"></i><span class="menu-text">List Builder</span></a></li>
                <li class="menu-item {{ (request()->is('support')) ? 'menu-item-active' : '' }}" aria-haspopup="true"><a href="/support" class="menu-link "><i class="far fa-question-circle menu-icon"></i><span class="menu-text">Support</span></a></li>
                <li class="menu-item " aria-haspopup="true"><a href="/logout" class="menu-link "><i class="fas fa-sign-out-alt menu-icon"></i><span class="menu-text">LogOut</span></a></li>
            </ul>
            <!--end::Menu Nav-->
        </div>
        <!--end::Menu Container-->
    </div>
    <!--end::Aside Menu-->
</div>
<!--end::Aside-->
