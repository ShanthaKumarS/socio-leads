<!--begin::Topbar-->
<div class="topbar">
    <!--begin::Search-->
    <div class="dropdown" id="kt_quick_search_toggle">
        <!--begin::Toggle-->
        <div class="topbar-item" data-toggle="dropdown">
            <div class="btn btn-icon btn-clean btn-lg btn-dropdown mr-1">
                <i class="fas fa-search"></i>
                </span>
            </div>
        </div>
        <!--end::Toggle-->
        <!--begin::Dropdown-->
        <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
            <div class="quick-search quick-search-dropdown" id="kt_quick_search_dropdown">
                <!--begin:Form-->
                <form method="get" class="quick-search-form">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="flaticon-search-magnifier-interface-symbol"></i>
                                        </span>
                        </div>
                        <input type="text" class="form-control" placeholder="Search..." />
                        <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="quick-search-close ki ki-close icon-sm"></i>
                                        </span>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
                <!--begin::Scroll-->
                <div class="quick-search-wrapper scroll" data-scroll="true" data-height="325" data-mobile-height="200">
                </div>
                <!--end::Scroll-->
            </div>
        </div>
        <!--end::Dropdown-->
    </div>
    <!--end::Search-->
    <!--begin::Languages-->
{{--    <div class="dropdown">--}}
{{--        <!--begin::Toggle-->--}}
{{--        <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">--}}
{{--            <div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1">--}}
{{--                <img class="h-20px w-20px rounded-sm" src="/assets/media/svg/flags/226-united-states.svg" alt="" />--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <!--end::Toggle-->--}}

{{--        <!--begin::Dropdown-->--}}
{{--        <div class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right">--}}
{{--            <!--begin::Nav-->--}}
{{--            <ul class="navi navi-hover py-4">--}}
{{--                <!--begin::Item-->--}}
{{--                <li class="navi-item">--}}
{{--                    <a href="/en/user/dashboard" class="navi-link">--}}
{{--                                    <span class="symbol symbol-20 mr-3">--}}
{{--                                        <img src="/assets/media/svg/flags/226-united-states.svg" alt=""/>--}}
{{--                                    </span>--}}
{{--                        <span class="navi-text">English</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <!--end::Item-->--}}

{{--                <!--begin::Item-->--}}
{{--                <li class="navi-item active">--}}
{{--                    <a href="/sp/user/dashboard" class="navi-link">--}}
{{--                                    <span class="symbol symbol-20 mr-3">--}}
{{--                                        <img src="/assets/media/svg/flags/128-spain.svg" alt=""/>--}}
{{--                                    </span>--}}
{{--                        <span class="navi-text">Spanish</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <!--end::Item-->--}}

{{--                <!--begin::Item-->--}}
{{--                <li class="navi-item">--}}
{{--                    <a href="/de/user/dashboard" class="navi-link">--}}
{{--                                    <span class="symbol symbol-20 mr-3">--}}
{{--                                        <img src="/assets/media/svg/flags/162-germany.svg" alt=""/>--}}
{{--                                    </span>--}}
{{--                        <span class="navi-text">German</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <!--end::Item-->--}}

{{--                <!--begin::Item-->--}}
{{--                <li class="navi-item">--}}
{{--                    <a href="/jp/user/dashboard" class="navi-link">--}}
{{--                                    <span class="symbol symbol-20 mr-3">--}}
{{--                                        <img src="/assets/media/svg/flags/063-japan.svg" alt=""/>--}}
{{--                                    </span>--}}
{{--                        <span class="navi-text">Japanese</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <!--end::Item-->--}}

{{--                <!--begin::Item-->--}}
{{--                <li class="navi-item">--}}
{{--                    <a href="/fr/user/dashboard" class="navi-link">--}}
{{--                                    <span class="symbol symbol-20 mr-3">--}}
{{--                                        <img src="/assets/media/svg/flags/195-france.svg" alt=""/>--}}
{{--                                    </span>--}}
{{--                        <span class="navi-text">French</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <!--end::Item-->--}}
{{--            </ul>--}}
{{--            <!--end::Nav-->--}}
{{--        </div>--}}
{{--        <!--end::Dropdown-->--}}
{{--    </div>--}}
    <!--end::Languages-->

    <!--begin::User-->
    <div class="topbar-item">
        <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
            <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,
                @isset($user['name_f'])
                    {{$user['name_f']}}
                @endisset
            </span>

            <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                <span class="symbol-label font-size-h5 font-weight-bold">
                    @isset($user['name_f'])
                        {{substr($user['name_f'], 0, 1)}}
                    @endisset
                </span>
            </span>

        </div>
    </div>
    <!--end::User-->
</div>
<!--end::Topbar-->
