<!DOCTYPE html>
<html>
<head>
    @include('layouts.head')
    <title>@yield('title')</title>
    @yield('style')
</head>
<!--begin::Body-->
<body id="kt_body" class="header-fixed header-mobile-fixed aside-enabled aside-fixed aside-minimize-hoverable">
    <!--begin::Main-->
    @include('layouts.header')
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
        @include('layouts.sidebar')
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            @yield('content')
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->
@yield('campaignModalStructure')
@include('layouts.script')
@include('layouts.footer')
@yield('script_function')
</body>
</html>
