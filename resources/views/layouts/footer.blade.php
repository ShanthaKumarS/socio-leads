<!-- begin::User Panel-->
<div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
    <!--begin::Header-->
    <div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
        <h3 class="font-weight-bold m-0">
            User Profile
        </h3>
        <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
            <i class="ki ki-close icon-xs text-muted"></i>
        </a>
    </div>
    <!--end::Header-->

    <!--begin::Content-->
    <div class="offcanvas-content pr-5 mr-n5">
        <!--begin::Header-->
        <?php if(isset($user['user_id'])){ ?>
        <div class="d-flex align-items-center mt-5">
            <div class="symbol symbol-100 mr-5">


            <span class="symbol symbol-light-success">
                <span class="symbol-label font-weight-bold display-4">
                    @isset($user['name_f'])
                        {{substr($user['name_f'], 0, 1)}}
                    @endisset
                </span>
            </span>

                <i class="symbol-badge bg-success"></i>
            </div>
            <div class="d-flex flex-column">
                <a href="/user/profile" class="font-weight-bold font-size-h5 text-dark-75">
                    @isset($user['name_f'])
                        {{$user['name_f']}}
                    @endisset
                    <i id="edit-profile" class='fas fa-pencil-alt text-hover-primary'></i>
                </a>

                <div class="navi mt-2">
                    <a href="/logout" class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">Sign Out</a>
                </div>
            </div>
        </div>
        <?php } ?>
        <!--end::Header-->
    </div>
    <!--end::Content-->
</div>
<!-- end::User Panel-->

<script type="text/javascript">
</script>
