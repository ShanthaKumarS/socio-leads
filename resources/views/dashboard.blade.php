@extends('layouts.master')
@section('title','SocioLeads | Dashboard')
@section('style')
    <style>
        .truncate-name {
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 200px;
        }

        .fixed-center {
            display: inline-flex;
            text-align: center;
            width: 100%;
        }

        @media screen and (max-width: 799px) {
            .fixed-center {
                display: block;
                text-align: center;
                width: 100%;
            }
        }

        .reauth-page-name {
            color: #f35e5e;
            font-weight: bold;
        }

    </style>
@endsection
@section('content')
    @if(Session::get('login-success'))
        @if(Session::has('type'))
            <script>
                var url = '/dashboard';
                @if(Session::has('preUrl'))
                    url = '<?php echo Session::get('preUrl')?>';
                @endif
                    window.opener.location = url;
                window.close();
            </script>
            {{Session::forget('type')}}
        @endif
    @endif
    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> Dashboard </h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm" onclick="myFacebookLogin(false);">  Add New </a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->

    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row">
                    <div class="col-lg-12 col-xxl-12">
                        <!--begin::Mixed Widget 1-->
                        <div class="card card-custom bg-gray-100 card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0 position-relative overflow-hidden">
                                <!--begin::Stats-->
                                <div class="card-custom gutter-b gutter-t">
                                    <!--begin::Row-->
                                    <div class="row m-0">
                                        <div class="col-sm">
                                            <div class="bg-light-primary px-6 py-8 rounded-xl border border-primary">
                                                <a href="#" class="text-primary font-weight-bold font-size-h4">
                                                    Sent Message
                                                </a>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-confetti text-primary icon-2x"></i>
                                                            </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-size-sm">Today</span>
                                                            <span class="font-size-h5 mgT">{{$messageCounts['day']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-pie-chart icon-2x text-primary"></i>
                                                            </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-size-sm">Past 7 Days</span>
                                                            <span class="ont-size-h5 mgW">{{$messageCounts['week']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-file-2 icon-2x text-primary"></i>
                                                            </span>
                                                        <div class="d-flex flex-column flex-lg-fill">
                                                            <span class="font-size-sm">Last Month</span>
                                                            <span class="font-size-h5 mgM">{{$messageCounts['month']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-chat-1 icon-2x text-primary"></i>
                                                            </span>
                                                        <div class="d-flex flex-column">
                                                            <span class="font-size-sm">Life time</span>
                                                            <span class="font-size-h5 mgTt">{{$messageCounts['total']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="bg-light-danger px-6 py-8 rounded-xl border border-danger">
                                                <a href="#" class="text-danger font-weight-bold font-size-h4">
                                                    Liked Comment
                                                </a>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-confetti text-danger icon-2x"></i>
                                                            </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-size-sm">Today</span>
                                                            <span class="font-size-h5 lkT">{{$likesCounts['day']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-pie-chart icon-2x text-danger"></i>
                                                            </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-size-sm">Past 7 Days</span>
                                                            <span class="ont-size-h5 lkW">{{$likesCounts['week']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-file-2 icon-2x text-danger"></i>
                                                            </span>
                                                        <div class="d-flex flex-column flex-lg-fill">
                                                            <span class="font-size-sm">Last Month</span>
                                                            <span class="font-size-h5 lkM">{{$likesCounts['month']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                <i class="flaticon-chat-1 icon-2x text-danger"></i>
                                                            </span>
                                                        <div class="d-flex flex-column">
                                                            <span class="font-size-sm">Life time</span>
                                                            <span class="font-size-h5 lkTt">{{$likesCounts['total']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="bg-light-info px-6 py-8 rounded-xl border border-info">
                                                <a href="#" class="text-info font-weight-bold font-size-h4">
                                                    Sent comment
                                                </a>
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                    <i class="flaticon-confetti text-info icon-2x"></i>
                                                                </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-size-sm">Today</span>
                                                            <span class="font-size-h5 cmT">{{$commentsCounts['day']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                    <i class="flaticon-pie-chart icon-2x text-info"></i>
                                                                </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-size-sm">Past 7 Days</span>
                                                            <span class="ont-size-h5 cmW">{{$commentsCounts['week']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                    <i class="flaticon-file-2 icon-2x text-info"></i>
                                                                </span>
                                                        <div class="d-flex flex-column flex-lg-fill">
                                                            <span class="font-size-sm">Last Month</span>
                                                            <span class="font-size-h5 cmM">{{$commentsCounts['month']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->

                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                                <span class="mr-4">
                                                                    <i class="flaticon-chat-1 icon-2x text-info"></i>
                                                                </span>
                                                        <div class="d-flex flex-column">
                                                            <span class="font-size-sm">Life time</span>
                                                            <span class="font-size-h5 cmTt">{{$commentsCounts['total']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Row-->
                                </div>
                                <!--end::Stats-->
                                <div class="card-columns dashboard-card-col" >
                                    <div id="facebookpages" class="card col-sm">
                                        <div class="addfbpg"></div>
                                    @if($pages)
                                        @foreach($pages as $page)
                                            <?php $pageReportRepository->setPageId($page->id);?>
                                            <!--begin::Card-->
                                            <div class="card-body dashboard-card-body">
                                                <!--begin::Top-->
                                                <div class="d-flex align-items-center">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-40 symbol-light-success mr-5">
                                                        <span class="symbol-label">
                                                            <img src="{{file_exists(ltrim($page->profile_picture, '/'))? $page->profile_picture : 'https://graph.facebook.com/'.$page->facebook_page_id.'/picture?type=small'}}"
                                                                 alt="" class="circle responsive-img activator card-profile-image h-100">
                                                        </span>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Info-->
                                                    <div class="d-flex flex-column flex-grow-1">
                                                        <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">{{$page->page_name}}</a>
                                                        <span class="text-muted font-weight-bold">Created at {{date('d M Y', strtotime($page->created_at))}}</span>
                                                    </div>
                                                    <!--end::Info-->
                                                </div>
                                                <!--end::Top-->
                                                <!--begin::Bottom-->
                                                <div class="pt-4">
                                                    <!--begin::Image-->
                                                    <div class="bgi-no-repeat bgi-size-cover rounded h-150px" style="background-image: url({{file_exists(ltrim($page->cover_photo, '/'))?$page->cover_photo:'/assets/images/default.jpg'}})"></div>
                                                    <!--end::Image-->
                                                    <!--begin::Text-->
                                                    <!--p class="text-dark-75 font-size-lg font-weight-normal pt-5 mb-2"></p-->
                                                    <!--end::Text-->
                                                    <!--begin::Action-->
                                                    <div class="btn-group btn-block" role="group">
                                                        <a href="#" class="btn btn-hover-text-primary btn-icon-primary btn-sm btn-text-dark-50 bg-hover-light-primary rounded font-weight-bolder font-size-sm p-2">
                                                            <i class="fas fa-envelope-open-text"></i>{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getMessagesCount()}}</a>
                                                        <a href="#" class="btn btn-icon-danger btn-sm btn-text-dark-50 bg-hover-light-danger btn-hover-text-danger rounded font-weight-bolder font-size-sm p-2">
                                                            <i class="fas fa-heart">{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getLikesCount()}}</i></a>
                                                        <a href="#" class="btn btn-icon-info btn-sm btn-text-dark-50 bg-hover-light-info btn-hover-text-info rounded font-weight-bolder font-size-sm p-2">
                                                            <i class="far fa-comments">{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getCommentsCount()}}</i></a>
                                                    </div>
                                                    <!--end::Action-->
                                                </div>
                                                <!--end::Bottom-->
                                                <!--begin::Separator-->
                                                <div class="separator separator-solid mt-2 mb-4"></div>
                                                <!--end::Separator-->
                                                <!--begin::Editor-->

                                                <div class="btn-block" role="group" aria-label="First group">
                                                    <a href="/facebook/page-settings/{{$page->id}}" class="btn btn-icon btn-light-dark btn-circle" data-toggle="tooltip" title="Go to Page settings">
                                                        <i class="fas fa-cog"></i></a>
                                                    <a href="/facebook/posts/{{$page->id}}" class="btn btn-icon btn-light-warning btn-circle" data-toggle="tooltip" title="Go to Posts">
                                                        <i class="far fa-copy"></i></a>
                                                    <a href="/facebook/broadcast-message/{{$page->id}}" class="btn btn-icon btn-light-info btn-circle" data-toggle="tooltip" title="Message Broadcast">
                                                        <i class="fas fa-mail-bulk"></i></a>
                                                    <a href="/facebook/report/page/{{$page->id}}" class="btn btn-icon btn-light-success btn-circle" data-toggle="tooltip" title="Page Reports">
                                                        <i class="far fa-clipboard"></i></a>
                                                    <a onclick="deletepage('/facebook/delete-page/{{$page->id}}')" class="btn btn-icon btn-light-danger btn-circle" data-toggle="tooltip" title="Delete">
                                                        <i class="fa fa-trash"></i></a>
                                                    <a href="https://www.facebook.com/{{$page->facebook_page_id}}" target="_blank" class="btn btn-icon btn-light-primary btn-circle" data-toggle="tooltip" title="Go To Facebook Page">
                                                        <i class="fab fa-facebook-f"></i></a>
                                                </div>
                                                <!--edit::Editor-->
                                            </div>
                                            <!--end::Card-->
                                            @endforeach
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Mixed Widget 1-->
                    </div>
                </div>
                <!--end::Row-->

                <!--end::Dashboard-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Footer-->
    @include('layouts.page-footer')
    <!--end::Footer-->

    <!-- Add fb Modal Structure -->
    <div id="addfbprofile_modal" class="modal">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="RopaSans_font push_dark_text">Which Fan Page Would You Like To Add ?</h5>
                    <a href="javascript:;" class="close" data-dismiss="modal">×</a>
                </div>

                <div class="modal-body">
                    <div id="page_names" class="row">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="collectData()" data-dismiss="modal">Done</button>
                </div>
        </div>
    </div>
    </div>

    <div id="Attention" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Re-Authenticate Pages</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <input id="fbid" type="text" value="{{env('FACEBOOK_APP_ID')}}" style="display: none;"/>
@endsection

@section('script_function')
    <script>
        // delete sweet alert
        function deletepage(url) {
            Swal.fire({
                    title: "Are you sure?",
                    text: "You will not be able to recover page details!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel pls!"
                }).then(function(result) {
                if (result.value) {
                        Swal.fire("Deleted!", "Your page details has been deleted.", "success");
                        window.location.href = url;
                    } else {
                        Swal.fire("Cancelled", "Your page is safe :)", "error");
                    }
                });
        }

        var fbid = $('#fbid').val();
        window.fbAsyncInit = function () {
            FB.init({
                appId: fbid,
                xfbml: true,
                cookie: true,
                version: 'v10.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        var fb_details = [];
        function myFacebookLogin(flag) {
            $('#Attention').modal('hide');
            //facebook login.......
            FB.login(function (response) {
                //welcome message in console
                if (response.status === "connected") {
                    r_token = response.authResponse.accessToken;
                    fb_id = response.authResponse.userID;
                    $('.lean-overlay').hide();
                    FB.api(response.authResponse.userID, function (response) {
                        if (response && !response.error) {
                            fb_name = response.name;
                        }
                    });
                    r_token = response.authResponse.accessToken;
                    fb_id = response.authResponse.userID;
                    var pages = [];
                    var url = '/facebook/get-pages';
                    if (flag)
                        url = '/facebook/reAuthAll';
                    $.ajax({
                        url: url,
                        method: 'get',
                        data: {
                            "facebookUserId": response.authResponse.userID,
                            "userAccessToken": response.authResponse.accessToken,
                        },
                        datatype: 'json',
                        success: function (pages) {
                            var i = 0;
                            if (pages.status === 200) {
                                $('#callModal').click();
                                var mainModel = $('#addfbprofile_modal');
                                mainModel.find('div#page_names').empty();
                                fb_details.push({
                                    userDetails: pages.user
                                });
                                if (pages.pages.length === 0) {
                                    var contents = '<span class="indigo-text">You do not have any page in your current logged in facebook account, but if you want to add more pages then please logout first from facebook then login in with the facebook account which has admin privilege of the pages you want to add to {{ env('APP_NAME') }}.</span>';
                                    mainModel.find('div#page_names').append(contents);
                                    mainModel.find('h5.RopaSans_font').attr('class', 'RopaSans_font red-text').html('There are no pages present to add to {{ env('APP_NAME') }} !');
                                } else {
                                    $.each(pages.pages ,function (ind, val) {
                                        var contents = '<div class="col-md-6 shadow-sm mb-3 py-2"><input type="checkbox" data_id="' + val.id + '" page_access_token="' + val.access_token + '" page_name="' + val.name + '" class="filled-in float-right" id="filled-in-box' + i +
                                            '" onclick="collectData(this.id);"/>' +
                                            '<label for="filled-in-box' + i + '">' +
                                            '<div class="row">' +
                                            '<div class="col-md-3">' +
                                            '<img class="img circle"  src="https://graph.facebook.com/' + val.id + '/picture?type=small">' +
                                            '</div>' +
                                            '<div class="col-md-9">' +
                                            '<span class="truncate-name">' + val.name + '</span>' +
                                            '</div>' +
                                            '</div>' +
                                            '</label>' +
                                            '</div>';
                                        mainModel.find('div#page_names').append(contents);
                                        i++;
                                    });
                                    mainModel.find('h5').attr('class', 'RopaSans_font push_dark_text').html('Which Fan Page Would You Like To Add ?');
                                }

                                $('#addfbprofile_modal').modal("show");
                            }
                            else if (pages.status === 201) {
                                var tokenDiv = $('.password_changed_div');
                                if (pages.code === 200) {
                                    tokenDiv.find('p').css('color', '#238723').html(pages.message);
                                    tokenDiv.find('.btn').css('display', 'none');
                                } else if (pages.code === 404) {
                                    tokenDiv.find('p').css('color', '#ff8400').html(pages.message);
                                }
                            } else if (pages.status === 400) {
                                Swal.fire('Connection Fail!', "Problem in facebook connection , please logout from facebook and try again", 'error');
                            }
                        },

                        error: function () {
                            // $('#blockDiv').addClass('hide');
                            Swal.fire('Connection Fail!', "Problem in facebook connection, please logout from facebook and try again", 'error');
                        }
                    });
                } else {
                    // $('#blockDiv').addClass('hide');
                    Swal.fire('Connection Fail!', "Problem in facebook connection, please logout from facebook and try again", 'error');
                }
            }, {scope: 'pages_show_list, pages_manage_metadata, pages_read_engagement, pages_read_user_content,pages_messaging,pages_messaging_subscriptions,pages_manage_engagement'})
        }

        var pages = [];
        function collectData(id) {
            if (id != null) {
                if ($('#' + id).prop("checked") == true) {
                    pages.push({
                        pageId: $('#' + id).attr('data_id'),
                        pageAccessToken: $('#' + id).attr('page_access_token'),
                        pageName: $('#' + id).attr('page_name')
                    });
                }
                else {
                    var index = pages.indexOf($('#' + id).attr('data_id'));
                    if (index >= -1) {
                        pages.splice(index, 1);
                    }
                }
            } else {
                $.ajax({
                    url: '/facebook/store-pages',
                    method: 'post',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        pages: pages,
                        fb_details: fb_details
                    },
                    datatype: 'json',
                    beforeSend: function () {
                        // $('#blockDiv').removeClass('hide');
                    },
                    complete: function () {
                        // $('#blockDiv').addClass('hide');
                    },
                    success: function (response) {
                        pages = [];
                        if (response['status'] == 200) {
                            var i = 1;
                            var load = false;

                            if (response.pages != undefined || response.pages != null) {
                                $.each(response.pages, function (ind, val) {
                                    console.log(val);
                                    var date = new Date(val.created_at).toDateString();
                                    date = date.split(' ');
                                    var createDate = date[2] + ' ' + date[1] + ' ' +date[3];

                                    var cover = val.cover_photo;
                                    if (val.cover_photo == undefined || val.cover_photo == null) {
                                        cover= '/assets/images/default.jpg';
                                    }

                                    var content = '<div class="card-body dashboard-card-body">' +
                                        '    <div class="d-flex align-items-center">' +
                                        '       <div class="symbol symbol-40 symbol-light-success mr-5">' +
                                        '           <span class="symbol-label">' +
                                        '          <img src="' + val.profile_picture + '" alt="" class="circle responsive-img activator card-profile-image h-100">' +
                                        '           </span>' +
                                        '       </div>' +
                                        '       <div class="d-flex flex-column flex-grow-1">' +
                                        '           <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">' + val.page_name + '</a>' +
                                        '           <span class="text-muted font-weight-bold">Created at ' + createDate + '</span>' +
                                        '       </div>' +
                                        '   </div>' +
                                        '   <div class="pt-4">' +
                                        '       <div class="bgi-no-repeat bgi-size-cover rounded h-150px" style="background-image: url(' + cover + ')"></div>' +
                                        '       <div class="btn-group btn-block" role="group">' +
                                        '           <a href="#" class="btn btn-hover-text-primary btn-icon-primary btn-sm btn-text-dark-50 bg-hover-light-primary rounded font-weight-bolder font-size-sm p-2">' +
                                        '               <i class="fas fa-envelope-open-text"></i>0</a>' +
                                        '           <a href="#" class="btn btn-icon-danger btn-sm btn-text-dark-50 bg-hover-light-danger btn-hover-text-danger rounded font-weight-bolder font-size-sm p-2">' +
                                        '               <i class="fas fa-heart"></i>0</a>' +
                                        '           <a href="#" class="btn btn-icon-info btn-sm btn-text-dark-50 bg-hover-light-info btn-hover-text-info rounded font-weight-bolder font-size-sm p-2">' +
                                        '               <i class="far fa-comments"></i>0</a>' +
                                        '       </div>' +
                                        '   </div>' +
                                        '   <div class="separator separator-solid mt-2 mb-4"></div>' +
                                        '   <div class="btn-block" role="group" aria-label="First group">' +
                                        '       <a href="/facebook/page-settings/' + val.id + '" class="btn btn-icon btn-light-dark btn-circle" data-toggle="tooltip" title="Go to Page settings">' +
                                        '           <i class="fas fa-cog"></i></a>' +
                                        '       <a href="/facebook/posts/' + val.id + '" class="btn btn-icon btn-light-warning btn-circle" data-toggle="tooltip" title="Go to Posts">' +
                                        '           <i class="far fa-copy"></i></a>' +
                                        '       <a href="/facebook/broadcast-message/' + val.id + '" class="btn btn-icon btn-light-info btn-circle" data-toggle="tooltip" title="Message Broadcast">' +
                                        '           <i class="fas fa-mail-bulk"></i></a>' +
                                        '       <a href="/facebook/report/page/"' + val.id + '" class="btn btn-icon btn-light-success btn-circle" data-toggle="tooltip" title="Page Reports">' +
                                        '           <i class="far fa-clipboard"></i></a>' +
                                        '       <a onclick=deletepage("/facebook/delete-page/' + val.id + '") class="btn btn-icon btn-light-danger btn-circle" data-toggle="tooltip" title="Delete">' +
                                        '           <i class="fa fa-trash"></i></a>' +
                                        '       <a href="https://www.facebook.com/' + val.facebook_page_id + '" target="_blank" class="btn btn-icon btn-light-primary btn-circle" data-toggle="tooltip" title="Go To Facebook Page">' +
                                        '           <i class="fab fa-facebook-f"></i></a>' +
                                        '   </div>';
                                    $('#facebookpages').find('.addfbpg').after(content);
                                });
                            }
                        }

                    }
                });
                pagesIds = [];

            }
        }

        function presentPages(pagename) {
            Swal.fire({
                title: "",
                text: 'Hey! Looks like ' + pagename + ' has already been added by another user. ' +
                'If you have not authorized anyone to access this page, please contact support at support@marketingsoftwares.com',
                icon: "info",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false
            });
        }

        //for pagination

        var dist = 100;
        var num = 0;

            <?php  if(isset($pagesPaginate) && !empty($pagesPaginate)){?>
        var arrayCount = '<?php echo count($pagesPaginate); ?>';

        var page = '<?php $pagesPaginate = addslashes(json_encode($pagesPaginate)); echo $pagesPaginate;?>';
        page = $.parseJSON(page);

        function loadPages() {

            if ($(window).scrollTop() >= ($(document).height() - $(window).height() - dist ) && (num <= arrayCount)) {
                if (page[num] != '') {
                    console.log(page[num]);
                    for(var k in page[num]) {
                        var val = page[num][k];

                        var date = new Date(val.time*1000).toDateString();
                        date = date.split(' ');
                        var createDate = date[2] + ' ' + date[1] + ' ' +date[3];

                        var picture = 'https://graph.facebook.com/' + val.pageid + '/picture?type=small';
                        if (val.picture !== '') {
                            picture = val.picture;
                        }
                        var cover = val.cover_photo;
                        if (val.cover_photo == '') {
                            cover= '/assets/images/default.jpg';
                        }

                        var content = '<div class="card-body dashboard-card-body">' +
                        '    <div class="d-flex align-items-center">' +
                        '       <div class="symbol symbol-40 symbol-light-success mr-5">' +
                        '           <span class="symbol-label">' +
                        '               <img src="' + picture + '" alt="" class="circle responsive-img activator card-profile-image h-100">' +
                        '           </span>' +
                        '       </div>' +
                        '       <div class="d-flex flex-column flex-grow-1">' +
                        '           <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">' + val.pagename + '</a>' +
                        '           <span class="text-muted font-weight-bold">Created at ' + createDate + '</span>' +
                        '       </div>' +
                        '   </div>' +
                        '   <div class="pt-4">' +
                        '       <div class="bgi-no-repeat bgi-size-cover rounded h-150px" style="background-image: url(' + cover + ')"></div>' +
                        '       <div class="btn-group btn-block" role="group">' +
                        '           <a href="#" class="btn btn-hover-text-primary btn-icon-primary btn-sm btn-text-dark-50 bg-hover-light-primary rounded font-weight-bolder font-size-sm p-2">' +
                        '               <i class="fas fa-envelope-open-text"></i>' + val.messages_count + '</a>' +
                        '           <a href="#" class="btn btn-icon-danger btn-sm btn-text-dark-50 bg-hover-light-danger btn-hover-text-danger rounded font-weight-bolder font-size-sm p-2">' +
                        '               <i class="fas fa-heart"></i>' + val.likes_count + '</a>' +
                        '           <a href="#" class="btn btn-icon-info btn-sm btn-text-dark-50 bg-hover-light-info btn-hover-text-info rounded font-weight-bolder font-size-sm p-2">' +
                        '               <i class="far fa-comments"></i>' + val.comments_count + '</a>' +
                        '       </div>' +
                        '   </div>' +
                        '   <div class="separator separator-solid mt-2 mb-4"></div>' +
                        '   <div class="btn-block" role="group" aria-label="First group">' +
                        '       <a href="/facebook/page-settings/' + val.id + '" class="btn btn-icon btn-light-dark btn-circle" data-toggle="tooltip" title="Go to Page settings">' +
                        '           <i class="fas fa-cog"></i></a>' +
                        '       <a href="/facebook/posts/' + val.id + '" class="btn btn-icon btn-light-warning btn-circle" data-toggle="tooltip" title="Go to Posts">' +
                        '           <i class="far fa-copy"></i></a>' +
                        '       <a href="/facebook/broadcast-message/' + val.id + '" class="btn btn-icon btn-light-info btn-circle" data-toggle="tooltip" title="Message Broadcast">' +
                        '           <i class="fas fa-mail-bulk"></i></a>' +
                        '       <a href="/facebook/report/page/' + val.id + '" class="btn btn-icon btn-light-success btn-circle" data-toggle="tooltip" title="Page Reports">' +
                        '           <i class="far fa-clipboard"></i></a>' +
                        '       <a onclick=deletepage("/facebook/delete-page/' + val.id + '") class="btn btn-icon btn-light-danger btn-circle" data-toggle="tooltip" title="Delete">' +
                        '           <i class="fa fa-trash"></i></a>' +
                        '       <a href="https://www.facebook.com/' + val.pageid + '" target="_blank" class="btn btn-icon btn-light-primary btn-circle" data-toggle="tooltip" title="Go To Facebook Page">' +
                        '           <i class="fab fa-facebook-f"></i></a>' +
                        '   </div>';


                        $('#facebookpages').append(content);
                    };
                    num++;
                }
            }
        }
        // loadPages();
        setInterval(loadPages, 60);

        <?php } ?>
        $(document).ready(function () {
            @if(isset($reauth)&&$reauth)
                $('#Attention').modal("show");
            @endif
        });

       $(document).bind('ajaxStart',function (e) {
           console.log('ajax call startting');
       });
       $(document).bind('ajaxSend',function (e) {
           console.log('ajax call start');
       });
        $(document).bind('ajaxSuccess',function (e) {
           console.log('ajax call success');
       });
        $(document).bind('ajaxComplete',function (e) {
           console.log('ajax call Complete');
       });
        $(document).bind('ajaxError',function (e) {
           console.log('ajax has error');
       });
        $(document).bind('ajaxStop',function () {
            console.log('Ajax stop');
        });

        function project(name,type){
            this.name = name;
            this.type = type;
        }
        var myProject = new project('auto','messaging');
        project.prototype.details = function () {
            return 'My project name is '+ this.name +' and it is for '+ this.type;
        };

        function company(name) {
            this.names=name;
        }
        company.prototype=new project('Own','pupblic');
        company.prototype.setCategory= function (cat) {
            this.category=cat;
        }
        var myCompany = new company('Makrs');
        myCompany.setCategory('hard');
        console.log(myCompany,myCompany.category,myCompany.details());
//--------------------------------------------------------------
        const promise = new Promise((resolve, reject) => {

            const request = new XMLHttpRequest();

        request.open("GET", "https://api.icndb.com/jokes/random");
        request.onload = () => {
            if (request.status === 200) {
                console.log("200 ",request.response);
                resolve(request.response);
            } else {
                console.log("400 ",request.statusText);
                reject(Error(request.statusText));
            }
        };

        request.onerror = () => {
            reject(Error("Error fetching data."));
        };

        request.send();
        });

        console.log("Asynchronous request made.");

        promise.then(
            data => {
            console.log("Got data! Promise fulfilled.");
        },
        ).catch((result)=> {
            console.log("Promise 456 rejected.");
            console.log(result);
        });
    </script>

@endsection
