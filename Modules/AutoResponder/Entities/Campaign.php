<?php

namespace Modules\AutoResponder\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\AutoResponder\Database\factories\CampaignFactory;

class Campaign extends Model
{
    protected $fillable = [
        'name',
        'button_text',
        'button_size',
        'button_text_color',
        'redirect_url',
        'user_id',
        'click_count',
        'lead_count'
    ];

    public function autoResponderCampaign()
    {
        return $this->belongsTo(AutoResponderCampaign::class);
    }
}
