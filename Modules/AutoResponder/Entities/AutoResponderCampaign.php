<?php

namespace Modules\AutoResponder\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AutoResponderCampaign extends Model
{

    protected $fillable = [
        'name',
        'unique_Id',
        'is_active'
    ];

    public function autoResponder()
    {
        return $this->belongsTo(AutoResponder::class);
    }
}
