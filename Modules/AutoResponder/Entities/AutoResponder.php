<?php

namespace Modules\AutoResponder\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AutoResponder extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * @return HasMany
     */
    public function autoResponderCredentials()
    {
        return $this->hasMany(AutoResponderCredential::class);
    }

    /**
     * @return HasMany
     */
    public function autoResponderCampaigns()
    {
        return $this->hasMany(AutoResponderCampaign::class);
    }
}
