<?php

namespace Modules\AutoResponder\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AutoResponderCredential extends Model
{
    protected $fillable = [
        'type',
        'value'
    ];

    /**
     * @return BelongsTo
     */
    public function autoResponder()
    {
        return $this->belongsTo(AutoResponder::class);
    }
}
