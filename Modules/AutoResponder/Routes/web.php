<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;
use Modules\AutoResponder\Http\Controllers\AutoResponderController;
use Modules\AutoResponder\Http\Controllers\CampaignController;
use Modules\AutoResponder\Http\Controllers\Responders\GetResponseController;
use Modules\AutoResponder\Http\Controllers\Responders\MailChimpController;

Route::middleware('auth')->prefix('auto-responder')->group(function() {

    Route::get('/', [AutoResponderController::class, 'index'])
        ->name('auto-responder.index');

    Route::get('/campaign', [CampaignController::class, 'index'])
        ->name('campaign');

    Route::get('/retrieve-campaigns/{autoResponderId}', [CampaignController::class, 'retrieve'])
        ->name('campaign.retrieve');

    Route::post('/store-campaign', [CampaignController::class, 'store'])
        ->name('campaign.store');

    Route::post('/get-response', [GetResponseController::class, 'store'])
        ->name('auto-responder.get-response');

    Route::post('/mail-chimp', [MailChimpController::class, 'store'])
        ->name('auto-responder.mail-chimp');
});

Route::get('/auth/facebook-login/{campaignId}', [CampaignController::class, 'loginToFacebook'])
    ->name('facebook.login');

Route::get('/facebook/callback', [CampaignController::class, 'callback'])
    ->name('facebook.callback');
