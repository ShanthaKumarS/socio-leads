<?php
/**
 * Created by Raushan Kumar <raushankumar@globussoft.in>
 * Date: 5/2/2016
 * Time: 1:12 PM
 */
?>

@extends('layouts.master')
@section('title','SocioLeads | Campaigns')
@section('title1','Campaigns')
@section('style')
{{--    <link type="text/css" rel="stylesheet" href="/assets/plugins/materialize/css/materialize.css"  media="screen,projection"/>--}}
<link rel="stylesheet" type="text/css" href="/assets/plugins/DataTables/media/css/jquery.dataTables.css">
    <style>
        .paginate_button.current{
            background: #d1d1d1 !important;
        }
        .btn_style_large {
            font-size: 20px;
            height: 40px;
            line-height: 37px;
            margin-left: 5px;
            padding: 0 35px;
            text-transform: none;
        }

        .sweet-alert button {
            font-size: 15px;
        }

        .create-campaing-btn {
            position: absolute;
            z-index: 1;
            margin-top: 15px;
            margin-left: -300px;
        }
        .copy-textarea {

            overflow-y: hidden;
            padding: .8rem 0 1.6rem 0;
            resize: none;
            min-height: 3rem;
            background-color: transparent;
            border: none;
            outline: none;
            width: 100%;
            font-size: 1rem;
            padding: 0;
            box-shadow: none;
            box-sizing: content-box;
            transition: all 0.3s;
            overflow: auto;
        }


        .hide{
            display: none;
        }


        #kt_datatable_paginate  .pagination li.active {
            background-color: #fff;
        }

        .dataTables_wrapper  .dataTables_paginate .paginate_button:hover {
            color: #7E8299 !important;
            border: 1px solid #fff;
            background-color: #fff;
            background: #fff;

        }

        .dataTables_wrapper .dataTables_paginate .pagination .page-item.active>.page-link {
            background-color: #E4E6EF;
            color: #7E8299;
        }

        .dataTables_wrapper .dataTables_paginate .pagination .page-item:hover:not(.disabled)>.page-link {
            background-color: #E4E6EF;
            color:  #7E8299;
        }
        .dataTables_wrapper .dataTables_paginate .pagination .page-item>.page-link {
            background-color: #F3F6F9;
        }
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {
            color: #181C32 !important;
        }
        .dataTables_wrapper .dataTables_paginate .pagination .page-item {
            margin-left: 1rem;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            padding: 0px;
        }

        .dataTables_wrapper .dataTables_filter input {
            margin-left: 0;
            margin-top: 1.4em;
        }

    </style>
@endsection
@section('content')
    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> Facebook Button Campaigns </h5>
                <!--end::Page Title-->
                <a href="/use/support" class="btn btn-light-success btn-sm"> <i class="fas fa-video mr-2"></i> Watch Help Videos </a>
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->

    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <div class="row">
                            <div class="col-md"></div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-primary btn-block create-campaing-btn" data-toggle="modal" data-target="#editCampaign">Create New Campaign</button>
                            </div>

                        </div>
                        <table class="table table-bordered table-checkable" id="kt_datatable_list">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Campaign Name</th>
                                <th>Redirect Url</th>
                                <th>Clicks</th>
                                <th>Leads</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($campaigns) > 0)
                                @foreach($campaigns as $key => $campaign)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$campaign->name}}</td>
                                        <td>{{$campaign->redirect_url}}</td>
                                        <td>{{$campaign->click}}</td>
                                        <td>{{$campaign->lead}}</td>
                                        <td>
                                            <button class="btn btn-icon btn-light-success btn-sm" title="Button Code" data-toggle="modal" data-target="#ButtonCode{{$key + 1}}"><i class="fas fa-code"></i></button>
                                            <button class="btn btn-icon btn-light-primary btn-sm" title="Subscribe Link" data-toggle="modal" data-target="#subscribeModel{{$key + 1}}"><i class="fas fa-code-branch"></i></button>
                                            <button class="btn btn-icon btn-light-info btn-sm edit-campaign"  data-id="{{$campaign->id}}" title="Edit Campaign" data-toggle="modal" data-target="#editCampaign"><i class="fas fa-edit"></i></button>
                                            <a href="/user/campaign-reports/{{$campaign->id}}" class="btn btn-icon btn-light-warning btn-sm" title="Reports"><i class="far fa-file-code"></i></a>
                                            <button class="btn btn-icon btn-light-danger btn-sm" title="Delete Campaign" onclick="deletecampaign('/user/delete-campaign/{{$campaign->id}}')"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>

                                    <!--begin::Modal ButtonCode-->
                                    <div class="modal fade" id="ButtonCode{{$key + 1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="buttonCodeModalLabel">Button Code</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <span>Copy and paste the html & css below on your website to start getting subscribers.</span>
                                                    <h5 class="text-center">HTML & CSS</h5>
                                                    <textarea id="textarea1" class="copy-textarea js-copytextarea" style="height: 210px;"><a id="socioleads-fb-button" href="javascript:window.open('{{env('APP_URL')}}auth/facebook-login/{{base64_encode($campaign->id)}}?redirect={{$campaign->redirect_url}}','_blank','height='+screen.height+', width='+screen.width);void(0);">{{$campaign->button_text}}</a>&#13;&#10;
                                                    @if($campaign->button_size == 1)
                                                            {{"#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 11px;padding: 0 5px;height: 30px;line-height: 29px;background-color: ".$campaign->button_color.";text-transform:none;color: ".$campaign->button_text_color .";cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;font-family: \"Roboto\",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}"}}
                                                    @elseif($campaign->button_size == 2)
                                                            {{"#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);background-color: ".$campaign->button_color.";text-transform:none;color: ".$campaign->button_text_color.";cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 36px;height: 36px;padding: 0 2rem;font-family: \"Roboto\",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}"}}
                                                    @else
                                                            {{"#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 20px;background-color: ".$campaign->button_color.";text-transform:none;color: ".$campaign->button_textColor.";cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 37px;height: 40px;padding: 0 35px;font-family: \"Roboto\",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}"}}
                                                    @endif
                                            </textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary js-textareacopybtn">Copy To Clipboard</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Modal-->

                                    <!--begin::Modal Subscribe Link-->
                                    <div class="modal fade" id="subscribeModel{{$key + 1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="subscribeLinkModalLabel">Subscribe Link</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Copy and paste the below subscribe link on your browser to start getting subscribers.</p>
                                                    <h5 class="text-center">Subscribe Link</h5>
                                                    <textarea id="textarea1" class="copy-textarea js-copytextarea1" spellcheck="false">{{env('APP_URL')}}auth/facebook-login/{{base64_encode($campaign->id)}}?redirect={{$campaign->redirect_url}}</textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary js-textareacopybtn1">Copy To Clipboard</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Modal-->
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Footer-->
    @include('layouts.page-footer')
    <!--end::Footer-->

    <!--begin::Modal editCampaign-->
    <div class="modal fade" id="editCampaign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="facebookButtonModalLabel">Facebook Button</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">

                    <input id="list_details" value="@if(isset($campaignData["listDetails"])){{$campaignData["listDetails"]}}@else false @endif" hidden/>
                    <input id="selected_tags" value="@if(isset($campaignData["tagDetails"])){{$campaignData["tagDetails"]}}@else false @endif" hidden/>
                    <input id="selected_sequence" value="@if(isset($campaignData["seqDetails"])){{$campaignData["seqDetails"]}}@else false @endif" hidden/>

                    <!--begin::Form-->
                    <form class="form" id="campaign-form" method="post" action="/auto-responder/store-campaign">
                        @csrf
                        <h5 class="text-center">Your Facebook Button Settings</h5>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Text:</label>
                                <input id="button-text" name="buttonText" type="text" class="form-control" placeholder="Chat With Us" />
                                <span style="color: red;" class="error hide">Please enter a button text</span>
                            </div>
                            <div class="col-lg-6">
                                <label>Please enter a redirect url:</label>
                                <input id="redirect-url" name="redirectUrl" type="url" class="form-control" placeholder="Please enter a redirect url" />
                                <span style="color: red;" class="error hide">Please enter a redirect url</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Campaign Name:</label>
                                <input id="camp_name" name="campName" type="text"  class="form-control" placeholder="Please enter campaign name" />
                                <span style="color: red;" class="error hide">Please enter campaign name</span>
                            </div>
                            <div class="col-lg-6">
                                <label for="AutoResponder1">Select Your AutoResponder</label>
                                <select class="form-control"  name="availableIntegretion" id="available_integretion" onchange="selectList(this)">
                                    <option value="" disabled selected>Choose your option</option>
                                    @foreach($autoResponders as $autoResponder)
                                        <option value="{{$autoResponder->id}}">{{$autoResponder->name}}({{$autoResponder->name}})</option>
                                    @endforeach
                                </select>
                                <span style="color: red;" class="error hide">Please select an autoresponder</span>

                                <div class="w-100 mt-3">
                                    <div class="input-field dropdown-content-small" id="autoresponderlist">

                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="separator separator-dashed my-7"></div>
                        <h5 class="text-center">Style Your Button</h5>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="Buttonsize">Select Button size</label>
                                <select class="form-control" id="button-size" name="buttonSize">
                                    <option value="1">Small</option>
                                    <option value="2" selected>Medium</option>
                                    <option value="3">Large</option>
                                </select>
                                <span style="color: red;" class="error hide">Please select button size</span>
                            </div>
                            <div class="col-md-4">
                                <label>Button Color Code:</label>
                                <input  type="text" id="button-color" name="buttonColor" data-opacity=".9"  value="#3b5998" class="form-control" placeholder="#000000" />
                                <span class="form-text text-muted">Please enter Button Color Code</span>
                            </div>
                            <div class="col-md-4">
                                <label>Button Text Color Code:</label>
                                <input  type="text" id="button-text-color" name="buttonTextColor" class="form-control"   data-opacity=".9" value="#ffffff" placeholder="#FFFFFF" />
                                <span class="form-text text-muted">Please enter Button Text Color Code</span>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-7"></div>
                        <h5 class="text-center">Publish Your Button</h5>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <button type="button" id="copy_html" class="btn btn-primary btn-sm btn-block" id="kt_sweetalert_demo_2">Copy Button HTML Code</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" id="copy_css" class="btn btn-primary btn-sm btn-block" id="kt_sweetalert_demo_2">Copy Button CSS Code</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" id="copy_redirect_url" class="btn btn-primary btn-sm btn-block" id="kt_sweetalert_demo_2">Copy Subscribe Link</button>
                            </div>
                        </div>
                        <div class="col l12 m12 s12 hide" id="show_html_div">
                           <h5 class="text-center">Markup</h5>
                           <pre class="language-markup">

                           </pre>
                           <hr class="margin-bottom-10">
                        </div>
                        <div class="col l12 m12 s12 hide" id="show_css_div">
                            <h5 class="RopaSans_font center">CSS</h5>
                            <pre class="csshtml language-markup"></pre>
                        </div>
                        <div class="col l12 m12 s12 hide" id="show_dlink_div">
                            <h5 class="RopaSans_font center">Direct Link</h5>
                            <pre class="dlink language-markup"></pre>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"  id="submit-button" >Update Campaign</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal-->





@endsection
@section('script_function')


{{--    <script type="text/javascript" src="/assets/plugins/materialize/js/materialize.min.js"></script>--}}

    <script src="/assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
    <script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>


{{--    <script src="/assets/plugins/sweetalert/dist/sweetalert.min.js"></script>--}}
    <script src="/assets/plugins/clipboard.js" type="text/javascript"></script>

    <script type="text/javascript">
        var kt_datatable_list;
        var campId;
        var projectUrl='<?php echo env("APP_URL");?>';
        var campaignData_redirect;



        $(document).ready(function () {
            // Setup - add a text input to each footer cell
            $('#campaignsTable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search... ' + title + '" />');
            });

            // DataTable
            var table = $('#campaignsTable').DataTable();

            // Apply the search
            table.columns().every(function () {
                var that = this;

                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });

            ///////////////////////////////////

            // Setup - add a text input to each footer cell
            $('#kt_datatable_list tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search... ' + title + '" />');
            });

            // DataTable
            kt_datatable_list = $('#kt_datatable_list').DataTable({
                "ordering": false,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search..."
                }
            });
        });

        // delete sweet alert
        function deletecampaign(url) {
            Swal.fire({
                    title: "Are you sure?",
                    text: "You will not be able to recover this campaign!",
                    icon: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#dd6b55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, I need this campaign!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }).then(function(result) {
                if (result.value) {
                        Swal.fire("Deleted!", "Your campaign has been deleted.", "success");
                        window.location.href = url;
                    }
                    else {
                        Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
                        document.getElementById('deleteCampaign').href = "javascript:;";
                    }
                });
        }

        $(document).on('click','.js-textareacopybtn',function(){

            var copyTextarea=$(this).parents('.modal:first').find('.js-copytextarea');

            copyTextarea.select();
            console.log(copyTextarea.text());
            clipboard.copy(copyTextarea.text());
        });

        $(document).on('click','.js-textareacopybtn1',function(){
            var copyTextarea1=$(this).parents('.modal:first').find('.js-copytextarea1');
            copyTextarea1.select();
            clipboard.copy(copyTextarea1.text());
        });



        //my
        function cleanModal(){

                page_campId = null;
                campId=null;
                campaignData_redirect='';

                $('#campaign-form').attr('action', '/auto-responder/store-campaign');

                $('#button-text').val('');
                $('#camp_name').val('');
                $('#redirect-url').val('');
                $('#available_integretion').val('');
                $('#button-size').val('2');
                $('#button-color').val('#3b5998');
                $('#button-text-color').val('#ffffff');
                $('#autoresponderlist').html('');

                $('#list_details').val('false');
                $('#selected_tags').val('false');
                $('#selected_sequence').val('false');

                $('#campaign-form span.error').addClass('hide');



        }


        $(document).off('click','.create-campaing-btn').on('click','.create-campaing-btn',function(){
             cleanModal();
             $('#facebookButtonModalLabel').html('Create Facebook Button');
            $('#submit-button').text('Create Campaign');
        });

        // $(document).off('click','.edit-campaign').on('click','.edit-campaign', function(){
        //
        //     $('#submit-button').text('Update Campaign');
        //
        //      let campaign_id = $(this).attr('data-id');
        //
        //      $.ajax({
        //          type: 'GET', //THIS NEEDS TO BE GET
        //          url: `/auto-responder/store-campaign/${campaign_id}`,
        //          success: function (data) {
        //
        //              setCampaignData(data);
        //          },
        //          error: function () {
        //
        //          }
        //      });
        // });


        function setCampaignData(data){
            cleanModal();

            let campaignData = data.campaignData;

            if(campaignData.campId){
                $('#campaign-form').attr('action', `/auto-responder/store-campaign/${campaignData.campId}`);
                campId = campaignData.campId;
            }
            campaignData_redirect = campaignData.redirectUrl;

            $('#facebookButtonModalLabel').html('Edit Facebook Button');
            $('#button-text').val(campaignData.buttonText);
            $('#camp_name').val(campaignData.campName);
            $('#redirect-url').val(campaignData.redirectUrl);
            $('#available_integretion').val(campaignData.availableIntegretion);
            $('#button-size').val(campaignData.buttonSize);
            $('#button-color').val(campaignData.buttonColor);
            $('#button-text-color').val(campaignData.buttonTextColor);

            if(campaignData.listDetails){
                $('#list_details').val(campaignData.listDetails);
            };
            if(campaignData.tagDetails){
                $('#selected_tags').val(campaignData.tagDetails);
            };
            if(campaignData.seqDetails){
                $('#selected_sequence').val(campaignData.seqDetails);
            };


            selectList('#available_integretion', campaignData.listDetails);



        }



    </script>













<script type="text/javascript" src="/assets/plugins/jquery-minicolors/jquery.minicolors.js"></script>
<script type="text/javascript" src="/assets/plugins/clipboard.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-58515856-37', 'auto');
    ga('send', 'pageview');

</script>

<script>

    $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left' // Displays dropdown with edge aligned to the left of button
        }
    );

    // delete sweet alert
    function deletepage() {


        Swal.fire({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                swal.fire("Deleted!", "Your imaginary file has been deleted.", "success");
            }
            else {
                Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
            }
        });





    }




    var buttonHtml='<a id="socioleads-fb-button" href="javascript:window.open(\''+projectUrl+'auth/facebook-login/'+btoa(campId)+'\',\'_blank\',\'height=\'+screen.height+\', width=\'+screen.width);void(0);">Chat with us</a>';
    var cssHtml='#socioleads-fb-button {font-family: "Roboto",sans-serif;text-decoration: none;border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 11px;padding: 0 5px;height: 30px;line-height: 29px;background-color: #3b5998;text-transform:none;color: #fff;cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';

    $(document).ready(function () {
        // for all the copy buttons

        $('#copy_html').on('click',function () {

            $('#show_css_div').addClass('hide');
            $('#show_html_div').removeClass('hide');
            $('.language-markup').text(buttonHtml);
            clipboard.copy(buttonHtml);

        });
        $('#copy_css').on('click',function () {
            $('#show_html_div').addClass('hide');
            $('#show_css_div').removeClass('hide');
            $('.csshtml').text(cssHtml);
            clipboard.copy(cssHtml);

        });
        $('#copy_redirect_url').on('click',function () {
            var directLink='';
            if(typeof campId!=='undefined' && campId!==null && campId!=""){
                var redirect = campaignData_redirect;
                directLink=projectUrl+'auth/facebook-login/'+btoa(campId)+'?redirect='+redirect;
            }else{
                directLink=projectUrl+'auth/facebook-login/';
            }
            clipboard.copy(directLink);
        });

        //To display the colors dropdown
        $('.demo').each(function () {
            $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                defaultValue: $(this).attr('data-defaultValue') || '',
                format: $(this).attr('data-format') || 'hex',
                keywords: $(this).attr('data-keywords') || '',
                inline: $(this).attr('data-inline') === 'true',
                letterCase: $(this).attr('data-letterCase') || 'lowercase',
                opacity: $(this).attr('data-opacity'),
                position: $(this).attr('data-position') || 'bottom left',
                swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
                change: function (hex, opacity) {
                    var log;
                    try {
                        log = hex ? hex : 'transparent';
                        if (opacity) log += ', ' + opacity;
                        console.log(log);
                    } catch (e) {
                    }
                },
                theme: 'default'
            });
        });






        //To hide the error message
        $('#redirect-url').on('keyup', function () {
            $(this).siblings('span.error').addClass('hide');
        });

        //To hide the error message
        $('#camp_name').on('keyup', function () {
            $(this).siblings('span.error').addClass('hide');
        });

        //To change the button text and hide the error message
        $('#button-text').on('keyup', function () {
            $('#previewButtonText').text($(this).val());
            $(this).siblings('span.error').addClass('hide');
        });

        //To change the button size
        $('#button-size').on('change', function () {
            $(this).parent().siblings('span.error').addClass('hide');
            if ($(this).val() == "1") {
                $('#previewButton').removeClass('btn_style_large').addClass('btn_style');
            } else if($(this).val() == "2") {
                $('#previewButton').removeClass('btn_style').removeClass('btn_style_large');
            }else {
                $('#previewButton').removeClass('btn_style').addClass('btn_style_large');
            }
        });

        //To change the button color
        $('#button-color').on('change', function () {
            $('#previewButton').attr('style', 'background-color: ' + $(this).val() + ' !important');
        });

        //To change the button text color
        $('#button-text-color').on('change', function () {
            $('#previewButtonText').css('color', $(this).val());
        });

        //to display button size
        var buttonSize = '<?php if (isset($campaignData['buttonSize'])) {
            echo $campaignData['buttonSize'];
        } else {
            echo "false";
        }?>';
        if (buttonSize !== "false") {
            $('#button-size').val(buttonSize).material_select();
            $('#button-size').trigger('onchange');
            if (buttonSize == "1") {
                $('#previewButton').removeClass('btn_style_large').addClass('btn_style');
            } else if(buttonSize == "2") {
                $('#previewButton').removeClass('btn_style').removeClass('btn_style_large');
            }else {
                $('#previewButton').removeClass('btn_style').addClass('btn_style_large');
            }
        }

        //to display autoresponders
        var availableIntegretion = '<?php if (isset($campaignData['availableIntegretion'])) {
            echo $campaignData['availableIntegretion'];
        } else {
            echo "false";
        }?>';
        if (availableIntegretion !== "false") {
            $('#available_integretion').val(availableIntegretion).material_select();
            $('#available_integretion').trigger('onchange');
        }

        //to display the button color
        var buttonColor = '<?php if (isset($campaignData['buttonColor'])) {
            echo $campaignData['buttonColor'];
        } else {
            echo "false";
        }?>';
        if (buttonColor !== "false") {
            $('#previewButton').attr('style', 'background-color: ' + buttonColor + ' !important');
        }

        //to display the button text color
        var buttonTextColor = '<?php if (isset($campaignData['buttonTextColor'])) {
            echo $campaignData['buttonTextColor'];
        } else {
            echo "false";
        }?>';
        if (buttonTextColor !== "false") {
            $('#previewButtonText').css('color', buttonTextColor);
        }

        //to display the button text
        var buttonText = '<?php if (isset($campaignData['buttonText'])) {
            echo addslashes($campaignData['buttonText']);
        } else {
            echo "false";
        }?>';


        // if (buttonText !== "false") {
        //     $('#previewButtonText').text(buttonText);
        //     $('#submit-button').val('Update Campaign');
        // }

        //to display the button html and css html
        var buttonText = '<?php if (isset($campaignData['buttonText'])) {
            echo addslashes($campaignData['buttonText']);
        } else {
            echo "false";
        }?>';
        if (buttonText !== "false") {
            var btntxt = '<?php if (isset($campaignData['buttonText'])) {echo addslashes($campaignData['buttonText']);}?>';
            var redirect= '<?php if (isset($campaignData['redirectUrl'])) {echo $campaignData['redirectUrl'];}?>';
            buttonHtml='<a id="socioleads-fb-button" href="javascript:window.open(\''+projectUrl+'auth/facebook-login/'+btoa(campId)+'?redirect='+redirect+'\',\'_blank\',\'height=\'+screen.height+\', width=\'+screen.width);void(0);">'+btntxt+'</a>';
            var btncolor='<?php if (isset($campaignData['buttonColor'])) {echo $campaignData['buttonColor'];}?>';
            var btntxtcolor='<?php if (isset($campaignData['buttonTextColor'])) {echo $campaignData['buttonTextColor'];}?>';
            var btnSize = '<?php if (isset($campaignData['buttonSize'])) {echo $campaignData['buttonSize'];} else {echo "false";}?>';
            if (btnSize !== "false") {
                if (buttonSize == "1") {
//                        cssHtml='#socioleads-fb-button {border-radius: 2px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 36px;height: 36px;padding: 0 2rem;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                    cssHtml='#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 11px;padding: 0 5px;height: 30px;line-height: 29px;background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                } else if(buttonSize == "2"){
                    cssHtml='#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 36px;height: 36px;padding: 0 2rem;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
//                        cssHtml='#socioleads-fb-button {border-radius: 2px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 11px;padding: 0 5px;height: 30px;line-height: 29px;background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                }else {
                    cssHtml='#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 20px;background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 37px;height: 40px;padding: 0 35px;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                }
            }
            $('.buttonhtml').text(buttonHtml);
            $('.csshtml').text(cssHtml);
        }else{
            $('.buttonhtml').text(buttonHtml);
            $('.csshtml').text(cssHtml);
        }

        //To save the campaign
        $('#submit-button').on('click', function (e) {


            e.preventDefault();
            var valid = true;
            $.each($('#campaign-form input,#campaign-form select').not('.no-validate'), function (i, v) {
                var value = $.trim($(this).val());
                var element = $(this);
                if ($(this).attr('id') == 'button-size' || $(this).attr('id') == 'seqDetails') {
                    element = $(this).parent();
                }
                element.siblings('span.error').addClass('hide');
                if ((value.length == 0) || $(this).hasClass('invalid')) {
                    if ($(this).hasClass('invalid')) {
//                            if(($(this).attr('type')==='url') && !($(this).val().startsWith("http://") || $(this).val().startsWith("https://"))){
                        element.siblings('span.error').text('Redirect url must starts with http:// or https://');
//                            }else{
//                                element.siblings('span.error').text('Invalid inputs');
//                            }
                    }
                    valid = false;
                    element.siblings('span.error').removeClass('hide');
                    return true;
                }
            });

            if (valid) {
                $("#campaign-form").submit();
            }
        });
    });




    //ajax to fetch and add the lists of selected integretions
    function selectList(element, sellistElement=null ) {
        $(element).parent().siblings('span.error').addClass('hide');
        var autoId = $('#available_integretion').val();
        var listDetails = $('#list_details').val();
        var tagDetails = $('#selected_tags').val();
        var seqDetails = $('#selected_sequence').val();
        var selectedtags='';
        var selectedsequence='';
        if ((tagDetails !== "false") &&  (seqDetails !== "false")){
            selectedtags = $('#selected_tags').val().split(",");
            selectedsequence = $('#selected_sequence').val().split(",");
        }
        $.ajax({
            url: '/auto-responder/retrieve-campaigns/' + autoId,
            type: 'GET',
            datatype: 'json',
            beforeSend: function () {
                $("#autoresponderlist").html('<select name="listDetails" class="form-control" id="listDetails">  <option  value="" selected="">Select List</option></select><span style="color: red;" class="error hide">Please select a list</span>');
            },
            complete: function () {
                initSelect2Widgets();
            },
            success: function (response) {
                console.log(response);
                var toAppendOption = '<select name="listDetails" class="form-control" id="listDetails">  <option  value="" selected="">Select List</option>';
                var toAppendDropdown = '';
                $(response).each(function (index, campaign) {
                    toAppendOption += (campaign.id == listDetails) ? '<option value="' + campaign.id + '" selected>' + campaign.name + '</option>' : '<option value="' + campaign.id + '">' + campaign.name + '</option>';
                    toAppendDropdown += '<li class=""><span>' + campaign.name + '</span></li>';
                });
                $("#autoresponderlist").html(toAppendOption + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                $(".ab .dropdown-content").html(toAppendDropdown);
                //mailchimp or ConstantContact or Drip or Aweber or Fluttermail or Sendgrid
                // if (lists['status'] == 200) {
                //     var toAppendOption = '<select name="listDetails" class="form-control" id="listDetails">  <option  value="" selected="">Select List</option>';
                //     var toAppendDropdown = '';
                //     $(lists.lists).each(function (ind, val) {
                //         toAppendOption += (val.id + '|' + val.name == listDetails) ? '<option value="' + val.id + '|' + val.name + '" selected>' + val.name + '</option>' : '<option value="' + val.id + '|' + val.name + '">' + val.name + '</option>';
                //         toAppendDropdown += '<li class=""><span>' + val.name + '</span></li>';
                //     });
                //     $("#autoresponderlist").html(toAppendOption + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                //     $(".ab .dropdown-content").html(toAppendDropdown);
                // }
                //Getresponse
//                 else if (lists['status'] == 201) {
//                     var count = Object.keys(lists['keys']).length;
//                     var keys = lists['keys'];
//                     var autolist = lists['lists'];
//
//                     var toAppendOptions = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
//                     var toAppendDropdowns = '';
//                     for (i = 0; i < count; i++) {
//                         toAppendOptions += (keys[i] + '|' + autolist[keys[i]]['name'] == listDetails) ? '<option value="' + keys[i] + '|' + autolist[keys[i]]['name'] + '" selected>' + autolist[keys[i]]['name'] + '</option>' : '<option value="' + keys[i] + '|' + autolist[keys[i]]['name'] + '">' + autolist[keys[i]]['name'] + '</option>';
//                         toAppendDropdowns += '<li class=""><span>' + autolist[keys[i]]['name'] + '</span></li>';
//                     }
//                     $("#autoresponderlist").html(toAppendOptions + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdowns);
//                 }
//                 //Hubspot
//                 else if (lists['status'] == 202) {
//                     var counthub = Object.keys(lists['lists']).length;
//                     var hublist = lists['lists'];
//                     var toAppendOptionshub = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
//                     var toAppendDropdownshub = '';
//                     for (i = 0; i < counthub; i++) {
//                         toAppendOptionshub += (hublist[i]['listId'] + '|' + hublist[i]['name'] == listDetails) ? '<option value="' + hublist[i]['listId'] + '|' + hublist[i]['name'] + '" selected>' + hublist[i]['name'] + '</option>' : '<option value="' + hublist[i]['listId'] + '|' + hublist[i]['name'] + '">' + hublist[i]['name'] + '</option>';
//                         toAppendDropdownshub += '<li class=""><span>' + hublist[i]['name'] + '</span></li>';
//                     }
//                     $("#autoresponderlist").html(toAppendOptionshub + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownshub);
//                 }
//                 //Madmimi
//                 else if (lists['status'] == 203) {
//                     var countMm = Object.keys(lists['lists']).length;
//                     var mmList = lists['lists'];
//                     var toAppendOptionsMm = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
//                     var toAppendDropdownsMm = '';
//                     for (i = 0; i < countMm; i++) {
//                         toAppendOptionsMm += (mmList[i]['@attributes']['id'] + '|' + mmList[i]['@attributes']['name'] == listDetails) ? '<option value="' + mmList[i]['@attributes']['id'] + '|' + mmList[i]['@attributes']['name'] + '" selected>' + mmList[i]['@attributes']['name'] + '</option>' : '<option value="' + mmList[i]['@attributes']['id'] + '|' + mmList[i]['@attributes']['name'] + '">' + mmList[i]['@attributes']['name'] + '</option>';
//                         toAppendDropdownsMm += '<li class=""><span>' + mmList[i]['@attributes']['name'] + '</span></li>';
//                     }
//                     $("#autoresponderlist").html(toAppendOptionsMm + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownsMm);
//                 }
//                 //iContact
//                 else if (lists['status'] == 204) {
//                     var toAppendOptionIc = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
// //                            var toAppendOption = '';
//                     var toAppendDropdownIc = '';
//                     $(lists.lists).each(function (ind, val) {
//                         toAppendOptionIc += (val.listId + '|' + val.name == listDetails) ? '<option value="' + val.listId + '|' + val.name + '" selected>' + val.name + '</option>' : '<option value="' + val.listId + '|' + val.name + '" >' + val.name + '</option>';
//                         toAppendDropdownIc += '<li class=""><span>' + val.name + '</span></li>';
//                     });
//                     $("#autoresponderlist").html(toAppendOptionIc + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownIc);
//                 }
//                 //Sendlane
//                 else if (lists['status'] == 205) {
//                     var toAppendOptionSe = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
// //                            var toAppendOption = '';
//                     var toAppendDropdownSe = '';
//                     $(lists.lists).each(function (ind, val) {
//                         toAppendOptionSe += (val.list_id + '|' + val.list_name == listDetails) ? '<option value="' + val.list_id + '|' + val.list_name + '" selected>' + val.list_name + '</option>' : '<option value="' + val.list_id + '|' + val.list_name + '">' + val.list_name + '</option>';
//                         toAppendDropdownSe += '<li class=""><span>' + val.list_name + '</span></li>';
//                     });
//                     $("#autoresponderlist").html(toAppendOptionSe + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownSe);
//                 }
//                 //GoToWebinar
//                 else if (lists['status'] == 206) {
//                     var toAppendOptionGo = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
//                     var toAppendDropdownGo = '';
//                     $(lists.lists).each(function (ind, val) {
//                         toAppendOptionGo += (val.webinarID + '|' + val.subject == listDetails) ? '<option value="' + val.webinarID + '|' + val.subject + '" selected>' + val.subject + '</option>' : '<option value="' + val.webinarID + '|' + val.subject + '">' + val.subject + '</option>';
//                         toAppendDropdownGo += '<li class=""><span>' + val.subject + '</span></li>';
//                     });
//                     $("#autoresponderlist").html(toAppendOptionGo + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownGo);
//                 }
//                 //CampaignMonitor
//                 else if (lists['status'] == 207) {
//                     var toAppendOptionCm = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
// //                            var toAppendOption = '';
//                     var toAppendDropdownCm = '';
//                     $(lists.lists).each(function (ind, val) {
//                         toAppendOptionCm += (val.ListID + '|' + val.Name == listDetails) ? '<option value="' + val.ListID + '|' + val.Name + '" selected>' + val.Name + '</option>' : '<option value="' + val.ListID + '|' + val.Name + '">' + val.Name + '</option>';
//                         toAppendDropdownCm += '<li class=""><span>' + val.Name + '</span></li>';
//                     });
//                     $("#autoresponderlist").html(toAppendOptionCm + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownCm);
//                 }
//                 //Sendy
//                 else if (lists['status'] == 208) {
//                     var toAppendOptionSen = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
// //                            var toAppendOption = '';
//                     var toAppendDropdownSen = '';
//                     $(lists.lists).each(function (ind, val) {
//                         toAppendOptionSen += (val.listid + '|' + lists['status'] == listDetails) ? '<option value="' + val.listid + '|' + lists['status'] + '" selected>' + lists['status'] + '</option>' : '<option value="' + val.listid + '|' + lists['status'] + '">' + lists['status'] + '</option>';
//                         toAppendDropdownSen += '<li class=""><span>' + lists['status'] + '</span></li>';
//                     });
//                     $("#autoresponderlist").html(toAppendOptionSen + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownSen);
//                 }
//                 //WebinarJam or EverWebinar
//                 else if (lists['status'] == 209) {
//                     var toAppendOptionJam = '<select name="listDetails" class="form-control" id="listDetails"> <option value="" selected="">Select List</option>';
//                     var toAppendDropdownJam = '';
//                     $(lists.lists).each(function (ind, val) {
//                         toAppendOptionJam += '<option value="' + val.webinar_id + '.' + val.scheduleId + '|' + val.name + ' [' + val.schedule + ']">' + val.name + ' [' + val.schedule + ']' + '</option>';
//                         toAppendDropdownJam += '<li class=""><span>' + val.name + ' [' + val.schedule + ']' + '</span></li>';
//                     });
//                     $("#autoresponderlist").html(toAppendOptionJam + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownJam);
//                 }
//                 //Ontraport
//                 else if (lists['status'] == 210) {
//                     var toAppendOptionOntraTags = '<select multiple name="tagDetails[]" id="listDetails"> <option value="" disabled="">Select Tags</option>';
//                     var toAppendDropdownOntraTags = '';
//                     $(lists.tags).each(function (ind, val) {
//                         var searchResult = $.inArray(val, selectedtags);
//                         toAppendOptionOntraTags += (searchResult >= 0) ? '<option value="' + val + '" selected>' + val + '</option>' : '<option value="' + val + '">' + val + '</option>';
//                         toAppendDropdownOntraTags += '<li class=""><span>' + val + '</span></li>';
//                     });
//                     $("#autoresponderlist").html(toAppendOptionOntraTags + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
//                     $(".ab .dropdown-content").html(toAppendDropdownOntraTags);
//
//                     var toAppendOptionOntraSeq = '<select multiple name="seqDetails[]" id="seqDetails"> <option value="" disabled="">Select Sequences</option>';
//                     var toAppendDropdownOntraSeq = '';
//                     $(lists.lists).each(function (ind, val) {
//                         var searchResult1 = $.inArray(val.id, selectedsequence);
//                         toAppendOptionOntraSeq += (searchResult1 >= 0) ? '<option value="' + val.id + '" selected>' + val.name + '</option>' : '<option value="' + val.id + '">' + val.name + '</option>';
//                         toAppendDropdownOntraSeq += '<li class=""><span>' + val.name + '</span></li>';
//                     });
//                     $("#autoresponderlist").append(toAppendOptionOntraSeq + ' </select><span style="color: red;" class="error hide">Please select a sequence</span>');
//                     $(".ab .dropdown-content").append(toAppendDropdownOntraSeq);
//                 }
//                 //custom html
//                 else if (lists['status'] == 211) {
//                     var toAppendOptionCh = '<textarea id="customHtmlch" name="customHtml" style="height: 200px;">' + lists["lists"] + '</textarea>';
//                     var toAppendDropdownCh = '';
//                     $("#autoresponderlist").html(toAppendOptionCh);
//                     $(".ab .dropdown-content").html(toAppendDropdownCh);
//                 }
//                 else if(lists['status']==400){
//                     console.log(lists['message']);
//                 }
//
//                 if(sellistElement){
//                     $('#listDetails').val(sellistElement);
//                 }
            }
        });
    }
    ;

    var initSelect2Widgets = function () {
        $(document).find('select');
    };

    $(document).ready(function () {
        // for all the copy buttons
        $('#copy_html').on('click', function () {
            $('#show_css_div').addClass('hide');
            $('#show_dlink_div').addClass('hide');
            $('#show_html_div').removeClass('hide');
            $('.language-markup').text(buttonHtml);
            clipboard.copy(buttonHtml);
            Swal.fire({
                title: "Copied!",
                text: "Your button html has been copied.",
                timer: 1500,
                showConfirmButton: false
            });
        });

        $('#copy_css').on('click', function () {
            $('#show_html_div').addClass('hide');
            $('#show_dlink_div').addClass('hide');
            $('#show_css_div').removeClass('hide');
            $('.csshtml').text(cssHtml);
            clipboard.copy(cssHtml);
            Swal.fire({
                title: "Copied!",
                text: "Your button css has been copied.",
                timer: 1500,
                showConfirmButton: false
            });
        });

        $('#copy_redirect_url').on('click', function () {
            var directLink = '';
            if (typeof campId !== 'undefined' && campId !== null && campId != "") {
                var redirect = campaignData_redirect;
                directLink = projectUrl + 'auth/facebook-login/' + btoa(campId) + '?redirect=' + redirect;
            } else {
                alert("Please save your campaign first");
                return false;
                directLink = projectUrl + 'auth/facebook-login/';
            }
            $('#show_html_div').addClass('hide');
            $('#show_css_div').addClass('hide');
            $('#show_dlink_div').removeClass('hide');
            $('.dlink').text(directLink);
            clipboard.copy(directLink);
            Swal.fire({
                title: "Copied!",
                text: "Your direct link has been copied.",
                timer: 1500,
                showConfirmButton: false
            });
        });

    });
</script>






















@endsection

