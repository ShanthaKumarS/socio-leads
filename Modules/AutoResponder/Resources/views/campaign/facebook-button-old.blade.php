<?php

/**
 * Created by Raushan Kumar (raushankumar@globussoft.in)
 * Date: 5/1/2017
 * Time: 1:38 PM
 */
?>
@extends('resources.views.layouts.master')
@section('title','SocioLeads | Facebook Button')
@section('title1','Facebook Button')
@section('style')
    <link rel="stylesheet" href="/assets/plugins/jquery-minicolors/jquery.minicolors.css">
    <style>
        .btn_style_large {
            font-size: 20px;
            height: 40px;
            line-height: 37px;
            margin-left: 5px;
            padding: 0 35px;
            text-transform: none;
        }
    </style>
@endsection
@section('content')
    <main>
        <div class="container">
            <div class="row margin-top-10">
                <div class="col m6 l6 s12">
                    <h5 class="RopaSans_font">Facebook Button
                    </h5>
                </div>
            </div>

            <div class="row">
                <input id="list_details" value="{{$campaignData['listDetails'] ?? 'false'}}" hidden/>
                <input id="selected_tags" value="@if(isset($campaignData["tagDetails"])){{$campaignData["tagDetails"]}}@else false @endif" hidden/>
                <input id="selected_sequence" value="@if(isset($campaignData["seqDetails"])){{$campaignData["seqDetails"]}}@else false @endif" hidden/>
                <form id="campaign-form" method="post" action="">
                    <div class="col l6 m6 s12">
                        <div class="row card white">
                            <div class="col s12">
                                <h5>Your Facebook Button Settings</h5>
                            </div>
                            <div class="input-field col s12 l12">

                                <input id="button-text" name="buttonText" type="text" class="validate" placeholder="Chat With Us" value="{{$campaignData['buttonText'] ?? ''}}">
                                <label for="button-text">Text</label>
                                <span style="color: red;" class="error hide">Please enter a button text</span>
                            </div>
                            <div class="input-field col s12 l12">
                                <input id="redirect-url" name="redirectUrl" type="url" class="validate" placeholder="http://socioleads.in" value="{{$campaignData['redirectUrl'] or ''}}">
                                <label for="redirect-url">Redirect URL (Must starts with http:// or https://)</label>
                                <span style="color: red;" class="error hide">Please enter a redirect url</span>
                            </div>
                            <div class="input-field col s12 l12">
                                <input id="camp_name" name="campName" type="text" class="validate" placeholder="TheBestCampaign" value="{{$campaignData['campName'] ?? ''}}">
                                <label for="camp_name">Campaign Name</label>
                                <span style="color: red;" class="error hide">Please enter campaign name</span>
                            </div>

                                <div class="input-field col s12 l12 margin-top-10 dropdown-content-small">
                                    <select name="availableIntegretion" id="available_integretion" onchange="selectList(this)">
                                        <option value="" disabled selected>Choose your option</option>
                                        @foreach($userCreatedAutoresponders as $res)
                                            <option value="{{$res->autoresponders_id}}">{{$res->autoresponders_name}}({{$res->account_name}})</option>
                                        @endforeach
                                    </select>
                                    <label for="available_integretion">Select Your AutoResponder</label>
                                    <span style="color: red;" class="error hide">Please select an autoresponder</span>
                                </div>
                                <div class="ab input-field col s12 l12 margin-top-10">
                                    <div class="input-field dropdown-content-small" id="autoresponderlist">

                                    </div>
                                </div>
                        </div>
                        <input type="submit" id="submit-button" class="btn push_dark text_style_none center-align" value="Save Campaign">
                    </div>
                    <div class="col l6 m6 s12">
                        <div class="card">
                            <div class="card-content row">
                                <div class="row no-space">
                                    <form>
                                        <div class="row no-space">
                                            <div class="col s12 m12 l12">
                                                <h5>Style Your Button</h5>
                                            </div>
                                        </div>
                                        <div class="row no-space">
                                            <div class="input-field col s12 m5 l5 dropdown-content-small">
                                                <select id="button-size" name="buttonSize">
                                                    <option value="" disabled >Choose your option</option>
                                                    <option value="1">Small</option>
                                                    <option value="2" selected>Medium</option>
                                                    <option value="3">Large</option>
                                                </select>
                                                <label for="button-size">Select Button size</label>
                                                <span style="color: red;" class="error hide">Please select button size</span>
                                            </div>
                                            <div class="input-field col s12 m3 l3">
                                                <h6>Button Color</h6>
                                                <input type="text" id="button-color" name="buttonColor" class="form-control demo" data-opacity=".9" value="{{$campaignData['buttonColor'] ?? '#3b5998'}}">
                                            </div>
                                            <div class="input-field col s12 m4 l4">
                                                <h6>Button Text Color</h6>
                                                <input type="text" id="button-text-color" name="buttonTextColor" class="form-control demo" data-opacity=".9" value="{{$campaignData['buttonTextColor'] ?? '#ffffff'}}">
                                            </div>
                                        </div>
                                        <div class="row no-space">
                                            <div class="col s12 m12 l12 center">
                                                <h5>Button Preview</h5>
                                                <a href="javascript:void(0);" id="previewButton" class="btn push_dark radius_10">
                                                    <span id="previewButtonText" style="text-transform:none;">CHAT WITH US</span>
                                                </a>
                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="row no-space">
                                    <div class="col s12">
                                        <h5>Publish Your Button</h5>
                                    </div>
                                    <div class="col l4 m4 s12">
                                        <a href="javascript:void(0);" class="btn btn_style push_dark col l12 m12 s12 " id="copy_html">Copy Button HTML Code</a>
                                    </div>
                                    <div class="col l4 m4 s12">
                                        <a href="javascript:void(0);" class="btn btn_style push_dark col l12 m12 s12" id="copy_css">Copy Button CSS Code</a>
                                    </div>
                                    <div class="col l4 m4 s12">
                                        <a href="javascript:void(0);" class="btn btn_style push_dark col l12 m12 s12" id="copy_redirect_url">Copy Subscribe Link</a>
                                    </div>
                                    <div class="col l12 s12">
                                        <br><br>
                                    </div>
                                    <hr class="margin-bottom-10">
                                    <div class="col l12 m12 s12 hide" id="show_html_div">
                                        <h5 class="RopaSans_font center">Markup</h5>
                                        <pre class="language-markup">

                                        </pre>
                                        <hr class="margin-bottom-10">
                                    </div>
                                    <div class="col l12 m12 s12 hide" id="show_css_div">
                                        <h5 class="RopaSans_font center">CSS</h5>
                                        <pre class="csshtml language-markup"></pre>
                                    </div>
                                    <div class="col l12 m12 s12 hide" id="show_dlink_div">
                                        <h5 class="RopaSans_font center">Direct Link</h5>
                                        <pre class="dlink language-markup"></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="blockDiv" class="hide">
            <div class="" style="padding: 0px; margin: 0px; text-align: center; color: rgb(0, 0, 0); border: 3px solid rgb(170, 170, 170); width: 100%; height: 100%; position: fixed; top: 0%; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.5; z-index: 1004; cursor: wait;"></div>
            <div class="blockUI blockMsg blockPage " style="padding: 0px; margin: 0px; top: 50%; color: rgb(0, 0, 0); border: 3px solid rgb(170, 170, 170); font-weight: normal; background-color: rgb(255, 255, 255); font-size: 20px; left: 35%; text-align: center; z-index: 999999 ! important; position: fixed; width: 30%;">
                <img src="/assets/images/facebook.gif" style="height:25px;">Just A Moment
            </div>
        </div>
    </main>
@endsection

@section('script_function')
    <script type="text/javascript" src="/assets/plugins/jquery-minicolors/jquery.minicolors.js"></script>
    <script type="text/javascript" src="/assets/plugins/clipboard.js"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-58515856-37', 'auto');
        ga('send', 'pageview');

    </script>

    <script>

        $('.dropdown-button').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: false, // Does not change width of dropdown to that of the activator
                    hover: true, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: false, // Displays dropdown below the button
                    alignment: 'left' // Displays dropdown with edge aligned to the left of button
                }
        );

        // delete sweet alert
        function deletepage() {
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        }
                        else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
        }


        var campId='<?php if (isset($campaignData['campId'])) {echo $campaignData['campId'];}?>';
        var projectUrl='<?php echo env("APP_URL");?>';

        var buttonHtml='<a id="socioleads-fb-button" href="javascript:window.open(\''+projectUrl+'auth/facebook-login/'+btoa(campId)+'\',\'_blank\',\'height=\'+screen.height+\', width=\'+screen.width);void(0);">Chat with us</a>';
        var cssHtml='#socioleads-fb-button {font-family: "Roboto",sans-serif;text-decoration: none;border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 11px;padding: 0 5px;height: 30px;line-height: 29px;background-color: #3b5998;text-transform:none;color: #fff;cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';

        $(document).ready(function () {
            // for all the copy buttons

            $('#copy_html').on('click',function () {

                $('#show_css_div').addClass('hide');
                $('#show_html_div').removeClass('hide');
                $('.language-markup').text(buttonHtml);
                clipboard.copy(buttonHtml);

            });
            $('#copy_css').on('click',function () {
                $('#show_html_div').addClass('hide');
                $('#show_css_div').removeClass('hide');
                $('.csshtml').text(cssHtml);
                clipboard.copy(cssHtml);

            });
            $('#copy_redirect_url').on('click',function () {
                var directLink='';
                if(typeof campId!=='undefined' && campId!==null && campId!=""){
                    var redirect= '<?php if (isset($campaignData['redirectUrl'])) {echo $campaignData['redirectUrl'];}?>';
                    directLink=projectUrl+'auth/facebook-login/'+btoa(campId)+'?redirect='+redirect;
                }else{
                    directLink=projectUrl+'auth/facebook-login/';
                }
                clipboard.copy(directLink);
            });

            //To display the colors dropdown
            $('.demo').each(function () {
                $(this).minicolors({
                    control: $(this).attr('data-control') || 'hue',
                    defaultValue: $(this).attr('data-defaultValue') || '',
                    format: $(this).attr('data-format') || 'hex',
                    keywords: $(this).attr('data-keywords') || '',
                    inline: $(this).attr('data-inline') === 'true',
                    letterCase: $(this).attr('data-letterCase') || 'lowercase',
                    opacity: $(this).attr('data-opacity'),
                    position: $(this).attr('data-position') || 'bottom left',
                    swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
                    change: function (hex, opacity) {
                        var log;
                        try {
                            log = hex ? hex : 'transparent';
                            if (opacity) log += ', ' + opacity;
                            console.log(log);
                        } catch (e) {
                        }
                    },
                    theme: 'default'
                });
            });






            //To hide the error message
            $('#redirect-url').on('keyup', function () {
                $(this).siblings('span.error').addClass('hide');
            });

            //To hide the error message
            $('#camp_name').on('keyup', function () {
                $(this).siblings('span.error').addClass('hide');
            });

            //To change the button text and hide the error message
            $('#button-text').on('keyup', function () {
                $('#previewButtonText').text($(this).val());
                $(this).siblings('span.error').addClass('hide');
            });

            //To change the button size
            $('#button-size').on('change', function () {
                $(this).parent().siblings('span.error').addClass('hide');
                if ($(this).val() == "1") {
                    $('#previewButton').removeClass('btn_style_large').addClass('btn_style');
                } else if($(this).val() == "2") {
                    $('#previewButton').removeClass('btn_style').removeClass('btn_style_large');
                }else {
                    $('#previewButton').removeClass('btn_style').addClass('btn_style_large');
                }
            });

            //To change the button color
            $('#button-color').on('change', function () {
                $('#previewButton').attr('style', 'background-color: ' + $(this).val() + ' !important');
            });

            //To change the button text color
            $('#button-text-color').on('change', function () {
                $('#previewButtonText').css('color', $(this).val());
            });

            //to display button size
            var buttonSize = '<?php if (isset($campaignData['buttonSize'])) {
                echo $campaignData['buttonSize'];
            } else {
                echo "false";
            }?>';
            if (buttonSize !== "false") {
                $('#button-size').val(buttonSize).material_select();
                $('#button-size').trigger('onchange');
                if (buttonSize == "1") {
                    $('#previewButton').removeClass('btn_style_large').addClass('btn_style');
                } else if(buttonSize == "2") {
                    $('#previewButton').removeClass('btn_style').removeClass('btn_style_large');
                }else {
                    $('#previewButton').removeClass('btn_style').addClass('btn_style_large');
                }
            }

            //to display autoresponders
            var availableIntegretion = '<?php if (isset($campaignData['availableIntegretion'])) {
                echo $campaignData['availableIntegretion'];
            } else {
                echo "false";
            }?>';
            if (availableIntegretion !== "false") {
                $('#available_integretion').val(availableIntegretion).material_select();
                $('#available_integretion').trigger('onchange');
            }

            //to display the button color
            var buttonColor = '<?php if (isset($campaignData['buttonColor'])) {
                echo $campaignData['buttonColor'];
            } else {
                echo "false";
            }?>';
            if (buttonColor !== "false") {
                $('#previewButton').attr('style', 'background-color: ' + buttonColor + ' !important');
            }

            //to display the button text color
            var buttonTextColor = '<?php if (isset($campaignData['buttonTextColor'])) {
                echo $campaignData['buttonTextColor'];
            } else {
                echo "false";
            }?>';
            if (buttonTextColor !== "false") {
                $('#previewButtonText').css('color', buttonTextColor);
            }

            //to display the button text
            var buttonText = '<?php if (isset($campaignData['buttonText'])) {
                echo addslashes($campaignData['buttonText']);
            } else {
                echo "false";
            }?>';
            if (buttonText !== "false") {
                $('#previewButtonText').text(buttonText);
                $('#submit-button').val('Update Campaign');
            }

            //to display the button html and css html
            var buttonText = '<?php if (isset($campaignData['buttonText'])) {
                echo addslashes($campaignData['buttonText']);
            } else {
                echo "false";
            }?>';
            if (buttonText !== "false") {
                var btntxt = '<?php if (isset($campaignData['buttonText'])) {echo addslashes($campaignData['buttonText']);}?>';
                var redirect= '<?php if (isset($campaignData['redirectUrl'])) {echo $campaignData['redirectUrl'];}?>';
                buttonHtml='<a id="socioleads-fb-button" href="javascript:window.open(\''+projectUrl+'auth/facebook-login/'+btoa(campId)+'?redirect='+redirect+'\',\'_blank\',\'height=\'+screen.height+\', width=\'+screen.width);void(0);">'+btntxt+'</a>';
                var btncolor='<?php if (isset($campaignData['buttonColor'])) {echo $campaignData['buttonColor'];}?>';
                var btntxtcolor='<?php if (isset($campaignData['buttonTextColor'])) {echo $campaignData['buttonTextColor'];}?>';
                var btnSize = '<?php if (isset($campaignData['buttonSize'])) {echo $campaignData['buttonSize'];} else {echo "false";}?>';
                if (btnSize !== "false") {
                    if (buttonSize == "1") {
//                        cssHtml='#socioleads-fb-button {border-radius: 2px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 36px;height: 36px;padding: 0 2rem;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                        cssHtml='#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 11px;padding: 0 5px;height: 30px;line-height: 29px;background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                    } else if(buttonSize == "2"){
                        cssHtml='#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 36px;height: 36px;padding: 0 2rem;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
//                        cssHtml='#socioleads-fb-button {border-radius: 2px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 11px;padding: 0 5px;height: 30px;line-height: 29px;background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                    }else {
                        cssHtml='#socioleads-fb-button {border-radius: 10px;box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);font-size: 20px;background-color: '+btncolor+';text-transform:none;color: '+btntxtcolor+';cursor: pointer;letter-spacing: 0.5px;display: inline-block;margin-left: 5px;line-height: 37px;height: 40px;padding: 0 35px;font-family: "Roboto",sans-serif;text-decoration: none;}\n\n#socioleads-fb-button:active,#socioleads-fb-button:hover {outline: 0 none;}';
                    }
                }
                $('.buttonhtml').text(buttonHtml);
                $('.csshtml').text(cssHtml);
            }else{
                $('.buttonhtml').text(buttonHtml);
                $('.csshtml').text(cssHtml);
            }

            //To save the campaign
            $('#submit-button').on('click', function (e) {
                e.preventDefault();
                var valid = true;
                $.each($('#campaign-form input,#campaign-form select').not('.no-validate'), function (i, v) {
                    var value = $.trim($(this).val());
                    var element = $(this);
                    if ($(this).attr('id') == 'button-size' || $(this).attr('id') == 'available_integretion' || $(this).attr('id') == 'listDetails' || $(this).attr('id') == 'seqDetails') {
                        element = $(this).parent();
                    }
                    element.siblings('span.error').addClass('hide');
                    if ((value.length == 0) || $(this).hasClass('invalid')) {
                        if ($(this).hasClass('invalid')) {
                                element.siblings('span.error').text('Redirect url must starts with http:// or https://');
                        }
                        valid = false;
                        element.siblings('span.error').removeClass('hide');
                        return true;
                    }
                });
                if (valid) {
                    $("#campaign-form").submit();
                }
            });
        });




        //ajax to fetch and add the lists of selected integretions
        function selectList(element) {
            $(element).parent().siblings('span.error').addClass('hide');
            var autoId = $('#available_integretion').val();
            var listDetails = $('#list_details').val();
            var tagDetails = $('#selected_tags').val();
            var seqDetails = $('#selected_sequence').val();
            var selectedtags='';
            var selectedsequence='';
            if ((tagDetails !== "false") &&  (seqDetails !== "false")){
                selectedtags = $('#selected_tags').val().split(",");
                selectedsequence = $('#selected_sequence').val().split(",");
            }
            $.ajax({
                url: '/auto-responder/retrieve-campaigns',
                data: {
                    autoId: autoId
                },
                type: 'POST',
                datatype: 'json',
                beforeSend: function () {
                    $('#blockDiv').removeClass('hide');
                    $("#autoresponderlist").html('<select name="listDetails" id="listDetails">  <option  value="" selected="">Select List</option></select><span style="color: red;" class="error hide">Please select a list</span>');
                },
                complete: function () {
                    $('#blockDiv').addClass('hide');
                    initSelect2Widgets();
                },
                success: function (lists) {
                    if(lists.trim()==''){
                        $('#blockDiv').addClass('hide');
                        return false;
                    }
                    lists = $.parseJSON(lists);
                    //mailchimp or ConstantContact or Drip or Aweber or Fluttermail or Sendgrid
                    if (lists['status'] == 200) {
                        var toAppendOption = '<select name="listDetails" id="listDetails">  <option  value="" selected="">Select List</option>';
                        var toAppendDropdown = '';
                        $(lists.lists).each(function (ind, val) {
                            toAppendOption += (val.id + '|' + val.name == listDetails) ? '<option value="' + val.id + '|' + val.name + '" selected>' + val.name + '</option>' : '<option value="' + val.id + '|' + val.name + '">' + val.name + '</option>';
                            toAppendDropdown += '<li class=""><span>' + val.name + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOption + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdown);
                    }
                    //Getresponse
                    else if (lists['status'] == 201) {
                        var count = Object.keys(lists['keys']).length;
                        var keys = lists['keys'];
                        var autolist = lists['lists'];

                        var toAppendOptions = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
                        var toAppendDropdowns = '';
                        for (i = 0; i < count; i++) {
                            toAppendOptions += (keys[i] + '|' + autolist[keys[i]]['name'] == listDetails) ? '<option value="' + keys[i] + '|' + autolist[keys[i]]['name'] + '" selected>' + autolist[keys[i]]['name'] + '</option>' : '<option value="' + keys[i] + '|' + autolist[keys[i]]['name'] + '">' + autolist[keys[i]]['name'] + '</option>';
                            toAppendDropdowns += '<li class=""><span>' + autolist[keys[i]]['name'] + '</span></li>';
                        }
                        $("#autoresponderlist").html(toAppendOptions + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdowns);
                    }
                    //Hubspot
                    else if (lists['status'] == 202) {
                        var counthub = Object.keys(lists['lists']).length;
                        var hublist = lists['lists'];
                        var toAppendOptionshub = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
                        var toAppendDropdownshub = '';
                        for (i = 0; i < counthub; i++) {
                            toAppendOptionshub += (hublist[i]['listId'] + '|' + hublist[i]['name'] == listDetails) ? '<option value="' + hublist[i]['listId'] + '|' + hublist[i]['name'] + '" selected>' + hublist[i]['name'] + '</option>' : '<option value="' + hublist[i]['listId'] + '|' + hublist[i]['name'] + '">' + hublist[i]['name'] + '</option>';
                            toAppendDropdownshub += '<li class=""><span>' + hublist[i]['name'] + '</span></li>';
                        }
                        $("#autoresponderlist").html(toAppendOptionshub + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownshub);
                    }
                    //Madmimi
                    else if (lists['status'] == 203) {
                        var countMm = Object.keys(lists['lists']).length;
                        var mmList = lists['lists'];
                        var toAppendOptionsMm = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
                        var toAppendDropdownsMm = '';
                        for (i = 0; i < countMm; i++) {
                            toAppendOptionsMm += (mmList[i]['@attributes']['id'] + '|' + mmList[i]['@attributes']['name'] == listDetails) ? '<option value="' + mmList[i]['@attributes']['id'] + '|' + mmList[i]['@attributes']['name'] + '" selected>' + mmList[i]['@attributes']['name'] + '</option>' : '<option value="' + mmList[i]['@attributes']['id'] + '|' + mmList[i]['@attributes']['name'] + '">' + mmList[i]['@attributes']['name'] + '</option>';
                            toAppendDropdownsMm += '<li class=""><span>' + mmList[i]['@attributes']['name'] + '</span></li>';
                        }
                        $("#autoresponderlist").html(toAppendOptionsMm + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownsMm);
                    }
                    //iContact
                    else if (lists['status'] == 204) {
                        var toAppendOptionIc = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
//                            var toAppendOption = '';
                        var toAppendDropdownIc = '';
                        $(lists.lists).each(function (ind, val) {
                            toAppendOptionIc += (val.listId + '|' + val.name == listDetails) ? '<option value="' + val.listId + '|' + val.name + '" selected>' + val.name + '</option>' : '<option value="' + val.listId + '|' + val.name + '" >' + val.name + '</option>';
                            toAppendDropdownIc += '<li class=""><span>' + val.name + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOptionIc + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownIc);
                    }
                    //Sendlane
                    else if (lists['status'] == 205) {
                        var toAppendOptionSe = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
//                            var toAppendOption = '';
                        var toAppendDropdownSe = '';
                        $(lists.lists).each(function (ind, val) {
                            toAppendOptionSe += (val.list_id + '|' + val.list_name == listDetails) ? '<option value="' + val.list_id + '|' + val.list_name + '" selected>' + val.list_name + '</option>' : '<option value="' + val.list_id + '|' + val.list_name + '">' + val.list_name + '</option>';
                            toAppendDropdownSe += '<li class=""><span>' + val.list_name + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOptionSe + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownSe);
                    }
                    //GoToWebinar
                    else if (lists['status'] == 206) {
                        var toAppendOptionGo = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
                        var toAppendDropdownGo = '';
                        $(lists.lists).each(function (ind, val) {
                            toAppendOptionGo += (val.webinarID + '|' + val.subject == listDetails) ? '<option value="' + val.webinarID + '|' + val.subject + '" selected>' + val.subject + '</option>' : '<option value="' + val.webinarID + '|' + val.subject + '">' + val.subject + '</option>';
                            toAppendDropdownGo += '<li class=""><span>' + val.subject + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOptionGo + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownGo);
                    }
                    //CampaignMonitor
                    else if (lists['status'] == 207) {
                        var toAppendOptionCm = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
//                            var toAppendOption = '';
                        var toAppendDropdownCm = '';
                        $(lists.lists).each(function (ind, val) {
                            toAppendOptionCm += (val.ListID + '|' + val.Name == listDetails) ? '<option value="' + val.ListID + '|' + val.Name + '" selected>' + val.Name + '</option>' : '<option value="' + val.ListID + '|' + val.Name + '">' + val.Name + '</option>';
                            toAppendDropdownCm += '<li class=""><span>' + val.Name + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOptionCm + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownCm);
                    }
                    //Sendy
                    else if (lists['status'] == 208) {
                        var toAppendOptionSen = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
//                            var toAppendOption = '';
                        var toAppendDropdownSen = '';
                        $(lists.lists).each(function (ind, val) {
                            toAppendOptionSen += (val.listid + '|' + lists['status'] == listDetails) ? '<option value="' + val.listid + '|' + lists['status'] + '" selected>' + lists['status'] + '</option>' : '<option value="' + val.listid + '|' + lists['status'] + '">' + lists['status'] + '</option>';
                            toAppendDropdownSen += '<li class=""><span>' + lists['status'] + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOptionSen + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownSen);
                    }
                    //WebinarJam or EverWebinar
                    else if (lists['status'] == 209) {
                        var toAppendOptionJam = '<select name="listDetails" id="listDetails"> <option value="" selected="">Select List</option>';
                        var toAppendDropdownJam = '';
                        $(lists.lists).each(function (ind, val) {
                            toAppendOptionJam += '<option value="' + val.webinar_id + '.' + val.scheduleId + '|' + val.name + ' [' + val.schedule + ']">' + val.name + ' [' + val.schedule + ']' + '</option>';
                            toAppendDropdownJam += '<li class=""><span>' + val.name + ' [' + val.schedule + ']' + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOptionJam + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownJam);
                    }
                    //Ontraport
                    else if (lists['status'] == 210) {
                        var toAppendOptionOntraTags = '<select multiple name="tagDetails[]" id="listDetails"> <option value="" disabled="">Select Tags</option>';
                        var toAppendDropdownOntraTags = '';
                        $(lists.tags).each(function (ind, val) {
                            var searchResult = $.inArray(val, selectedtags);
                            toAppendOptionOntraTags += (searchResult >= 0) ? '<option value="' + val + '" selected>' + val + '</option>' : '<option value="' + val + '">' + val + '</option>';
                            toAppendDropdownOntraTags += '<li class=""><span>' + val + '</span></li>';
                        });
                        $("#autoresponderlist").html(toAppendOptionOntraTags + ' </select><span style="color: red;" class="error hide">Please select a list</span>');
                        $(".ab .dropdown-content").html(toAppendDropdownOntraTags);

                        var toAppendOptionOntraSeq = '<select multiple name="seqDetails[]" id="seqDetails"> <option value="" disabled="">Select Sequences</option>';
                        var toAppendDropdownOntraSeq = '';
                        $(lists.lists).each(function (ind, val) {
                            var searchResult1 = $.inArray(val.id, selectedsequence);
                            toAppendOptionOntraSeq += (searchResult1 >= 0) ? '<option value="' + val.id + '" selected>' + val.name + '</option>' : '<option value="' + val.id + '">' + val.name + '</option>';
                            toAppendDropdownOntraSeq += '<li class=""><span>' + val.name + '</span></li>';
                        });
                        $("#autoresponderlist").append(toAppendOptionOntraSeq + ' </select><span style="color: red;" class="error hide">Please select a sequence</span>');
                        $(".ab .dropdown-content").append(toAppendDropdownOntraSeq);
                    }
                    //custom html
                    else if (lists['status'] == 211) {
                        var toAppendOptionCh = '<textarea id="customHtmlch" name="customHtml" style="height: 200px;">' + lists["lists"] + '</textarea>';
                        var toAppendDropdownCh = '';
                        $("#autoresponderlist").html(toAppendOptionCh);
                        $(".ab .dropdown-content").html(toAppendDropdownCh);
                    }
                    else if(lists['status']==400){
                        console.log(lists['message']);
                    }
                }
            });
        }
        ;

        var initSelect2Widgets = function () {
            $(document).find('select').material_select();
        };

        $(document).ready(function () {
            // for all the copy buttons
            $('#copy_html').on('click', function () {
                $('#show_css_div').addClass('hide');
                $('#show_dlink_div').addClass('hide');
                $('#show_html_div').removeClass('hide');
                $('.language-markup').text(buttonHtml);
                clipboard.copy(buttonHtml);
                swal({
                    title: "Copied!",
                    text: "Your button html has been copied.",
                    timer: 1500,
                    showConfirmButton: false
                });
            });

            $('#copy_css').on('click', function () {
                $('#show_html_div').addClass('hide');
                $('#show_dlink_div').addClass('hide');
                $('#show_css_div').removeClass('hide');
                $('.csshtml').text(cssHtml);
                clipboard.copy(cssHtml);
                swal({
                    title: "Copied!",
                    text: "Your button css has been copied.",
                    timer: 1500,
                    showConfirmButton: false
                });
            });

            $('#copy_redirect_url').on('click', function () {
                var directLink = '';
                if (typeof campId !== 'undefined' && campId !== null && campId != "") {
                    var redirect = '<?php if (isset($campaignData['redirectUrl'])) {
                        echo $campaignData['redirectUrl'];
                    }?>';
                    directLink = projectUrl + 'auth/facebook-login/' + btoa(campId) + '?redirect=' + redirect;
                } else {
                    alert("Please save your campaign first");
                    return false;
                    directLink = projectUrl + 'auth/facebook-login/';
                }
                $('#show_html_div').addClass('hide');
                $('#show_css_div').addClass('hide');
                $('#show_dlink_div').removeClass('hide');
                $('.dlink').text(directLink);
                clipboard.copy(directLink);
                swal({
                    title: "Copied!",
                    text: "Your direct link has been copied.",
                    timer: 1500,
                    showConfirmButton: false
                });
            });

        });
    </script>





@endsection
