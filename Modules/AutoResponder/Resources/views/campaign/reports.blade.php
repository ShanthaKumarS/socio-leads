<?php
/**
 * Created by Raushan Kumar <raushankumarglobussoft.in>
 * Date: 06/05/2017
 * Time: 04:20 PM
 */
?>
@extends('User::resources.views.layouts.master')
@section('title','SocioLeads | Reports')
@section('title1',' Reports')
@section('content')

    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed ">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> Facebook Button Campaign Reports </h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <a href="/auto-responder/campaign" type="button" class="btn btn-light-primary btn-sm">List Builder</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('user::resources.views.layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->

    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--Begin::Row-->
                <div class="row">
                    <div class="col-xl-4">
                        <div class="card bg-primary card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <i class="icon-2x text-white flaticon-like"></i>
                                    <div class="d-flex flex-column text-white text-right">
                                        <span class="font-weight-bolder font-size-h3">{{$countViews ?? '0'}}</span>
                                        <span class="font-weight-bold mt-2">Total Clicks</span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card bg-warning card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <i class="icon-2x text-white flaticon2-group"></i>
                                    <div class="d-flex flex-column text-white text-right">
                                        <span class="font-weight-bolder font-size-h3">{{$countClick ?? '0'}}</span>
                                        <span class="font-weight-bold mt-2">Total Leads</span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card bg-success card-stretch gutter-b">
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                                    <i class="icon-2x text-white flaticon2-graphic"></i>
                                    <div class="d-flex flex-column text-white text-right">
                                        <span class="font-weight-bolder font-size-h3"><?php if ($countViews != 0) {
                                                echo(number_format((float)((($countClick) / ($countViews)) * 100), 0, '.', ''));
                                            } else echo '0';?>%</span>
                                        <span class="font-weight-bold mt-2">Conversion Rate</span>
                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                </div>
                <!--End::Row-->

                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Card-->
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        Subscribers
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <!--begin::Chart-->
                                <div id="Campaign_Reports"></div>
                                <!--end::Chart-->

                            </div>
                        </div>
                        <!--end::Card-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Footer-->
    @include('User::resources.views.layouts.page-footer')



@endsection
@section('script_function')



<!--begin::Page Scripts(used by this page)-->
<script src="{{asset('assets/js/pages/features/charts/apexcharts.js')}}"></script>
<script src="/assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
<!--end::Page Scripts-->

    <script type="text/javascript">
        // delete alert
        function DeleteIntegration() {
            Swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover this Integration!",
                icon: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                Swal.fire("Deleted!", "Your Integration has been deleted.", "success");
            });
        };

        let date_camp = [];
        let total_view_camp = [];
        let leads_camp = [];

        <?php if(!empty($clicksByDate)){ foreach($clicksByDate as $clickByDate){$leads = round((isset($clickByDate->total_click) ? $clickByDate->total_click : 0) / $clickByDate->total_view * 100, 2);?>

             date_camp.push('<?php echo $clickByDate->date; ?>');
             total_view_camp.push('<?php echo  $clickByDate->total_view; ?>');
             leads_camp.push('<?php  echo $leads; ?>');

        <?php } }else{?>

             date_camp.push('<?php echo date("Y-m-d");?>');
             total_view_camp.push('0');
             leads_camp.push('0');

          <?php } ?>

        $(document).ready(function () {

            campaignsToGraph(total_view_camp,leads_camp,date_camp);

        });


    </script>

@endsection
