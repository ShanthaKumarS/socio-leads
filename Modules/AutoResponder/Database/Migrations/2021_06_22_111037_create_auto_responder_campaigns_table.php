<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoResponderCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_responder_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('unique_id');
            $table->boolean('is_active');
            $table->unsignedInteger('auto_responder_id');

            $table->foreign('auto_responder_id')->references('id')->on('auto_responders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_responder_lists');
    }
}
