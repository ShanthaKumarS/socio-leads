<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('button_text');
            $table->integer('button_size')->default(Config::get('constants.CAMPAIGN.DEFAULT_BUTTON_SIZE'));
            $table->string('button_color')->default(Config::get('constants.CAMPAIGN.DEFAULT_BUTTON_COLOR'));
            $table->string('button_text_color')->default(Config::get('constants.CAMPAIGN.DEFAULT_BUTTON_TEXT_COLOR'));
            $table->string('redirect_url');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('click_count')->default(0);
            $table->unsignedInteger('lead_count')->default(0);
            $table->unsignedInteger('auto_responder_campaign_id');

            $table->foreign('auto_responder_campaign_id')->references('id')->on('auto_responder_campaigns')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
