<?php

namespace Modules\AutoResponder\Http\Controllers;

use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Redirector;
use Modules\AutoResponder\Entities\AutoResponder;
use Modules\AutoResponder\Entities\Campaign;
use Modules\Facebook\Http\Controllers\LoginController;

/**
 * Class CampaignController
 * @package Modules\AutoResponder\Http\Controllers
 */
class CampaignController extends Controller
{
    private $bindResponder = [
        'getResponse' => 'Modules\\AutoResponder\\Http\\Controllers\\GetResponseController',
    ];

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $user = session()->get('user');
        $campaigns = Campaign::where('user_id', $user['user_id'])->get();
        $autoResponders = AutoResponder::where('user_id', $user['user_id'])->get();
        return view('autoresponder::campaign.campaigns',
            [
                'user' => $user,
                'autoResponders' => $autoResponders,
                'campaigns' => $campaigns
            ]
        );
    }

    /**
     * @param $autoResponderId
     * @return array
     */
    public function retrieve($autoResponderId)
    {
        $autoResponder = AutoResponder::find($autoResponderId);
        return $autoResponder->AutoResponderCampaigns;
    }

    /**
     * @param Request $request
     * @param Campaign $campaign
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request, Campaign $campaign)
    {
        $campaign->name = $request->campName;
        $campaign->redirect_url = $request->redirectUrl;
        $campaign->button_text = $request->buttonText;
        $campaign->button_size = $request->buttonSize;
        $campaign->button_color = $request->buttonColor;
        $campaign->button_text_color = $request->buttonTextColor;
        $campaign->auto_responder_campaign_id = $request->listDetails;
        $campaign->user_id = session()->get('user')['user_id'];

        $campaign->save();

        return redirect('campaign');
    }

    /**
     * @param Request $request
     * @param string $campaignId
     * @return RedirectResponse|Redirector
     * @throws FacebookSDKException
     */
    public function loginToFacebook(Request $request, string $campaignId)
    {
        $request->session()->put('redirectUrl', $request->redirect);
        $request->session()->put('campaignId', $campaignId);

        $facebookController = new FacebookController();
        return redirect($facebookController->getLoginUrl());
    }

    /**
     * @param Request $request
     * @param AutoResponderController $autoResponderController
     * @return RedirectResponse|Redirector
     * @throws FacebookSDKException
     */
    public function callback(Request $request, AutoResponderController $autoResponderController)
    {
        session_start();
        $fb = LoginController::getFacebookGraphApi();
        $helper = $fb->getRedirectLoginHelper();

        try {
            $response = $fb->get('/me?fields=name,email', $helper->getAccessToken()->getValue());
            $user = $response->getGraphUser();

            $campaign = Campaign::find(base64_decode($request->session()->get('campaignId')));
            $autoResponder = $campaign->autoResponderCampaign->autoResponder;

            $responderController = new $autoResponderController->bindResponder[$autoResponder->responder]($autoResponder);
            $this->increaseClickCount($campaign);
            $responderController->  addUserToResponder($user, $campaign);
            $this->increaseLeadCount($campaign);

            return redirect($request->session()->get('redirectUrl'));

        } catch(\Exception $e) {
            dd($e);
            return redirect($request->session()->get('redirectUrl'));
        }
    }

    /**
     * @param Campaign $campaign
     */
    private function increaseClickCount(Campaign $campaign)
    {
        $campaign->click_count ++;
        $campaign->save();
    }

    /**
     * @param Campaign $campaign
     */
    private function increaseLeadCount(Campaign $campaign)
    {
        $campaign->view_count ++;
        $campaign->save();
    }
}
