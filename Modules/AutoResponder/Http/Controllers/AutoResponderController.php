<?php

namespace Modules\AutoResponder\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\AutoResponder\Entities\AutoResponder;

/**
 * Class AutoResponderController
 * @package Modules\AutoResponder\Http\Controllers
 */
class AutoResponderController extends Controller
{
    public $bindResponder = [
        'getResponse' => 'Modules\\AutoResponder\\Http\\Controllers\\Responders\\GetResponseController',
        'mailChimp' => 'Modules\\AutoResponder\\Http\\Controllers\\Responders\\MailChimpController'
    ];

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $user = session()->get('user');
        $responders = $this->getAllResponders($user['user_id']);

        return view('autoresponder::auto-responder',
            [
                'user' => $user,
                'responders' => $responders
            ]
        );
    }

    /**
     * @param $userId
     * @return array
     */
    public function getAllResponders($userId)
    {
        $autoResponders = AutoResponder::where('user_id', $userId)->get();
        $responders = [];
        foreach ($autoResponders as $autoResponder) {
            $responderController = new $this->bindResponder[$autoResponder->responder]($autoResponder);
            $responders[$autoResponder->responder][] =  $responderController->getResponder();
        }

        return $responders;
    }
}
