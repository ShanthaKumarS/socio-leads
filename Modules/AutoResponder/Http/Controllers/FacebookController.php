<?php

namespace Modules\AutoResponder\Http\Controllers;

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Routing\Controller;
use Illuminate\Support\Env;

/**
 * Class FacebookController
 * @package Modules\AutoResponder\Http\Controllers
 */
class FacebookController extends Controller
{
    /**
     * @return Facebook
     * @throws FacebookSDKException
     * @throws FacebookSDKException
     */
    public static function getFacebookGraphApi()
    {
        return new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'),
            'app_secret' => env('FACEBOOK_SECRET_KEY'),
            'default_graph_version' => 'v10.0',
        ]);
    }

    /**
     * @return string
     * @throws FacebookSDKException
     */
    public function getLoginUrl()
    {
        session_start();
        $facebook = $this->getFacebookGraphApi();
        $helper = $facebook->getRedirectLoginHelper();
        return $helper->getLoginUrl(Env::get('FACEBOOK_CALLBACK_URL'));
    }
}
