<?php

namespace Modules\AutoResponder\Http\Controllers\Responders;

use Getresponse\Sdk\GetresponseClientFactory;
use Getresponse\Sdk\Operation\Campaigns\GetCampaigns\GetCampaigns;
use Getresponse\Sdk\Operation\Contacts\CreateContact\CreateContact;
use Getresponse\Sdk\Operation\Model\CampaignReference;
use Getresponse\Sdk\Operation\Model\NewContact;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Modules\AutoResponder\Entities\AutoResponderCampaign;
use Modules\AutoResponder\Entities\AutoResponderCredential;
use Modules\AutoResponder\Entities\AutoResponders\GetResponse;
use Modules\AutoResponder\Entities\Campaign;

/**
 * Class GetResponseController
 * @package Modules\AutoResponder\Http\Controllers\Responders
 */
class GetResponseController extends ResponderControllerAbstract
{

    /**
     * @param Request $request
     * @param AutoResponderCampaign $autoResponderCampaign
     * @param AutoResponderCredential $autoResponderCredential
     * @return JsonResponse
     */
    public function store(
        Request $request,
        AutoResponderCampaign $autoResponderCampaign,
        AutoResponderCredential $autoResponderCredential
    )
    {
        $getCampaigns = new GetCampaigns();
        $client = GetresponseClientFactory::createWithApiKey($request->apikey);
        $response = $client->call($getCampaigns);

        if ($response->getResponse()->getStatusCode() == 200) {
            try {
                $this->storeAutoResponder($request, 'getResponse');

                $autoResponderCredential = $this->hydrateAutoResponderCredentials($autoResponderCredential, $request);
                $this->autoResponder->autoResponderCredentials()->save($autoResponderCredential);

                $campaignList = $this->hydrateAutoResponderCampaign($response->getData(), $autoResponderCampaign);
                $this->autoResponder->autoResponderCampaigns()->saveMany($campaignList);

                return Response::json(null, 200);
            } catch (\Exception $e) {
                return Response::json(null, 500);
            }
        } else {
            return Response::json('', $response->getResponse()->getStatusCode());
        }
    }

    /**
     * @param array $campaigns
     * @param AutoResponderCampaign $autoResponderCampaignTemplate
     * @return array
     */
    protected function hydrateAutoResponderCampaign
    (
        array $campaigns,
        AutoResponderCampaign $autoResponderCampaignTemplate
    )
    {
        $autoResponderCampaigns = [];
        foreach ($campaigns as $campaign) {
            $autoResponderCampaign = clone $autoResponderCampaignTemplate;

            $autoResponderCampaign->unique_id = $campaign['campaignId'];
            $autoResponderCampaign->name = $campaign['name'];
            $autoResponderCampaign->is_active = true;

            $autoResponderCampaigns[] = $autoResponderCampaign;
        }
        return $autoResponderCampaigns;
    }

    /**
     * @return GetResponse
     */
    public function getResponder()
    {
        $responder = new GetResponse();
        $responderCredentials = $this->autoResponder->autoResponderCredentials()->where('name', Config::get('constants.AUTO_RESPONDERS.GET_RESPONSE.CREDENTIALS.API_KEY'))->get()->first();

        $responder->setId($this->autoResponder->id);
        $responder->setName($this->autoResponder->name);
        $responder->setApiKey($responderCredentials->value);

        return $responder;
    }

    /**
     * @param $user
     * @param Campaign $campaign
     * @return bool
     */
    public function addUserToResponder($user, $campaign)
    {
        $responder = $this->getResponder();
        $client = GetresponseClientFactory::createWithApiKey($responder->getApiKey());

        $newContact = new NewContact(
            new CampaignReference(
                $campaign->autoResponderCampaign->unique_id
            ), $user->getEmail()
        );

        $newContact->setName($user->getName());
        $createContact = new CreateContact($newContact);

        $createContactResponse = $client->call($createContact);

        return $createContactResponse->isSuccess();
    }
}
