<?php

namespace Modules\AutoResponder\Http\Controllers\Responders;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Modules\AutoResponder\Entities\AutoResponder;
use Modules\AutoResponder\Entities\AutoResponderCampaign;
use Modules\AutoResponder\Entities\AutoResponderCredential;
use Modules\AutoResponder\Entities\AutoResponders\GetResponse;
use Modules\AutoResponder\Entities\Campaign;

/**
 * Interface ResponderControllerInterface
 * @package Modules\AutoResponder\Http\Controllers\Responders
 *
 */
abstract class ResponderControllerAbstract extends Controller
{
    /**
     * @var AutoResponder
     */
    protected $autoResponder;

    /**
     * GetResponseController constructor.
     * @param AutoResponder $autoResponder
     */
    public function __construct(AutoResponder $autoResponder)
    {
        $this->autoResponder = $autoResponder;
    }


    /**
     * @param Request $request
     * @param AutoResponderCampaign $autoResponderCampaign
     * @param AutoResponderCredential $autoResponderCredential
     * @return JsonResponse
     */
    public abstract function store(
        Request $request,
        AutoResponderCampaign $autoResponderCampaign,
        AutoResponderCredential $autoResponderCredential
    );

    protected function storeAutoResponder(Request $request, $responder)
    {
        $autoResponderCredentials = AutoResponderCredential::where('name', Config::get('constants.AUTO_RESPONDERS.GET_RESPONSE.CREDENTIALS.API_KEY'))->where('value', $request->apikey)->get()->first();

        if ($autoResponderCredentials) {
            return Response::json('', 402);
        }
        $this->autoResponder->responder = $responder;
        $this->autoResponder->user_id = session()->get('user')['user_id'];
        $this->autoResponder->name = $request->accname;
        $this->autoResponder->save();
    }

    /**
     * @param AutoResponderCredential $autoResponderCredential
     * @param Request $request
     * @return AutoResponderCredential
     */
    public function hydrateAutoResponderCredentials
    (
        AutoResponderCredential $autoResponderCredential,
        Request $request
    )
    {
        $autoResponderCredential->name = Config::get('constants.AUTO_RESPONDERS.GET_RESPONSE.CREDENTIALS.API_KEY');
        $autoResponderCredential->value = $request->apikey;

        return $autoResponderCredential;
    }

    /**
     * @param array $campaigns
     * @param AutoResponderCampaign $autoResponderCampaignTemplate
     * @return array
     */
    protected abstract function hydrateAutoResponderCampaign
    (
        array $campaigns,
        AutoResponderCampaign $autoResponderCampaignTemplate
    );

    /**
     * @return GetResponse
     */
    public abstract function getResponder();

    /**
     * @param $user
     * @param Campaign $campaign
     */
    public abstract function addUserToResponder($user, $campaign);
}
