<?php

namespace Modules\AutoResponder\Http\Controllers\Responders;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use MailchimpMarketing\ApiClient;
use Modules\AutoResponder\Entities\AutoResponderCampaign;
use Modules\AutoResponder\Entities\AutoResponderCredential;
use Modules\AutoResponder\Entities\AutoResponders\GetResponse;
use Modules\AutoResponder\Entities\AutoResponders\MailChimp;
use Modules\AutoResponder\Entities\Campaign;

/**
 * Class GetResponseController
 * @package Modules\AutoResponder\Http\Controllers\Responders
 */
class MailChimpController extends ResponderControllerAbstract
{

    /**
     * @param Request $request
     * @param AutoResponderCampaign $autoResponderCampaign
     * @param AutoResponderCredential $autoResponderCredential
     * @return JsonResponse
     */
    public function store(
        Request $request,
        AutoResponderCampaign $autoResponderCampaign,
        AutoResponderCredential $autoResponderCredential
    )
    {
        $mailChimp = $this->getMailChimpConnection($request->apikey);
        $response = $mailChimp->ping->get();

        if ($response->health_status == "Everything's Chimpy!") {
            try {
                $this->storeAutoResponder($request, 'mailChimp');

                $autoResponderCredential = $this->hydrateAutoResponderCredentials($autoResponderCredential, $request);
                $this->autoResponder->autoResponderCredentials()->save($autoResponderCredential);

                $campaignList = $this->hydrateAutoResponderCampaign($mailChimp->lists->getAllLists()->lists, $autoResponderCampaign);
                $this->autoResponder->autoResponderCampaigns()->saveMany($campaignList);

                return Response::json(null, 200);
            } catch (\Exception $e) {
                dd($e);
                return Response::json(null, 500);
            }
        } else {
            return Response::json('', $response->getResponse()->getStatusCode());
        }
    }


    /**
     * @param array $lists
     * @param AutoResponderCampaign $autoResponderCampaignTemplate
     * @return array
     */
    public function hydrateAutoResponderCampaign
    (
        array $lists,
        AutoResponderCampaign $autoResponderCampaignTemplate
    )
    {
        $autoResponderCampaigns = [];
        foreach ($lists as $list) {
            $autoResponderCampaign = clone $autoResponderCampaignTemplate;

            $autoResponderCampaign->unique_id = $list->id;
            $autoResponderCampaign->name = $list->name;
            $autoResponderCampaign->is_active = true;

            $autoResponderCampaigns[] = $autoResponderCampaign;
        }
        return $autoResponderCampaigns;
    }

    /**
     * @return GetResponse
     */
    public function getResponder()
    {
        $responder = new MailChimp();
        $responderCredentials = $this->autoResponder->autoResponderCredentials()->where('name', Config::get('constants.AUTO_RESPONDERS.GET_RESPONSE.CREDENTIALS.API_KEY'))->get()->first();

        $responder->setId($this->autoResponder->id);
        $responder->setName($this->autoResponder->name);
        $responder->setApiKey($responderCredentials->value);

        return $responder;
    }

    /**
     * @param $user
     * @param Campaign $campaign
     * @return mixed|string
     */
    public function addUserToResponder($user, $campaign)
    {
        $responder = $this->getResponder();
        $mailChimp = $this->getMailChimpConnection($responder->getApiKey());
        $body = [
            'email_address' => $user->getEmail(),
            'status' => 'subscribed',
            'name' => $user->getName()
        ];

        return $mailChimp->lists->addListMember($campaign->autoResponderCampaign->unique_id, $body);
    }

    /**
     * @param $apiKey
     * @return ApiClient
     */
    private function getMailChimpConnection($apiKey)
    {
        $mailChimp = new ApiClient();
        $config = explode('-', $apiKey);
        $mailChimp->setConfig([
            'apiKey' => $config[0],
            'server' => $config[1]
        ]);

        return $mailChimp;
    }
}
