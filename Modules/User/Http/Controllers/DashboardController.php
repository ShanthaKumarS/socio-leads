<?php

namespace Modules\User\Http\Controllers;

use app\Modules\User\Controllers\MsgBotController;
use App\Modules\User\Models\facebookpages;
use App\Modules\User\Models\FbAccountDetails;
use App\Modules\User\Models\PostSettings;
use App\Modules\User\Models\ServiceStatus;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Modules\Facebook\Repositories\FacebookPagesRepository;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param FacebookPagesRepository $facebookPagesRepository
     * @return void
     */
    public function index(Request $request,   FacebookPagesRepository $facebookPagesRepository)
    {

    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
