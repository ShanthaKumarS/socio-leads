<?php

namespace Modules\User\Http\Controllers\Authentication;

use App\Modules\User\Models\ActiveUser;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Modules\User\Entities\Authentication\UserEmailVerification;
use Modules\User\Entities\User;
use Modules\User\Repository\UserRepository;
use SendGrid\Mail\TypeException;


class EmailVerificationController extends Controller
{
    /**
     * @return UserEmailVerification
     */
    public function createEmailVerification()
    {
        $userEmailVerification = new UserEmailVerification();

        $userEmailVerification->token = Str::random(32);
        $userEmailVerification->sent_at = new \DateTime();

        return  $userEmailVerification;
    }

    /**
     * @param User $user
     * @return JsonResponse
     * @throws TypeException
     */
    public function sendUserEmailVerificationLink(User $user)
    {
        try {
            $subject = "Activate SocioLeads Account";
            $content = "Congratulation you Registered youself to SocioLeads Successfully,Please Click on bellow link to activate your Account";

            $resendLink = sprintf("%sverification-link/resend?userId=%s", config("app.url"), $user->id);

            $mailInfo = [
                'title' => "Active Your Account",
                'name' => $user->first_name,
                'content' => $content,
                "link" => sprintf("%verify?userId=%s&token=%s", config("app.url"), $user->id, $user->userEmailVerification->token),
                "buttonName" => "Activate Account"
            ];

            $template = view('user::mailTemplate', $mailInfo);

            $data = sendMail($subject, $template->render(), $user);

            $response = response()->json([
                "status" => 200,
                "content" => sprintf("Verification link sent to your email id <a href='%s'>Resend link again</a>",$resendLink),
                "header" => $data
            ]);

        } catch (Exception $e) {
            $response = response()->json([
                "code" => 511,
                "status" => "Cannot send mail: Network Authentication is required",
                "error" => $e->getMessage()
            ]);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @return JsonResponse
     * @throws TypeException
     * @author shantha kumar<shanthakumar@globussoft.in>
     */
    public function resendVerificationLink(Request $request, UserRepository $userRepository)
    {
        $user = $userRepository->getUserById($request->userId);

        $emailActivationExpiresAt = strtotime($user->userEmailVerification->sent_at) + 60*60;
        $currentTime = time();


            $user->userEmailVerification->token = Str::random(32);
            $user->userEmailVerification->sent_at =  new \DateTime();

            $user->userEmailVerification->save();


        $this->sendUserEmailVerificationLink($user);

        $resendLink = sprintf("%sverification-link/resend?userId=%s", config("app.url"), $user->id);
        $content = sprintf("Activation link sent to your email id <a href='%s'>Resend link again</a>",$resendLink);

        return redirect('/');
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Factory|View|RedirectResponse|Redirector
     */
    public function verifyUserEmail(Request $request, UserRepository $userRepository)
    {
        $userId = $request->userId;
        $emailActivationKey = $request->token;

        $user = $userRepository->getUserById($userId);

        $emailActivationExpiresAt = strtotime($user->userEmailVerification->sent_at) + 60*60;
        $currentTime = time();

        if ($user->userEmailVerification->token   == $emailActivationKey) {
            if ($currentTime <  $emailActivationExpiresAt) {
                $user->userEmailVerification->is_verified = Config::get('constants.VERIFIED');
                $user->userEmailVerification->verified_at = new \DateTime();
                $user->save();

                $response = response()->json([
                    "status" => 200,
                    "content" => "User activated successfully",
                ]);

                Session::put('user', $user);

                return redirect('/dashboard');
            }
            else {
                $resendLink = sprintf("%sverification-link/resend?userId=%s", config("app.url"), $userId);

                $response = [
                    "status" => 408,
                    "content" => sprintf("Activation link is expired: <a href='%s'>Resend link again</a>",$resendLink)
                ];

                return redirect("/");
            }
        } else {
            $response = [
                "status" => 400,
                "content" => "URL is not Valid",
            ];
            return redirect("/");
        }
    }




    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('user::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
