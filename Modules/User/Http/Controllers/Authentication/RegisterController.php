<?php

namespace Modules\User\Http\Controllers\Authentication;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Modules\User\Entities\User;
use Modules\User\Http\Requests\RegisterRequest;


class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('user::login');
    }

    /**
     * Store a newly created resource in storage.
     * @param RegisterRequest $request
     * @param User $user
     * @param EmailVerificationController $userEmailVerificationController
     * @return void
     * @throws \SendGrid\Mail\TypeException
     */
    public function store(
        RegisterRequest $request,
        User $user,
        EmailVerificationController $userEmailVerificationController
    )
    {
        try {
            $this->hydrateUser($user, $request);
            $userEmailVerification = $userEmailVerificationController->createEmailVerification();
            $user->save();
            $user->userEmailVerification()->save($userEmailVerification);

            $response = $userEmailVerificationController->sendUserEmailVerificationLink($user);
        } catch (Exception $e) {
            $response = response()->json([
                "status" => 502,
                "content" => "Bad Gateway - table name: users",
            ]);
        }

        return $response;
    }

    /**
     * @param $user
     * @param $request
     */
    private function hydrateUser(&$user, $request) {
        $user->first_name = $request->fname;
        $user->last_name = $request->lname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone_number = $request->phno;
        $user->city = $request->city;
        $user->country = $request->country;
        $user->role_id = Config::get('constants.USER_ROLES.USER');
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
