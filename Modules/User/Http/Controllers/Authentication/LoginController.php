<?php

namespace Modules\User\Http\Controllers\Authentication;

use App\Modules\User\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Modules\User\Http\Requests\LoginRequest;
use Modules\User\Repository\UserRepository;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('user::login');
    }

    /**
     * @param LoginRequest $request
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function authenticate(LoginRequest $request, UserRepository $userRepository)
    {
        $user = $userRepository->getUserByEmail($request->email);

        if (
            $user != null &&
            $this->verifyUserWithPassword($user, $request->password)
        ) {
            $resendLink = sprintf("%sverification-link/resend?userId=%s", config("app.url"), $user->id);
            if ($user->userEmailVerification->is_verified) {
                $request->session()->put('user', $user);

                $response = response()->json([
                    "status" => 200,
                    "content" => "Logged in",
                ]);
            } else {
                $response = response()->json([
                    "status" => 404,
                    "content" => sprintf("Verify you email address <a href='%s'>Resend link again</a>",$resendLink),
                ]);
            }

        } else {
            $response = response()->json([
                "status" => 403,
                "content" => "Email or password wrong",
            ]);
        }

        return $response;
    }

    /**
     * @param User $user
     * @param String $password
     * @return bool
     */
    private function verifyUserWithPassword($user, $password)
    {
        return Hash::check($password, $user->password);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
