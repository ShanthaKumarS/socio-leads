<?php
namespace Modules\User\Repository;

use Modules\User\Entities\Role;
use Modules\User\Entities\User;

class UserRepository
{
    /**
     * @param $email
     * @return User
     */
    public function getUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * @param $userId
     * @return User
     */
    public function getUserById($userId)
    {
        return User::find($userId)->first();
    }

    public function getGetUserRoleById($id)
    {
        return Role::find($id)->first();
    }

    public function getUserEmailVerificationByUserId($user_id)
    {
        return User::find($user_id)->userEmailVerification;
    }
}
