<?php


namespace Modules\User\Entities;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Facebook\Entities\Facebook;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\UserCredential;
use Modules\User\Entities\Authentication\UserEmailVerification;
use Modules\User\Entities\Authentication\UserResestPassword;

class User extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'picture',
        'city',
        'country'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the phone associated with the user.
     */
    public function userEmailVerification()
    {
        return $this->hasOne(UserEmailVerification::class);
    }

    /**
     * Get the phone associated with the user.
     */
    public function userResetPassword()
    {
        return $this->hasOne(UserResestPassword::class);
    }

    public function facebookCredential()
    {
        return $this->hasOne(UserCredential::class);
    }

    public function pages()
    {
        return $this->hasMany(Page::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    protected static function newFactory()
    {
        return \Modules\User\Database\factories\UserCredentialFactory::new();
    }
}
