<!DOCTYPE html>
<html>
<head>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Globussoft">
  <title>Fan Pages</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/images/leads.png">

  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.css"  media="screen,projection"/>

  <link rel="stylesheet" type="text/css" href="assets/plugins/font-awesome/css/font-awesome.css">

</head>

<body>

 <main>
    <nav class="grey">
      <div class="nav-wrapper">
        <a href="#!" class="brand-logo center">
          <img src="assets/images/small-logo.png" alt="FanPages">
        </a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      </div>
    </nav>
    <div class="container center">
      <a href="../../../../resources/views/dashboard.blade.php">Go to Dashboard</a>
    </div>
  </main>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-58515856-27', 'auto');
    ga('send', 'pageview');

  </script>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="assets/plugins/jquery-3.0.0.min.js"></script>
  <script type="text/javascript" src="assets/plugins/materialize/js/materialize.min.js"></script>
</body>
</html>
