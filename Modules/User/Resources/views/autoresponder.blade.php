@extends('User::resources.views.layouts.master')
@section('title','SocioLeads | Auto Responders')
@section('title1',isset($pageDetails->pagename)?'Broadcast Messages:-'.$pageDetails->pagename:'Broadcast Messages:-')
@section('style')

    <!--begin::Layout Themes(used by all pages)-->

    <link href="{{asset('assets/css/themes/layout/header/base/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/themes/layout/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/themes/layout/brand/dark.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/themes/layout/aside/dark.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->
    <link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.png')}}" />






@endsection








@section('content')


    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                    Connected Autoresponders </h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('user::resources.views.layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->













    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">









                <!--begin::Content-->
                <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">

                    <!--end::Subheader-->

                    <!--begin::Entry-->
                    <div class="d-flex flex-column-fluid">
                        <!--begin::Container-->
                        <div class="container">
                            <!--begin::Accordion-->
                            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="newAutoresponder">
                                <div class="card mb-10">
                                    <div class="card-header">
                                        <div class="card-title" data-toggle="collapse" data-target="#Autoresponder">
                                            <div class="card-label text-dark">Connect New Autoresponder : <span class="text-primary">Select Your Autoresponder</span></div>
                                            <span class="svg-icon"><svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span> </div>
                                    </div>
                                    <div id="Autoresponder" class="collapse" data-parent="#newAutoresponder">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-primary btn-sm btn-block mb-2" data-toggle="modal" data-target="#GetResponse">GetResponse</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-info btn-sm btn-block mb-2" data-toggle="modal" data-target="#Mailchimp">Mailchimp</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-warning btn-sm btn-block mb-2" data-toggle="modal" data-target="#iContact">iContact</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-success btn-sm btn-block mb-2" data-toggle="modal" data-target="#Hubspot">Hubspot</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-success btn-sm btn-block mb-2" data-toggle="modal" data-target="#Madmimi">Madmimi</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-primary btn-sm btn-block mb-2" data-toggle="modal" data-target="#Constant">Constant Contact</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-info btn-sm btn-block mb-2" data-toggle="modal" data-target="#Campaign">Campaign Monitor</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-warning btn-sm btn-block mb-2" data-toggle="modal" data-target="#Drip">Drip</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-warning btn-sm btn-block mb-2" data-toggle="modal" data-target="#Aweber">Aweber</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-success btn-sm btn-block mb-2" data-toggle="modal" data-target="#FlutterMail">FlutterMail</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-primary btn-sm btn-block mb-2" data-toggle="modal" data-target="#Sendlane">Sendlane</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-info btn-sm btn-block mb-2" data-toggle="modal" data-target="#GotoWebinar">GotoWebinar</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-info btn-sm btn-block mb-2" data-toggle="modal" data-target="#Sendgrid">Sendgrid</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-warning btn-sm btn-block mb-2" data-toggle="modal" data-target="#WebinarJam">WebinarJam</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-success btn-sm btn-block mb-2" data-toggle="modal" data-target="#EverWebinar">EverWebinar</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" class="btn btn-primary btn-sm btn-block mb-2" data-toggle="modal" data-target="#Ontraport">Ontraport</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Accordion-->




                        <?php $flag_first=true; ?>

                        @if(isset($emptyCheck) && !empty($emptyCheck))
                            <!--begin::Card-->
                            <div class="card card-custom gutter-b">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">
                                            Connected Autoresponders
                                        </h3>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin::Accordion-->
                                    <div class="accordion accordion-solid accordion-toggle-plus" id="ConnectedAutoresponders">


                                        {{--Getresponse--}}
                                        @if($getresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}}" data-toggle="collapse" data-target="#GetresponseCollapse">
                                                        Getresponse
                                                    </div>
                                                </div>
                                                <div id="GetresponseCollapse" class="collapse  {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/getresponse_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Getresponse</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'getresponse'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($getresult as $get)
                                                        <p class="text-dark m-0 pt-5 font-weight-normal">
                                                            Account Name : <span class="text-primary">{{$get->account_name}}</span>
                                                            <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$get->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse">Disconnect</button>
                                                        </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $flag_first=false; ?>
                                        @endif







                                        {{--Mailchimp--}}
                                        @if($mailresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#MailchimpCollapse">
                                                        Mailchimp
                                                    </div>
                                                </div>
                                                <div id="MailchimpCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/mailchimp_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Mailchimp</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'mailchimp'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($mailresult as $mail)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$mail->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$mail->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $flag_first=false; ?>
                                        @endif





                                        {{--iContact--}}
                                        @if($iconresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#iContactCollapse">
                                                        iContact
                                                    </div>
                                                </div>
                                                <div id="iContactCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/icontact_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate iContact</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'icontact'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($iconresult as $icon)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$icon->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$icon->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif



                                        {{--Hubspot--}}
                                        @if($hubresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#HubspotCollapse">
                                                        Hubspot
                                                    </div>
                                                </div>
                                                <div id="HubspotCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/hubspot_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Hubspot</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'hubspot'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($hubresult as $hub)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$hub->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$hub->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif



                                        {{--Constant Contact--}}
                                        @if($hubresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#ConstantContactCollapse">
                                                        Constant Contact
                                                    </div>
                                                </div>
                                                <div id="ConstantContactCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/ctct_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Constant Contact</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'constantcontact'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @if($ctctresult)
                                                            @foreach($ctctresult as $ctct)
                                                                <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                    Account Name : <span class="text-primary">{{$ctct->account_name}}</span>
                                                                    <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$ctct->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                                </p>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif

                                        {{--Madmimi--}}
                                        @if($mimiresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#MadmimiCollapse">
                                                        Madmimi
                                                    </div>
                                                </div>
                                                <div id="MadmimiCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/mimi_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Madmimi</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'madmimi'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($mimiresult as $mimi)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$mimi->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$mimi->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif

                                        {{--Sendy--}}
                                        @if($sendresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#SendyCollapse">
                                                        Sendy
                                                    </div>
                                                </div>
                                                <div id="SendyCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/sendy_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Sendy</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'sendy'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($sendresult as $send)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$send->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$send->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif



                                        {{--Campaign Monitor--}}
                                        @if($camoresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#CampaignMonitorCollapse">
                                                        Campaign Monitor
                                                    </div>
                                                </div>
                                                <div id="CampaignMonitorCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/campaignmonitor_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Campaign Monitor</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'campaignmonitor'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($camoresult as $camo)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$camo->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$camo->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif





                                        {{--Drip--}}
                                        @if($dripresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#DripCollapse">
                                                        Drip
                                                    </div>
                                                </div>
                                                <div id="DripCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/drip_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Drip</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'drip'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($dripresult as $drip)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$drip->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$drip->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif




                                        {{--Aweber--}}
                                        @if($aweberresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#AweberCollapse">
                                                        Aweber
                                                    </div>
                                                </div>
                                                <div id="AweberCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/aweber_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Aweber</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'aweber'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($aweberresult as $aweber)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$aweber->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$aweber->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif





                                        {{--FlutterMail--}}
                                        @if($flutterresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#FlutterMailCollapse">
                                                        FlutterMail
                                                    </div>
                                                </div>
                                                <div id="FlutterMailCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/fluttermail_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate FlutterMail</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'fluttermail'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($flutterresult as $flutter)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$flutter->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$flutter->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif

                                        {{--Gvo--}}
                                        @if($gvoresult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#GvoCollapse">
                                                        Gvo
                                                    </div>
                                                </div>
                                                <div id="GvoCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/gvo_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Gvo</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'gvo'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($gvoresult as $gvo)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$gvo->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$gvo->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif


                                        {{--Sendlane--}}
                                        @if($sendlResult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#SendlaneCollapse">
                                                        Sendlane
                                                    </div>
                                                </div>
                                                <div id="SendlaneCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/sendlane_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Sendlane</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'sendlane'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($sendlResult as $sendlane)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$sendlane->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$sendlane->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif



                                        {{--Gotowebinar--}}
                                        @if($gotoResult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#GotowebinarCollapse">
                                                        Gotowebinar
                                                    </div>
                                                </div>
                                                <div id="GotowebinarCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/gotowebinar_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Gotowebinar</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'gotowebinar'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($gotoResult as $gotowebinar)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$gotowebinar->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$gotowebinar->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif




                                        {{--SendGrid--}}
                                        @if($sendgridResult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#SendGridCollapse">
                                                        SendGrid
                                                    </div>
                                                </div>
                                                <div id="SendGridCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/sendgrid_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate SendGrid</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'sendgrid'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($sendgridResult as $sendgrid)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$sendgridResult->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$sendgridResult->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif



                                        {{--WebinarJAm--}}
                                        @if($webinarjamResult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#WebinarJAmCollapse">
                                                        SendGrid
                                                    </div>
                                                </div>
                                                <div id="WebinarJAmCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/webinarjam_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate WebinarJAm</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'webinarjam'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($webinarjamResult as $jam)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$jam->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$jam->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif



                                        {{--Everwebinar--}}
                                        @if($everwebinarResult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#EverwebinarCollapse">
                                                        Everwebinar
                                                    </div>
                                                </div>
                                                <div id="EverwebinarCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/everwebinar_logo.png" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Everwebinar</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'everwebinar'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($everwebinarResult as $ever)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$ever->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$ever->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif






                                        {{--Ontraport--}}
                                        @if($ontraportResult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#OntraportCollapse">
                                                        Ontraport
                                                    </div>
                                                </div>
                                                <div id="OntraportCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/ontraport_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate Everwebinar</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'ontraport'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($ontraportResult as $ontra)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$ontra->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$ontra->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif






                                        {{--CustomHtml--}}
                                        @if($customhtmlResult)
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title {{$flag_first ? '': 'collapsed'}} " data-toggle="collapse" data-target="#CustomHtmlCollapse">
                                                        CustomHtml
                                                    </div>
                                                </div>
                                                <div id="CustomHtmlCollapse" class="collapse {{$flag_first ? 'show': ''}}" data-parent="#ConnectedAutoresponders">
                                                    <div class="card-body">
                                                        <div class="d-flex align-items-center">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-45 symbol-light mr-5">
                                                            <span class="symbol-label">
                                                                <img src="/assets/images/autoresponders/gvo_logo.jpg" class="h-50 align-self-center" alt="">
                                                            </span>
                                                            </div>
                                                            <!--end::Symbol-->

                                                            <!--begin::Text-->
                                                            <div class="d-flex flex-column flex-grow-1">
                                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"></a>
                                                                <span class="text-muted font-weight-bold">Integrate CustomHtml</span>
                                                            </div>
                                                            <!--end::Text-->
                                                            <button type="button"   onclick="DeleteAutoresponders('/user/delete-autoresponder/{{'customhtml'}}');"  class="btn btn-danger btn-sm float-right kt_sweetalert_collapse" ><i class="far fa-times-circle mr-2"></i>Disconnect All</button>
                                                        </div>
                                                        @foreach($customhtmlResult as $ch)
                                                            <p class="text-dark m-0 pt-5 font-weight-normal">
                                                                Account Name : <span class="text-primary">{{$ch->account_name}}</span>
                                                                <button type="button"  onclick="DeleteAutoresponders('/user/disconnect-autoresponder/{{$ch->autoresponders_id}}');"  class="btn btn-outline-danger btn-sm kt_sweetalert_collapse" >Disconnect</button>
                                                            </p>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $flag_first=false; ?>
                                    @endif








                                    <!--end::Accordion-->
                                </div>
                            </div>
                            <!--end::Card-->

                            @endif


                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Entry-->
                </div>
                <!--end::Content-->

        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->


    <!--begin::Modal GetResponse -->
    <div class="modal fade" id="GetResponse" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_GetResponse" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="accname" name="accname" type="text"  placeholder="Enter Your Account Name" />
                                    <span id="spnError1" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="apikey" name="apikey"  type="text" placeholder="Enter API Key" />
                                    <span id="spnError2" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError3" style="color: Red; display: none">*Invalid Api key.</span>
                        <span id="spnError4" style="color: Red; display: none">*This Getresponse account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button"  data-type="GetResponse" class="btn btn-primary mr-2" id="connectGetresponse">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Mailchimp -->
    <div class="modal fade" id="Mailchimp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Mailchimp" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamem" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError5" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeym" type="text" name="apikey"  placeholder="Enter API Key" />
                                    <span id="spnError6" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError7" style="color: Red; display: none">*Invalid Api Key.</span>
                        <span id="spnError8" style="color: Red; display: none">*This Mailchimp account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button id="connectMailchimp" type="button"  data-type="Mailchimp" class="btn btn-primary mr-2">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal iContact -->
    <div class="modal fade" id="iContact" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i></button>
                </div>
                <form class="form" method="post">
                    <input id="token_iContact" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamei" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError9" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeyi" type="text" name="apikey"  placeholder="Enter API Key" />
                                    <span id="spnError10" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Password</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apipasswordi" name="apipassword" type="password" placeholder="Enter API Password" />
                                    <span id="spnError11" style="color: Red; display: none">*Please Enter Api Password.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Username</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apiusernamei" name="apiusername" type="text" placeholder="Enter API Username" />
                                    <span id="spnError12" style="color: Red; display: none">*Please Enter Api Usernme.</span>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <span id="spnError13" style="color: Red; display: none">*Invalid Credentials.</span>
                            <span id="spnError14" style="color: Red; display: none">*This Icontact account already integrated.</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="iContact" class="btn btn-primary mr-2" id="connectIcontact">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Hubspot -->
    <div class="modal fade" id="Hubspot" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form">
                    <input id="token_Hubspot" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnameh" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError15" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeyh" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError16" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <span id="spnError17" style="color: Red; display: none">*Invalid Api key.</span>
                        <span id="spnError18" style="color: Red; display: none">*This Hubspot account already integrated.</span>
                    </div>

                    <div class="modal-footer">
                        <button type="button" data-type="Hubspot" class="btn btn-primary mr-2" id="connectHubspot">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Madmimi -->
    <div class="modal fade" id="Madmimi" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Madmimi" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamemm" type="text" name="accname_Madmimi" placeholder="Enter Your Account Name" />
                                    <span id="spnError19" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input  class="form-control" id="apikeymm" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError20" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter Your Email</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input  id="emailmm" name="email" class="form-control" type="email" placeholder="Enter Your Email" />
                                    <span id="spnError21" style="color: Red; display: none">*Please Enter Email.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError22" style="color: Red; display: none">*Invalid Credentials.</span>
                        <span id="spnError23" style="color: Red; display: none">*This Madmimi account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="connectMadmimi" data-type="Madmimi" class="btn btn-primary mr-2">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Constant Contact -->
    <div class="modal fade" id="Constant" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Constant" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamecc" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError24" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeycc" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError25" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Access Token</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apiacctokencc" name="apiacctoken" type="text" placeholder="Enter API Access Token" />
                                    <span id="spnError26" style="color: Red; display: none">*Please Enter Api Access Token.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="center">
                        <span id="spnError27" style="color: Red; display: none">*Invalid Credentials.</span>
                        <span id="spnError28" style="color: Red; display: none">*This Constant Contact account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="Constant" class="btn btn-primary mr-2" id="connectConstantcontact">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Campaign Monitor -->
    <div class="modal fade" id="Campaign" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Campaign" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamecm" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError29" style="color: #ff0000; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="apikeycm" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError30" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter Client ID</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="clientidcm" name="clientid" type="text" placeholder="Enter Client ID" />
                                    <span id="spnError31" style="color: Red; display: none">*Please Enter Client Id.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError32" style="color: Red; display: none">*Invalid Credentials.</span>
                        <span id="spnError33" style="color: Red; display: none">*This Campaign Monitor account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="Campaign" class="btn btn-primary mr-2" id="connectCampaignmonitor">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Drip -->
    <div class="modal fade" id="Drip" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Drip" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="accnamed" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError34" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeyd" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError35" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter Account ID</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="accountidd" type="text" name="accountid"  placeholder="Enter Account ID" />
                                    <span id="spnError135" style="color: Red; display: none">*Please Enter An Account Id.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError37" style="color: Red; display: none">*Invalid Credntials.</span>
                        <span id="spnError38" style="color: Red; display: none">*This Drip account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="Drip" class="btn btn-primary mr-2" id="connectDrip">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Aweber -->
    <div class="modal fade" id="Aweber" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form method="post" class="form">
                    <input id="token_Aweber" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">To get the Authorized Code</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <a type="button" class="btn btn-primary" href="/user/connect-to-aweber" target="_blank">Click Here</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnameaw" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError39" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter Your Authorized Code</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accesstokenaw" type="text" name="accesstoken" placeholder="Enter Authorized Code" />
                                    <span id="spnError40" style="color: Red; display: none">*Please Enter Authorized Code.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError41" style="color: Red; display: none">*Invalid Accesstoken.</span>
                        <span id="spnError42" style="color: Red; display: none">*This Aweber account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="connectAweber" data-type="Aweber" class="btn btn-primary mr-2">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal FlutterMail -->
    <div class="modal fade" id="FlutterMail" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_FlutterMail" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamef" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError43" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeyf" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError44" style="color: Red; display: none">*Please Enter Api Key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter your API URL</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apiurlf" name="apiurl" type="text" placeholder="Enter API URL" />
                                    <span id="spnError45" style="color: Red; display: none">*Please Enter Api Url.</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <span id="spnError46" style="color: Red; display: none">*Invalid Credentials.</span>
                        <span id="spnError47" style="color: Red; display: none">*This Fluttermail account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="FlutterMail" class="btn btn-primary mr-2" id="connectFluttermail">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Sendlane -->
    <div class="modal fade" id="Sendlane" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Sendlane" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamese" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError51" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Subdomain URL.</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="domainNamese" type="text" name="domainName" placeholder=" Ex:yourdomainname.sendlane.com" />
                                    <span id="spnError52" style="color: Red; display: none">*Please Enter Domain Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter your API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apiKeyse" type="text" name="apiKey" placeholder="Enter API Key" />
                                    <span id="spnError53" style="color: Red; display: none">*Please Enter Api Key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter your Login Email</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="emailse" type="email" name="email" placeholder="Enter Login Email" />
                                    <span id="spnError54" style="color: Red; display: none">*Please Enter Login Email.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError55" style="color: Red; display: none">*Invalid Credentials.</span>
                        <span id="spnError56" style="color: Red; display: none">*This Sendlane account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="Sendlane" class="btn btn-primary mr-2" id="connectSendlane">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal GotoWebinar -->
    <div class="modal fade" id="GotoWebinar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_GotoWebinar" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamego" type="text" name="accname"  placeholder="Enter Your Account Name" />
                                    <span id="spnError57" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter your API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="apikeygo" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError58" style="color: Red; display: none">*Please Enter Api Key.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter your Email</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="emailgo" name="email" type="email" placeholder="Enter Email" />
                                    <span id="spnError59" style="color: Red; display: none">*Please Enter Email.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter your Password</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="passwordgo" name="password" type="password" placeholder="Enter your Password" />
                                    <span id="spnError60" style="color: Red; display: none">*Please Enter Password.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError61" style="color: Red; display: none">*Webinars are not present in your account.</span>
                        <span id="spnError62" style="color: Red; display: none">*This Gotowebinar account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="GotoWebinar" class="btn btn-primary mr-2" id="connectGotowebinar">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Sendgrid -->
    <div class="modal fade" id="Sendgrid" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Sendgrid" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamesg" type="text" name="accnam" placeholder="Enter Your Account Name" />
                                    <span id="spnError63" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeysg" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError64" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError65" style="color: Red; display: none">*Invalid Api Key.</span>
                        <span id="spnError66" style="color: Red; display: none">*This SendGrid account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button id="connectSendgrid"  type="button" data-type="Sendgrid" class="btn btn-primary mr-2">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal WebinarJam -->
    <div class="modal fade" id="WebinarJam" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_WebinarJam" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="accnamejam" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError67" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeyjam" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError68" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError69" style="color: Red; display: none">*Invalid Api key.</span>
                        <span id="spnError70" style="color: Red; display: none">*This WebinarJam account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="WebinarJam" class="btn btn-primary mr-2" id="connectWebinarjam">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal EverWebinar -->
    <div class="modal fade" id="EverWebinar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_EverWebinar" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="accnameever" type="text" name="accname"  placeholder="Enter Your Account Name" />
                                    <span id="spnError71" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter API Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="apikeyever" type="text" name="apikey" placeholder="Enter API Key" />
                                    <span id="spnError72" style="color: Red; display: none">*Please Enter Api key.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError73" style="color: Red; display: none">*Invalid Api key.</span>
                        <span id="spnError74" style="color: Red; display: none">*This EverWebinar account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="EverWebinar" class="btn btn-primary mr-2" id="connectEverwebinar">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Ontraport -->
    <div class="modal fade" id="Ontraport" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Enter Autoresponder Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="post">
                    <input id="token_Ontraport" type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="text-right col-lg-3 col-sm-12">Enter Your Account Name(for reference)</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="accnameon" type="text" name="accname" placeholder="Enter Your Account Name" />
                                    <span id="spnError75" style="color: Red; display: none">*Please Enter Account Name.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter App ID</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="appidon" type="text" name="appid" placeholder="Enter API ID" />
                                    <span id="spnError76" style="color: Red; display: none">*Please Enter App Id.</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Enter your App Key</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control"  id="appkeyon" name="appkey" type="text" placeholder="Enter API Key" />
                                    <span id="spnError77" style="color: Red; display: none">*Please Enter App Key .</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <span id="spnError78" style="color: Red; display: none">*Invalid Credentials.</span>
                        <span id="spnError79" style="color: Red; display: none">*This Ontraport account already integrated.</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-type="Ontraport" class="btn btn-primary mr-2" id="connectOntraport">Connect</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
















    @include('User::resources.views.layouts.page-footer')


@endsection









@section('script_function')
    <script src="{{asset('assets/js/pages/features/miscellaneous/sweetalert2.js')}}"></script>


    <script>
        // $(function () {
        //     $('.campaigns_scroll').slimScroll({
        //         color: '#2b2b2b',
        //         size: '10px',
        //         height: '224px',
        //         alwaysVisible: true,
        //         allowPageScroll: true
        //     });
        // });

        $(document).ready(function () {

//      Getresponse
            $(document.body).on('click', '#connectGetresponse', function (e) {

                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $(`#accname`).val();
                var apiKey = $(`#apikey`).val();

                console.log(token,accName, apiKey );

                $("#spnError1").css("display", "none");
                $("#spnError2").css("display", "none");
                $("#spnError3").css("display", "none");
                $("#spnError4").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError1").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError2").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/autoresponder/getresponse',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError3").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError4").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("GetResponse");
                        }
                    }
                });
            });

//      MAilchimp
            $(document.body).on('click', '#connectMailchimp', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#toke_${type_modal}`).val();
                var accName = $('#accnamem').val();
                var apiKey = $('#apikeym').val();
                $("#spnError5").css("display", "none");
                $("#spnError6").css("display", "none");
                $("#spnError7").css("display", "none");
                $("#spnError8").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError5").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError6").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/auto-responder/mail-chimp',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError7").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError8").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("MailChimp");
                        }
                    }
                });
            });

//      Icontact
            $(document.body).on('click', '#connectIcontact', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamei').val();
                var apiKey = $('#apikeyi').val();
                var apipassword = $('#apipasswordi').val();
                var apiusername = $('#apiusernamei').val();
                $("#spnError9").css("display", "none");
                $("#spnError10").css("display", "none");
                $("#spnError11").css("display", "none");
                $("#spnError12").css("display", "none");
                $("#spnError13").css("display", "none");
                $("#spnError14").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError9").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError10").css("display", "block");
                    return false;
                }
                if (apipassword == null || apipassword == "") {
                    $("#spnError11").css("display", "block");
                    return false;
                }
                if (apiusername == null || apiusername == "") {
                    $("#spnError12").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-icontact',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey,
                        apipassword: apipassword,
                        apiusername: apiusername,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError13").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError14").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("iContact");
                        }
                    }
                });
            });

//      Hubspot
            $(document.body).on('click', '#connectHubspot', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnameh').val();
                var apiKey = $('#apikeyh').val();
                $("#spnError15").css("display", "none");
                $("#spnError16").css("display", "none");
                $("#spnError17").css("display", "none");
                $("#spnError18").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError15").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError16").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-hubspot',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError17").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError18").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Hubspot");
                        }
                    }
                });
            });

//      Madmimi
            $(document.body).on('click', '#connectMadmimi', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#tokenmm_${type_modal}`).val();
                var accName = $(`#accnamemm`).val();
                var apiKey = $(`#apikeymm`).val();
                var email = $(`#emailmm`).val();
                $("#spnError19").css("display", "none");
                $("#spnError20").css("display", "none");
                $("#spnError21").css("display", "none");
                $("#spnError22").css("display", "none");
                $("#spnError23").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError19").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError20").css("display", "block");
                    return false;
                }
                if (email == null || email == "") {
                    $("#spnError21").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-madmimi',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey,
                        email: email,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError22").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError23").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Madmimi");
                        }
                    }
                });
            });

//      ConstantContact
            $(document.body).on('click', '#connectConstantcontact', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamecc').val();
                var apiKey = $('#apikeycc').val();
                var apiacctoken = $('#apiacctokencc').val();
                $("#spnError24").css("display", "none");
                $("#spnError25").css("display", "none");
                $("#spnError26").css("display", "none");
                $("#spnError27").css("display", "none");
                $("#spnError28").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError24").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError25").css("display", "block");
                    return false;
                }
                if (apiacctoken == null || apiacctoken == "") {
                    $("#spnError26").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-constantcontact',
                    data: {
                        _token: token,
                        accname: accName,
                        apikey: apiKey,
                        apiacctoken: apiacctoken,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError27").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError28").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("ConstantContact");
                        }
                    }
                });
            });

//      CampaignMonitor
            $(document.body).on('click', '#connectCampaignmonitor', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamecm').val();
                var apiKey = $('#apikeycm').val();
                var clientid = $('#clientidcm').val();
                $("#spnError29").css("display", "none");
                $("#spnError30").css("display", "none");
                $("#spnError31").css("display", "none");
                $("#spnError32").css("display", "none");
                $("#spnError33").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError29").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError30").css("display", "block");
                    return false;
                }
                if (clientid == null || clientid == "") {
                    $("#spnError31").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-campaignmonitor',
                    data: {
                        _token: token,
                        accname: accName,
                        apikey: apiKey,
                        clientid: clientid,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError32").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError33").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Campaign Monitor");
                        }
                    }
                });
            });

//      Drip
            $(document.body).on('click', '#connectDrip', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamed').val();
                var apiKey = $('#apikeyd').val();
                var accountId = $('#accountidd').val();
                $("#spnError34").css("display", "none");
                $("#spnError35").css("display", "none");
                $("#spnError37").css("display", "none");
                $("#spnError38").css("display", "none");
                $("#spnError135").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError34").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError35").css("display", "block");
                    return false;
                }
                if (accountId == null || accountId == "") {
                    $("#spnError135").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-drip',
                    data: {
                        _token: token,
                        accname: accName,
                        apikey: apiKey,
                        accountid: accountId,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError37").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError38").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Drip");
                        }
                    }
                });
            });

//      Aweber
            $(document.body).on('click', '#connectAweber', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnameaw').val();
                var accesstoken = $('#accesstokenaw').val();
                $("#spnError39").css("display", "none");
                $("#spnError40").css("display", "none");
                $("#spnError41").css("display", "none");
                $("#spnError42").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError39").css("display", "block");
                    return false;
                }
                if (accesstoken == null || accesstoken == "") {
                    $("#spnError40").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-aweber',
                    data: {
                        _token: token,
                        accname: accName,
                        accesstoken: accesstoken,
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError41").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError42").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Aweber");
                        }
                    }
                });
            });

//      FlutterMail
            $(document.body).on('click', '#connectFluttermail', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamef').val();
                var apikey = $('#apikeyf').val();
                var apiurl = $('#apiurlf').val();
                $("#spnError43").css("display", "none");
                $("#spnError44").css("display", "none");
                $("#spnError45").css("display", "none");
                $("#spnError46").css("display", "none");
                $("#spnError47").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError43").css("display", "block");
                    return false;
                }
                if (apikey == null || apikey == "") {
                    $("#spnError44").css("display", "block");
                    return false;
                }
                if (apiurl == null || apiurl == "") {
                    $("#spnError45").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-fluttermail',
                    data: {
                        _token: token,
                        accname: accName,
                        apikey: apikey,
                        apiurl: apiurl
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError46").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError47").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("FlutterMail");
                        }
                    }
                });
            });

//      Gvo
            $(document.body).on('click', '#connectGvo', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $('#tokengvo').val();
                var accName = $('#accnamegvo').val();
                var customHtml = $('#customHtmlgvo').val();
                $("#spnError48").css("display", "none");
                $("#spnError49").css("display", "none");
                $("#spnError50").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError48").css("display", "block");
                    return false;
                }
                if (customHtml == null || customHtml == "") {
                    $("#spnError49").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-gvo',
                    data: {
                        _token: token,
                        accname: accName,
                        customHtml: customHtml
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 402) {
                            $("#spnError50").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("GVO");
                        }
                    }
                });
            });

//     Sendlane
            $(document.body).on('click', '#connectSendlane', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamese').val();
                var apiKey = $('#apiKeyse').val();
                var domainName = $('#domainNamese').val();
                var email = $('#emailse').val();
                $("#spnError51").css("display", "none");
                $("#spnError52").css("display", "none");
                $("#spnError53").css("display", "none");
                $("#spnError54").css("display", "none");
                $("#spnError55").css("display", "none");
                $("#spnError56").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError51").css("display", "block");
                    return false;
                }
                if (domainName == null || domainName == "") {
                    $("#spnError52").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError53").css("display", "block");
                    return false;
                }
                if (email == null || email == "") {
                    $("#spnError54").css("display", "block");
                    return false;
                }

                $.ajax({
                    url: '/user/connect-to-sendlane',
                    data: {
                        _token: token,
                        accname: accName,
                        apiKey: apiKey,
                        domainName: domainName,
                        email: email
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 402) {
                            $("#spnError56").css("display", "block");
                        }
                        else if (mailresponse['status'] == 401) {
                            $("#spnError55").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("SendLane");
                        }
                    }
                });
            });

//     Gotowebinar
            $(document.body).on('click', '#connectGotowebinar', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamego').val();
                var apiKey = $('#apikeygo').val();
                var email = $('#emailgo').val();
                var password = $('#passwordgo').val();
                $("#spnError57").css("display", "none");
                $("#spnError58").css("display", "none");
                $("#spnError59").css("display", "none");
                $("#spnError60").css("display", "none");
                $("#spnError61").css("display", "none");
                $("#spnError62").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError57").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError58").css("display", "block");
                    return false;
                }
                if (email == null || email == "") {
                    $("#spnError59").css("display", "block");
                    return false;
                }
                if (password == null || password == "") {
                    $("#spnError60").css("display", "block");
                    return false;
                }

                $.ajax({
                    url: '/user/connect-to-gotowebinar',
                    data: {
                        _token: token,
                        accname: accName,
                        apikey: apiKey,
                        email: email,
                        password: password
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 402) {
                            $("#spnError62").css("display", "block");
                        }
                        else if (mailresponse['status'] == 401) {
                            $("#spnError61").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Gotowebinar");
                        }
                    }
                });
            });

//      Sendgrid
            $(document.body).on('click', '#connectSendgrid', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnamesg').val();
                var apiKey = $('#apikeysg').val();
                $("#spnError63").css("display", "none");
                $("#spnError64").css("display", "none");
                $("#spnError65").css("display", "none");
                $("#spnError66").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError63").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError64").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-sendgrid',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError65").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError66").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Sendgrid");
                        }
                    }
                });
            });

//      WebinarJam
            $(document.body).on('click', '#connectWebinarjam', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#tokenjam_${type_modal}`).val();
                var accName = $('#accnamejam').val();
                var apiKey = $('#apikeyjam').val();
                $("#spnError67").css("display", "none");
                $("#spnError68").css("display", "none");
                $("#spnError69").css("display", "none");
                $("#spnError70").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError67").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError68").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-webinarjam',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError69").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError70").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("WebinarJam");
                        }
                    }
                });
            });

//      EverWebinar
            $(document.body).on('click', '#connectEverwebinar', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#token_${type_modal}`).val();
                var accName = $('#accnameever').val();
                var apiKey = $('#apikeyever').val();
                $("#spnError71").css("display", "none");
                $("#spnError72").css("display", "none");
                $("#spnError73").css("display", "none");
                $("#spnError74").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError71").css("display", "block");
                    return false;
                }
                if (apiKey == null || apiKey == "") {
                    $("#spnError72").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-everwebinar',
                    data: {
                        accname: accName,
                        _token: token,
                        apikey: apiKey
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError73").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError74").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("EverWebinar");
                        }
                    }
                });
            });

            //      Ontraport
            $(document.body).on('click', '#connectOntraport', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $(`#tokenon_${type_modal}`).val();
                var accName = $('#accnameon').val();
                var appId = $('#appidon').val();
                var appKey = $('#appkeyon').val();
                $("#spnError75").css("display", "none");
                $("#spnError76").css("display", "none");
                $("#spnError77").css("display", "none");
                $("#spnError78").css("display", "none");
                $("#spnError79").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError75").css("display", "block");
                    return false;
                }
                if (appId == null || appId == "") {
                    $("#spnError76").css("display", "block");
                    return false;
                }
                if (appKey == null || appKey == "") {
                    $("#spnError77").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connect-to-ontraport',
                    data: {
                        accname: accName,
                        _token: token,
                        appId: appId,
                        appKey: appKey
                    },
                    type: 'POST',
                    datatype: 'json',
                    beforeSend:function(){
                    },
                    complete:function(){
                    },
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 401) {
                            $("#spnError78").css("display", "block");
                        }
                        else if (mailresponse['status'] == 402) {
                            $("#spnError79").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            showSuccessMessage("Ontraport");
                        }
                    }
                });
            });

//      Custom Html
            $(document.body).on('click', '#connectCustomHtml', function (e) {
                let type_modal = $(this).attr('data-type');

                e.preventDefault();
                var token = $('#tokench').val();
                var accName = $('#accnamech').val();
                var customHtml = $('#customHtmlch').val();
                $("#spnError80").css("display", "none");
                $("#spnError81").css("display", "none");
                $("#spnError82").css("display", "none");
                if (accName == null || accName == "") {
                    $("#spnError80").css("display", "block");
                    return false;
                }
                if (customHtml == null || customHtml == "") {
                    $("#spnError81").css("display", "block");
                    return false;
                }
                $.ajax({
                    url: '/user/connectCustomhtml',
                    data: {
                        accname: accName,
                        _token: token,
                        customHtml: customHtml
                    },
                    type: 'POST',
                    datatype: 'json',
                    success: function (mailresponse) {
                        mailresponse = $.parseJSON(mailresponse);
                        if (mailresponse['status'] == 402) {
                            $("#spnError82").css("display", "block");
                        }
                        else if (mailresponse['status'] == 200) {
                            location.reload();
                        }
                    }
                });
            });
        });

        function showSuccessMessage(accountName){
            // swal({
            //     title: "Added",
            //     text: "Your "+accountName+" account is added successfully.",
            //     timer: 1500,
            //     showConfirmButton: false
            // },function(){
            //     location.reload();
            // });

            Swal.fire({
                title: "Added",
                text: "Your "+accountName+" account is added successfully.",
                timer: 1500,
                showConfirmButton: false
            }).then(function(result) {

                location.reload();

            });

        }
    </script>

    <script type="text/javascript">
        function errorPresent() {
            $("select#colorselector option").filter(function () {
                return $(this).val() == "2";
            }).prop('selected', true);
        }
    </script>

    <script type="text/javascript">
        // delete sweet alert
        function ConnectedAutoresponders() {
            swal("Good job!", "You Connected With Autoresponders!", "success")
        }
        // delete sweet alert
        function DeleteAutoresponders(url) {
            // swal({
            //             title: "Are you sure?",
            //             text: "Once deleted you will need to authenticate again.",
            //             type: "info",
            //             showCancelButton: true,
            //             confirmButtonColor: "#DD6B55",
            //             confirmButtonText: "Yes, Disconnect !",
            //             cancelButtonText: "No, It was a mistake!",
            //             closeOnConfirm: false,
            //             closeOnCancel: false
            //         },
            //         function (isConfirm) {
            //             if (isConfirm) {
            //                 swal("Deleted!", "Your Autoresponders has been deleted.", "success");
            //                 window.location.href = url;
            //             }
            //             else {
            //                 swal("Cancelled", "Your Autoresponders are safe :)", "error");
            //             }
            //         });



            Swal.fire({
                title: "Are you sure?",
                text: "Once deleted you will need to authenticate again.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Disconnect !",
                cancelButtonText: "No, It was a mistake!",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function(result) {
                if (result.value) {
                    swal.fire("Deleted!", "Your Autoresponders has been deleted.", "success");
                    window.location.href = url;
                }   else {
                    swal("Cancelled", "Your Autoresponders are safe :)", "error");
                }
            })





        }

        function setVisibility(id, visibility) {
            document.getElementById(id).style.display = visibility;
        }

        $(function () {
            $('#colorselector a').click(function () {
                var element=$(this);
                var targetForm='#' + $(element).data('value');
                $('#colorselector').find('a').css('background-color','');
                $(element).css('background-color','#ddd');
                $('.colors').hide();
                $(targetForm).find('input[type="text"],input[type="password"],input[type="email"],textarea').val('');
                $(targetForm).show();
            });
        });
    </script>
@endsection
