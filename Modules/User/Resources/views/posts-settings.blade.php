@extends('User::resources.views.layouts.master')
@section('title','SocioLeads | PostSettings')
@section('title1','Post Settings')
@section('style')
    <style>
        .close {
            cursor:pointer;
        }
        .icon-star{
            cursor: pointer;
            float: right;
            font-size: 16px;
            line-height: 32px;
            padding-left: 8px;
        }
        .icon-close{
            cursor: pointer;
        }
        .h6New{
            font-size: 0.9rem;!important;
        }

        .mam, .mac{
            border: 4px double #3677bc;
        }

        .activeButton{
            border: 4px double rgba(255, 255, 255, 0.52);
            background-color: #3677bc !important;
        }


        @media screen and (min-width: 480px) {
            .card_inline_off{
                display: inline-flex;
            }

            .l20, .m20 {
                width: 20% !important;
            }
        }
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .card_inline_off{
                display: block;
            }
        }

        .settings-text {
            font-size: 1.3rem;
            line-height: 140%;
            margin: 0.5rem 0 0.4rem;
        }

        .switch-text {
            font-size: 1rem;
        }

        .tt .toltip{display: none;}
        .tt:hover .toltip {
            display: block;
            margin: -6px 0 0 -37px;
            position: absolute;
            width: 90px;
        }


        .arrow_box {
            position: relative;
            background: #73b4f9;
            border: 2px solid #61a2e7;
            color: #fff;
            padding: 4px 7px;font-size: 12px;
            border-radius: 10px;
            margin: 10px 0 0 0;
        }

        .arrow_box a{ color: #fff; text-decoration: underline;}
        .arrow_box:after, .arrow_box:before {
            bottom: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(136, 183, 213, 0) rgba(136, 183, 213, 0) #73b4f9;
            border-width: 11px;
            margin-left: -11px;
        }
        .arrow_box:before {
            border-color:  rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #61a2e7;
            border-width: 13px;
            margin-left: -13px;
        }

        .modal {
            overflow-y: visible !important;
            max-height: 80% !important;
        }
    </style>
@endsection
@section('content')
    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">

                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Set Comments</h5>
                <!--end::Page Title-->

                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <!--end::Actions-->
                <a href="/facebook/posts/{{$pages->id}}" class="btn btn-light-warning font-weight-bolder btn-sm mr-2">Back To Posts</a>
                <a href="/facebook/page-settings/{{$pages->id}}" class="btn btn-light-primary font-weight-bolder btn-sm mr-2">Page Settings</a>
                <button type="button" class="btn btn-light-success font-weight-bolder btn-sm" data-toggle="modal" data-target="#edit_Comments_modal">Add New comment</button>

            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('user::resources.views.layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->

    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Row-->
                <div class="d-flex flex-row">
                    <!--begin::Content-->
                    <div class="flex-row-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="addfanscmt">
                                @if($fanscomments)
                                    @foreach($fanscomments as $fans)
                                        @if($data)
                                <!--begin::Forms Widget-->
                                <div class="card card-custom gutter-b" id="commentDiv-{{$fans->id}}">
                                    <!--begin::Body-->
                                    <div class="card-body">
                                        <!--begin::Header-->
                                        <div class="d-flex align-items-center">
                                            <!--begin::Symbol-->
                                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                                <span class="symbol-label">
                                                    <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="">
                                                </span>
                                            </div>
                                            <!--end::Symbol-->

                                            <!--begin::Info-->
                                            <div class="d-flex flex-column flex-grow-1">
                                                <a href="javascript:;" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">{{$data->pagename}}</a>
                                                <span class="text-muted font-weight-bold">{{$fans->date}}</span>
                                            </div>
                                            <!--end::Info-->
                                            <button class="btn btn-icon btn-outline-danger btn-circle btn-sm mr-2" data-toggle="tooltip" title="" data-original-title="Delete" id="{{$fans->id}}"  onclick="deletecomment(this.id)"><i class="fa fa-trash"></i></button>
                                            <button class="btn btn-icon btn-outline-success btn-circle btn-sm" title="" data-toggle="modal" data-target="#edit_Comments_modal" id="comments-fans-{{$fans->id}}" onclick="select(this.id,'fans-update');"><i class="far fa-edit"></i></button>
                                        </div>
                                        <!--end::Header-->

                                        <!--begin::Body-->
                                        <div class="pt-5">
                                            <!--begin::Text-->
                                            <p class="text-dark-75 font-size-lg font-weight-normal"><span id="comm-fan-{{$fans->id}}">{{$fans->comment}}</span></p>
                                            <!--end::Text-->
                                            <div class="separator separator-solid mt-2 mb-4"></div>
                                        </div>
                                        <p id="FanCmtFilter{{$fans->id}}" class="text-dark-75 font-size-lg font-weight-normal mb-2">
                                            <?php $msgFilter=($fans->cmt_filter!='')?explode(',',$fans->cmt_filter):false;?>
                                            Filters:
                                            @if($msgFilter && !empty($msgFilter))
                                                @foreach($msgFilter as $msg)
                                                    @if(trim($msg))<span class="label label-info label-inline">{{$msg}} </span>@endif
                                                @endforeach
                                            @endif
                                        </p>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Body-->
                                </div>
                                <!--end::Forms Widget-->
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
        <!-- Add new comment Modal-->
        <div class="modal fade" id="edit_Comments_modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Draft Your Auto Comment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="exampleTextarea">Auto Commend send by SL</label>
                                        <textarea id="comment" class="form-control" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="">Filter Words</label>
                                        <input id="kt_tagify_1" class="form-control tagify filter-list" name='tags' placeholder='type...' value='' autofocus data-blacklist='.NET,PHP' />

                                        <div class="mt-3">
                                            <a href="javascript:;" id="kt_tagify_1_remove" class="btn btn-sm btn-primary font-weight-bold">Remove tags</a>
                                        </div>

                                        <div class="mt-3 text-muted">
                                            Simply use below short codes and full name or first name will be dynamically added to your reply. FacebookName or fb_first_name Copy paste in your message to display persons full name on your comment.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input id="comment-type" type="hidden" value="fans">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold close-modal" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary font-weight-bold" onclick="sendComments()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /End comment -->

    <!--begin::Footer-->
    @include('User::resources.views.layouts.page-footer')
    <!--end::Footer-->

    <!-- edit Messages fan modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content" id="closepop">
            <a href="javascript:void(0);"
               class="modal-action modal-close waves-effect waves-green btn-flat right"><i class="fa fa-close"></i></a>
            <h4 class="RopaSans_font push_dark_text">Draft Your Auto Message</h4>
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="row card no-space lighten-5">
                        <div class="input-field col s12 m12 l12">
                            <textarea id="message" class="materialize-textarea"></textarea>
                            <div class="center">
                                <span id="spnError1" style="color: Red; display: none">Please Enter Your Message.*</span>
                            </div>
                            <label for="message" class=""> <i class="icon-pencil right"></i>Enter Your Messages here
                            </label>
                        </div>
                        <div class="col s12 m12 l12 margin-bottom-10">
                            <a href="#" id="save"
                               class=" btn push_dark text_style_none col s12"
                               onclick="sendmessage();"><strong class="strong_700">Save</strong> Message</a></a>
                        </div>
                    </div>
                    @if($data)
                        <ul class="collection">
                            <li class="collection-item avatar">
                                <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt=""
                                     class="circle">
                                <span class="title"><a href=""
                                                       class="push_dark_text RopaSans_font">{{$data->pagename}}</a></span>
                                <p id="message_text" style="word-wrap: break-word;"></p>
                                <a href="javascript:void(0);" class="secondary-content"><i
                                            class="icon-star blue-text text-darken-3"></i></a>
                            </li>
                        </ul>
                    @endif
                    <input id="message-type" type="hidden" value="">
                </div>
                <div class="col m6 l6 s12">
                    <div class="card-panel padding_10 no-margin filterDiv">
                        <h5 class="center push_dark_text no-space"><strong class="strong_700">Filter Words</strong></h5>
                        <hr>
                        <div class="row no-space">
                            <div class="input-field col s12" style="margin-top: .5rem; padding: 0;">
                                <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                    <i class="icon-question push_dark_text " ></i>
                                    <div class="toltip">
                                        <div class="arrow_box">
                                            <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                            <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>
                                        </div>
                                    </div>
                                </div>
                                <i class="icon-plus push_dark_text add-filters positive" style="position: absolute;right: 35px;top: 15px;"></i>
                                <input id="msg_filter_word-po" type="text" class="validate" style="margin: 0 0 7px 0;">
                                <label for="msg_filter_word-po" style="left: 0;">Positive Filter Word</label>
                            </div>
                        </div>
                        <div class="row no-space">
                            <div class="input-field col s12" style="margin-top: .5rem; padding: 0;">
                                <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                    <i class="icon-question push_dark_text " ></i>
                                    <div class="toltip">
                                        <div class="arrow_box">
                                            <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                            <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>
                                        </div>
                                    </div>
                                </div>
                                <i class="icon-plus push_dark_text add-filters negative" style="position: absolute;right: 35px;top: 15px;"></i>
                                <input id="msg_filter_word-ne" type="text" class="validate" style="margin: 0 0 7px 0;">
                                <label for="msg_filter_word-ne" style="left: 0;">Negative Filter Word</label>
                            </div>
                        </div>
                        <div id="message-filters" class="filter-list"></div>
                    </div>
                    <div class="card-panel padding_10 no-margin push_dark">
                        <h5 class="center white-text no-space"><strong class="strong_700">Customize</strong> Your Message</h5>
                        <hr>
                        <div class="white-text">
                            <h6 style="font-weight: 300;">Simply use below short codes and full name or first name will be dynamically added to your reply.</h6>
                            <span><?php echo '{{FacebookName}} ?? {{fb_first_name}}';?></span>
                            <h6 style="font-weight: 300;"><i>Copy paste in your message to display persons full name on your Message.</i></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- edit Comments fan modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content" id="closepopcmts">
            <a href="javascript:void(0);"
               class="modal-action modal-close waves-effect waves-green btn-flat right"><i class="fa fa-close"></i></a>
            <h4 class="RopaSans_font push_dark_text">Draft Your Auto Comment</h4>
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="row card no-space lighten-5">
                        <div class="input-field col s12 m12 l12">
                            <textarea id="comment" class="materialize-textarea"></textarea>
                            <div class="center">
                                <span id="spnError2" style="color: Red; display: none">Please Enter your comment.*</span>
                            </div>
                            <label for="comment" class=""> <i class="icon-pencil right"></i>Enter Your Comment here
                            </label>
                        </div>
                        <div class="col s12 m12 l12 margin-bottom-10">
                            <a href="#" id="commentsave" onclick="sendcomments();"
                               class="btn push_dark text_style_none col s12"><strong class="strong_700">Save</strong> Comment</a>
                        </div>
                    </div>
                    @if($data)
                        <ul class="collection">
                            <li class="collection-item avatar">
                                <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt=""
                                     class="circle">
                                <span class="title"><a href="" class="push_dark_text RopaSans_font">{{$data->pagename}}</a></span>
                                <p id="comment_text" style="word-wrap: break-word;"></p>
                                <a href="javascript:void(0);" class="secondary-content"><i
                                            class="icon-star blue-text text-darken-3"></i></a>
                            </li>
                        </ul>
                    @endif
                    <input id="comment-type" type="hidden" value="fans">
                </div>
                <div class="col m6 l6 s12">
                    <div class="card-panel padding_10 no-margin filterDiv">
                        <h5 class="center push_dark_text no-space"><strong class="strong_700">Filter Words</strong></h5>
                        <hr>

                        <div class="row no-space">
                            <div class="input-field col s12" style="margin-top: .5rem; padding: 0;">
                                <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                    <i class="icon-question push_dark_text" ></i>
                                    <div class="toltip">
                                        <div class="arrow_box">
                                            <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                            <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>
                                        </div>
                                    </div>
                                </div>
                                <i class="icon-plus push_dark_text add-filters positive" style="position: absolute;right: 35px;top: 15px;"></i>
                                <input id="cmt_filter_word-po" type="text" class="validate" style="margin: 0 0 7px 0;">
                                <label for="cmt_filter_word-po" style="left: 0;">Positive Filter Word</label>
                            </div>
                        </div>
                        <div class="row no-space">
                            <div class="input-field col s12" style="margin-top: .5rem; padding: 0;">
                                <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                    <i class="icon-question push_dark_text" ></i>
                                    <div class="toltip">
                                        <div class="arrow_box">
                                            <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                            <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>
                                        </div>
                                    </div>
                                </div>
                                <i class="icon-plus push_dark_text add-filters negative" style="position: absolute;right: 35px;top: 15px;"></i>
                                <input id="cmt_filter_word-ne" type="text" class="validate" style="margin: 0 0 7px 0;">
                                <label for="cmt_filter_word-ne" style="left: 0;">Negative Filter Word</label>
                            </div>
                        </div>
                        <div id="comment-filters" class="filter-list"></div>
                    </div>
                    <div class="card-panel padding_10 no-margin push_dark">
                        <h5 class="center white-text no-space"><strong class="strong_700">Customize</strong> Your Comment</h5>
                        <hr>
                        <div class="white-text">
                            <h6 style="font-weight: 300;">Simply use below short codes and full name or first name will be dynamically added to your reply.</h6>
                            <span><?php echo '{{FacebookName}} ?? {{fb_first_name}}';?></span>
                            <h6 style="font-weight: 300;"><i>Copy paste in your message to display persons full name on your comment.</i></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script_function')
    <!--begin::Page Scripts(used by this page)-->
    <script src="/assets/js/pages/crud/forms/widgets/tagify.js"></script>
    <!--end::Page Scripts-->
    <script>
        $('.waves-green').removeClass('waves-green');
        $('#message').on('keyup', function () {
            $('#message_text').text($(this).val());
            $("#spnError1").css("display", "none");
        });
        $('#comment').on('keyup', function () {
            $('#comment_text').text($(this).val());
            $("#spnError2").css("display", "none");
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document.body).on('click', '#save', function (e) {
                // alert("hhgfhgfg");
                e.preventDefault();
                var message = $('#message').val();
                $("#spnError1").css("display", "none");
                //if({$fanmsg->fan_type)
                if (message == null || message == "") {
                    $("#spnError1").css("display", "block");
                    return false;
                }
            });

        });

        $(document).ready(function () {
            $(document.body).on('click', '#commentsave', function (e) {
                // alert("fsfsfd");
                e.preventDefault();
                var comment = $('#comment').val();
                $("#spnError2").css("display", "none");
                if (comment == null || comment == "") {
                    // alert("bbbbb");
                    $("#spnError2").css("display", "block");
                    return false;
                }
            });

        });


        $('.dropdown-button').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: false, // Does not change width of dropdown to that of the activator
                    hover: true, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: false, // Displays dropdown below the button
                    alignment: 'right' // Displays dropdown with edge aligned to the left of button
                }
        );

        function callModal(type) {
            $('#closepop').show().find('label').attr('class', '');
            $('#spnError1').hide();
            $('#message').val(null);
            $('#message_text').text(null);
            $('#materialize-lean-overlay-1').show();
            $('#msg_filter').val(null);
            var model=$('#modal1');
            model.find('input#message-type').val(type);
            model.find('.filter-list').html('');
            model.find('.filter-list').parent().css('display','none');
        }

        function callModal2(type) {
            $('#closepopcmts').show().find('label').attr('class', '');
            $('#spnError2').hide();
            $('#comment').val(null);
            $('#comment_text').text(null);
            $('#materialize-lean-overlay-1').show();
            $('#cmt_filter').val(null);
            var model=$('#modal2');
            model.find('input#comment-type').val(type);
            model.find('.filter-list').html('');
            model.find('.filter-list').parent().css('display','none');
        }

        // div toogle
        $(document).ready(function () {
            $(".mam").click(function () {
                $(".mac_div").hide();
                $(".mac").removeClass('activeButton');
                $(".mam_div").show();
                $(".mam").addClass('activeButton');
            });
            $(".mac").click(function () {
                $(".mam_div").hide();
                $(".mam").removeClass('activeButton');
                $(".mac_div").show();
                $(".mac").addClass('activeButton');
            });
        });

        function sendComments() {
            var status = $('#comment-type').val();
            if (status) {
                var model=$('#edit_Comments_modal');
                // $.each(model.find('.add-filters'),function () {
                //     $(this).click();
                // });
                var id = '<?php echo $id ?>';
                var type = status;
                var message = $("#comment").val();
//                var cmt_filter = $("#cmt_filter").val();
                var filter_details={};
                var cmt_filter = '';
                let filterListString = '';

                if($('#kt_tagify_1').val().trim()) {
                    let filterListStringObject = JSON.parse($('#kt_tagify_1').val());
                    filterListStringObject     = filterListStringObject.map(el => el.value);
                    filterListString       = filterListStringObject.join(',');
                }

                //let filterListString = model.find('.filter-list').val();
                if (filterListString.trim()) {
                    let filterDetailsArr = filterListString.split(',');
                    $.each(filterDetailsArr, function(i, v) {
                        filter_details[v.trim()] = 1;
                        filterDetailsArr[i] = v.trim();
                    });
                    cmt_filter = filterDetailsArr.join(',');
                } else {
                    cmt_filter = null;
                }
                filter_details = $.extend({}, filter_details);
                if (message == null || message == "") {
                    $("#spnError2").css("display", "block");
                    return false;
                } else {
                    $('.edit_Comments_modal').trigger('click');
                    $.ajax({
                        url: '/user/post-comments',
                        data: {
                            for_post_id: id,
                            fan_type: type,
                            comment: message,
                            cmt_filter: cmt_filter,
                            filter_details: filter_details,
                            type: 'post'
                        },
                        type: 'post',
                        dataType: 'json',
                        success: function (response) {
                            if (response['status'] == 200) {
                                if (type == 'fans') {
                                    var data = "'fans-update'";


                                    $('#addfanscmt').prepend(
                                    '<div class="card card-custom gutter-b" id="commentDiv-' + response['id'] + '">' +
                                    '    <div class="card-body">' +
                                    '        <div class="d-flex align-items-center">' +
                                    '            <div class="symbol symbol-40 symbol-light-success mr-5">' +
                                    '            <span class="symbol-label">' +
                                    '                <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="">' +
                                    '            </span>' +
                                    '            </div>' +
                                    '            <div class="d-flex flex-column flex-grow-1">' +
                                    '                <a href="javascript:;" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">{{$data->pagename ?? 'page'}}</a>' +
                                    '                <span class="text-muted font-weight-bold">Just Now</span>' +
                                    '            </div>' +
                                    '            <button class="btn btn-icon btn-outline-danger btn-circle btn-sm mr-2" data-toggle="tooltip" title="" data-original-title="Delete" id="' + response['id'] + '"  onclick="deletecomment(this.id)"><i class="fa fa-trash"></i></button>' +
                                    '            <button class="btn btn-icon btn-outline-success btn-circle btn-sm" title="" data-toggle="modal" data-target="#edit_Comments_modal" id="comments-fans-' + response['id'] + '" onclick="select(this.id,' + data + ');"><i class="far fa-edit"></i></button>' +
                                    '        </div>' +
                                    '        <div class="pt-5">' +
                                    '            <p class="text-dark-75 font-size-lg font-weight-normal"><span id="comm-fan-' + response['id'] + '">' + response['message'] + '</span></p>' +
                                    '            <div class="separator separator-solid mt-2 mb-4"></div>' +
                                    '        </div>' +
                                    '        <div id="FanCmtFilter' + response['id'] + '" class="text-dark-75 font-size-lg font-weight-normal mb-2">' + response['div'] + '</div>' +
                                    '    </div>' +
                                    '</div>');

                                }
                                else if (type == "nonfans") {
                                    var data1 = "'nonfans-update'";

                                    $('#addfanscmt').prepend(
                                        '<div id="commentDiv-' + response['id'] + '">' +
                                        '    <div class="d-flex mb-9">' +
                                        '        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">' +
                                        '            <div class="symbol symbol-50 symbol-lg-120">' +
                                        '                <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=large" alt="image">' +
                                        '            </div>' +
                                        '            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">' +
                                        '                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>' +
                                        '            </div>' +
                                        '        </div>' +
                                        '        <div class="flex-grow-1">' +
                                        '            <div class="d-flex justify-content-between flex-wrap mt-1">' +
                                        '                <div class="d-flex mr-3">' +
                                        '                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$data->pagename ?? 'page'}}</a>' +
                                        '                    <a href="#"><i class="flaticon2-correct text-success font-size-h5"></i></a>' +
                                        '                </div>' +
                                        '                <div class="my-lg-0 my-3">' +
                                        '                    <button type="button" class="btn btn-sm btn-light-danger font-weight-bolder text-uppercase" id="' + response['id'] + '" onclick="deletecomment(this.id)">Delete</button>' +
                                        '                    <button  id="comments-fans-' + response['id'] + '" onclick="select(this.id,' + data1 + ');" class="btn btn-sm btn-info font-weight-bolder text-uppercase" data-toggle="modal" data-target="#edit_Comments_modal">Edit</button>' +
                                        '                </div>' +
                                        '            </div>' +
                                        '            <div class="d-flex flex-wrap justify-content-between mt-1">' +
                                        '                <div class="d-flex flex-column flex-grow-1 pr-8">' +
                                        '                    <span class="font-weight-bold text-dark-50" id="comm-fan-' + response['id'] + '">' + response['message'] + '</span>' +
                                        '                </div>' +
                                        '            </div>' +
                                        '        </div>' +
                                        '    </div>' +
                                        '       <div id="FanCmtFilter' + response['id'] + '" class="text-dark-75 font-size-lg font-weight-normal mb-2">' + response['div'] + '</div>' +
                                        '    <div class="separator separator-solid mb-4"></div>' +
                                        '</div>');
                                }
                            }
                            else if (response['status'] == 198) {
//                                $('#closepopcmts').show();
                            }
                            else if (response['status'] == 201) {
                                $("#comm-fan-" + response['id']).html(response['message']);
                                $('#FanCmtFilter' + response['id']).html(response['div']);
                            }
                            else if (response['status'] == 202) {
                                $("#comm-nonfan-" + response['id']).html(response['message']);
                                $('#nonFanCmtFilter' + response['id']).html(response['div']);
                            }
                            $("#comment").val('');
                            tagifyDemo1.removeAllTags();
                            model.find('#comment-type').val('fans');
                            model.find('.close-modal').click();
                        }
                    });
                }

            }
        }


        function changeStatuspost(type) {
            if (type) {
                var idObj = $('#' + type);
                var status;
                if (idObj.prop("checked") == true) {
                    status = idObj.val();
                } else {
                    status = 'off';
                }
                var id = '<?php echo $id ?>';
                $.ajax({
                    url: '/user/post-status',
                    data: {
                        for_page_id: id,
                        status: status,
                        type: type
                    },
                    type: 'post',
                    datatype: 'json',
                    success: function (response) {
                        response = $.parseJSON(response);
                        if (response['status'] == 200 && type == 'fans') {
                            $("#success-comments").html('Comment added for fans').show();
                        }
                        else if (response['status'] == 200 && type == 'nonfans') {
                            $("#success-comments").html('Comment added for nonfans').show();
                        }
                        else if (response['status'] == 200 && type == 'global') {
                            $("#success-comments").html('Comment added Global').show();
                        }
                    }
                });
            }
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
            // $('.modal-trigger-log-in').leanModal();


            $('.modal-trigger').leanModal({
                        dismissible: true // Modal can be dismissed by clicking outside of the modal
                        // Opacity of modal background
                        // Ending top style attribute
                        // ready: function() { alert('Ready'); }, // Callback for Modal open
                        // complete: function() { alert('Closed'); } // Callback for Modal close
                    }
            );
        });

        $('#fanmsg').on('click', function () {
            alert('dfgdf');
            console.log("me");
            return false;

            var message = $('#addfanscmt').val();
            $.ajax({
                url: '/user/deletemsg',
                data: {
                    message: message,
                    type: 'post'
                }
            })
        });

//         function deletemessageOLD(id) {
//             var element = $('#'+id).parents('.message-div:first');
//             var message = $('#fanmsgid').val();
//             swal({
//                         title: "Are you sure?",
//                         text: "You will not be able to recover this imaginary file!",
//                         type: "info",
//                         showCancelButton: true,
//                         confirmButtonColor: "#DD6B55",
//                         confirmButtonText: "Yes, delete it!",
//                         cancelButtonText: "No, cancel pls!",
//                         closeOnConfirm: false,
//                         closeOnCancel: false
//                     },
//                     function (isConfirm) {
//                         if (isConfirm) {
//                             $.ajax({
//                                 url: '/user/deletePostMsg',
//                                 data: {
//                                     message: message,
//                                     id: id,
//                                     type: 'page'
//                                 },
//                                 type: 'post',
//                                 dataType: 'json',
//                                 success: function (response) {
//                                     console.log(response);
//                                     if (response.code === 200) {
//                                         $(element).remove();
//                                         swal("Deleted!", "Your imaginary file has been deleted.", "success");
//                                     } else {
// //                            alert(response.message);
//                                     }
//                                 }
//                             });
//                         } else {
//                             swal("Cancelled", "Your imaginary file is safe :)", "error");
//                         }
//                     });
//         }

        function deletecomment(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(function(result) {
                if (result.value) {
                    var message = $('#fanmsgid').val();
                    $.ajax({
                        url: '/user/deletePostComment',
                        data: {
                            message: message,
                            id: id,
                            type: 'page'
                        },
                        type: 'post',
                        datatype: 'json',
                        success: function (data) {
                            data = $.parseJSON(data);
                            if (data['code'] == 200) {
                                if (data['data'] == 1) {
                                    Swal.fire(
                                        'Deleted!',
                                        'Your file has been deleted.',
                                        'success'
                                    );
                                    $('#commentDiv-' + data['id']).css('display', 'none');
                                } else
                                    Swal.fire(
                                        'Error!',
                                        'The comment not deleted there is some error.',
                                        'info'
                                    );

                            } else if (data['code'] == 400) {
                                Swal.fire(
                                    'Error!',
                                    'The comment not deleted there is some error.',
                                    'info'
                                );
                            }
                        }
                    });
                }
            });
        }


//         function deletecommentOLd(id) {
//             //alert(id);
//             var element = $('#'+id).parents('.comment-div:first');
//             var message = $('#fanmsgid').val();
//             swal({
//                         title: "Are you sure?",
//                         text: "You will not be able to recover this imaginary file!",
//                         type: "info",
//                         showCancelButton: true,
//                         confirmButtonColor: "#DD6B55",
//                         confirmButtonText: "Yes, delete it!",
//                         cancelButtonText: "No, cancel pls!",
//                         closeOnConfirm: false,
//                         closeOnCancel: false
//                     },
//                     function (isConfirm) {
//                         if (isConfirm) {
//                             $.ajax({
//                                 url: '/user/deletePostComment',
//                                 data: {
//                                     message: message,
//                                     id: id,
//                                     type: 'page'
//                                 },
//                                 type: 'post',
//                                 dataType: 'json',
//                                 success: function (response) {
//                                     console.log(response);
//                                     if (response.code === 200) {
//                                         $(element).remove();
//                                         swal("Deleted!", "Your imaginary file has been deleted.", "success");
//                                     } else {
// //                                    alert(response.message);
//                                     }
//                                 }
//                             });
//                         } else {
//                             swal("Cancelled", "Your imaginary file is safe :)", "error");
//                         }
//                     });
//         }

        function select(id, update) {
            //$('#spnError2,#spnError1').hide();
            //$('#edit_Messages_modal,#edit_Comments_modal').find('label').addClass('active');
            var str = id.split('-');
            var filters = '';
            var filter_details={};
            $.ajax({
                url: '/user/post-select',
                data: {
                    id: id,
                    type: 'post',
                },
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    // console.log(data,data['status'],data.status);
                    if (data['status'] == 200) {
                        $('#message').val(data['message'].message);
                        $('#edit_Messages_modal').find('input#message-type').val(update + '-' + data['message'].id);
                        $('#message_text').html(data['message'].message);
                        $('#msg_filter').val('');
                        filters = data['message'].msg_filter.split(",");
                        if(data['message'].filter_details)
                            filter_details = $.parseJSON(data['message'].filter_details);
                    } else if (data['status'] == 201) {
                        $('#comment').val(data['message'].comment);
                        $('#edit_Comments_modal').find('input#comment-type').val(update + '-' + data['message'].id);
                        $('#comment_text').html(data['message'].comment);
                        $('#cmt_filter').val('');
                        filters = data['message'].cmt_filter;
                        if(data['message'].filter_details)
                            filter_details = $.parseJSON(data['message'].filter_details);
                    }
                    if (str[0] == 'messages') {
                        var messageFilter=$('#edit_Comments_modal').find('.filter-list');
                        messageFilter.html('');
                        $.each(filters, function (i, v) {
                            var wordType='';
                            if(filter_details && filter_details.hasOwnProperty(v)){
                                if(parseInt(filter_details[v])===1)
                                    wordType='<i class="fa fa-plus-square" aria-hidden="true" style="color: #39e600; font-size: 110%"></i> ';
                                else if(parseInt(filter_details[v])===0)
                                    wordType='<i class="fa fa-minus-square" aria-hidden="true" style="color: #ff8080; font-size: 110%"></i> ';
                            }
                            if (v !== "") {
                                messageFilter.append('<div class="label label-info label-inline">' + wordType + v + ' <i class="close icon-close delete-filter"></i></div>');
                            }
                        });
                        if(messageFilter.find('.label').length<1)
                            messageFilter.parent().css('display','none');
                        else
                            messageFilter.parent().css('display','block');
                        $(document.body).find('#callToEdit_Comments_modal').trigger('click');
                    }
                    else {
                        // var commentFilter=$('#edit_Comments_modal #kt_tagify_1');
                        tagifyDemo1.addTags(filters.split(','));
                        //$(document.body).find('#callToEdit_Comments_modal').trigger('click');
                    }

                }
            });

        }

//         function select(id, update) {
//             var str = id.split('-');
//             $('#spnError2,#spnError1').hide();
//             $('#modal1,#modal2').find('label').addClass('active');
//             var filters='';
//             var filter_details={};
//             $.ajax({
//                 url: '/user/post-select',
//                 data: {
//                     id: id,
//                     type: 'post'
//                 },
//                 type: 'post',
//                 datatype: 'json',
//                 success: function (data) {
//                     data = $.parseJSON(data);
//                     if (data['status'] == 200) {
//                         $('#message').val(data['message'].message);
//                         $('#modal1').find('input#message-type').val(update + '-' + data['message'].id);
//                         $('#message_text').html(data['message'].message);
// //                        $('#msg_filter').val(data['message'].msg_filter);
//                         filters = data['message'].msg_filter.split(",");
//                         if (data['message'].filter_details)
//                             filter_details =$.parseJSON( data['message'].filter_details);
//
//                     } else if (data['status'] == 201) {
//                         $('#comment').val(data['message'].comment);
//                         $('#modal2').find('input#comment-type').val(update + '-' + data['message'].id);
//                         $('#comment_text').html(data['message'].comment);
// //                        $('#cmt_filter').val(data['message'].cmt_filter);
//                         filters = data['message'].cmt_filter.split(",");
//                         if (data['message'].filter_details)
//                             filter_details =$.parseJSON( data['message'].filter_details);
//                     }
//                     if (str[0] == 'messages') {
//                             var messageFilter=$('#modal1').find('.filter-list');
//                             messageFilter.html('');
//                             $.each(filters,function(i,v){
//                                 var wordType='';
//                                 if(filter_details && filter_details.hasOwnProperty(v)){
//                                     if(parseInt(filter_details[v])===1)
//                                         wordType='<i class="fa fa-plus-square" aria-hidden="true" style="color: #39e600; font-size: 110%"></i> ';
//                                     else if(parseInt(filter_details[v])===0)
//                                         wordType='<i class="fa fa-minus-square" aria-hidden="true" style="color: #ff8080; font-size: 110%"></i> ';
//
//                                 }
//                                 if(v!=="") {
//                                     messageFilter.append('<div class="label label-info label-inline">' + wordType + v + ' <i class="close icon-close delete-filter"></i></div>');
//                                 }
//                             });
//                         if(messageFilter.find('.label').length<1)
//                             messageFilter.parent().css('display','none');
//                         else
//                             messageFilter.parent().css('display','block');
//                         $(document.body).find('#callToModel').trigger('click');
//                         $('#closepop').attr("style", "display:block").find('label').attr('class', 'active');
//                     }
//                     else {
//                             var commentFilter=$('#modal2').find('.filter-list');
//                             commentFilter.html('');
//                             $.each(filters,function(i,v){
//                                 var wordType='';
//                                 if(filter_details && filter_details.hasOwnProperty(v)){
//                                     if(parseInt(filter_details[v])===1)
//                                         wordType='<i class="fa fa-plus-square" aria-hidden="true" style="color: #39e600; font-size: 110%"></i> ';
//                                     else if(parseInt(filter_details[v])===0)
//                                         wordType='<i class="fa fa-minus-square" aria-hidden="true" style="color: #ff8080; font-size: 110%"></i> ';
//
//                                 }
//                                 if(v!=="") {
//                                     commentFilter.append('<div class="label label-info label-inline">' + wordType + v + ' <i class="close icon-close delete-filter"></i></div>');
//                                 }
//                             });
//                         if(commentFilter.find('.label').length<1)
//                             commentFilter.parent().css('display','none');
//                         else
//                             commentFilter.parent().css('display','block');
//                         $(document.body).find('#callToModel2').trigger('click');
//                         $('#closepopcmts').attr("style", "display:block").find('label').attr('class', 'active');
//                     }
//                     $('#postRequestData').html(data);
//                 }
//             });
//
//         }
    </script>

    <script>
        //wrote by raushan
        $(document).ready(function(){
            $('#edit_Comments_modal .ki-close, #edit_Comments_modal .close-modal').on('click', function (){
                $("#comment").val('');
                tagifyDemo1.removeAllTags();
                $('#edit_Comments_modal').find('#comment-type').val('fans');
            })

            $('.add-filters').on('click',function(){
                var grantParent = $(this).parents('.filterDiv:first');
                var parent = $(this).parents('.input-field:first');
                var filter=$(parent).find("input").val().trim();
                if(filter !==''){
                    filter=filter.split(',');
                    var wordType='';
                    if($(this).hasClass('positive')){
                        wordType='<i class="fa fa-plus-square" aria-hidden="true" style="color: #39e600; font-size: 110%"></i> ';
                    }else if($(this).hasClass('negative')) {
                        wordType = '<i class="fa fa-minus-square" aria-hidden="true" style="color: #ff8080; font-size: 110%"></i> ';
                    }
                    $(grantParent).find('.filter-list').parent().css('display','block');
                    $.each(filter,function (i,val) {
                        val=val.trim();
                        if(val !=='') {
                            $(grantParent).find('.filter-list').append('<div class="label label-info label-inline">' + wordType + val + ' <i class="close icon-close delete-filter"></i></div>');
                        }
                    });
                    $(parent).find("input").val('');
                }
            });

            $(document).on('click','.delete-filter',function(){
                $(this).parent().remove();
            });
        });
        $('#msg_filter_word-po,#cmt_filter_word-po,#msg_filter_word-ne,#cmt_filter_word-ne').keypress(function (e) {
            if (e.which == 13) {
                $(this).siblings().trigger('click');
            }
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.more-info').on('mouseover',function(){
                $(this).addClass('hide');
                $(this).parents('.toltip:first').css('width','300px').css('margin','-6px 0 0 -143px');
                $(this).siblings('.more-info-text').removeClass('hide');
            });
            $('.more-info-text').on('mouseout',function(){
                $(this).addClass('hide');
                $(this).parents('.toltip:first').css('width','90px').css('margin','-6px 0 0 -37px');
                $(this).siblings('.more-info').removeClass('hide');
            });
        });
        $(function () {
            $('.filter-list').slimScroll({
                color:'#2b2b2b',
                size:'10px',
                height:'70px',
                alwaysVisible:true,
                allowPageScroll:true
            });
        });
    </script>
@endsection
