<div class="card-search col-sm" >
@if($pages)
    <?php $i = 1; ?>
    @foreach($pages as $page)
        <!--begin::Card-->
        <div class="card-body dashboard-card-body card-search-body">
            <!--begin::Top-->
            <div class="d-flex align-items-center">
                <!--begin::Symbol-->
                <div class="symbol symbol-40 symbol-light-success mr-5">
                    <span class="symbol-label">
                        <img src="{{file_exists(ltrim($page->picture, '/'))? $page->picture : 'https://graph.facebook.com/'.$page->pageid.'/picture?type=small'}}"
                             alt="" class="circle responsive-img activator card-profile-image h-100">
                    </span>
                </div>
                <!--end::Symbol-->
                <!--begin::Info-->
                <div class="d-flex flex-column flex-grow-1">
                    <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">{{$page->pagename}}</a>
                </div>
                <!--end::Info-->
            </div>
            <!--end::Top-->
            <!--begin::Separator-->
            <div class="separator separator-solid mt-2 mb-4"></div>
            <!--end::Separator-->
            <!--begin::Editor-->
            <div class="btn-block" role="group" aria-label="First group">
                <a href="/facebook/page-settings/{{$page->id}}" class="btn btn-icon btn-light-dark btn-circle" data-toggle="tooltip" title="Go to Page settings">
                    <i class="fas fa-cog"></i></a>
                <a href="/facebook/posts/{{$page->id}}" class="btn btn-icon btn-light-warning btn-circle" data-toggle="tooltip" title="Go to Posts">
                    <i class="far fa-copy"></i></a>
                <a href="/facebook/broadcast-message/{{$page->id}}" class="btn btn-icon btn-light-info btn-circle" data-toggle="tooltip" title="Message Broadcast">
                    <i class="fas fa-mail-bulk"></i></a>
                <a href="/facebook/report/page/{{$page->id}}" class="btn btn-icon btn-light-success btn-circle" data-toggle="tooltip" title="Page Reports">
                    <i class="far fa-clipboard"></i></a>
                <a onclick="deletepage('/facebook/delete-page/{{$page->id}}')" class="btn btn-icon btn-light-danger btn-circle" data-toggle="tooltip" title="Delete">
                    <i class="fas fa-trash"></i></a>
                <a href="https://www.facebook.com/{{$page->pageid}}" target="_blank" class="btn btn-icon btn-light-primary btn-circle" data-toggle="tooltip" title="Go To Facebook Page">
                    <i class="fab fa-facebook-f"></i></a>
            </div>
            <!--edit::Editor-->
        </div>
        <!--end::Card-->
        <?php $i = $i + 1; ?>
        @endforeach
    @endif
</div>
