<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <meta charset="utf-8" />
    <title>SocioLeads | Sign in/Up</title>
    <meta name="description" content="Facebook Controls" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="324322529311-2cekdvq5f7tpnc6hdr0lbjpf17t370jb.apps.googleusercontent.com">

    <!--begin::Page Custom Styles(used by this page)--> 
    <link href="../assets/css/pages/login/classic/login-3.css" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles-->

    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles-->

    <!--begin::Layout Themes(used by all pages)-->
    <link href="../assets/css/themes/layout/header/base/light.css" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->

    <link rel="shortcut icon" href="../assets/media/logos/favicon.png" />

</head>
<!--end::Head-->

<!--begin::Body-->

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed subheader-mobile-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">

    <!--begin::Main-->
    <div class="d-flex flex-column flex-root">

        <!--begin::Login-->
        <div class="login login-3 login-welcome-on d-flex flex-row-fluid" id="kt_login">
            <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid" style="background-image: url(../assets/media/bg/bg-2.jpg">
                <div class="login-form text-center text-white px-30 pt-7 position-relative overflow-hidden">
                    <!--begin::Login Header-->
                    <div class="d-flex flex-center mb-5">
                        <a href="index.html">
                            <img src="../assets/media/logos/socioleads_wh.png" class="max-h-100px" alt="" />
                        </a>
                    </div>
                    <!--end::Login Header-->

                    <!--begin::Login Sign up form-->
                    <div class="login-resetpwd">
                        <div class="">
                            <h3>Reset Password</h3>
                        </div>
                        <form class="form text-center" id="kt_login_resetpwd_form">
                            <div class="form-group row">
                                <input class="form-control h-auto rounded-pill border-0 py-4 px-8" type="text" placeholder="" name="email" value="{{ $user->email }}" readonly />
                            </div>
                            <input name="token" type="hidden" value="{{ $token }}">
                            <div class="form-group row">
                                <input class="form-control h-auto text-white bg-dark-o-70 rounded-pill border-0 py-4 px-8" type="password" placeholder="Password" name="password" />
                            </div>
                            <div class="form-group row">
                                <input class="form-control h-auto text-white bg-dark-o-70 rounded-pill border-0 py-4 px-8" type="password" placeholder="Confirm Password" name="cpassword" />
                            </div>
                            <div class="form-group">
                                <button id="kt_login_resetpwd_cancel" class="btn btn-pill btn-outline-white font-weight-bold px-12 ">Cancel</button>
                                <button id="kt_login_resetpwd_submit" class="btn btn-pill btn-outline-white font-weight-bold px-12  ">Reset</button>
                            </div>
                        </form>
                    </div>
                    <!--end::Login Sign up form-->

                    <!--begin::Expired Link-->
                    <div class="login-resetpwd">
                        <div class="">
                            <h3>Reset Password</h3>
                        </div>
                        <form class="form text-center" id="kt_login_resetpwd_form">
                            <div class="form-group row">
                                <input class="form-control h-auto rounded-pill border-0 py-4 px-8" type="text" placeholder="" name="email" value="{{ $user->email }}" readonly />
                            </div>
                            <input name="token" type="hidden" value="{{ $token }}">
                            <div class="form-group row">
                                <input class="form-control h-auto text-white bg-dark-o-70 rounded-pill border-0 py-4 px-8" type="password" placeholder="Password" name="password" />
                            </div>
                            <div class="form-group row">
                                <input class="form-control h-auto text-white bg-dark-o-70 rounded-pill border-0 py-4 px-8" type="password" placeholder="Confirm Password" name="cpassword" />
                            </div>
                            <div class="form-group">
                                <button id="kt_login_resetpwd_cancel" class="btn btn-pill btn-outline-white font-weight-bold px-12 ">Cancel</button>
                                <button id="kt_login_resetpwd_submit" class="btn btn-pill btn-outline-white font-weight-bold px-12  ">Reset</button>
                            </div>
                        </form>
                    </div>
                    <!--end::Login Sign up form-->
                </div>
            </div>
        </div>
        <!--end::Login-->
    </div>
    <!--end::Main-->

    <!--begin::Global Config(global config for global JS scripts)-->
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1400
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#3699FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#E4E6EF",
                        "dark": "#181C32"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1F0FF",
                        "secondary": "#EBEDF3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#3F4254",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#EBEDF3",
                    "gray-300": "#E4E6EF",
                    "gray-400": "#D1D3E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#7E8299",
                    "gray-700": "#5E6278",
                    "gray-800": "#3F4254",
                    "gray-900": "#181C32"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    
    <!--end::Global Config-->

    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Theme Bundle-->


    <!--begin::Page Scripts(used by this page)-->
    <script src="../assets/js/pages/custom/login/reset-general.js"></script>
    <!--end::Page Scripts-->
</body>
<!--end::Body-->

</html>