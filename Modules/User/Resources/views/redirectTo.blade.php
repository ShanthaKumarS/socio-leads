<html>
/**
* Created by Bibhudatta Sahoo (bibhuduttasahoo@globussoft.in)
* Date: 3/30/2017
* Time: 1:55 PM
*/

<head>
    @if(Session::get('login-success'))
        @if(Session::has('type'))
            <script>
                window.opener.location = '<?php echo '/user/'.$redTo; ?>';
                window.close();
            </script>
            {{Session::forget('type')}}
        @endif
    @endif
</head>
<body>

</body>

</html>
