@extends('User::resources.views.layouts.master')
@section('title','SocioLeads | Dashboard')
@section('style')
    <style>
        .truncate-name {
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: 200px;
        }

        .fixed-center {
            display: inline-flex;
            text-align: center;
            width: 100%;
        }

        @media screen and (max-width: 799px) {
            .fixed-center {
                display: block;
                text-align: center;
                width: 100%;
            }
        }

        .reauth-page-name {
            color: #f35e5e;
            font-weight: bold;
        }

    </style>
@endsection
@section('content')
    @if(Session::get('login-success'))
        @if(Session::has('type'))
            <script>
                var url = '/dashboard';
                @if(Session::has('preUrl'))
                    url = '<?php echo Session::get('preUrl')?>';
                @endif
                    window.opener.location = url;
                window.close();
            </script>
            {{Session::forget('type')}}
        @endif
    @endif
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header-->
            <div id="kt_header" class="header header-fixed">
                <!--begin::Container-->
                <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
                    <!--begin::Header Menu Wrapper-->
                    <!--begin::Info-->
                    <div class="d-flex align-items-center flex-wrap mr-2">

                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                            Account Settings </h5>
                        <!--end::Page Title-->

                        <!--begin::Actions-->
                        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                        <!--end::Actions-->
                    </div>
                    <!--end::Info-->
                    <!--end::Header Menu Wrapper-->
                    @include('user::resources.views.layouts.top-toolbar')
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->

            <!--begin::Content-->
            <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class=" container ">
                        <div class="card card-custom gutter-bs">
                            <!--begin::Header-->
                            <div class="card-header card-header-tabs-line">
                                <div class="card-toolbar">
                                    <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#Personal_Information">
                                                <i class="fas fa-user-tie mr-3"></i>
                                                <span class="nav-text font-weight-bold">Personal Information</span>
                                            </a>
                                        </li>
{{--                                        <li class="nav-item mr-3">--}}
{{--                                            <a class="nav-link" data-toggle="tab" href="#Account_Information">--}}
{{--                                                <i class="fas fa-file-invoice mr-3"></i>--}}
{{--                                                <span class="nav-text font-weight-bold">Account Information</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                        <li class="nav-item">--}}
{{--                                            <a class="nav-link" data-toggle="tab" href="#Email_Settings">--}}
{{--                                                <i class="fas fa-envelope mr-3"></i>--}}
{{--                                                <span class="nav-text font-weight-bold">Email Settings</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
                                    </ul>
                                </div>
                            </div>
                            <!--end::Header-->

                            <!--begin::Body-->
                            <div class="card-body px-0">
                                <div class="tab-content pt-5">
                                    <!--begin::Tab Content-->
                                    <div class="tab-pane active" id="Personal_Information" role="tabpanel">
                                        <div class="col-lg-10 offset-xl-1">
                                            <!--begin::Profile Card-->
                                            <div class="card card-custom card-stretch">
                                                <!--begin::Header-->
                                                <div class="card-header py-3">
                                                    <div class="card-title align-items-start flex-column">
                                                        <h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
                                                        <span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span>
                                                    </div>
                                                </div>
                                                <!--end::Header-->

                                                <!--begin::Form-->
                                                <form id="kt_profile_form" class="form"  files="true" >
                                                    <!--begin::Body-->
                                                    <div class="card-body">
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Profile Picture</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url(../assets/media/users/blank.png)">
                                                                    @isset($user['picture'])
                                                                        <div class="image-input-wrapper" style="background-image: url({{$user['picture']}})"></div>
                                                                    @else
                                                                        <div class="image-input-wrapper" style="background-image: url(assets/media/users/300_21.jpg)"></div>
                                                                    @endisset

                                                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                                                        <input type="file" name="picture" accept=".png, .jpg, .jpeg"/>
                                                                        <input type="hidden" name="profile_avatar_remove"/>
                                                                    </label>

                                                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                                        </span>

                                                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                                        </span>
                                                                </div>
                                                                <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Name</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <input class="form-control form-control-lg form-control-solid" type="text" name="name" placeholder="Name" value="{{$user['firstName']}} {{$user['lastName']}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <div class="input-group input-group-lg input-group-solid">
                                                                    <input type="text" class="form-control form-control-lg form-control-solid" name="phno" placeholder="Phone" value="{{$user['phoneNumber']}}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                @isset($resendActivationLink)
                                                                    <p class="text-danger">Your email Address is not verified yet <a id="resend-verification-link" href="{{$resendActivationLink}}">Resend link</a></p>
                                                                @endisset
                                                                <div class="input-group input-group-lg input-group-solid">
                                                                    @if(isset($user['email']) && ! isset($resendActivationLink))
                                                                        <input type="text" class="form-control form-control-lg form-control-solid" placeholder="Email" value="{{$user['email']}}" readonly/><p class="text-success">verified</p>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-lg form-control-solid" name="email" placeholder="Email" value="{{$user['email']}}" />
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <input class="form-control form-control-lg form-control-solid" type="text" name="city" placeholder="City" value="{{$user['city']}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Country</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <input class="form-control form-control-lg form-control-solid" type="text" name="country" placeholder="Country" value="{{$user['country']}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="card-toolbar text-right">
                                                            <button id="save-profile" class="btn btn-success btn-sm mr-2">Save Changes</button>
                                                        </div>
                                                    </div>
                                                    <!--end::Body-->
                                                </form>
                                                <!--end::Form-->
                                            </div>
                                            <!--end::Profile Card-->
                                        </div>
                                    </div>
                                    <!--end::Tab Content-->
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
            <!--end::Content-->

            <!--begin::Footer-->
            <div class="footer bg-white py-4 d-flex flex-lg-column " id="kt_footer">
                <!--begin::Container-->
                <div class=" container-fluid  d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted font-weight-bold mr-2">&copy;2021</span>
                        <a href="#" target="_blank" class="text-dark-75 text-hover-primary">Socio Leads</a>
                    </div>
                    <!--end::Copyright-->

                    <!--begin::Nav-->
                    <div class="nav nav-dark">
                        <!-- <a href="#" target="_blank" class="nav-link pl-0 pr-5">About</a> -->
                        <a href="Privacy_Policy.html" target="_blank" class="nav-link pl-0 pr-5">Privacy & Policy</a>
                        <!-- <a href="#" target="_blank" class="nav-link pl-0 pr-0">Contact</a> -->
                    </div>
                    <!--end::Nav-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
<!--end::Main-->

<!--begin::Global Theme Bundle(used by all pages)-->
    <script>

    </script>
<script src="../assets/plugins/global/plugins.bundle.js"></script>
<script src="../assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
<script src="../assets/js/scripts.bundle.js"></script>
<!--end::Global Theme Bundle-->
<!--begin::Page Scripts(used by this page)-->
<script src="../assets/js/pages/widgets.js"></script>
<script src="../assets/js/pages/custom/profile/profile.js"></script>

<!--end::Page Scripts-->
</body>
<!--end::Body-->

</html>
