<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<center style="padding-top: 94px;">
{{--<img src="/assets/images/small-logo.png" alt="SocioLeads" >--}}
    <img src="/assets/images/socioleads2.png" alt="SocioLeads" width="17%"; height="10%">

    <div class="container">
    <p class="content">
        <div class="title">You Don’t Have Access To This Software.</div>
        <p style="font-size: 20px; font-weight: 800; font-family:Georgia;"> Please Email Support@MarketerSoftware.com</p>
    </div>
</div>
</center>
</body>
</html>
