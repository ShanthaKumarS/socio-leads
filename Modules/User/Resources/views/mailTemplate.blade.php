<html xmlns="http://www.w3.org/1999/xhtml">

<head></head>

</html>
<title>SocioLeads</title>

<!-- START OF HEADER BLOCK-->
<table align="center" bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0" style="margin-top: 20px;border-left: 5px solid #e6e6e6;border-right: 5px solid #e6e6e6;border-top: 5px solid #e6e6e6;">
    <tbody>
        <tr>
            <td valign="top" width="100%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="100%">
                                <!-- START OF VERTICAL SPACER-->

                                <!-- END OF VERTICAL SPACER-->

                                <table border="0" cellpadding="0" cellspacing="0" class="table_scale" width="650">
                                    <tbody>
                                        <tr>
                                            <td width="100%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="650">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <!-- START OF LOGO IMAGE TABLE-->
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="auto">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center" style="padding: 40px 0 30px 0; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                                                                                {{$title}}</span>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <!-- END OF LOGO IMAGE TABLE-->
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- START OF VERTICAL SPACER-->


                                <!-- END OF VERTICAL SPACER-->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END OF HEADER BLOCK-->
<!-- START OF FEATURED AREA BLOCK-->

<table align="center" bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0" style="border: 5px solid #e6e6e6;">
    <tbody>
        <tr>
            <td valign="top" width="100%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="100%">
                                <table bgcolor="#1D6AD2" border="0" cellpadding="0" cellspacing="0" class="table_scale" width="650">
                                    <tbody>
                                        <tr>
                                            <td width="100%">
                                                <table bgcolor="#f4f7fe" border="0" cellpadding="0" cellspacing="0" width="650">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 0px;" valign="middle" width="650">
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="full" width="650">
                                                                    <tbody>
                                                                        <tr style="text-align: center;">
                                                                            <td style="padding: 20px 70px;">
                                                                                <p style="color: #2c3541; font-size: 15px; font-family:Verdana, Geneva, Tahoma, sans-serif; line-height: 1.5;">Hi, {{$name}}</p>
                                                                                <p style="color: #2c3541; font-size: 15px; font-family:Verdana, Geneva, Tahoma, sans-serif; line-height: 1.5;">{{$content}}</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <a style="text-decoration: none; font-family:Verdana, Geneva, Tahoma, sans-serif; line-height: 1.5;" href={{$link}}>
                                                                                    <h3 style="background-color: #2785d8; 
                                                                        padding: 8px; text-align: center;margin: 00 10rem;color: #ffffff;border-radius: 8px;"> {{$buttonName}} </h3>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="text-align: center;">
                                                                            <td style="padding: 20px 20px;">
                                                                                <p style="color: #2c3541; font-size: 15px; font-family:Verdana, Geneva, Tahoma, sans-serif; line-height: 1.5;">If above button is not working then please copy paste this URL <span style="color: #2785d8;">{{$link}}</span></p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <!--[if gte mso 9]> </v:textbox> </v:rect> <![endif]-->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END OF FEATURED AREA BLOCK-->
<!-- START OF SOCIAL BLOCK-->

<table align="center" bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0" style="border-left: 5px solid #e6e6e6;border-right: 5px solid #e6e6e6;">
    <tbody>
        <tr>
            <td valign="top" width="100%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="100%">
                                <!-- START OF VERTICAL SPACER-->
                                <table align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" class="table_scale" width="650">
                                    <tbody>
                                        <tr style="text-align: center;">
                                            <td style="padding: 20px 20px;">
                                                <p style="font-family:Verdana, Geneva, Tahoma, sans-serif;">Regards</p>
                                                <p style="color: #7e8081; font-family:Verdana, Geneva, Tahoma, sans-serif;">SocioLeads Suport</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END OF SOCIAL BLOCK-->
<!-- START OF SUB-FOOTER BLOCK-->

<table align="center" bgcolor="#f8f8f8" border="0" cellpadding="0" cellspacing="0" style="border-left: 5px solid #e6e6e6;border-right: 5px solid #e6e6e6; border-bottom: 5px solid #e6e6e6">
    <tbody>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td>
                                <table align="center" bgcolor="#666666" border="0" cellpadding="0" cellspacing="0" width="650">
                                    <tbody>
                                        <tr>
                                            <td height="40" style="margin: 0; font-size:12px ; color:#ededed; font-family: Helvetica, Arial, sans-serif; line-height: 18px; text-align: center;" width="100%">Copyright &copy; 2021 SocioLeads. All Rights Reserved.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- END OF SUB-FOOTER BLOCK-->