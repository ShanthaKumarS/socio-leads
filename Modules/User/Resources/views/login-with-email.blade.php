<!DOCTYPE html>
<html>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Globussoft">

    <title>SocioLeads | SignIn</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/assets/images/small-logo.png">

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="/assets/css/style.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="/assets/plugins/materialize/css/materialize.css" media="screen,projection"/>

    <link rel="stylesheet" type="text/css" href="/assets/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/particles_js/demo/css/style.css">

</head>

<body>

<main>
    <div class="navbar-fixed">
        <nav class="  grey lighten-5">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo center"><img src="/assets/images/small-logo.png" alt="SocioLeads"></a>
            </div>
        </nav>
    </div>

    <div id="particles-js">
        <div class="row">
            <div class="col offset-m4 m4 particles_position" style="margin-top: 8%;">
                <form action="" method="post">
                    <div class="card">
                        <div class="card-content">
                            <div>
                                <h5 class="card-title center-align">Sign In</h5>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input name="email" id="email" type="email" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input name="password" id="password" type="password" class="validate">
                                    <label for="password">Password</label>
                                </div>
                            </div>
                            @if(session()->has('loginErrorMsg'))
                                <div class="rowc center">
                                    <span  style="color:red;">Credentials Not Matched</span>
                                </div>
                            @endif
                        </div>
                        <div class="card-action center">
                            <input type="submit" class="btn grey darken-3" value="LOG IN"/>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-58515856-25', 'auto');
    ga('send', 'pageview');

</script>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="/assets/plugins/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="/assets/plugins/materialize/js/materialize.min.js"></script>
<script type="text/javascript" src="/assets/js/init.js"></script>
<!-- particles scripts -->
<script src="/assets/plugins/particles_js/particles.js"></script>
<script src="/assets/plugins/particles_js/demo/js/app.js"></script>

</body>
</html>
