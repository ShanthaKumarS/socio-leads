@extends('User::resources.views.layouts.master')
@section('title','SocioLeads | PostSettings')
@section('title1','Post Settings')
@section('style')
    <style>
        .close {
            cursor:pointer;
        }
        .icon-star{
            cursor: pointer;
            float: right;
            font-size: 16px;
            line-height: 32px;
            padding-left: 8px;
        }
        .icon-close{
            cursor: pointer;
        }
        .h6New{
            font-size: 0.9rem;!important;
        }

        .mam, .mac{
            border: 4px double #3677bc;
        }

        .activeButton{
            border: 4px double rgba(255, 255, 255, 0.52);
            background-color: #3677bc !important;
        }


        @media screen and (min-width: 480px) {
            .card_inline_off{
                display: inline-flex;
            }

            .l20, .m20 {
                width: 20% !important;
            }
        }
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .card_inline_off{
                display: block;
            }
        }

        .settings-text {
            font-size: 1.3rem;
            line-height: 140%;
            margin: 0.5rem 0 0.4rem;
        }

        .switch-text {
            font-size: 1rem;
        }

        .tt .toltip{display: none;}
        .tt:hover .toltip {
            display: block;
            margin: -6px 0 0 -37px;
            position: absolute;
            width: 90px;
        }


        .arrow_box {
            position: relative;
            background: #73b4f9;
            border: 2px solid #61a2e7;
            color: #fff;
            padding: 4px 7px;font-size: 12px;
            border-radius: 10px;
            margin: 10px 0 0 0;
        }

        .arrow_box a{ color: #fff; text-decoration: underline;}
        .arrow_box:after, .arrow_box:before {
            bottom: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(136, 183, 213, 0) rgba(136, 183, 213, 0) #73b4f9;
            border-width: 11px;
            margin-left: -11px;
        }
        .arrow_box:before {
            border-color:  rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #61a2e7;
            border-width: 13px;
            margin-left: -13px;
        }

        .modal {
            overflow-y: visible !important;
        }
    </style>
@endsection
@section('content')

    <main>
        <div class="container">
            <div class="row">
                <div class="col l6 m6">
                    <h5 class="RopaSans_font">Currently Viewing &amp; Editing: <span
                                class="push_dark_text">{{$pages->pagename ?? 'Page'}}</span></h5>
                </div>
                <div class="col l6 m6 margin-top-10">
                    <a href="/facebook/posts/{{$pages->id}}" class="btn btn_style push_dark">Back To All Posts</a>
                    <a href="/facebook/page-settings/{{$pages->id}}" class="btn btn_style push_dark">View Page Settings</a>
                    <a href="/user/reports?type=post&postId={{$id}}" class="btn btn_style push_dark">View Post Report</a>
                </div>
            </div>
            <br>
            <!--Summary of Activities stats start-->
            <!--Summary of Activities stats end-->


            <div class="row card-panel no-space">
                <div class="manage-mesage-comments">
                    <div class="col s12 m6">
                        <a class="card-panel push_dark mac col l12 m12 s12" style="cursor: pointer;">
                            <span class="white-text center"><h5>Set <strong class="strong_700"> Comments</strong></h5></span>
                        </a>
                    </div>
                    <div class="col s12 m6">
                        <a class="card-panel push_dark mam col l12 m12 s12" style="cursor: pointer;">
                            <span class="white-text center"><h5>Set <strong class="strong_700">Private Messages</strong></h5></span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col l12 m12 s12 mac_div " style="display: none;">
                    <div class="row">
                        <div class="col l12 m12 s12">
                            <div class="card-panel">
                                <div class="row">
                                    <span class="left"><h5 class="RopaSans_font">Set Comments for Everyone</h5></span>
                                    <span class="right"><a class="btn btn_style push_dark darken-4 modal-trigger"
                                                           href="#modal2" onclick="callModal2('giveaway');"><i
                                                    class="icon-plus"></i><span> New</span></a></span>
                                </div>
                                <div class="row">
                                    <div class="scroll">
                                        <div class="col l12 m10 s10">
                                            <div id="addfanscmt">
                                            </div>
                                            @if($fanscomments)
                                                @foreach($fanscomments as $fans)
                                                    @if($data)
                                                        <div class="comment-div card-panel grey lighten-5 z-depth-1">
                                                            <div class="row valign-wrapper">
                                                                <div class="col s2 center">
                                                                    <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="Post Image" class="circle responsive-img">
                                                                </div>
                                                                <div class="col s10">
                                                                    <div class="black-text">
                                                                        <a class="push_dark_text right" href="javascript:void(0);" id="{{$fans->id}}" onclick="deletecomment(this.id)" style="cursor:pointer"><i class="icon-close"></i></a>
                                                                        <h6>{{$data->pagename}}</h6>
                                                                        <span id="comm-fan-{{$fans->id}}">{{$fans->comment}}</span>
                                                                    </div>
                                                                    <div class="row no-space">
                                                                        <a id="comments-fans-{{$fans->id}}" onclick='select(this.id,"fans-update");' class="push_dark_text modal-trigger right" href="#modal2"><i class="icon-pencil"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col l12 m12 s12 mam_div " style="display: none;">
                    <div class="row">
                        <div class="col l12 m12 s12">
                            <div class="card-panel">
                                <div class="row">
                                    <span class="left"><h5 class="RopaSans_font">Set Messages for Everyone</h5></span>
                                    <span class="right"><a id="fans"
                                                           class="btn btn_style push_dark darken-4 modal-trigger"
                                                           href="#modal1"
                                                           onclick="callModal('giveaway');"><i
                                                    class="icon-plus"></i><span> New</span></a></span>
                                </div>
                                <div class="row">
                                    <div class="scroll">
                                        <div class="col l12 m10 s10">
                                            <div id="addfanmsg">
                                            </div>
                                            @if($fansmsg)
                                                @foreach($fansmsg as $fans)
                                                    @if($data)
                                                        <div class="message-div card-panel grey lighten-5 z-depth-1">
                                                            <div class="row valign-wrapper">
                                                                <div class="col s2 center">
                                                                    <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="Post Image" class="circle responsive-img">
                                                                </div>
                                                                <div class="col s10">
                                                                    <div class="black-text">
                                                                        <a class="push_dark_text right" href="javascript:void(0);" id="{{$fans->id}}" onclick="deletemessage(this.id)" style="cursor:pointer"><i class="icon-close"></i></a>
                                                                        <h6>{{$data->pagename}}</h6>
                                                                        <span id="mess-fan-{{$fans->id}}">{{$fans->message}}</span>
                                                                    </div>
                                                                    <div class="row no-space">
                                                                        <a id="messages-fans-{{$fans->id}}"
                                                                           onclick='select(this.id,"giveaway-update");'
                                                                           class="push_dark_text modal-trigger right"
                                                                           href="#modal1" style="cursor:pointer"><i class="icon-pencil"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a href="#modal1" id="callToModel" class="right modal-trigger"></a>
        <a href="#modal2" id="callToModel2" class="right modal-trigger"></a>

    </main>
    <!-- edit Messages fan modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content" id="closepop">
            <a href="javascript:void(0);"
               class="modal-action modal-close waves-effect waves-green btn-flat right"><i class="fa fa-close"></i></a>
            <h4 class="RopaSans_font push_dark_text">Draft Your Auto Message</h4>
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="row card no-space lighten-5">
                        <div class="input-field col s12 m12 l12">
                            <textarea id="message" class="materialize-textarea"></textarea>
                            <div class="center">
                                <span id="spnError1" style="color: Red; display: none">Please Enter Your Message.*</span>
                            </div>
                            <label for="message" class=""> <i class="icon-pencil right"></i>Enter Your Messages here
                            </label>
                        </div>
                        <div class="col s12 m12 l12 margin-bottom-10">
                            <a href="#" id="save"
                               class=" btn push_dark text_style_none col s12"
                               onclick="sendmessage();"><strong class="strong_700">Save</strong> Message</a></a>
                        </div>
                    </div>
                    @if($data)
                        <ul class="collection">
                            <li class="collection-item avatar">
                                <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt=""
                                     class="circle">
                                <span class="title"><a href=""
                                                       class="push_dark_text RopaSans_font">{{$data->pagename}}</a></span>
                                <p id="message_text" style="word-wrap: break-word;"></p>
                                <a href="javascript:void(0);" class="secondary-content"><i
                                            class="icon-star blue-text text-darken-3"></i></a>
                            </li>
                        </ul>
                    @endif
                    <input id="message-type" type="hidden" value="">
                </div>
                <div class="col m6 l6 s12">
                    {{--<div class="card-panel padding_10 no-margin filterDiv">--}}
                        {{--<h5 class="center push_dark_text no-space"><strong class="strong_700">Filter Words</strong></h5>--}}
                        {{--<hr>--}}
                        {{--<div class="input-field col s12" style="margin-top: .5rem; padding: 0;">--}}
                            {{--<div class="tt" style="top: 12px; position: absolute; right: 13px;">--}}
                                {{--<i class="icon-question push_dark_text add-filters" ></i>--}}
                                {{--<div class="toltip">--}}
                                    {{--<div class="arrow_box">--}}
                                        {{--<a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>--}}
                                        {{--<div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to NOT to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<i class="icon-plus push_dark_text add-filters" style="position: absolute;right: 35px;top: 15px;"></i>--}}
                            {{--<input id="msg_filter_word" type="text" class="validate" style="margin: 0 0 7px 0;">--}}
                            {{--<label for="msg_filter_word" style="left: 0;">Filter Word</label>--}}
                        {{--</div>--}}
                        {{--<div id="message-filters" class="filter-list"></div>--}}
                    {{--</div>--}}
                    <div class="card-panel padding_10 no-margin push_dark">
                        <h5 class="center white-text no-space"><strong class="strong_700">Customize</strong> Your Message</h5>
                        <hr>
                        <div class="white-text">
                            <h6 style="font-weight: 300;">Simply use below short codes and full name or first name will be dynamically added to your reply.</h6>
                            <span><?php echo '{{FacebookName}} or {{fb_first_name}}';?></span>
                            <h6 style="font-weight: 300;"><i>Copy paste in your message to display persons full name on your Message.</i></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- edit Comments fan modal Structure -->
    <div id="modal2" class="modal">
        <div class="modal-content" id="closepopcmts">
            <a href="javascript:void(0);"
               class="modal-action modal-close waves-effect waves-green btn-flat right"><i class="fa fa-close"></i></a>
            <h4 class="RopaSans_font push_dark_text">Draft Your Auto Comment</h4>
            <div class="row">
                <div class="col s12 m6 l6">
                    <div class="row card no-space lighten-5">
                        <div class="input-field col s12 m12 l12">
                            <textarea id="comment" class="materialize-textarea"></textarea>
                            <div class="center">
                                <span id="spnError2" style="color: Red; display: none">Please Enter your comment.*</span>
                            </div>
                            <label for="comment" class=""> <i class="icon-pencil right"></i>Enter Your Comment here
                            </label>
                        </div>
                        <div class="col s12 m12 l12 margin-bottom-10">
                            <a href="#" id="commentsave" onclick="sendcomments();"
                               class="btn push_dark text_style_none col s12"><strong class="strong_700">Save</strong> Comment</a>
                        </div>
                    </div>
                    @if($data)
                        <ul class="collection">
                            <li class="collection-item avatar">
                                <img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt=""
                                     class="circle">
                                <span class="title"><a href="" class="push_dark_text RopaSans_font">{{$data->pagename}}</a></span>
                                <p id="comment_text" style="word-wrap: break-word;"></p>
                                <a href="javascript:void(0);" class="secondary-content"><i
                                            class="icon-star blue-text text-darken-3"></i></a>
                            </li>
                        </ul>
                    @endif
                    <input id="comment-type" type="hidden" value="fans">
                </div>
                <div class="col m6 l6 s12">
                    {{--<div class="card-panel padding_10 no-margin filterDiv">--}}
                        {{--<h5 class="center push_dark_text no-space"><strong class="strong_700">Filter Words</strong></h5>--}}
                        {{--<hr>--}}
                        {{--<div class="input-field col s12" style="margin-top: .5rem; padding: 0;">--}}
                            {{--<div class="tt" style="top: 12px; position: absolute; right: 13px;">--}}
                                {{--<i class="icon-question push_dark_text add-filters" ></i>--}}
                                {{--<div class="toltip">--}}
                                    {{--<div class="arrow_box">--}}
                                        {{--<a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>--}}
                                        {{--<div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to NOT to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<i class="icon-plus push_dark_text add-filters" style="position: absolute;right: 35px;top: 15px;"></i>--}}
                            {{--<input id="cmt_filter_word" type="text" class="validate" style="margin: 0 0 7px 0;">--}}
                            {{--<label for="cmt_filter_word" style="left: 0;">Filter Word</label>--}}
                        {{--</div>--}}
                        {{--<div id="comment-filters" class="filter-list"></div>--}}
                    {{--</div>--}}
                    <div class="card-panel padding_10 no-margin push_dark">
                        <h5 class="center white-text no-space"><strong class="strong_700">Customize</strong> Your Comment</h5>
                        <hr>
                        <div class="white-text">
                            <h6 style="font-weight: 300;">Simply use below short codes and full name or first name will be dynamically added to your reply.</h6>
                            <span><?php echo '{{FacebookName}} or {{fb_first_name}}';?></span>
                            <h6 style="font-weight: 300;"><i>Copy paste in your message to display persons full name on your comment.</i></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script_function')
    <script>
        $('.waves-green').removeClass('waves-green');
        $('#message').on('keyup', function () {
            $('#message_text').text($(this).val());
            $("#spnError1").css("display", "none");
        });
        $('#comment').on('keyup', function () {
            $('#comment_text').text($(this).val());
            $("#spnError2").css("display", "none");
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document.body).on('click', '#save', function (e) {
                // alert("hhgfhgfg");
                e.preventDefault();
                var message = $('#message').val();
                $("#spnError1").css("display", "none");
                //if({$fanmsg->fan_type)
                if (message == null || message == "") {
                    $("#spnError1").css("display", "block");
                    return false;
                }
            });

        });

        $(document).ready(function () {
            $(document.body).on('click', '#commentsave', function (e) {
                // alert("fsfsfd");
                e.preventDefault();
                var comment = $('#comment').val();
                $("#spnError2").css("display", "none");
                if (comment == null || comment == "") {
                    // alert("bbbbb");
                    $("#spnError2").css("display", "block");
                    return false;
                }
            });

        });


        $('.dropdown-button').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: false, // Does not change width of dropdown to that of the activator
                    hover: true, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: false, // Displays dropdown below the button
                    alignment: 'right' // Displays dropdown with edge aligned to the left of button
                }
        );

        function callModal(type) {
            $('#closepop').show().find('label').attr('class', '');
            $('#spnError1').hide();
            $('#message').val('');
            $('#message_text').text('');
            $('#materialize-lean-overlay-1').show();
            $('#modal1').find('input#message-type').val(type);
            $('#msg_filter').val('');
//            $('#modal1').find('.filter-list').html('<div><br><br><br></div>');
        }

        function callModal2(type) {
            $('#closepopcmts').show().find('label').attr('class', '');
            $('#spnError2').hide();
            $('#comment').val('');
            $('#comment_text').text('');
            $('#materialize-lean-overlay-1').show();
            $('#modal2').find('input#comment-type').val(type);
            $('#cmt_filter').val('');
//            $('#modal2').find('.filter-list').html('<div><br><br><br></div>');
        }

        // div toogle
        $(document).ready(function () {
            $(".mam").click(function () {
                $(".mac_div").hide();
                $(".mac").removeClass('activeButton');
                $(".mam_div").show();
                $(".mam").addClass('activeButton');
            });
            $(".mac").click(function () {
                $(".mam_div").hide();
                $(".mam").removeClass('activeButton');
                $(".mac_div").show();
                $(".mac").addClass('activeButton');
            });
        });

        function sendmessage() {
            var status = $('#message-type').val();
            if (status) {
                var id = '<?php echo $id ?>';
                var type = status;
                var message = $("#message").val();
//                var msg_filter = $("#msg_filter").val();
                var msg_filter='';
                if ($('#modal1').find('.filter-list').children().length > 0 ) {
                    $.each($('#modal1').find('.filter-list').children(),function(i,v){
                        if($(this).text().trim()!='')
                            msg_filter +=$(this).text().trim()+",";
                    });
                }else{
                    msg_filter=null;
                }
                if($.trim(message)!=='') {
//                    $('#modal1').modal('close');
                    $('#modal1').closeModal();
                    $.ajax({
                        url: '/user/post-messages',
                        data: {
                            for_post_id: id,
                            fan_type: type,
                            message: message,
                            msg_filter: msg_filter,
                            type: 'post'
                        },
                        type: 'post',
                        datatype: 'json',
                        success: function (response) {
                            response = $.parseJSON(response);
                            if (response['status'] == 200) {
                                var html = '';
                                if (type == 'giveaway') {
                                    $('#closepop').hide();
                                    $('#materialize-lean-overlay-1').hide();
                                    var data = "'fans-update'";
                                    html +='<div class="message-div card-panel grey lighten-5 z-depth-1"><div class="row valign-wrapper"><div class="col s2 center">';
                                    html +='<img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="Post Image" class="circle responsive-img">';
                                    html +='</div><div class="col s10"><div class="black-text">';
                                    html+='<a class="push_dark_text right" href="javascript:void(0);" id="' + response['id'] + '" onclick="deletemessage(this.id)" style="cursor:pointer"><i class="icon-close"></i></a>';
                                    html+='<h6>{{$data->pagename}}</h6><span id="mess-fan-' + response['id'] + '">' + response['message'] + '</span></div><div class="row no-space">';
                                    html+='<a id="messages-fans-' + response['id'] + '" onclick="select(this.id,' + data + ');" class="push_dark_text modal-trigger right" href="javascript:void(0);" style="cursor:pointer">';
                                    html +='<i class="icon-pencil"></i></a></div></div></div></div>';
                                    $('#addfanmsg').prepend(html);
                                } else if (type == "nonfans") {
                                    $('#closepop').hide();
                                    $('#materialize-lean-overlay-1').hide();
                                    var data1 = "'nonfans-update'";
                                    html +='<div class="message-div card-panel grey lighten-5 z-depth-1"><div class="row valign-wrapper"><div class="col s2 center">';
                                    html +='<img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="Post Image" class="circle responsive-img">';
                                    html +='</div><div class="col s10"><div class="black-text">';
                                    html+='<a class="push_dark_text right" href="javascript:void(0);" id="' + response['id'] + '" onclick="deletemessage(this.id)" style="cursor:pointer"><i class="icon-close"></i></a>';
                                    html+='<h6>{{$data->pagename}}</h6><span id="mess-nonfan-' + response['id'] + '">' + response['message'] + '</span></div><div class="row no-space">';
                                    html+='<a id="messages-nonfans-' + response['id'] + '" onclick="select(this.id,' + data1 + ');" class="push_dark_text modal-trigger right" href="javascript:void(0);" style="cursor:pointer">';
                                    html +='<i class="icon-pencil"></i></a></div><div id="nonFanMsgFilter' + response['id'] + '">' + response['div'] + '</div> </div></div></div>';
                                    $('#addnonfanmsg').prepend(html);
                                }
                            }
                            else if (response['status'] == 198) {
                                $('#closepop').show();
                            }
                            else if (response['status'] == 201) {
                                $("#mess-fan-" + response['id']).html(response['message']);
                                $('#FanMsgFilter'+ response['id']).html(response['div']);
                            }
                            else if (response['status'] == 202) {
                                $("#mess-nonfan-" + response['id']).html(response['message']);
                                $('#nonFanMsgFilter'+response['id']).html(response['div']);
                            }
                        }
                    });
                }else{
                    $("#spnError1").css("display", "block");
                }
            }
        }

        function sendcomments() {
            var status = $('#comment-type').val();
            if (status) {
                var id = '<?php echo $id ?>';
                var type = status;
                var message = $("#comment").val();
//                var cmt_filter = $("#cmt_filter").val();
                var cmt_filter='';
                if ($('#modal2').find('.filter-list').children().length > 0 ) {
                    $.each($('#modal2').find('.filter-list').children(),function(i,v){
                        if($(this).text().trim()!='')
                            cmt_filter +=$(this).text().trim()+",";
                    });
                }else{
                    cmt_filter=null;
                }
                if($.trim(message)!=='') {
//                    $('#modal2').modal('close');
                    $('#modal2').closeModal();
                    $.ajax({
                        url: '/user/post-comments',
                        data: {
                            for_post_id: id,
                            fan_type: type,
                            comment: message,
                            cmt_filter: cmt_filter,
                            type: 'post'
                        },
                        type: 'post',
                        datatype: 'json',
                        success: function (response) {
                            response = $.parseJSON(response);
                            if (response['status'] == 200) {
                                var html = '';
                                if (type == 'giveaway') {
                                    $('#closepopcmts').hide();
                                    $('#materialize-lean-overlay-1').hide();
                                    $('#materialize-lean-overlay-2').hide();
                                    var data = "'fans-update'";
                                    html +='<div class="comment-div card-panel grey lighten-5 z-depth-1"><div class="row valign-wrapper"><div class="col s2 center">';
                                    html +='<img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="Post Image" class="circle responsive-img">';
                                    html +='</div><div class="col s10"><div class="black-text">';
                                    html +='<a class="push_dark_text right" href="javascript:void(0);" id="' + response['id'] + '" onclick="deletecomment(this.id)" style="cursor:pointer"><i class="icon-close"></i></a>';
                                    html +='<h6>{{$data->pagename}}</h6><span id="comm-fan-' + response['id'] + '">' + response['message'] + '</span></div><div class="row no-space">';
                                    html +='<a id="comments-fans-' + response['id'] + '" onclick="select(this.id,' + data + ');" class="push_dark_text modal-trigger right" href="javascript:void(0);"><i class="icon-pencil"></i></a></div></div></div></div>';
                                    $('#addfanscmt').prepend(html);
                                } else if (type == "nonfans") {
                                    $('#closepopcmts').hide();
                                    $('#materialize-lean-overlay-1').hide();
                                    $('#materialize-lean-overlay-2').hide();
                                    var data1 = "'nonfans-update'";
                                    html +='<div class="comment-div card-panel grey lighten-5 z-depth-1"><div class="row valign-wrapper"><div class="col s2 center">';
                                    html +='<img src="https://graph.facebook.com/{{$data->pageid}}/picture?type=small" alt="Post Image" class="circle responsive-img">';
                                    html +='</div><div class="col s10"><div class="black-text">';
                                    html +='<a class="push_dark_text right" href="javascript:void(0);" id="' + response['id'] + '" onclick="deletecomment(this.id)" style="cursor:pointer"><i class="icon-close"></i></a>';
                                    html +='<h6>{{$data->pagename}}</h6><span id="comm-nonfan-' + response['id'] + '">' + response['message'] + '</span></div><div class="row no-space">';
                                    html +='<a id="comments-nonfans-' + response['id'] + '" onclick="select(this.id,' + data1 + ');" class="push_dark_text modal-trigger right" href="javascript:void(0);"><i class="icon-pencil"></i></a></div><div id="nonFanCmtFilter' + response['id'] + '">' + response['div'] + '</div></div></div></div>';
                                    $('#addnonfanscmt').prepend(html);
                                }
                            }
                            else if (response['status'] == 198) {
                                $('#closepopcmts').show();
                            }
                            else if (response['status'] == 201) {
                                $("#comm-fan-" + response['id']).html(response['message']);
                                $('#FanCmtFilter'+ response['id']).html(response['div']);
                            }
                            else if (response['status'] == 202) {
                                $("#comm-nonfan-" + response['id']).html(response['message']);
                                $('#nonFanCmtFilter'+ response['id']).html(response['div']);
                            }

                        }
                    });
                }else{
                    $("#spnError2").css("display", "block");
                }
            }
        }

        function changeStatuspost(type) {
            if (type) {
                var idObj = $('#' + type);
                var status;
                if (idObj.prop("checked") == true) {
                    status = idObj.val();
                } else {
                    status = 'off';
                }
                var id = '<?php echo $id ?>';
                $.ajax({
                    url: '/user/post-status',
                    data: {
                        for_page_id: id,
                        status: status,
                        type: type
                    },
                    type: 'post',
                    datatype: 'json',
                    success: function (response) {
                        response = $.parseJSON(response);
                        if (response['status'] == 200 && type == 'fans') {
                            $("#success-comments").html('Comment added for fans').show();
                        }
                        else if (response['status'] == 200 && type == 'nonfans') {
                            $("#success-comments").html('Comment added for nonfans').show();
                        }
                        else if (response['status'] == 200 && type == 'global') {
                            $("#success-comments").html('Comment added Global').show();
                        }
                    }
                });
            }
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal-trigger').leanModal();
            // $('.modal-trigger-log-in').leanModal();


            $('.modal-trigger').leanModal({
                        dismissible: true // Modal can be dismissed by clicking outside of the modal
                        // Opacity of modal background
                        // Ending top style attribute
                        // ready: function() { alert('Ready'); }, // Callback for Modal open
                        // complete: function() { alert('Closed'); } // Callback for Modal close
                    }
            );
        });

        $('#fanmsg').on('click', function () {
            alert('dfgdf');
            console.log("me");
            return false;

            var message = $('#addfanscmt').val();
            $.ajax({
                url: '/user/deletemsg',
                data: {
                    message: message,
                    type: 'post'
                }
            })
        });

        function deletemessage(id) {
            var element = $('#'+id).parents('.message-div:first');
            var message = $('#fanmsgid').val();
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: '/user/deletePostMsg',
                                data: {
                                    message: message,
                                    id: id,
                                    type: 'page'
                                },
                                type: 'post',
                                dataType: 'json',
                                success: function (response) {
                                    console.log(response);
                                    if (response.code === 200) {
                                        $(element).remove();
                                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                    } else {
//                            alert(response.message);
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
        }

        function deletecomment(id) {
            //alert(id);
            var element = $('#'+id).parents('.comment-div:first');
            var message = $('#fanmsgid').val();
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: '/user/deletePostComment',
                                data: {
                                    message: message,
                                    id: id,
                                    type: 'page'
                                },
                                type: 'post',
                                dataType: 'json',
                                success: function (response) {
                                    console.log(response);
                                    if (response.code === 200) {
                                        $(element).remove();
                                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                    } else {
//                                    alert(response.message);
                                    }
                                }
                            });
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
        }

        function select(id, update) {
            var str = id.split('-');
            $('#spnError2,#spnError1').hide();
            $('#modal1,#modal2').find('label').addClass('active');
            var filters='';
            $.ajax({
                url: '/user/post-select',
                data: {
                    id: id,
                    type: 'post'
                },
                type: 'post',
                datatype: 'json',
                success: function (data) {
                    data = $.parseJSON(data);
                    if (data['status'] == 200) {
                        $('#message').val(data['message'].message);
                        $('#modal1').find('input#message-type').val(update + '-' + data['message'].id);
                        $('#message_text').html(data['message'].message);
//                        $('#msg_filter').val(data['message'].msg_filter);
                        filters = data['message'].msg_filter.split(",");
                    } else if (data['status'] == 201) {
                        $('#comment').val(data['message'].comment);
                        $('#modal2').find('input#comment-type').val(update + '-' + data['message'].id);
                        $('#comment_text').html(data['message'].comment);
//                        $('#cmt_filter').val(data['message'].cmt_filter);
                        filters = data['message'].cmt_filter.split(",");
                    }
                    if (str[0] == 'messages') {
                        var messageFilter=$('#modal1').find('.filter-list');
                        messageFilter.html('<div><br><br><br></div>');
                        $.each(filters,function(i,v){
                            if(v!=="") {
                                messageFilter.append('<div class="chip">' + v + ' <i class="close icon-close delete-filter"></i></div>');
                            }
                        });
                        $(document.body).find('#callToModel').trigger('click');
                        $('#closepop').attr("style", "display:block").find('label').attr('class', 'active');
                    }
                    else {
                        var commentFilter=$('#modal2').find('.filter-list');
                        commentFilter.html('<div><br><br><br></div>');
                        $.each(filters,function(i,v){
                            if(v!=="") {
                                commentFilter.append('<div class="chip">' + v + ' <i class="close icon-close delete-filter"></i></div>');
                            }
                        });
                        $(document.body).find('#callToModel2').trigger('click');
                        $('#closepopcmts').attr("style", "display:block").find('label').attr('class', 'active');
                    }
                    $('#postRequestData').html(data);
                }
            });

        }
    </script>

    <script>
        //wrote by raushan
        $(document).ready(function(){
            $('.add-filters').on('click',function(){
                var parent = $(this).parents('.filterDiv:first');
                var filter=$(parent).find("input").val().trim();
                if(filter !=''){
                    filter=filter.split(',');
                    $.each(filter,function (i,val) {
                        val=val.trim();
                        if(val !='') {
                            $(parent).find('.filter-list').append('<div class="chip">' + val + ' <i class="close icon-close delete-filter"></i></div>');
                        }
                    });
                    $(parent).find("input").val('');
                }
            });

            $(document).on('click','.delete-filter',function(){
                $(this).parent().remove();
            });
        });
        $('#msg_filter_word,#cmt_filter_word').keypress(function (e) {
            if (e.which == 13) {
                $(this).siblings().trigger('click');
            }
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.more-info').on('mouseover',function(){
                $(this).addClass('hide');
                $(this).parents('.toltip:first').css('width','300px').css('margin','-6px 0 0 -143px');
                $(this).siblings('.more-info-text').removeClass('hide');
            });
            $('.more-info-text').on('mouseout',function(){
                $(this).addClass('hide');
                $(this).parents('.toltip:first').css('width','90px').css('margin','-6px 0 0 -37px');
                $(this).siblings('.more-info').removeClass('hide');
            });
        });
    </script>
@endsection
