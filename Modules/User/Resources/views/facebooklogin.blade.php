<!DOCTYPE html>
<html>
<head>
    {{--<title>Facebook Login</title>--}}
    {{--<meta charset="UTF-8">--}}
</head>
<body>
<h1>click here to login to your facebook account</h1>
<button onclick="myFacebookLogin()">Login Here</button>
<input id="fbid" type="text" value="{{env('FACEBOOK_APP_ID')}}" style="display: none;"/>
<script>
    var fbid=$('#fbid').val();
    window.fbAsyncInit = function () {
        FB.init({
            appId: fbid,
            xfbml: true,
            cookie: true,
            version: 'v10.v'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function myFacebookLogin() {

        //facebook login.......

        FB.login(function (response) {
            console.log('response',response);

            //welcome message in console
            console.log('Welcome!  you are logged in.... ');
            if (response.status === "connected") {
                r_token = response.authResponse.accessToken;
                fb_id = response.authResponse.userID;
                exp_in = response.authResponse.expiresIn;

                //fetching response indivisually
                console.log(r_token);
                console.log(fb_id);
                console.log(exp_in);
            }

            //fetching facebook username
            FB.api(response.authResponse.userID, function (response) {
                console.log('response',response);
                if (response && !response.error) {
                    fb_name = response.name;
                    console.log(fb_name);
                }
            });

        }, {scope: 'manage_pages,publish_pages,publish_actions,read_page_mailboxes,public_profile'});
    }
</script>
</body>
</html>