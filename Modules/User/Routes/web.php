<?php

use Illuminate\Support\Facades\Route;
use Modules\User\Http\Controllers\Authentication\EmailVerificationController;
use Modules\User\Http\Controllers\Authentication\LoginController;
use Modules\User\Http\Controllers\Authentication\RegisterController;
use Modules\User\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('guest')->group(function() {

    Route::post('/authenticate', [LoginController::class, 'authenticate'])
        ->name('login.authenticate');

    Route::post('/store', [RegisterController::class, 'store'])
        ->name('register.store');

    Route::get('/verification-link/resend', [EmailVerificationController::class, 'resendVerificationLink'])
        ->name('register.verification.resend');

    Route::get('/verify', [EmailVerificationController::class, 'verifyUserEmail'])
        ->name('register.verify');
});
