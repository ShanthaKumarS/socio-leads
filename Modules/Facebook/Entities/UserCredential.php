<?php

namespace Modules\Facebook\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Database\factories\UserProfileFactory;

/**
 * Class UserCredential
 * @package Modules\Facebook\Entities
 */
class UserCredential extends Model
{
    protected $table = 'user_facebook_credentials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'facebook_user_id',
        'facebook_user_access_token',
        'is_facebook_password_changed',
        'user_id'
    ];
}
