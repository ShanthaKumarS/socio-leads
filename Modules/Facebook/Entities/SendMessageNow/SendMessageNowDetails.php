<?php

namespace Modules\Facebook\Entities\SendMessageNow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Facebook\Entities\Page;

/**
 * Class SendMessageNowDetails
 * @package Modules\Facebook\Entities\SendMessageNow
 */
class SendMessageNowDetails extends Model
{
    protected $table="send_message_now_details";

    protected $fillable = [
        'tag',
        'sent_count',
        'delivered_count'
    ];

    /**
     * @return HasMany
     */
    public function texts()
    {
        return $this->hasMany(Text::class,'send_message_now_detail_id');
    }

    /**
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class, 'send_message_now_detail_id');
    }

    /**
     * @return BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
