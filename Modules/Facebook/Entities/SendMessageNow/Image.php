<?php

namespace Modules\Facebook\Entities\SendMessageNow;

use Modules\Facebook\Entities\ImageInterface;

/**
 * Class Image
 * @package Modules\Facebook\Entities\SendMessageNow
 */
class Image extends \Modules\Facebook\Entities\SequenceMessage\Image implements ImageInterface
{
    protected $table = "send_message_now_images";

}
