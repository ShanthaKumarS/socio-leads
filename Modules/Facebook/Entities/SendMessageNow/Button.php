<?php

namespace Modules\Facebook\Entities\SendMessageNow;

use Modules\Facebook\Entities\ButtonInterface;

/**
 * Class Button
 * @package Modules\Facebook\Entities\SendMessageNow
 */
class Button extends \Modules\Facebook\Entities\SequenceMessage\Button implements ButtonInterface
{
    protected $table = "send_message_now_text_buttons";
}
