<?php

namespace Modules\Facebook\Entities\SendMessageNow;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Facebook\Entities\TextInterface;

/**
 * Class Text
 * @package Modules\Facebook\Entities\SendMessageNow
 */
class Text extends \Modules\Facebook\Entities\SequenceMessage\Text implements TextInterface
{
    protected $table = "send_message_now_texts";

    /**
     * @return HasMany
     */
    public function buttons()
    {
        return $this->hasMany(Button::class, 'send_message_now_text_id');
    }
}
