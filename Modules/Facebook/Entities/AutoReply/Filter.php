<?php

namespace Modules\Facebook\Entities\AutoReply;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Filter
 * @package Modules\Facebook\Entities\AutoReply
 */
class Filter extends Model
{
    /**
     * table name
     *
     * @var string
     */
    protected $table = "fb_auto_filters";

    /**
     * @var array
     */
    protected $fillable = [
        'filter_text'
    ];

    /**
     * @return MorphTo
     */
    public function filterable()
    {
        return $this->morphTo();
    }
}
