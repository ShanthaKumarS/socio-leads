<?php


namespace Modules\Facebook\Entities\AutoReply;

use Illuminate\Database\Eloquent\Model;
use Modules\Facebook\Entities\ReportInterface;

class PostLikesReport extends Model implements ReportInterface
{
    public static function getInstance()
    {
        return new PostLikesReport();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'likes_count'
    ];
}
