<?php


namespace Modules\Facebook\Entities\AutoReply;

use Illuminate\Database\Eloquent\Model;
use Modules\Facebook\Entities\ReportInterface;

/**
 * Class PostMessagesReport
 * @package Modules\Facebook\Entities\AutoReply
 */
class PostMessagesReport extends Model implements ReportInterface
{
    /**
     * @return PostMessagesReport
     */
    public static function getInstance()
    {
        return new PostMessagesReport();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'messages_count'
    ];
}
