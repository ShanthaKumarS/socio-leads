<?php
namespace Modules\Facebook\Entities\AutoReply;

use Illuminate\Database\Eloquent\Model;
use Modules\Facebook\Entities\ReportInterface;
use Modules\User\Database\factories\UserCredentialFactory;

/**
 * Class PostCommentsReport
 * @package Modules\Facebook\Entities\AutoReply
 */
class PostCommentsReport extends Model implements ReportInterface
{
    /**
     * @return PostCommentsReport
     */
    public static function getInstance()
    {
        return new PostCommentsReport();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comments_count'
    ];

}
