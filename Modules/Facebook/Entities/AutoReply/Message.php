<?php

namespace Modules\Facebook\Entities\AutoReply;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Message
 * @package Modules\Facebook\Entities\AutoReply
 */
class Message extends Model
{
    /**
     * @var string
     */
    protected $table = "fb_auto_messages";

    /**
     * @var array
     */
    protected $fillable = [
        'text'
    ];

    /**
     * @return MorphTo
     */
    public function messageable()
    {
        return $this->morphTo();
    }

    /**
     * @return MorphMany
     */
    public function filters()
    {
        return $this->morphMany(Filter::class, 'filterable');
    }

}
