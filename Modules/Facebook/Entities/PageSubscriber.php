<?php

namespace Modules\Facebook\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageSubscriber
 * @package Modules\Facebook\Entities
 */
class PageSubscriber extends Model
{
    protected $table = "facebook_page_subscribers";

    /**
     * @var array
     */
    protected $fillable = [
        'facebook_page_subscriber_id',
        'is_subscribed',
        'subscribed_at'
    ];
}
