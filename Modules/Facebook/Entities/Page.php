<?php
namespace Modules\Facebook\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Facebook\Entities\AutoReply\Comment;
use Modules\Facebook\Entities\AutoReply\Message;
use Modules\Facebook\Entities\AutoReply\PostCommentsReport;
use Modules\Facebook\Entities\AutoReply\PostLikesReport;
use Modules\Facebook\Entities\AutoReply\PostMessagesReport;
use Modules\Facebook\Entities\BotMessage\Filter;
use Modules\Facebook\Entities\SequenceMessage\DateSequence;
use Modules\Facebook\Entities\SequenceMessage\RecurringSequence;
use Modules\Facebook\Entities\SequenceMessage\SequenceMessage;
use Modules\Facebook\Entities\SequenceMessage\StepSequence;

/**
 * Class Page
 * @package Modules\Facebook\Entities
 */
class Page extends Model
{
    /**
     * @var string
     */
    protected $table = 'facebook_pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_access_token',
        'facebook_page_id',
        'page_name',
        'subscription_message',
        'subscribe_button_name',
        'unsubscribe_button_name',
        'like_count',
        'cover_picture',
        'profile_picture',
        'do_reply_message_fan',
        'do_reply_message_non_fan',
        'do_like_fan',
        'do_like_non_fan',
        'do_reply_comment_fan',
        'do_reply_comment_non_fan',
        'do_broadcast_message',
    ];

    /**
     * Get the posts associated with the Page.
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'page_id');
    }

    /**
     * Get the subscriptionButtons associated with the Page.
     */
    public function botMessageFilter()
    {
        return $this->hasMany(Filter::class);
    }

    /**
     * Get the SetpSequence Messages associated with the Page.
     */
    public function stepSequences()
    {
        return $this->hasMany(StepSequence::class);
    }

    /**
     * Get the Sequence Messages associated with the Page.
     */
    public function dateSequences()
    {
        return $this->hasMany(DateSequence::class);
    }

    /**
     * Get the recurringSequences Messages associated with the Page.
     * @return HasMany
     */
    public function recurringSequences()
    {
        return $this->hasMany(RecurringSequence::class);
    }

    /**
     * @return MorphMany
     */
    public function autoComments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return MorphMany
     */
    public function autoMessages()
    {
        return $this->morphMany(Message::class, 'messageable');
    }

    /**
     * @return HasMany
     */
    public function pageSubscribers()
    {
        return $this->hasMany(PageSubscriber::class);
    }

    /**
     * Get the postMessageReports associated with the Page.
     */
    public function postMessageReports()
    {
        return $this->hasMany(PostMessagesReport::class, 'page_id');
    }

    /**
     * Get the postLikeReports associated with the Page.
     */
    public function postLikeReports()
    {
        return $this->hasMany(PostLikesReport::class, 'page_id');
    }

    /**
     * Get the postCommentReports associated with the Page.
     */
    public function postCommentReports()
    {
        return $this->hasMany(PostCommentsReport::class, 'page_id');
    }
}
