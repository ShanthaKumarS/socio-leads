<?php
namespace Modules\Facebook\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Facebook\Entities\AutoReply\Comment;
use Modules\Facebook\Entities\AutoReply\Message;
use Modules\Facebook\Entities\AutoReply\PostCommentsReport;
use Modules\Facebook\Entities\AutoReply\PostLikesReport;
use Modules\Facebook\Entities\AutoReply\PostMessagesReport;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'facebook_post_id',
        'facebook_post_date',
        'feed_picture',
        'feed_message',
        'feed_caption',
        'feed_title',
        'feed_story',
        'feed_description',
        'feed_source',
        'de_reply_message',
        'do_like_comment',
        'do_reply_comment',
    ];

    /**
     * @return BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    /**
     * @return MorphMany
     */
    public function autoComments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return MorphMany
     */
    public function autoMessages()
    {
        return $this->morphMany(Message::class, 'messageable');
    }

    /**
     * Get the postLikesReports associated with the Post.
     */
    public function postLikesReports()
    {
        return $this->hasMany(PostLikesReport::class);
    }

    /**
     * Get the postMessageReports associated with the Post.
     */
    public function postMessageReports()
    {
        return $this->hasMany(PostMessagesReport::class);
    }

    /**
     * Get the postCommentReports associated with the Post.
     */
    public function postCommentReports()
    {
        return $this->hasMany(PostCommentsReport::class);
    }
}
