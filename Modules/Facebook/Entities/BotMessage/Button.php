<?php
namespace Modules\Facebook\Entities\BotMessage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Button
 * @package Modules\Facebook\Entities\BotMessage
 */
class Button extends Model
{
    /**
     * @var string
     */
    protected $table = "bot_message_buttons";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'link'
    ];
}
