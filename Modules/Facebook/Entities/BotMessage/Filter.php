<?php

namespace Modules\Facebook\Entities\BotMessage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Filter
 * @package Modules\Facebook\Entities\BotMessage
 */
class Filter extends Model
{
    /**
     * @var string
     */
    protected $table = "bot_message_filters";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filter'
    ];

    /**
     * Get the imageMessage associated with the Filter.
     */
    public function imageMessage()
    {
        return $this->hasOne(ImageMessage::class, 'bot_image_message_id', 'bot_message_filter_id');
    }

    /**
     * Get the textMessage associated with the Filter.
     */
    public function textMessage()
    {
        return $this->hasOne(TextMessage::class, 'bot_message_filter_id');
    }

}
