<?php

namespace Modules\Facebook\Entities\BotMessage;

use Illuminate\Database\Eloquent\Model;
use Modules\Facebook\Entities\Button;

/**
 * Class TextMessage
 * @package Modules\Facebook\Entities\BotMessage
 */
class TextMessage extends Model
{
    /**
     * @var string
     */
    protected $table = "bot_text_messages";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text'
    ];

    /**
     * Get the posts associated with the Page.
     */
    public function buttons()
    {
        return $this->hasMany(Button::class, 'bot_message_button_id');
    }
}
