<?php

namespace Modules\Facebook\Entities\BotMessage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ImageMessage
 * @package Modules\Facebook\Entities\BotMessage
 */
class ImageMessage extends Model
{
    protected $table = "bot_image_messages";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path'
    ];

}
