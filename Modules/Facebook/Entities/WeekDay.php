<?php

namespace Modules\Facebook\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WeekDay
 * @package Modules\Facebook\Entities
 */
class WeekDay extends Model
{

    protected $fillable = [];
}
