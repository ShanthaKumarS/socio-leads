<?php

namespace Modules\Facebook\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageFansReport
 * @package Modules\Facebook\Entities
 */
class PageFansReport extends Model implements ReportInterface
{
    protected $table = "page_fans_reports";


    /**
     * @return PageFansReport
     */
    public static function getInstance()
    {
        return new PageFansReport();
    }

    /**
     * @var array
     */
    protected $fillable = [
        'date',
        'fan_count'
    ];
}
