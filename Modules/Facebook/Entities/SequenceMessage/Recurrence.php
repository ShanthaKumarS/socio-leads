<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Recurrence
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class Recurrence extends Model
{
    protected $table = "recurring_sequence_message_recurrences";

    public $timestamps = false;

    protected $fillable = [
        'week_day_id'
    ];

    /**
     * @return BelongsTo
     */
    public function sequenceMessage()
    {
        return $this->belongsTo(SequenceMessage::class);
    }

}
