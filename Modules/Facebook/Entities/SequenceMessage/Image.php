<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Modules\Facebook\Entities\ImageInterface;

/**
 * Class Image
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class Image extends Model implements ImageInterface
{
    protected $table = "sequence_message_images";

    protected $fillable = [
        'path'
    ];

}
