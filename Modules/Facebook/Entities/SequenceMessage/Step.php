<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Step
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class Step extends Model
{
    protected $table = "step_sequence_message_steps";

    public $timestamps = false;

    protected $fillable = ['count'];

    public function sequenceMessage()
    {
        return $this->belongsTo(SequenceMessage::class);
    }
}
