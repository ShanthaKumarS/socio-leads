<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Facebook\Entities\TextInterface;

/**
 * Class Text
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class Text extends Model implements TextInterface
{
    protected $table = "sequence_message_texts";

    protected $fillable = [
        'text'
    ];

    /**
     * @return HasMany
     */
    public function buttons()
    {
        return $this->hasMany(Button::class, 'sequence_message_text_id');
    }
}
