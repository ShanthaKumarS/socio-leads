<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Facebook\Entities\Page;

/**
 * Class Sequence
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class Sequence extends Model
{
    protected $fillable = ['name'];

    /**
     * @return MorphMany
     */
    public function sequenceMessages()
    {
        return $this->morphMany(SequenceMessage::class, 'sequence');
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
