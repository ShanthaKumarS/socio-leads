<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Date
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class Date extends Model
{
    protected $table = "date_sequence_message_dates";

    public $timestamps = false;

    protected $fillable = ['date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sequenceMessage()
    {
        return $this->belongsTo(SequenceMessage::class);
    }
}
