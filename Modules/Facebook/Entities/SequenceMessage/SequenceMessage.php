<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SequenceMessage
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class SequenceMessage extends Model
{
    protected $fillable = [
        'time_zone',
        'send_time',
        'tag'
    ];

    /**
     * @return HasOne
     */
    public function step()
    {
        return $this->hasOne(Step::class);
    }

    /**
     * @return HasOne
     */
    public function date()
    {
        return $this->hasOne(Date::class);
    }

    /**
     * @return HasMany
     */
    public function recurrences()
    {
        return $this->hasMany(Recurrence::class);
    }

    /**
     * @return HasMany
     */
    public function texts()
    {
        return $this->hasMany(Text::class);
    }

    /**
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function sequencer()
    {
        return $this->morphTo(__FUNCTION__, 'sequence_type', 'sequence_id');
    }
}
