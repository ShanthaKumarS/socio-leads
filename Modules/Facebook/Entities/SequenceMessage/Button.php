<?php

namespace Modules\Facebook\Entities\SequenceMessage;

use Illuminate\Database\Eloquent\Model;
use Modules\Facebook\Entities\ButtonInterface;

/**
 * Class Button
 * @package Modules\Facebook\Entities\SequenceMessage
 */
class Button extends Model implements ButtonInterface
{
    protected $table = "sequence_message_text_buttons";

    protected $fillable = [
        'name',
        'link'
    ];
}
