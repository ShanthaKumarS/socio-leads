<?php


namespace Modules\Facebook\Http\Requests;

/**
 * Interface SequenceMessageRequestInterface
 * @package Modules\Facebook\Http\Requests
 */
interface SequenceMessageRequestInterface
{
    /**
     * @return array
     */
    public function rules();
}
