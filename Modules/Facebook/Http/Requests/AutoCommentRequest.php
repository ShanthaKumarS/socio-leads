<?php

namespace Modules\Facebook\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AutoCommentRequest
 * @package Modules\Facebook\Http\Requests
 */
class AutoCommentRequest extends FormRequest implements AutoReplyInterface
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'required',
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
