<?php


namespace Modules\Facebook\Http\Requests;

/**
 * Interface AutoReplyInterface
 * @package Modules\Facebook\Http\Requests
 */
interface AutoReplyInterface
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules();
}
