<?php

namespace Modules\Facebook\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RecurringSequenceMessageRequest
 * @package Modules\Facebook\Http\Requests
 */
class RecurringSequenceMessageRequest extends FormRequest implements SequenceMessageRequestInterface
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time_zone' => 'required',
            'send_time' => 'required',
            'week_days' => 'required',
            'msg_tag' => 'required',
            'texts' => 'required_without_all:images',
            'images.*' => 'required_without_all:texts||mimes:jpg,jpeg,png',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
