<?php

namespace Modules\Facebook\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AutoMessageRequest
 * @package Modules\Facebook\Http\Requests
 */
class AutoMessageRequest extends FormRequest implements AutoReplyInterface
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required',
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
