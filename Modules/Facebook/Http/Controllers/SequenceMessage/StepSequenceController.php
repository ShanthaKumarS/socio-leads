<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\SequenceMessage\Step;
use Modules\Facebook\Entities\SequenceMessage\StepSequence;
use Modules\Facebook\Http\Requests\StepSequenceMessageRequest;
use Modules\Facebook\Repositories\SequenceMessageRepository;

/**
 * Class StepSequenceController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class StepSequenceController extends Controller
{
    /**
     * @var StepSequenceMessageRequest
     */
    private $request;

    /**
     * @var SequenceController
     */
    private $sequenceController;

    /**
     * @var StepSequence
     */
    private $stepSequence;

    /**
     * @var Step
     */
    private $step;


    /**
     * StepSequenceController constructor.
     *
     * @param SequenceController $sequenceController
     * @param StepSequence $stepSequence
     * @param Step $step
     */
    public function __construct(
        SequenceController $sequenceController,
        StepSequence $stepSequence,
        Step $step
    )
    {
        $this->sequenceController = $sequenceController;
        $this->stepSequence = $stepSequence;
        $this->step = $step;
    }

    /**
     * @param int $sequenceId
     * @return JsonResponse
     */
    public function index($sequenceId)
    {
        try {
            $stepSequence = SequenceMessageRepository::getStepSequenceById($sequenceId);
            $data['id'] =  $stepSequence->id;
            $data['name'] =  $stepSequence->name;
            $sequenceMessages = [];


            foreach ($stepSequence->sequenceMessages as $sequenceMessage) {
                $sequenceMessages[] = [
                    "messageId" => $sequenceMessage->id,
                    "step" => $sequenceMessage->step->count,
                    "totalSend" => 0, /** @todo update count*/
                    "deliveryCount" => 0 /** @todo update count*/
                ];
            }
            $data['sequenceMessages'] = $sequenceMessages;

            return Response::json($data, 200);
        } catch (Exception $e) {
            dd($e);
            return Response::json("Something Went Wrong", 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {

        try {
            $this->stepSequence->name = $request->name;
            $this->stepSequence->page_id = $request->pageId;

            $this->stepSequence->save();
            return Response::json($this->stepSequence, 200);

        } catch (Exception $e) {
            return Response::json("Something Went Wrong", 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $this->stepSequence->find($request->id)->delete();
            return Response::json("Sequence Deleted Succesfully", 200);

        } catch (Exception $e) {
            echo $e->getMessage();
            return Response::json("Something went Wrong", 500);
        }
    }

    /**
     * @param StepSequenceMessageRequest $request
     * @return JsonResponse
     */
    public function save(StepSequenceMessageRequest $request)
    {
        $this->request = $request;
        try {
            $stepSequence = $this->stepSequence->find($request->sequence_id);

            $this->sequenceController->storeMessage($request, $stepSequence, function ($sequenceMessage) {
                $this->step->count = $this->request->step;
                $this->step->send_at = $this->computeSendingDate($this->request);
                $sequenceMessage->step()->save($this->step);
            });

            $response = Response::json("Message inserted Successfully", 200);

        } catch (Exception $e) {
            $response = Response::json("failed to insert message", 500);
        }

        return $response;
    }

    /**
     * @param StepSequenceMessageRequest $request
     * @return \DateTime
     */
    public function computeSendingDate(StepSequenceMessageRequest $request)
    {
        $currentDate = Carbon::now(new \DateTimeZone($request->time_zone));
        $stepDate = $currentDate->add($request->steps, 'day');
        $messageSendingDate = new \DateTime($stepDate->toDateString() . $request->send_time, new \DateTimeZone($request->time_zone));

        return $messageSendingDate->setTimezone(new \DateTimeZone("UTC"));
    }
}
