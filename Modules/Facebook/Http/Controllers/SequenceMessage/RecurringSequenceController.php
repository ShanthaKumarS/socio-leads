<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\SequenceMessage\Recurrence;
use Modules\Facebook\Entities\SequenceMessage\RecurringSequence;
use Modules\Facebook\Entities\SequenceMessage\SequenceMessage;
use Modules\Facebook\Http\Requests\RecurringSequenceMessageRequest;
use Modules\Facebook\Repositories\SequenceMessageRepository;

/**
 * Class RecurringSequenceController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class RecurringSequenceController extends Controller
{
    /**
     * @var SequenceController
     */
    private $sequenceController;

    /**
     * @var Recurrence
     */
    private $recurrence;

    /**
     * @var RecurringSequenceMessageRequest
     */
    private $request;

    /**
     * @var RecurringSequence
     */
    private $recurringSequence;

    /**
     * StepSequenceController constructor.
     *
     * @param SequenceController $sequenceController
     * @param RecurringSequence $recurringSequence
     * @param Recurrence $recurrence
     */
    public function __construct(
        SequenceController $sequenceController,
        RecurringSequence $recurringSequence,
        Recurrence $recurrence
    )
    {
        $this->sequenceController = $sequenceController;
        $this->recurringSequence = $recurringSequence;
        $this->recurrence = $recurrence;
    }

    /**
     * @param int $sequenceId
     * @return JsonResponse
     */
    public function index($sequenceId)
    {
        try {
            $recurringSequence = SequenceMessageRepository::getRecurringSequenceById($sequenceId);
            $data['id'] =  $recurringSequence->id;
            $data['name'] =  $recurringSequence->name;
            $sequenceMessages = [];
            foreach ($recurringSequence->sequenceMessages as $sequenceMessage) {
                $days = [];
                foreach($sequenceMessage->recurrences as $recurrence) {
                    $days[] = Config::get('constants.WEEK_DAYS')[$recurrence->week_day_id];
                }

                $sequenceMessages[] = [
                    "messageId" => $sequenceMessage->id,
                    "days" => implode(',', $days),
                    "totalSend" => 0, /** @todo update count*/
                    "deliveryCount" => 0 /** @todo update count*/
                ];
            }

            $data['sequenceMessages'] = $sequenceMessages;
            $request =  Response::json($data, 200);
        } catch (Exception $e) {
            dd($e);
            $request =  Response::json("Something Went Wrong", 500);
        }

        return $request;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->recurringSequence->name = $request->name;
            $this->recurringSequence->page_id = $request->pageId;

            $this->recurringSequence->save();
            return Response::json($this->recurringSequence, 200);

        } catch (Exception $e) {
            return Response::json("Something Went Wrong", 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $this->recurringSequence->find($request->id)->delete();
            return Response::json("Sequence Deleted Succesfully", 200);

        } catch (Exception $e) {
            echo $e->getMessage();
            return Response::json("Something went Wrong", 500);
        }
    }

    /**
     * @param RecurringSequenceMessageRequest $request
     * @return JsonResponse
     */
    public function save(RecurringSequenceMessageRequest $request)
    {
        $this->request = $request;
        try {
            $recurringSequence = $this->recurringSequence->find($request->sequence_id);

            $this->sequenceController->storeMessage($request, $recurringSequence, function (SequenceMessage $sequenceMessage) {
                $recurrences = [];
                foreach ($this->request->week_days as $weekDay) {
                    $recurrence = clone $this->recurrence;
                    $recurrence->week_day_id = $weekDay;
                    $recurrence->send_at = $this->computeSendingDate($this->request, $weekDay);
                    $recurrences[] = $recurrence;
                }
                $sequenceMessage->recurrences()->saveMany($recurrences);
            });

            $response = Response::json("Message inserted Successfully", 200);

        } catch (Exception $e) {
            $response = Response::json("failed to insert message", 500);
        }

        return $response;
    }

    /**
     * @param RecurringSequenceMessageRequest $request
     * @param int $weekDay
     * @return \DateTime
     */
    public function computeSendingDate(RecurringSequenceMessageRequest $request, int $weekDay)
    {
        $currentTime = Carbon::now(new \DateTimeZone($request->time_zone));
        $messageTime = new Carbon($request->send_time, new \DateTimeZone($request->time_zone));

        if ($messageTime->isFuture($currentTime)) {
            if ($currentTime->is(Config::get('constants.WEEK_DAYS')[$weekDay])) {
                $sendAt = new \DateTime($currentTime->toDateString(), new \DateTimeZone($request->time_zone));
            } else {
                $currentTime->next(Config::get('constants.WEEK_DAYS')[$weekDay]);
                $sendAt = new \DateTime($currentTime->toDateString(), new \DateTimeZone($request->time_zone));
            }
        } else {
            $currentTime->next(Config::get('constants.WEEK_DAYS')[$weekDay]);
            $sendAt = new \DateTime($currentTime->toDateString(), new \DateTimeZone($request->time_zone));
        }

        return $sendAt->setTimezone(new \DateTimeZone("UTC"));
    }
}
