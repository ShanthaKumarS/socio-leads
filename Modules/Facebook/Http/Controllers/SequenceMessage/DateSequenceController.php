<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\SequenceMessage\Date;
use Modules\Facebook\Entities\SequenceMessage\DateSequence;
use Modules\Facebook\Entities\SequenceMessage\SequenceMessage;
use Modules\Facebook\Http\Requests\DateSequenceMessageRequest;
use Modules\Facebook\Repositories\SequenceMessageRepository;

/**
 * Class DateSequenceController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class DateSequenceController extends Controller
{
    /**
     * @var DateSequenceMessageRequest
     */
    private $request;

    /**
     * @var SequenceController
     */
    private $sequenceController;

    /**
     * @var DateSequence
     */
    private $dateSequence;

    /**
     * @var Date
     */
    private $date;


    /**
     * StepSequenceController constructor.
     *
     * @param SequenceController $sequenceController
     * @param DateSequence $dateSequence
     * @param Date $date
     */
    public function __construct(
        SequenceController $sequenceController,
        DateSequence $dateSequence,
        Date $date
    )
    {
        $this->sequenceController = $sequenceController;
        $this->dateSequence = $dateSequence;
        $this->date = $date;
    }

    /**
     * @param int $sequenceId
     * @return JsonResponse
     */
    public function index($sequenceId)
    {
        try {
            $dateSequence = SequenceMessageRepository::getDateSequencesById($sequenceId);
            $data['id'] =  $dateSequence->id;
            $data['name'] =  $dateSequence->name;
            $sequenceMessages = [];

            foreach ($dateSequence->sequenceMessages as $sequenceMessage) {
                /**
                 * @var SequenceMessage $sequenceMessage
                 */
                $sequenceMessages[] = [
                    "messageId" => $sequenceMessage->id,
                    "date" => $sequenceMessage->date->date,
                    "totalSend" => $sequenceMessage->date->sent_count,
                    "deliveryCount" => $sequenceMessage->date->delivered_count
                ];
            }

            $data['sequenceMessages'] = $sequenceMessages;
            return Response::json($data, 200);
        } catch (Exception $e) {
            dd($e);
            return Response::json("Something Went Wrong", 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {

        try {
            $this->dateSequence->name = $request->name;
            $this->dateSequence->page_id = $request->pageId;

            $this->dateSequence->save();
            return Response::json($this->dateSequence, 200);

        } catch (Exception $e) {
            return Response::json("Something Went Wrong", 500);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            $this->dateSequence->find($request->id)->delete();
            return Response::json("Sequence Deleted Succesfully", 200);

        } catch (Exception $e) {
            echo $e->getMessage();
            return Response::json("Something went Wrong", 500);
        }
    }

    /**
     * @param DateSequenceMessageRequest $request
     * @return JsonResponse
     */
    public function save(DateSequenceMessageRequest $request)
    {
        $this->request = $request;
        try {
            $dateSequence = $this->dateSequence->find($request->sequence_id);
            $this->sequenceController->storeMessage($request, $dateSequence, function (SequenceMessage &$sequenceMessage) {
                $this->date->date = new \DateTime($this->request->date);
                $this->date->send_at = $this->computeSendingDate($this->request);
                $sequenceMessage->date()->save($this->date);
            });

            $response = Response::json("Message inserted Successfully", 200);

        } catch (Exception $e) {
            $response = Response::json("failed to insert message", 500);
        }

        return $response;
    }

    /**
     * @param DateSequenceMessageRequest $request
     * @return \DateTime
     */
    public function computeSendingDate(DateSequenceMessageRequest $request)
    {
        $messageSendingDate = new \DateTime(  $request->date . $request->send_time, new \DateTimeZone($request->time_zone));
        return $messageSendingDate->setTimezone(new \DateTimeZone("UTC"));
    }
}
