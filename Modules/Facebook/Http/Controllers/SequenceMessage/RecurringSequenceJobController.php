<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Modules\Facebook\Jobs\RecurringSequenceMessageJob;

/**
 * Class RecurringSequenceJobController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class RecurringSequenceJobController extends SequenceJobController
{
    /**
     * @param array $data
     * @return bool
     */
    public function scheduleMessage(array $data)
    {
        $dateSequenceMessagesJob = new RecurringSequenceMessageJob($data);
        $dateSequenceMessagesJob = $dateSequenceMessagesJob->onQueue('recurringSequence');
        $this->dispatch($dateSequenceMessagesJob, true);

        return true;
    }

    /**
     * @param $deliveredCount
     */
    protected function updateSequenceMessageDelivered($deliveredCount)
    {
        $currentTime = Carbon::now(new \DateTimeZone($this->sequence->sequenceMessage->time_zone));
        $currentTime->next(Config::get('constants.WEEK_DAYS')[$this->sequence->week_day]);
        $sendAt = new \DateTime($currentTime->toDateString(), new \DateTimeZone($this->sequence->sequenceMessage->time_zone));

        $this->send_at = $sendAt;
        $this->sequence->delivered_count =+ $deliveredCount;

        $this->sequence->save();
    }
}
