<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\SendMessageNow\Image;
use Modules\Facebook\Entities\SendMessageNow\Text;
use Modules\Facebook\Http\Controllers\MessageController;
use Modules\Facebook\Http\Controllers\PageSubscriberController;

/**
 * Class SequenceJobController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class SequenceJobController extends Controller
{
    use DispatchesJobs;

    /**
     * @var array
     */
    private $page;

    /**
     * @var Text[]
     */
    private $texts;

    /**
     * @var Image[]
     */
    private $image;

    /**
     * @var int
     */
    private $subscriberFbId;

    /**
     * @var PageSubscriberController
     */
    private $pageSubscriberController;

    /**
     * @var MessageController
     */
    private $messageController;

    /**
     * @var array
     */
    protected $sequence;


    /**
     * @param $data
     */
    public function handleSequenceMessage(array $data)
    {
        $subscribers = $data['subscribers'];
        $this->sequence = $data['sequence'];

        $this->messageController = MessageController::getInstance();
        $this->messageController->setPage($this->sequence->sequenceMessage->sequencer->page);
        $this->messageController->setTexts($this->sequence->sequenceMessage->texts);
        $this->messageController->setImages($this->sequence->sequenceMessage->images);

        if($subscribers && count($subscribers) > 0) {
            $this->scheduleAdditionalSubscribers($data);
            $this->updateSequenceMessageSent(count($subscribers));

            $subscribers = $data['subscribers']->slice(Config::get('constant.ZERO'), Config::get('constants.NO_OF_JOBS_TO_WORK') - 1);
            $deliveredCount = $this->messageController->sendMessageToSubscribers($subscribers);
            $this->updateSequenceMessageDelivered($deliveredCount);
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    private function scheduleAdditionalSubscribers($data)
    {
        $data['subscribers'] = $data['subscribers']->slice(Config::get('constants.NO_OF_JOBS_TO_WORK'));

        if (count($data['subscribers']) > 0) {
            $this->scheduleSubscribers($data);
        }

        return true;
    }


    /**
     * @param $sentCount
     */
    protected function updateSequenceMessageSent($sentCount)
    {
        $this->sequence->sent_count =+ $sentCount;
        $this->sequence->save();
    }

    /**
     * @param $deliveredCount
     */
    protected function updateSequenceMessageDelivered($deliveredCount)
    {
        $this->sequence->delivered_count =+ $deliveredCount;
        $this->sequence->save();
    }
}
