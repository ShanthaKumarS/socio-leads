<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\SequenceMessage\Button;
use Modules\Facebook\Entities\SequenceMessage\Image;
use Modules\Facebook\Entities\SequenceMessage\SequenceInterface;
use Modules\Facebook\Entities\SequenceMessage\SequenceMessage;
use Modules\Facebook\Entities\SequenceMessage\Text;
use Modules\Facebook\Http\Requests\SequenceMessageRequestInterface;
use Modules\Facebook\Repositories\SequenceMessageRepository;

/**
 * Class SequenceController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class SequenceController extends Controller
{
    /**
     * @var SequenceMessage
     */
    private $sequenceMessage;

    /**
     * @var Text
     */
    private $text;

    /**
     * @var Button
     */
    private $button;

    /**
     * @var Image
     */
    private $image;


    /**
     * SequenceController constructor.
     *
     * @param Text $text
     * @param Image $image
     * @param SequenceMessage $sequenceMessage
     * @param Button $button
     */
    public function __construct(
        Text $text,
        Image $image,
        SequenceMessage $sequenceMessage,
        Button $button
    )
    {
        $this->sequenceMessage = $sequenceMessage;
        $this->text = $text;
        $this->image = $image;
        $this->button = $button;
    }

    /**
     * @param SequenceMessageRequestInterface $request
     * @param SequenceInterface $sequence
     * @param callable $storeSequence
     * @return void
     */
    public function storeMessage
    (
        SequenceMessageRequestInterface $request,
        SequenceInterface $sequence,
        callable $storeSequence
    )
    {
        $this->sequenceMessage->time_zone = $request->time_zone;
        $this->sequenceMessage->send_time = $request->send_time;
        $this->sequenceMessage->tag = $request->msg_tag;

        $sequence->sequenceMessages()->save($this->sequenceMessage);

        $storeSequence($this->sequenceMessage);

        if (! empty($request->texts)) {
            $this->storeMessageTexts($request->texts);
        }

        if (! empty($request->images)) {
            $this->storeMessageImages($request->images);
        }
    }

    /**
     * Store texts of sequence message
     * @param array $texts
     * @return void
     */
    private function storeMessageTexts(array $texts)
    {
        foreach ($texts as $formText) {
            $formText = json_decode($formText);

            $text = clone $this->text;
            $text->text = $formText->text;
            $text->sequence_message_id = $this->sequenceMessage->id;
            $text->save();

            if (isset($formText->buttons) && !empty($formText->buttons)) {
                $this->storeMessageTextButtons($formText->buttons, $text);
            }
        }
    }

    /**
     * Store images of sequence message
     * @param array $formImages
     * @return void
     */
    private function storeMessageImages(array $formImages)
    {
        $images = [];
        foreach ($formImages as $formImage) {
            $image = clone $this->image;
            $image->path = storeImage($formImage);
            $image->sequence_message_id = $this->sequenceMessage->id;
            $images[] = $image;
        }

        $this->sequenceMessage->images()->saveMany($images);
    }

    /**
     * Store buttons of text
     *
     * @param array $fromButtons
     * @param Text $text
     * @return void
     */
    private function storeMessageTextButtons(array $fromButtons, Text $text)
    {
        $buttons = [];

        foreach ($fromButtons as $fromButton) {
            $button = clone $this->button;

            $button->name = $fromButton->title;
            $button->link = $fromButton->url;

            $buttons[] = $button;
        }

        $text->buttons()->saveMany($buttons);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroyMessage(Request $request)
    {
        try {
            SequenceMessageRepository::deleteSequenceMessage($request->sequenceMessageId);
            return Response::json("Sequence Deleted Succesfully", 200);
        } catch (Exception $e) {
            return Response::json("Something went Wrong", 500);
        }
    }

    /**
     * @param $sequenceMessageId
     * @return mixed
     */
    public function editMessage($sequenceMessageId)
    {
        $bindSequence = [
            'Modules\\Facebook\\Entities\\SequenceMessage\\StepSequence' => 'getStep',
            'Modules\\Facebook\\Entities\\SequenceMessage\\DateSequence' => 'getDate',
            'Modules\\Facebook\\Entities\\SequenceMessage\\RecurringSequence' => 'getRecurring'
        ];

        $sequenceMessage = SequenceMessage::find($sequenceMessageId);
        $sequence = $bindSequence[$sequenceMessage->sequence_type];
        $this->$sequence($sequenceMessage);

        $sequenceMessage->sequencer;
        $sequenceMessage->texts;
        foreach ($sequenceMessage->texts as $text) {
            $text->buttons;
        }
        $sequenceMessage->images;

        return Response::json($sequenceMessage, 200);
    }

    /**
     * @param $sequenceMessage
     */
    public function getStep(&$sequenceMessage)
    {
        $sequenceMessage->step;
    }

    /**
     * @param $sequenceMessage
     */
    public function getDate(&$sequenceMessage)
    {
        $sequenceMessage->date;
    }

    /**
     * @param $sequenceMessage
     */
    public function getRecurring(&$sequenceMessage)
    {
        $sequenceMessage->recurrence;
    }
}
