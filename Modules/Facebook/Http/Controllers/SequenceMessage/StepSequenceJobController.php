<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Modules\Facebook\Jobs\StepSequenceMessageJob;

/**
 * Class StepSequenceJobController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class StepSequenceJobController extends SequenceJobController
{
    /**
     * @param array $data
     * @return bool
     */
    public function scheduleMessage(array $data)
    {
        $stepSequenceMessagesJob = new StepSequenceMessageJob($data);
        $stepSequenceMessagesJob = $stepSequenceMessagesJob->onQueue('stepSequence');
        $this->dispatch($stepSequenceMessagesJob, true);

        return true;
    }

}
