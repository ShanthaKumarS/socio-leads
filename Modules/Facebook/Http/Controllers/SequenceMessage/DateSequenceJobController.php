<?php

namespace Modules\Facebook\Http\Controllers\SequenceMessage;

use Modules\Facebook\Jobs\DateSequenceMessageJob;;

/**
 * Class DateSequenceJobController
 * @package Modules\Facebook\Http\Controllers\SequenceMessage
 */
class DateSequenceJobController extends SequenceJobController
{
    /**
     * @param array $data
     * @return bool
     */
    public function scheduleMessage(array $data)
    {
        $dateSequenceMessagesJob = new DateSequenceMessageJob($data);
        $dateSequenceMessagesJob = $dateSequenceMessagesJob->onQueue('dateSequence');
        $this->dispatch($dateSequenceMessagesJob, true);

        return true;
    }

}
