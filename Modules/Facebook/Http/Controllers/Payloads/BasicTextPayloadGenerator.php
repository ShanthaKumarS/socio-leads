<?php

namespace Modules\Facebook\Http\Controllers\Payloads;

use Illuminate\Routing\Controller;

/**
 * Class BasicTextPayloadGenerator
 * @package Modules\Facebook\Http\Controllers\Payloads
 */
class BasicTextPayloadGenerator extends Controller implements PayloadGenerator
{

    /**
     * @var int
     */
    private $recipientId;

    /**
     * @var string
     */
    private $text;

    /**
     * @param array $payloadInfo
     * @return array mixed
     */
    public function generate(array $payloadInfo)
    {
        $this->recipientId = $payloadInfo['recipientId'];
        $this->text = $payloadInfo['text'];

       $payload["recipient"] = ["id" => $this->recipientId];
       $payload["message"] = ["text" => $this->text->text];

       return $payload;
    }
}
