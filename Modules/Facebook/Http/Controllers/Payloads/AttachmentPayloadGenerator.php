<?php

namespace Modules\Facebook\Http\Controllers\Payloads;

use Illuminate\Routing\Controller;

/**
 * Class AttachmentPayloadGenerator
 * @package Modules\Facebook\Http\Controllers\Payloads
 */
class AttachmentPayloadGenerator extends Controller implements PayloadGenerator
{
    /**
     * @var int
     */
    private $recipientId;

    /**
     * @var string
     */
    private $url;

    /**
     * @param array $payloadInfo
     * @return array
     */
    public function generate(array $payloadInfo)
    {
        $this->recipientId = $payloadInfo['recipientId'];
        $this->url = $payloadInfo['url'];

       $payload["recipient"] = ["id" => $this->recipientId];
       $payload["message"] = [
           "attachment" => [
               "type" => "image",
               "payload" => [
                   "url" => $this->url,
                   "is_reusable" => true
               ]
           ]
       ];

       return $payload;
    }
}
