<?php

namespace Modules\Facebook\Http\Controllers\Payloads;

use Illuminate\Routing\Controller;

/**
 * Class TextButtonPayloadGenerator
 * @package Modules\Facebook\Http\Controllers\Payloads
 */
class TextButtonPayloadGenerator extends Controller implements PayloadGenerator
{

    /**
     * @var int
     */
    private $recipientId;

    /**
     * @var string
     */
    private $text;

    /**
     * @param array $payloadInfo
     * @return array
     */
    public function generate(array $payloadInfo)
    {
        $this->recipientId = $payloadInfo['recipientId'];
        $this->text = $payloadInfo['text'];

        $payload["recipient"] = ["id" => $this->recipientId];
        $payload["message"] = [
            "attachment" => [
                "type" => "template",
                "payload" => [
                    "template_type" => "button",
                    "text" => $this->text->text,
                    "buttons" => $this->hydrateButtons()
                ]
            ]
        ];

        return $payload;
    }

    /**
     * @return array
     */
    private function hydrateButtons()
    {
        $fbButtons = [];
        foreach ($this->text->buttons as $button) {
            $fbButton['type'] = "web_url";
            $fbButton['title'] = $button->name;
            $fbButton['url'] = $button->link;

            $fbButtons[] = $fbButton;

        }

        return $fbButtons;
    }
}
