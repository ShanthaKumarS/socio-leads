<?php

namespace Modules\Facebook\Http\Controllers\Payloads;

/**
 * Interface PayloadGenerator
 * @package Modules\Facebook\Http\Controllers\Payloads
 */
interface PayloadGenerator
{
    /**
     * @param array $payload
     * @return array
     */
    public function generate(array $payload);
}
