<?php

namespace Modules\Facebook\Http\Controllers;

use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Routing\Controller;

/**
 * Class PageSubscriberController
 * @package Modules\Facebook\Http\Controllers
 */
class PageSubscriberController extends Controller
{
    /**
     * @return PageSubscriberController
     */
    public static function getInstance()
    {
        return new PageSubscriberController();
    }

    /**
     * @param $senderId
     * @param $pageAccessToken
     * @param $text
     * @return string
     * @throws FacebookSDKException
     */
    public function replaceFacebookName($senderId, $pageAccessToken, $text)
    {
        $senderInfo = $this->getSenderInfo($senderId, $pageAccessToken);
        return preg_replace("/({{fb_first_name}}|{{FacebookName}})/i", $senderInfo['first_name'], $text);
    }

    /**
     * @param $senderId
     * @param $pageAccessToken
     * @return mixed
     * @throws FacebookSDKException
     */
    private function getSenderInfo($senderId, $pageAccessToken)
    {
        $fb = LoginController::getFacebookGraphApi();
        $response = $fb->get('/' . $senderId . '?fields=first_name,last_name,gender', $pageAccessToken);
        return $response->getDecodedBody();
    }
}
