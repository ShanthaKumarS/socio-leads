<?php

namespace Modules\Facebook\Http\Controllers\Cron;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Modules\Facebook\Entities\SequenceMessage\Date;
use Modules\Facebook\Http\Controllers\SequenceMessage\DateSequenceJobController;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class DateSequenceScheduleController
 * @package Modules\Facebook\Http\Controllers\Cron
 */
class DateSequenceScheduleController extends Controller
{
    use DispatchesJobs;

    /**
     * @return bool
     */
    public function scheduleMessage()
    {
//        $dates = Date::all();
        $dates = Date::all()->where('send_at', Carbon::now()->startOfDay());

        $dateSequenceJobController = new DateSequenceJobController();
        if ($dates && count($dates) > 0) {
            foreach ($dates as $date) {
                $data['subscribers'] = FacebookPagesRepository::fetchSubscribers($date->sequenceMessage->sequencer->page);
                $data['sequence'] = $date;
                $dateSequenceJobController->scheduleMessage($data);
            }
        }
        return true;
    }
}
