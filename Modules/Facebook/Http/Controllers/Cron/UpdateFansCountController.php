<?php

namespace Modules\Facebook\Http\Controllers\Cron;

use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\SequenceMessage\Step;
use Modules\Facebook\Http\Controllers\FacebookController;
use Modules\Facebook\Http\Controllers\SequenceMessage\StepSequenceJobController;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class StepSequenceScheduleController
 * @package Modules\Facebook\Http\Controllers\Cron
 */
class UpdateFansCountController extends Controller
{
    /**
     * @return bool
     */
    public function updateFanCount()
    {
        $pages = Page::all();
        foreach ($pages as $page) {
            try {
                $fb = FacebookController::getFacebookGraphApi();
                $likesCount = $fb->get('/' . $page->facebook_page_id . '?fields=fan_count', $page->page_access_token);
                $likesCount = $likesCount->getDecodedBody();

                if ($page->like_count != $likesCount['fans_count']) {
                    $page->like_count = $likesCount['fans_count'];
                }
                $page->save();
            } catch (\Exception $e) {

            }



        }
    }
}
