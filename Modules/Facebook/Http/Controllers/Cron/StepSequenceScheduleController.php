<?php

namespace Modules\Facebook\Http\Controllers\Cron;

use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Modules\Facebook\Entities\SequenceMessage\Step;
use Modules\Facebook\Http\Controllers\SequenceMessage\StepSequenceJobController;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class StepSequenceScheduleController
 * @package Modules\Facebook\Http\Controllers\Cron
 */
class StepSequenceScheduleController extends Controller
{
    /**
     * @return bool
     */
    public function scheduleMessage()
    {
//        $steps = Step::all();
        $steps = Step::all()->where('send_at', Carbon::now()->startOfDay());

        $stepSequenceJobController = new StepSequenceJobController();
        if ($steps && count($steps) > 0) {
            foreach ($steps as $step) {
                $data['subscribers'] = FacebookPagesRepository::fetchSubscribers($step->sequenceMessage->sequencer->page);
                $data['sequence'] = $step;
                $stepSequenceJobController->scheduleMessage($data);
            }
        }
        return true;
    }
}
