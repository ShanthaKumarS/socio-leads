<?php

namespace Modules\Facebook\Http\Controllers\Cron;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Modules\Facebook\Entities\SequenceMessage\Recurrence;
use Modules\Facebook\Http\Controllers\SequenceMessage\RecurringSequenceJobController;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class RecurringSequenceScheduleController
 * @package Modules\Facebook\Http\Controllers\Cron
 */
class RecurringSequenceScheduleController extends Controller
{
    use DispatchesJobs;

    /**
     * @return bool
     */
    public function scheduleMessage()
    {
        $recurrences = Recurrence::all()->where('send_at', Carbon::now()->startOfDay());
//        $recurrences = Recurrence::all();

        $recurringSequenceJobController = new RecurringSequenceJobController();
        if ($recurrences && count($recurrences) > 0) {
            foreach ($recurrences as $recurrence) {
                $data['subscribers'] = FacebookPagesRepository::fetchSubscribers($recurrence->sequenceMessage->sequencer->page);
                $data['sequence'] = $recurrence;
                $recurringSequenceJobController->scheduleMessage($data);
            }
        }
        return true;
    }
}
