<?php

namespace Modules\Facebook\Http\Controllers;

use App\Http\Controllers\Controller;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\UserCredential;

/**
 * Class FacebookController
 * @package Modules\Facebook\Http\Controllers
 */
class FacebookController extends Controller
{
    /**
     * @return Facebook
     * @throws FacebookSDKException
     */
    public static function getFacebookGraphApi()
    {
        return new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'),
            'app_secret' => env('FACEBOOK_SECRET_KEY'),
            'default_graph_version' => 'v10.0',
        ]);
    }

    /**
     * @param Request $request
     * @param UserCredential $facebookCredentials
     * @return JsonResponse
     * @throws FacebookSDKException
     */
    public function storeCredentials(
        Request $request,
        UserCredential $facebookCredentials
    )
    {
        $fb = $this->getFacebookGraphApi();

        try {
            $facebookResponse = $fb->get('/' . $request->facebookUserId . '?fields=id,first_name,last_name,email,picture.type(large)', $request->userAccessToken);
            $facebookUserInfo = json_decode($facebookResponse->getBody());
            $this->hydrateFacebookCredential($facebookCredentials, $facebookUserInfo, $request);
            $facebookCredentials->save();

            return  response()->json([
                "code" => 200,
                "status" => "facebook login successfull",
            ]);
        } catch (\Exception $e) {
            Log::Error("FacebookController: storeCredentials : " . $e->getMessage());
            return  response()->json([
                "code" => 500,
                "status" => "Something went wrong Please check the log",
            ]);
        }
    }


    /**
     * @param UserCredential $facebookCredential
     * @param $facebookUserInfo
     * @param Request $request
     */
    public function hydrateFacebookCredential(
        UserCredential &$facebookCredential,
        $facebookUserInfo,
        Request $request
    )
    {
        $user = $request->session()->get("user");
        $facebookCredential->facebook_user_id = $facebookUserInfo->id;
        $facebookCredential->facebook_user_access_token = $request->userAccessToken;
        $facebookCredential->user_id = $user['user_id'];
    }

}
