<?php

namespace Modules\Facebook\Http\Controllers\Post;

use Braintree\Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\Post;

/**
 * Class PostSettingController
 * @package Modules\Facebook\Http\Controllers\Post
 */
class PostSettingController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateFanMessageStatus(Request $request)
    {
        try {
            $post = Post::find($request->postId);
            $post->do_reply_message = filter_var($request->status, FILTER_VALIDATE_BOOLEAN);
            $post->save();

            $response = Response::json("success", 200);
        } catch (Exception $e) {
            $response = Response::json("error", 500);
        }

        return  $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateNonFanLikeStatus(Request $request)
    {
        try {
            $post = Post::find($request->postId);
            $post->do_like_comment = filter_var($request->status, FILTER_VALIDATE_BOOLEAN);
            $post->save();

            $response = Response::json("success", 200);
        } catch (Exception $e) {
            $response = Response::json("error", 500);
        }

        return  $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateFanCommentStatus(Request $request)
    {
        try {
            $post = Post::find($request->postId);
            $post->do_reply_comment = filter_var($request->status, FILTER_VALIDATE_BOOLEAN);
            $post->save();

            $response = Response::json("success", 200);
        } catch (Exception $e) {
            $response = Response::json("error", 500);
        }

        return  $response;
    }
}
