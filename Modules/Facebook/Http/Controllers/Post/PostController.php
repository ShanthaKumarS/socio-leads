<?php
namespace Modules\Facebook\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Modules\User\Models\AutoComments;
use App\Modules\User\Models\AutoMessages;
use App\Modules\User\Models\facebookpages;
use App\Modules\User\Models\postpages;
use App\Modules\User\Models\PostSettings;
use App\Modules\User\Models\PostCommentsReport;
use App\Modules\User\Models\PostMessagesReport;
use App\Modules\User\Models\SendLikes;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\Post;
use Modules\Facebook\Repositories\FacebookPagesRepository;
use Modules\Facebook\Repositories\FacebookPostRepository;
use Modules\Facebook\Repositories\FacebookReportRepository;
use Modules\Facebook\Repositories\PageReportRepository;
use Modules\Facebook\Repositories\PostReportRepository;

/**
 * Class PostController
 * @package Modules\Facebook\Http\Controllers\Post
 */
class PostController extends Controller
{
    /**
     * @var Post
     */
    private $post;

    /**
     * PostController constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @param Request $request
     * @param PostReportRepository $postReportRepository
     * @param $pageId
     * @return Factory|View
     */
    public function index(
        Request $request,
        PostReportRepository $postReportRepository,
        $pageId
    )
    {
        try {
            $user = $request->session()->get('user');
            $page = FacebookPagesRepository::getPageById($pageId);
            $posts = FacebookPostRepository::getLatestPosts($pageId, 1, 10);

            return view('facebook::posts',
                [
                    'user' => $user,
                    'page' => $page,
                    'posts' => $posts,
                    'postReportRepository' => $postReportRepository
                ]
            );
        } catch (\Exception $e) {
            return view('errors.Error');
        }
    }

    /**
     * @param Page $facebookPage
     * @param Facebook $fb
     * @param Post $postTemplate
     * @return array
     * @throws FacebookSDKException
     */
    public function hydratePagePosts (
        Page $facebookPage,
        Facebook $fb
    )
    {
        $postsDetails = $fb->get('/' . $facebookPage->facebook_page_id . '/posts?fields=full_picture,created_time,message,attachments{description,unshimmed_url,title,media{source}},story,from,likes', $facebookPage->page_access_token);
        $postsDetails = $postsDetails->getDecodedBody();

        $posts = [];
        foreach ($postsDetails['data'] as $postDetails) {
            $post = clone $this->post;

            $post->facebook_post_id = $postDetails['id'];
            $post->facebook_post_date = new \DateTime($postDetails['created_time']);

            if (isset($postDetails['full_picture'])) {
                $post->feed_picture = $postDetails['full_picture'];
            }

            if (isset($postDetails['message'])) {
                $post->feed_message = $postDetails['message'];
            }

            if (isset($postDetails['feed_story'])) {
                $post->feed_picture = $postDetails['story'];
            }

            $posts[] = $post;
        }

        return $posts;
    }
}
