<?php

namespace Modules\Facebook\Http\Controllers\Reports;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Facebook\Repositories\FacebookReportRepository;
use Modules\Facebook\Repositories\PostReportRepository;

class PostReportController extends ReportController
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $postId
     * @return Renderable
     */
    public function postReport(Request $request, $postId)
    {
        try {
            $user = $request->session()->get('user');

            return view('reports',
                [
                    'user' => $user,
                    'postId' => $postId
                ]
            );

        } catch (\Exception $e) {
            return view('error');
        }
    }

    /**
     * @param $postId
     * @return array
     */
    public function postReportGraphDetails($postId)
    {
        $this->setCommentReports(PostReportRepository::getPostCommentsReportByMonth($postId, self::TWO_YEARS));
        $this->setMessageReports(PostReportRepository::getPostMessagesReportByMonth($postId, self::TWO_YEARS));
        $this->setLikeReports(PostReportRepository::getPostLikesReportByMonth($postId,self::TWO_YEARS));

        return ['code' => 200, 'response' => $this->getReport()];
    }
}
