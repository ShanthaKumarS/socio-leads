<?php

namespace Modules\Facebook\Http\Controllers\Reports;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Facebook\Repositories\FacebookReportRepository;

class FacebookReportController extends ReportController
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Renderable
     */
    public function report(Request $request)
    {
        try {
            $user = $request->session()->get('user');

            return view('reports',
                [
                    'user' => $user,
                ]
            );

        } catch (\Exception $e) {
            return view('error');
        }
    }

    /**
     * @return array
     */
    public function reportGraphDetails()
    {
        $this->setCommentReports(FacebookReportRepository::getFacebookCommentsReportByMonth(self::TWO_YEARS));
        $this->setMessageReports(FacebookReportRepository::getFacebookMessagesReportByMonth(self::TWO_YEARS));
        $this->setLikeReports(FacebookReportRepository::getFacebookLikesReportByMonth(self::TWO_YEARS));
        $this->setFanReports(FacebookReportRepository::getGainedFacebookFansByMonth(self::TWO_YEARS));

        return ['code' => 200, 'response' => $this->getReport()];
    }
}
