<?php

namespace Modules\Facebook\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use stdClass;

class ReportController extends Controller
{
    const TWO_YEARS = 24;

    const FIVE_MONTHS = 5;

    /**
     * @var stdClass[]
     */
    private $pageCommentReports;

    /**
     * @var stdClass[]
     */
    private $pageMessageReports;

    /**
     * @var stdClass[]
     */
    private $pageLikeReports;

    /**
     * @var stdClass[]
     */
    private $postFanReports;

    /**
     * @param stdClass[] $pageCommentReports
     */
    public function setCommentReports($pageCommentReports)
    {
        $this->pageCommentReports = $pageCommentReports;
    }

    /**
     * @param stdClass[] $pageMessageReports
     */
    public function setMessageReports($pageMessageReports)
    {
        $this->pageMessageReports = $pageMessageReports;
    }

    /**
     * @param stdClass[] $pageLikeReports
     */
    public function setLikeReports($pageLikeReports)
    {
        $this->pageLikeReports = $pageLikeReports;
    }

    /**
     * @param stdClass[] $postFanReports
     */
    public function setFanReports($postFanReports)
    {
        $this->postFanReports = $postFanReports;
    }

    /**
     * @return array
     */
    public function getReport()
    {
        $months = getDatesFromRangeByMonth(self::TWO_YEARS, date('Y-m'));

        $pageCommentReportResult = [];
        $pageMessageReportResult = [];
        $pageLikeReportResult = [];
        $pageFanReportResult = [];

        foreach($months as $month) {

            if ($this->pageCommentReports instanceof Collection) {
                $pageCommentReports = $this->pageCommentReports->where('month', $month)->first();
            } else {
                $pageCommentReports = null;
            }

            if ($this->pageMessageReports instanceof Collection) {
                $pageMessageReports = $this->pageMessageReports->where('month', $month)->first();
            } else {
                $pageMessageReports = null;
            }

            if ($this->pageLikeReports instanceof Collection) {
                $pageLikeReports = $this->pageLikeReports->where('month', $month)->first();
            } else {
                $pageLikeReports = null;
            }

            if ($this->postFanReports instanceof Collection) {
                $postFanReports = $this->postFanReports->where('month', $month)->first();
            } else {
                $postFanReports = null;
            }

            $pageCommentReportResult[] = $this->getArrayReport($pageCommentReports, $month);
            $pageMessageReportResult[] = $this->getArrayReport($pageMessageReports, $month);
            $pageLikeReportResult[] = $this->getArrayReport($pageLikeReports, $month);
            $pageFanReportResult[] = $this->getArrayReport($postFanReports, $month);
        }

        return [
            'commentResult' => $pageCommentReportResult,
            'messageResult'=>$pageMessageReportResult,
            'likeResult' => $pageLikeReportResult,
            'fansResult' => $pageFanReportResult,

            'commentResultF5M' => array_slice($pageCommentReportResult, -(self::FIVE_MONTHS)),
            'messageResultF5M'=> array_slice($pageMessageReportResult, -(self::FIVE_MONTHS)),
            'likeResultF5M' => array_slice($pageLikeReportResult, -(self::FIVE_MONTHS)),
            'fansResultF5M' => array_slice($pageFanReportResult, -(self::FIVE_MONTHS))
        ];
    }

    /**
     * @param $postReport
     * @param $month
     * @return object
     */
    private function getArrayReport($postReport, $month)
    {
        if ($postReport) {
            $arrayReport = ['date' => $month, 'count' => $postReport->count];
        } else {
            $arrayReport = ['date' => $month, 'count' => 0];
        }

        return (object)$arrayReport;
    }
}
