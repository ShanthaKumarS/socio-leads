<?php

namespace Modules\Facebook\Http\Controllers\Reports;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Facebook\Repositories\FacebookPagesRepository;
use Modules\Facebook\Repositories\PageReportRepository;

/**
 * Class PageReportController
 * @package Modules\Facebook\Http\Controllers\Reports
 */
class PageReportController extends ReportController
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $pageId
     * @return Renderable
     */
    public function pageReport(Request $request, $pageId)
    {
        try {
            $user = $request->session()->get('user');

            return view('reports',
                [
                    'user' => $user,
                    'pageId' => $pageId,
                ]
            );

        } catch (\Exception $e) {
            return view('error');
        }
    }

    /**
     * @param $pageId
     * @return array
     */
    public function pageReportGraphDetails($pageId)
    {
        $this->setCommentReports(PageReportRepository::getPageCommentsReportByMonth($pageId,self::TWO_YEARS));
        $this->setMessageReports(PageReportRepository::getPageMessagesReportByMonth($pageId, self::TWO_YEARS));
        $this->setLikeReports(PageReportRepository::getPageLikesReportByMonth($pageId, self::TWO_YEARS));
        $this->setFanReports(PageReportRepository::getGainedPageFansByMonth($pageId, self::TWO_YEARS));

        return ['code' => 200, 'response' => $this->getReport()];
    }
}
