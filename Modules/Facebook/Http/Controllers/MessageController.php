<?php

namespace Modules\Facebook\Http\Controllers;

use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\ImageInterface;
use Modules\Facebook\Entities\PageSubscriber;
use Modules\Facebook\Entities\SendMessageNow\Image;
use Modules\Facebook\Entities\SendMessageNow\Text;
use Modules\Facebook\Entities\TextInterface;
use Modules\Facebook\Http\Controllers\Payloads\AttachmentPayloadGenerator;
use Modules\Facebook\Http\Controllers\Payloads\BasicTextPayloadGenerator;
use Modules\Facebook\Http\Controllers\Payloads\TextButtonPayloadGenerator;

/**
 * Class MessageController
 * @package Modules\Facebook\Http\Controllers
 */
class MessageController extends Controller
{
    /**
     * @var int
     */
    private $subscriberFbId;

    /**
     * @var TextInterface
     */
    private $texts;

    /**
     * @var ImageInterface
     */
    private $images;

    /**
     * @var Page
     */
    private $page;

    /**
     * @var PageSubscriberController
     */
    private $pageSubscriberController;

    /**
     * @var int
     */
    private $sentCount = 0;

    /**
     * @param TextInterface $texts
     */
    public function setTexts($texts)
    {
        $this->texts = $texts;
    }

    /**
     * @param ImageInterface $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @param Page $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return MessageController
     */
    public static function getInstance()
    {
        return new MessageController();
    }

    /**
     * @param PageSubscriber[] $subscribers
     * @return int
     */
    public function sendMessageToSubscribers($subscribers)
    {
        $this->pageSubscriberController = PageSubscriberController::getInstance();

        foreach ($subscribers as $subscriber) {
            $this->subscriberFbId = $subscriber->facebook_page_subscriber_id;

            if ($this->sendTextMessages() && $this->sendImageMessages()) {
                $this->sentCount++;
            }
        }

        return count($subscribers);
    }

    /**
     * @return bool
     */
    private function sendTextMessages()
    {
        $success = true;

        foreach ($this->texts as $text) {
            try {
                $payload = $this->getTextPayload($text);
                $this->sendMessage($payload, $this->page);

            } catch (\Exception $e) {
                $success = false;
                Log::info($e);
            }
        }

        return $success;
    }

    /**
     * @param Text $text
     * @return array
     * @throws FacebookSDKException
     */
    private function getTextPayload($text)
    {
        $text->text = $this->pageSubscriberController->replaceFacebookName(
            $this->subscriberFbId,
            $this->page->page_access_token,
            $text->text
        );

        if ($text->buttons && count($text->buttons) > 0) {
            $payloadGenerator = new TextButtonPayloadGenerator();
        } else {
            $payloadGenerator = new BasicTextPayloadGenerator();
        }

        $payloadDetails = [
            "recipientId" => $this->subscriberFbId,
            "text" => $text
        ];

        return $payloadGenerator->generate($payloadDetails);
    }

    /**
     * @return bool
     */
    private function sendImageMessages()
    {
        $success = true;

        foreach ($this->images as $image) {
            try {
                $payload = $this->getImagePayload($image);
                $this->sendMessage($payload, $this->page);
                destroyImage($image->url);
            } catch (\Exception $e) {
                $success = false;
                Log::info($e);
            }
        }

        return $success;
    }

    /**
     * @param Image $image
     * @return array
     */
    private function getImagePayload($image)
    {
        $payloadGenerator = new AttachmentPayloadGenerator();

        $payloadDetails = [
            "recipientId" => $this->subscriberFbId,
            "url" => $image->url
        ];

        return $payloadGenerator->generate($payloadDetails);
    }


    /**
     * @param $payload
     * @param $page
     * @throws FacebookSDKException
     */
    public function sendMessage($payload, $page)
    {
        $fb = LoginController::getFacebookGraphApi();
        $response = $fb->post('/me/messages', $payload, $page->page_access_token);
        $response->getDecodedBody();
    }
}
