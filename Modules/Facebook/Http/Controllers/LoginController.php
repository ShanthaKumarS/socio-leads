<?php

namespace Modules\Facebook\Http\Controllers;

use App\Http\Controllers\Controller;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Modules\Facebook\Entities\UserCredential;
use Modules\Facebook\Repositories\FacebookRepository;
use Modules\User\Entities\User;

/**
 * Class LoginController
 * @package Modules\Facebook\Http\Controllers
 */
class LoginController extends Controller
{
    /**
     * @return Facebook
     * @throws FacebookSDKException
     */
    public static function getFacebookGraphApi()
    {
        return new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'),
            'app_secret' => env('FACEBOOK_SECRET_KEY'),
            'default_graph_version' => 'v10.0',
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @param UserCredential $facebookCredentials
     * @param FacebookRepository $facebookRepository
     * @return JsonResponse
     * @throws FacebookSDKException
     */
    public function login(
        Request $request,
        User $user,
        UserCredential $facebookCredentials,
        FacebookRepository $facebookRepository
    )
    {
        $facebook = $this->getFacebookGraphApi();

        if (! $facebookRepository->getCredentialByFbId($request->facebookUserId)) {

            $facebookResponse = $facebook->get('/' . $request->facebookUserId . '?fields=id,first_name,last_name,email,picture.type(large)', $request->userAccessToken);
            $facebookUserInfo = json_decode($facebookResponse->getBody());

            if ($facebookUserInfo->id != null && $facebookUserInfo->id != "") {

                $user->first_name = $facebookUserInfo->first_name;
                $user->last_name = $facebookUserInfo->last_name;
                $user->picture = $facebookUserInfo->picture->data->url;
                $user->role_id = Config::get('constants.USER_ROLES.USER');
                $user->save();

                $this->hydrateFacebookCredential($facebookCredentials, $facebookUserInfo, $request);
                $user->facebookCredential()->save($facebookCredentials);

            } else {
                return response()->json([
                    "code" => 401,
                    "status" => "unable to login with facebook"
                ]);
            }
        } else {
            $facebookCredential = $facebookRepository->getCredentialByFbId($request->facebookUserId);
            $user = $facebookCredential->user;
        }

        Session::put('user', $user);

        return  response()->json([
            "code" => 200,
            "status" => "facebook login successfull",
        ]);
    }

    /**
     * @param UserCredential $facebookCredential
     * @param $facebookUserInfo
     * @param Request $request
     */
    public function hydrateFacebookCredential(
        UserCredential &$facebookCredential,
        $facebookUserInfo,
        Request $request
    )
    {
        $facebookCredential->facebook_user_id = $facebookUserInfo->id;
        $facebookCredential->facebook_user_access_token = $request->userAccessToken;
    }

}
