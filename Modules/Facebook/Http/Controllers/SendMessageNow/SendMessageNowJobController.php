<?php

namespace Modules\Facebook\Http\Controllers\SendMessageNow;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Modules\Facebook\Entities\SendMessageNow\SendMessageNowDetails;
use Modules\Facebook\Http\Controllers\MessageController;
use Modules\Facebook\Jobs\SendMessageNowJob;

/**
 * Class SendMessageNowJobController
 * @package Modules\Facebook\Http\Controllers\SendMessageNow
 */
class SendMessageNowJobController extends Controller
{

    use DispatchesJobs;

    /**
     * @var MessageController
     */
    private $messageController;

    /**
     * @var SendMessageNowDetails
     */
    private $sendMessageDetails;


    /**
     * @param array $data
     * @return bool
     */
    public function scheduleSubscribers(array $data)
    {
        $sendNowMessagesJob = new SendMessageNowJob($data);
        $sendNowMessagesJob = $sendNowMessagesJob->onQueue('sendMessageNow');
        $this->dispatch($sendNowMessagesJob, true);

        return true;
    }

    /**
     * @param array $data
     */
    public function handleSendNowMessage(array $data)
    {
        $this->sendMessageDetails = SendMessageNowDetails::find($data['sendMessageNowDetailsId']);
        $this->messageController = MessageController::getInstance();
        $this->scheduleAdditionalSubscribers($data);

        $subscribers = $data['subscribers']->slice(Config::get('constant.ZERO'), Config::get('constants.NO_OF_JOBS_TO_WORK') - 1);

        $this->messageController->setPage($this->sendMessageDetails->page);
        $this->messageController->setTexts($this->sendMessageDetails->texts);
        $this->messageController->setImages($this->sendMessageDetails->images);

        if ($subscribers && count($subscribers) > 0) {
            $this->updateSentMessageNowDetails(count($subscribers));
            $deliveredCount = $this->messageController->sendMessageToSubscribers($subscribers);
            $this->updateDeliveredMessageNowDetails($deliveredCount);
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    private function scheduleAdditionalSubscribers(array $data)
    {
        $data['subscribers'] = $data['subscribers']->slice(Config::get('constants.NO_OF_JOBS_TO_WORK'));

        if (count($data['subscribers']) > 0) {
            $this->scheduleSubscribers($data);
        }

        return true;
    }

    /**
     * @param $count
     */
    private function updateSentMessageNowDetails($sentCount)
    {
        $this->sendMessageDetails->delivered_count = $sentCount;
        $this->sendMessageDetails->save();
    }

    /**
     * @param $count
     */
    private function updateDeliveredMessageNowDetails($deliveredCount)
    {
        $this->sendMessageDetails->delivered_count = $deliveredCount;
        $this->sendMessageDetails->save();
    }
}
