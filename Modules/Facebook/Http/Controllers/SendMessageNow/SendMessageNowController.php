<?php


namespace Modules\Facebook\Http\Controllers\SendMessageNow;

use App\Http\Controllers\Controller;
use App\Modules\User\Models\BroadcastMessageLists;
use App\Modules\User\Models\FanMessages;
use App\Modules\User\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\DateSequence;
use Modules\Facebook\Entities\RecurringSequence;
use Modules\Facebook\Entities\SendMessageNow\Button;
use Modules\Facebook\Entities\SendMessageNow\Image;
use Modules\Facebook\Entities\SendMessageNow\SendMessageNowDetails;
use Modules\Facebook\Entities\SendMessageNow\Text;
use Modules\Facebook\Http\Requests\SendMessageNowRequest;
use Modules\Facebook\Http\Requests\SequenceMessageRequest;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class SendMessageNowController
 * @package Modules\Facebook\Http\Controllers\SendMessageNow
 */
class SendMessageNowController extends Controller
{
    /**
     * @var Text
     */
    private $text;

    /**
     * @var Image
     */
    private $image;

    /**
     * @var Button
     */
    private $button;

    /**
     * @var SendMessageNowRequest
     */
    private $request;

    /**
     * @var SendMessageNowDetails
     */
    private $sendMessageNowDetails;

    /**
     * @var SendMessageNowJobController
     */
    private $sendMessageNowJobController;

    /**
     * SendMessageNowController constructor.
     * @param Text $text
     * @param Image $image
     * @param Button $button
     * @param SendMessageNowDetails $sendMessageNowDetails
     * @param SendMessageNowJobController $sendMessageNowJobController
     */
    public function __construct(
        Text $text,
        Image $image,
        Button $button,
        SendMessageNowDetails $sendMessageNowDetails,
        SendMessageNowJobController $sendMessageNowJobController
    )
    {
        $this->text = $text;
        $this->image = $image;
        $this->button = $button;
        $this->sendMessageNowDetails = $sendMessageNowDetails;
        $this->sendMessageNowJobController = $sendMessageNowJobController;
    }

    /**
     * @param SendMessageNowRequest $request
     * @return void
     */
    public function sendMessage(SendMessageNowRequest $request)
    {
        $this->request = $request;
        $subscribers = [];
        if ($request->withInADay == "true") {
            $subscribers = FacebookPagesRepository::fetchSubscribersOfTheDay($request->pageId);
        } else if ($request->outSideADay == "true") {
            $subscribers = FacebookPagesRepository::fetchSubscribersBeforeToday($request->pageId);
        }

        if (count($subscribers) > 0) {
            $this->storeSendMessageNowDetails(count($subscribers));

            $data = [
                "sendMessageNowDetailsId" => $this->sendMessageNowDetails->id,
                "subscribers" => $subscribers
            ];
            $this->sendMessageNowJobController->scheduleSubscribers($data);

            return Response::json(["count" => count($subscribers)], 200);
        } else {

            return Response::json(["errors" => ["texts" => ["No subscribers Found"]]], 422);
        }
    }

    /**
     * @param $subscribersCount
     */
    private function storeSendMessageNowDetails(int $subscribersCount)
    {
        $this->sendMessageNowDetails->tag = $this->request->tag;
        $this->sendMessageNowDetails->sent_count = $subscribersCount;
        $this->sendMessageNowDetails->page_id = $this->request->pageId;

        $this->sendMessageNowDetails->save();

        if (! empty($this->request->texts)) {
            $this->storeMessageTexts($this->request->texts);
        }

        if (! empty($this->request->images)) {
            $this->storeMessageImages($this->request->images);
        }
    }

    /**
     * Store texts of sequence message
     * @param array $texts
     * @return void
     */
    private function storeMessageTexts(array $texts)
    {
        foreach ($texts as $formText) {
            $formText = json_decode($formText);
            $text = clone $this->text;

            $text->text = $formText->text;
            $text->send_message_now_detail_id = $this->sendMessageNowDetails->id;
            $text->save();

            if (isset($formText->buttons) && !empty($formText->buttons)) {
                $this->storeMessageTextButtons($formText->buttons, $text);
            }
        }
    }

    /**
     * Store buttons of text
     *
     * @param array $fromButtons
     * @param Text $text
     * @return void
     */
    private function storeMessageTextButtons(array $fromButtons, Text $text)
    {
        $buttons = [];
        foreach ($fromButtons as $fromButton) {
            $button = clone $this->button;

            $button->name = $fromButton->title;
            $button->link = $fromButton->url;

            $buttons[] = $button;
        }
        $text->buttons()->saveMany($buttons);
    }

    /**
     * Store images of sequence message
     * @param array $formImages
     * @return void
     */
    private function storeMessageImages(array $formImages)
    {
        $images = [];
        foreach ($formImages as $formImage) {
            $image = clone $this->image;

            $image->url = storeImage($formImage);
            $image->send_message_now_detail_id = $this->sendMessageNowDetails->id;
            $images[] = $image;
        }

        $this->sendMessageNowDetails->images()->saveMany($images);
    }

    /**
     * @param int $pageId
     * @return JsonResponse
     */
    public function getStats($pageId)
    {
        $this->sendMessageNowDetails = SendMessageNowDetails::where('page_id', $pageId)->get();
        $stats = [];
        if (count($this->sendMessageNowDetails) > 0) {
            foreach ($this->sendMessageNowDetails as $sendMessageNowDetails) {
                $stats[] = [
                    'sentAt' => $sendMessageNowDetails->created_at,
                    'sentCount' => $sendMessageNowDetails->sent_count,
                    'deliveredCount' => $sendMessageNowDetails->delivered_count
                ];
            }
        }

        return Response::json($stats, 200);
    }
}
