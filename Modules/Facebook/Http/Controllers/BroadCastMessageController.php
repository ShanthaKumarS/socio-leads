<?php


namespace Modules\Facebook\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\User\Models\BroadcastMessageLists;
use App\Modules\User\Models\FanMessages;
use App\Modules\User\Models\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\DateSequence;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\RecurringSequence;
use Modules\Facebook\Http\Requests\SequenceMessageRequest;
use Modules\Facebook\Repositories\FacebookPagesRepository;
use Modules\Facebook\Repositories\PageReportRepository;
use Modules\Facebook\Repositories\SequenceMessageRepository;

/**
 * Class BroadCastMessageController
 * @package Modules\Facebook\Http\Controllers
 */
class BroadCastMessageController extends Controller
{
    /**
     * @param Request $request
     * @param int $pageId
     * @param PageReportRepository $pageReportRepository
     * @return Factory|View
     */
    public function index(
        Request $request,
        $pageId,
        PageReportRepository $pageReportRepository
    )
    {
        try {
            $page = FacebookPagesRepository::getPageById($pageId);
            $user = $request->session()->get('user');
            $pageWelcomeMessage = FacebookPagesRepository::getPageWelcomeMessage($page);
            $stepSequences = SequenceMessageRepository::getStepSequences($page);
            $dateSequences = SequenceMessageRepository::getDateSequences($page);
            $recurringSequences = SequenceMessageRepository::getRecurringSequences($page);
            $subscribers = FacebookPagesRepository::fetchSubscribersOfTheDay($page->id);

            return view('facebook::messageBot.broadCastMessage', [
                'page' => $page,
                'user' => $user,
                'pageWelcomeMessage' => $pageWelcomeMessage,
                'stepSequences' => $stepSequences,
                'dateSequences' => $dateSequences,
                'recurringSequences' => $recurringSequences,
                'subscribers' => $subscribers,
                'pageReportRepository' => $pageReportRepository
            ]);

        } catch (Exception $e) {
            dd($e);
            return view('errors.Error');
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function configure(Request $request)
    {
        try {
            $page = FacebookPagesRepository::getPageById($request->pageId);
            $page->do_broadcast_message = filter_var($request->status, FILTER_VALIDATE_BOOLEAN);
            $page->subscribe_button_name = $request->subsBtnText;
            $page->unsubscribe_button_name = $request->unsubsBtnText;
            $page->subscription_message = $request->subsMsg;

            FacebookPagesRepository::storePage($page);
            return Response::json(["code" => 200, "status" => "success", "message" => "Message inserted successfully"]);
        } catch (Exception $e) {
            dd($e);
            return Response::json(["code" => 400, "status" => "failure", "message" => "Some error occured"]);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getSubscribedUsers(Request $request)
    {
        try {
            $page = Page::find($request->pageId);
            $data = [];
            if ($request->withInADay == 'true') {
                $subscribers = FacebookPagesRepository::fetchSubscribersOfTheDay($page->id);
                $data = [
                    'type' => 'withIn24hrs',
                    'subscribers' => $subscribers
                ];

            } elseif ($request->outSideADay == 'true') {
                $subscribers = FacebookPagesRepository::fetchSubscribersBeforeToday($page->id);
                $data = [
                    'type' => 'beforeIn24hrs',
                    'subscribers' => $subscribers
                ];
            }

            return Response::json($data, 200);
        } catch (Exception $e) {
            return Response::json([], 400);
        }
    }
}
