<?php

namespace Modules\Facebook\Http\Controllers\Page;

use App\Modules\User\Models\facebookpages;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Facebook\Entities\UserCredential;
use Modules\Facebook\Http\Controllers\FacebookController;
use Modules\Facebook\Repositories\FacebookPagesRepository;
use Modules\Facebook\Repositories\FacebookRepository;

/**
 * Class PageDisplayController
 * @package Modules\Facebook\Http\Controllers\Page
 */
class PageDisplayController extends Controller
{
    /**
     * @var FacebookController
     */
    private $facebookController;

    public function __construct(FacebookController $facebookController)
    {
        $this->facebookController = $facebookController;
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        try {
            $fb = FacebookController::getFacebookGraphApi();
            if (
                FacebookRepository::getFacebookCredentials($request->facebookUserId)
                instanceof UserCredential
            ) {
                $this->facebookController->storeCredentials($request);
            }

            $user = $request->session()->get('user');

            $facebookResponse = $fb->get('/' . $request->facebookUserId . '/accounts', $request->userAccessToken);
            $facebookUserInfo = json_decode($facebookResponse->getBody());
        } catch (\Exception $e) {
            return response()->json([
                "status" => 400,
                "pages" => "User Access Token is null",
                "user" => $user
            ]);
        }

        $dashBoardPages =  FacebookPagesRepository::getAllPages($user['user_id']);

        $pages = $facebookUserInfo->data;
        foreach ($dashBoardPages as $currentPage) {
            foreach ($facebookUserInfo->data as $key => $allPage) {
                if($currentPage->facebook_page_id == $allPage->id) {
                    unset($pages[$key]);
                }
            }
        }

        return response()->json([
            "status" => 200,
            "pages" => $pages,
            "user" => $user
        ]);
    }
}
