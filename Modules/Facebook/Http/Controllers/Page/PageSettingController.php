<?php

namespace Modules\Facebook\Http\Controllers\Page;

use Braintree\Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Repositories\FacebookPagesRepository;
use Modules\Facebook\Repositories\PageReportRepository;

/**
 * Class PageSettingController
 * @package Modules\Facebook\Http\Controllers\Page
 */
class PageSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param PageReportRepository $pageReportRepository
     * @param $pageId
     * @return Renderable
     * @throws \Exception
     */
    public function index(
        Request $request,
        PageReportRepository $pageReportRepository,
        $pageId
    )
    {
        $user = $request->session()->get('user');
        $page = FacebookPagesRepository::getPageById($pageId);
        $serviceStatus = ['currentFan' => 0, 'fansGained' => 0, 'average' => 0];

        return view('facebook::page-setting',
            [
                'user' => $user,
                'page' => $page,
                'serviceStatus' => $serviceStatus,
                'pageReportRepository' => $pageReportRepository
            ]
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateFanMessageStatus(Request $request)
    {
        try {
            $page = Page::find($request->pageId);
            $page->do_reply_message_fan = filter_var($request->status, FILTER_VALIDATE_BOOLEAN);
            $page->save();

            $response = Response::json("success", 200);
        } catch (Exception $e) {
            $response = Response::json("error", 500);
        }

        return  $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateNonFanLikeStatus(Request $request)
    {
        try {
            $page = Page::find($request->pageId);
            $page->do_like_non_fan = filter_var($request->status, FILTER_VALIDATE_BOOLEAN);
            $page->save();

            $response = Response::json("success", 200);
        } catch (Exception $e) {
            $response = Response::json("error", 500);
        }

        return  $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateFanCommentStatus(Request $request)
    {
        try {
            $page = Page::find($request->pageId);
            $page->do_reply_comment_fan = filter_var($request->status, FILTER_VALIDATE_BOOLEAN);
            $page->save();

            $response = Response::json("success", 200);
        } catch (Exception $e) {
            $response = Response::json("error", 500);
        }

        return  $response;
    }
}
