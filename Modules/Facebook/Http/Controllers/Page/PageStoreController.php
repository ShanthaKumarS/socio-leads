<?php

namespace Modules\Facebook\Http\Controllers\Page;

use App\Modules\User\Models\facebookpages;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Http\Controllers\LoginController;
use Modules\Facebook\Http\Controllers\Post\PostController;

/**
 * Class PageStoreController
 * @package Modules\Facebook\Http\Controllers\Page
 */
class PageStoreController extends Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var PostController
     */
    protected $postController;

    /**
     * @var Page
     */
    protected $page;

    /**
     * PageStoreController constructor.
     * @param PostController $postsController
     * @param Page $page
     */
    public function __construct(
        PostController $postsController,
        Page $page
    )
    {
        $this->postController = $postsController;
        $this->page = $page;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws FacebookSDKException
     */
    public function store(Request $request)
    {
        try {
            $this->request = $request;
            $pages = $this->storeFacebookPages();

            $response = response()->json([
                "status" => 200,
                'message' => 'Success',
                'pages' => $pages
            ]);

        } catch (Exception $e) {
            $response = response()->json([
                "status" => 400,
                'message' => $e->getMessage()
            ]);
        }

        return $response;
    }

    /**
     * @return array
     * @throws FacebookSDKException
     */
    private function storeFacebookPages()
    {
        $user = $this->request->session()->get('user');
        $fb = LoginController::getFacebookGraphApi();

        $pages = [];
        foreach ($this->request->pages as $pageInfo) {
            $subscribe['subscribed_fields'] = [
                'messages',
                'message_echoes',
                'message_deliveries',
                'messaging_optins',
                'messaging_postbacks',
                'message_reads',
                'messaging_referrals',
                'feed'
            ];

            $subscribeStatus = $fb->post('/' . $pageInfo['pageId'] . '/subscribed_apps', $subscribe, $pageInfo['pageAccessToken']);
            $subscribeStatus = $subscribeStatus->getDecodedBody();

            if ($subscribeStatus['success']) {

                $longLivedPageToken = $fb->get("/oauth/access_token?grant_type=fb_exchange_token&client_id=" . $fb->getApp()->getId() . "&client_secret=" . $fb->getApp()->getSecret() . "&fb_exchange_token=" . $pageInfo['pageAccessToken'], $pageInfo['pageAccessToken']);
                $longLivedPageToken = $longLivedPageToken->getDecodedBody();

                if ($this->persistMenu($fb, $pageInfo) == 200) {
                    $pageDetails = $fb->get('/' . $pageInfo['pageId'] . '?fields=fan_count,username,cover,picture.type(large)', $pageInfo['pageAccessToken']);
                    $pageDetails = $pageDetails->getDecodedBody();


                    $page = clone $this->page;
                    $page->page_access_token = $longLivedPageToken['access_token'];
                    $page->facebook_page_id = $pageInfo['pageId'];
                    $page->page_name = $pageInfo['pageName'];
                    $page->like_count = $pageDetails['fan_count'];
                    $page->user_id = $user['user_id'];

                    if (isset($pageDetails['cover']['source'])) {
                        $page->cover_picture = $pageDetails['cover']['source'];
                    }

                    if ($pageDetails['picture']['data']['url']) {
                        $page->profile_picture = $pageDetails['picture']['data']['url'];
                    }

                    $page->save();
                    $this->storePosts($page, $fb);

                    $pages[] = $page;
                }
            }
        }
        $this->request->session()->put('pages', $pages);
        return $pages;
    }

    /**
     * @param Facebook $fb
     * @param array $pageInfo
     * @return int
     * @throws FacebookSDKException
     */
    private function persistMenu(Facebook $fb, array $pageInfo)
    {
        $payload = [
            "get_started" => [
                "payload" => "request12"
            ],
            "persistent_menu" => [
                ["locale" => "default",
                    "composer_input_disabled" => false,
                    "call_to_actions" => [
                        [
                            "title" => "Subscribe",
                            "type" => "postback",
                            "payload" => "request12"
                        ]
                    ]
                ],
                [
                    "locale" => "zh_CN",
                    "composer_input_disabled" => false
                ]
            ]
        ];
        $persistMenu = $fb->post('/' . $pageInfo['pageId'] . '/messenger_profile', $payload, $pageInfo['pageAccessToken']);

        return $persistMenu->getHttpStatusCode();
    }

    /**
     * @param Page $page
     * @param Facebook $fb
     * @throws FacebookSDKException
     */
    private function storePosts($page, $fb)
    {
        $posts = $this->postController->hydratePagePosts($page, $fb);
        $page->posts()->saveMany($posts);
    }
}
