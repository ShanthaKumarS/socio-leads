<?php

namespace Modules\Facebook\Http\Controllers\Page;

use App\Modules\User\Models\facebookpages;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Modules\Facebook\Http\Controllers\FacebookController;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class PageDeleteController
 * @package Modules\Facebook\Http\Controllers\Page
 */
class PageDeleteController extends Controller
{
    /**
     * Remove the specified page from dashboard.
     *
     * @param Request $request
     * @param $pageId
     * @return RedirectResponse
     * @throws FacebookSDKException
     * @throws \Exception
     */
    public function delete(Request $request, $pageId)
    {
        try {
            $fb = FacebookController::getFacebookGraphApi();
            $page = FacebookPagesRepository::getPageById($pageId);
            if (FacebookPagesRepository::removePage($page)) {
                $subscribe['subscribed_fields'] = ['messages','message_echoes','message_deliveries','messaging_optins','messaging_postbacks','message_reads', 'messaging_referrals','feed','messages'];
                $persistent_menu = ['fields' => ['persistent_menu']];

                $fb->delete('/' . $page->facebook_page_id . '/subscribed_apps', $subscribe, $page->page_access_token);
                $fb->delete('/' . $page->facebook_page_id . '/messenger_profile', $persistent_menu, $page->page_access_token);
            }

            $user = $request->session()->get('user');
            $pages = FacebookPagesRepository::getAllPages($user['user_id']);
            $request->session()->put('pages', $pages);

            return Redirect::back();
        } catch (Exception $e) {
            $user = $request->get('user');
            $pages = FacebookPagesRepository::getAllPages($user['user_id']);
            $request->session()->put('pages', $pages);

            return Redirect::back();
        }
    }
}
