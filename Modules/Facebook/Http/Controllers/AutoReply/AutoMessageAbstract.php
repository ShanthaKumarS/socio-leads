<?php
namespace Modules\Facebook\Http\Controllers\AutoReply;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\AutoReply\Message;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\Post;
use Modules\Facebook\Http\Requests\AutoMessageRequest;

/**
 * Class AutoMessageAbstract
 * @package Modules\Facebook\Http\Controllers\AutoReply
 */
abstract class AutoMessageAbstract implements AutoReplyInterface
{

    /**
     * @var FilterController
     */
    protected $filterController;

    /**
     * @var Message
     */
    protected $message;

    /**
     * AutoMessageAbstract constructor.
     *
     * @param FilterController $filterController
     * @param Message $message
     */
    public function __construct(
        FilterController $filterController,
        Message $message
    )
    {
        $this->filterController = $filterController;
        $this->message = $message;
    }

    /**
     * Store a newly created resource in storage.
     * @param AutoMessageRequest $request
     * @param $foreignKey
     * @return JsonResponse
     */
    public function storeMessage(AutoMessageRequest $request, $foreignKey)
    {
        return $this->store($request, $foreignKey);
    }

    /**
     * Store a newly created resource in storage.
     * @param \Modules\Facebook\Http\Requests\AutoReplyInterface $request
     * @param $foreignKey
     * @return JsonResponse
     */
    public function store(\Modules\Facebook\Http\Requests\AutoReplyInterface $request, $foreignKey)
    {
        $messageable = $this->getMessageable($foreignKey);

        try {
            if ($request->messageId) {
                $this->message = Message::find($request->messageId);
                $this->message->filters()->delete();
            }
            $this->message->text = $request->message;
            $messageable->autoMessages()->save($this->message);

            $filtersInfo = $this->filterController->hydrateFilters(json_decode($request->filters));
            $this->message->filters()->saveMany($filtersInfo["filters"]);

            $response = Response::json([
                "id" => $this->message->id,
                "message" => $this->message->message_text,
                'filters' => $filtersInfo["tags"],
            ], 200);
        } catch (Exception $e) {
            dd($e);
            $response = Response::json("error in inserting message", 500);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            Message::destroy($request->messageId);
            $response = Response::json(["id" => $request->messageId, "message" => "message is successfully deleted"], 200);
        } catch (Exception $e) {
            $response = Response::json("error in inserting message", 500);
        }

        return $response;
    }

    /**
     * @param $foreignKey
     * @return Post|Page
     */
    protected abstract function getMessageable($foreignKey);
}
