<?php

namespace Modules\Facebook\Http\Controllers\AutoReply;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Session;
use Modules\Facebook\Entities\Post;

/**
 * Class PostAutoCommentController
 * @package Modules\Facebook\Http\Controllers\AutoReply
 */
class PostAutoCommentController extends AutoCommentAbstract
{

    /**
     * @param $postId
     * @return Factory|View
     */
    public function index($postId)
    {

        return view('facebook::posts-settings',[
            'user' => Session::get('user'),
            'post' => Post::find($postId)
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function getCommentable($foreignKey)
    {
        return Post::find($foreignKey);
    }
}
