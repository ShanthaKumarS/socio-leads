<?php
namespace Modules\Facebook\Http\Controllers\AutoReply;

use Modules\Facebook\Entities\Page;

/**
 * Class PageAutoCommentController
 * @package Modules\Facebook\Http\Controllers\AutoReply
 */
class PageAutoCommentController extends AutoCommentAbstract
{
    /**
     * @inheritDoc
     */
    protected function getCommentable($foreignKey)
    {
        return Page::find($foreignKey);
    }
}
