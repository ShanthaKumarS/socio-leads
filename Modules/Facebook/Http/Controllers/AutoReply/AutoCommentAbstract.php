<?php
namespace Modules\Facebook\Http\Controllers\AutoReply;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Facebook\Entities\AutoReply\Comment;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\Post;
use Modules\Facebook\Http\Controllers\AutoReply\AutoReplyAbstract;
use Modules\Facebook\Http\Requests\AutoCommentRequest;

/**
 * Class AutoCommentAbstract
 * @package Modules\Facebook\Http\Controllers\AutoReply
 */
abstract class AutoCommentAbstract implements AutoReplyInterface
{
    /**
     * @var FilterController
     */
    protected $filterController;

    /**
     * @var Comment
     */
    protected $comment;

    /**
     * AutoCommentAbstract constructor.
     *
     * @param FilterController $filterController
     * @param Comment $comment
     */
    public function __construct(
        FilterController $filterController,
        Comment $comment
    )
    {
        $this->filterController = $filterController;
        $this->comment = $comment;
    }

    /**
     * @param AutoCommentRequest $request
     * @param $foreignKey
     * @return JsonResponse
     */
    public function storeComment(AutoCommentRequest $request, $foreignKey)
    {
        return $this->store($request, $foreignKey);
    }

    /**
     * Store a newly created resource in storage.
     * @param \Modules\Facebook\Http\Requests\AutoReplyInterface $request
     * @param $foreignKey
     * @return JsonResponse
     */
    public function store(\Modules\Facebook\Http\Requests\AutoReplyInterface $request, $foreignKey)
    {
        $commentable = $this->getCommentable($foreignKey);

        try {
            if ($request->commentId) {
                $this->comment = Comment::find($request->commentId);
                $this->comment->filters()->delete();
            }
            $this->comment->text = $request->comment;
            $commentable->autoComments()->save($this->comment);

            $filtersInfo = $this->filterController->hydrateFilters(json_decode($request->filters));
            $this->comment->filters()->saveMany($filtersInfo["filters"]);

            $response = Response::json([
                "id" => $this->comment->id,
                "comment" => $this->comment->text,
                'filters' => $filtersInfo["tags"],
            ], 200);
        } catch (Exception $e) {
            dd($e);
            $response = Response::json("error in inserting comment", 500);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {
            Comment::destroy($request->commentId);
            $response = Response::json(["id" => $request->commentId, "message" => "comment is successfully deleted"], 200);
        } catch (Exception $e) {
            $response = Response::json("error in inserting comment", 500);
        }

        return $response;
    }

    /**
     * @param $foreignKey
     * @return Post|Page
     */
    protected abstract function getCommentable($foreignKey);
}
