<?php
namespace Modules\Facebook\Http\Controllers\AutoReply;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Interface AutoReplyInterface
 * @package Modules\Facebook\Http\Controllers\AutoReply
 */
interface AutoReplyInterface
{
    /**
     * Store a newly created resource in storage.
     * @param \Modules\Facebook\Http\Requests\AutoReplyInterface $request
     * @param $foreignKey
     * @return JsonResponse
     */
     function store(\Modules\Facebook\Http\Requests\AutoReplyInterface $request, $foreignKey);


    /**
     * @param Request $request
     * @return JsonResponse
     */
    function destroy(Request $request);
}
