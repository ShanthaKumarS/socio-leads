<?php
namespace Modules\Facebook\Http\Controllers\AutoReply;

use Modules\Facebook\Entities\AutoReply\Filter;
use Modules\Facebook\Http\Controllers\AutoReply\AutoReplyAbstract;

/**
 * Class FilterController
 * @package Modules\Facebook\Http\Controllers\AutoReply
 */
class FilterController
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * FilterController constructor.
     * @param Filter $filter
     */
    public function __construct(Filter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param array $requestFilters
     * @return array
     */
    public function hydrateFilters(array $requestFilters)
    {
        $filters = [];
        $tags = [];
        foreach ($requestFilters as $requestFilter) {
            $filter = clone $this->filter;
            $filter->filter_text = $requestFilter->value;

            $filters[] = $filter;
            $tags[] = $requestFilter->value;
        }

        return [
            "filters" => $filters,
            "tags" => $tags
        ];
    }
}
