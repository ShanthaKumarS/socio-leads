<?php

namespace Modules\Facebook\Http\Controllers\AutoReply;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Session;
use Modules\Facebook\Entities\Post;

/**
 * Class PostAutoMessageController
 * @package Modules\Facebook\Http\Controllers\AutoReply
 */
class PostAutoMessageController extends AutoMessageAbstract
{
    /**
     * @param $postId
     * @return Factory|View
     */
    public function index($postId)
    {
        return view('facebook::posts-message-settings',[
            'user' => Session::get('user'),
            'post' => Post::find($postId)
        ]);
    }

    /**
     * @param $foreignKey
     * @return Post
     */
    protected function getMessageable($foreignKey)
    {
        return Post::find($foreignKey);
    }
}
