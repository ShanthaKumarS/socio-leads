<?php

namespace Modules\Facebook\Http\Controllers\Webhook;

use Braintree\Exception;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\FacebookUser;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\PageSubscriber;
use Modules\Facebook\Http\Controllers\LoginController;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class MessageController
 * @package Modules\Facebook\Http\Controllers\Webhook
 */
class MessageController extends Controller
{
    private $bindSubscriptionMethod = [
        'Subscribed' => 'subscribe',
        'UnSubscribed' => 'unSubscribe'
    ];

    /**
     * @var PayloadController
     */
    private $payloadController;

    /**
     * @return MessageController
     */
    public static function getInstance()
    {
        return new MessageController();
    }

    /**
     * @param array $data
     * @return void
     */
    public function worker($data)
    {
        try {
            $this->payloadController = PayloadController::getInsttance();
            $page = FacebookPagesRepository::getPageByFbId($data['messaging' ][0]['recipient']['id']);

            if ($page instanceof  Page && $page->do_broadcast_message) {
                $senderInfo = $this->getSenderInfo(
                    $data['messaging' ][0]['sender']['id'],
                    $page->page_access_token
                );

                if (isset($data['messaging'][0]['postback']['payload'])) {
                    $payload = $this->payloadController->getSubscriptionMessagePayload($data, $page, $senderInfo);
                    $this->sendMessage($payload, $page);
                } elseif (isset($data['messaging'][0]['message']['quick_reply']['payload'])) {
                    $subscription = $this->bindSubscriptionMethod[trim($data['messaging'][0]['message']['quick_reply']['payload'])];
                    $this->$subscription(
                        $data,
                        $page,
                        $senderInfo
                    );
                }
            }
        } catch (\Exception $e) {
            Log::info(print_r($e, true));
        }
    }

    /**
     * @param array $data
     * @param Page $page
     * @param array $senderInfo
     * @throws FacebookSDKException
     */
    private function subscribe(
        array $data,
        Page $page,
        array $senderInfo
    )
    {
        try {
            if (! PageSubscriber::where('facebook_page_subscriber_id', $data['messaging' ][0]['sender']['id'])->first()) {
                $pageSubscriber = new PageSubscriber();
                $pageSubscriber->facebook_page_subscriber_id = $data['messaging' ][0]['sender']['id'];
                $pageSubscriber->subscribed_at = new \DateTime();
                $pageSubscriber->is_subscriber = true;
                $pageSubscriber->page_id = $page->id;

                $pageSubscriber->save();
            }

        } catch (Exception $e) {

        }

        $payload = $this->payloadController->getWelcomeMessagePayload($data, $page, $senderInfo);
        $this->sendMessage($payload, $page);
    }

    /**
     * @param array $data
     * @param Page $page
     * @param array $senderInfo
     * @throws FacebookSDKException
     */
    private function unSubscribe(
        array $data,
        Page $page,
        array $senderInfo
    )
    {
        try {
            $pageSubscriber = $page->pageSubscribers->where('facebook_page_subscriber_id', $data['messaging' ][0]['sender']['id'])->first();
            $pageSubscriber->delete();
        } catch (\Exception $e) {
            Log::info(print_r($e, true));
        }
        $payload = $this->payloadController->getUnsubscribedPayload($data, $page, $senderInfo);
        $this->sendMessage($payload, $page);

    }

    /**
     * @param $payload
     * @param $page
     * @throws FacebookSDKException
     */
    private function sendMessage($payload, $page)
    {
        $fb = LoginController::getFacebookGraphApi();
        $response = $fb->post('/' . $page->facebook_page_id . '/messages', $payload, $page->page_access_token);
        $response->getDecodedBody();
    }

    /**
     * @param $senderId
     * @param $pageAccessToken
     * @return mixed
     * @throws FacebookSDKException
     */
    private function getSenderInfo($senderId, $pageAccessToken)
    {
        $fb = LoginController::getFacebookGraphApi();
        $response = $fb->get('/' . $senderId . '?fields=first_name,last_name', $pageAccessToken);
        return $response->getDecodedBody();
    }
}
