<?php

namespace Modules\Facebook\Http\Controllers\Webhook;

use Carbon\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\FacebookUser;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\PageFansReport;

/**
 * Class FanController
 * @package Modules\Facebook\Http\Controllers\Webhook
 */
class FanController extends Controller
{
    /**
     * @return FanController
     */
    public static function getInstance()
    {
        return new FanController();
    }

    /**
     * @param array $data
     * @return void
     */
    public function worker($data)
    {
        try {
            $page = Page::where('facebook_page_id', $data['id'])->get()->first();
            if ($page) {
                $this->updatePageFansReport($page);
            }
        } catch (\Exception $e) {
            Log::info(print_r($e, true));
        }
    }

    /**
     * updates the count of todays like for the post
     * @param Page $page
     */
    private function updatePageFansReport($page)
    {
        $pageFanReport = PageFansReport::where('page_id', $page->id)->where('date', Carbon::now()->startOfDay())->first();
        if ($pageFanReport instanceof PageFansReport) {
            $pageFanReport->date = Carbon::now()->startOfDay();
            $pageFanReport->fan_count ++;
        } else {
            $postLikesReport = PageFansReport::getInstance();

            $postLikesReport->date = Carbon::now()->startOfDay();
            $postLikesReport->fans_count = 1;
            $postLikesReport->user_id = $page->user_id;
            $postLikesReport->page_id = $page->id;
        }

        $postLikesReport->save();
    }
}
