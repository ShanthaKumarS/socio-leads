<?php

namespace Modules\Facebook\Http\Controllers\Webhook;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Repositories\FacebookPagesRepository;

/**
 * Class PayloadController
 * @package Modules\Facebook\Http\Controllers\Webhook
 */
class PayloadController extends Controller
{
    /**
     * @return PayloadController
     */
    public static function getInsttance()
    {
        return new PayloadController();
    }

    /**
     * @param array $data
     * @param Page $page
     * @return array
     */
    public function getSubscriptionMessagePayload(array $data, Page $page, array $senderInfo)
    {
        $text = preg_replace("/({{fb_first_name}}|{{FacebookName}})/i", $senderInfo['first_name'], $page->subscription_message);

        $payload["recipient"] = ["id" => $data['messaging'][0]['sender']['id']];
        $payload["tag"] = "NON_PROMOTIONAL_SUBSCRIPTION";
        $payload["message"] = [
            "text" => $text,
            "quick_replies" => [
                [
                    "content_type" => "text",
                    "title" => $page->subscribe_button_name,
                    "payload" => "Subscribed"
                ],
                [
                    "content_type" => "text",
                    "title" => $page->unsubscribe_button_name,
                    "payload" => "UnSubscribed"
                ]
            ]
        ];

        return $payload;
    }

    /**
     * @param array $data
     * @param Page $page
     * @param array $senderInfo
     * @return array
     */
    public function getWelcomeMessagePayload(array $data, Page $page, array $senderInfo)
    {
        $pageWelcomeMessage = FacebookPagesRepository::getPageWelcomeMessage($page);
        $text = preg_replace("/({{fb_first_name}}|{{FacebookName}})/i", $senderInfo['first_name'], $pageWelcomeMessage);

        return $this->subscriptionPayload($data, $text);
    }

    /**
     * @param array $data
     * @param Page $page
     * @param array $senderInfo
     * @return array
     */
    public function getUnsubscribedPayload(array $data, Page $page, array $senderInfo)
    {
        $pageWelcomeMessage = FacebookPagesRepository::getPageUnsubscribedMessage($page);
        $text = preg_replace("/({{fb_first_name}}|{{FacebookName}})/i", $senderInfo['first_name'], $pageWelcomeMessage);

        return $this->subscriptionPayload($data, $text);
    }

    /**
     * @param array $data
     * @param string $text
     * @return array mixed
     */
    private function subscriptionPayload(array $data, string $text)
    {
        $payload["recipient"] = ["id" => $data['messaging'][0]['sender']['id']];
        $payload["tag"] = "NON_PROMOTIONAL_SUBSCRIPTION";
        $payload["message"] = [
            "text" => $text,
        ];

        return $payload;
    }
}
