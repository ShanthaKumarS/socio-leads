<?php

namespace Modules\Facebook\Http\Controllers\Webhook;

use Carbon\Carbon;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\AutoReply\PostCommentsReport;
use Modules\Facebook\Entities\AutoReply\PostLikesReport;
use Modules\Facebook\Entities\AutoReply\PostMessagesReport;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\Post;
use Modules\Facebook\Http\Controllers\LoginController;
use Modules\Facebook\Http\Controllers\PageSubscriberController;

/**
 * Class CommentController
 * @package Modules\Facebook\Http\Controllers\Webhook
 */
class CommentController extends Controller
{
    /**
     * @var string
     */
    private $filterText;

    /**
     * @var array $bindPostAction
     */
    private $bindPostAction = [
        'add' => 'autoReply'
    ];

    /**
     * @var PageSubscriberController
     */
    private $pageSubscriberController;

    /**
     * @var Page $page
     */
    private $page;

    /**
     * @var int $senderId
     */
    private $senderId;

    /**
     * @var int $commentId
     */
    private $commentId;

    /**
     * @var Post $post
     */
    private $post;

    /**
     * @var \Facebook\Facebook
     */
    private $fb;

    /**
     * CommentController constructor.
     * @param PageSubscriberController $pageSubscriberController
     */
    public function __construct(PageSubscriberController $pageSubscriberController)
    {
        $this->pageSubscriberController = $pageSubscriberController;
    }

    /**
     * @param array $data
     */
    public function worker($data)
    {
        try {
            if (isset($this->bindPostAction[$data['changes'][0]['value']['verb']])) {
                $action = $this->bindPostAction[$data['changes'][0]['value']['verb']];
                $this->$action($data);
            }
        } catch (\Exception $e) {
            Log::info(print_r($e, true));
        }
    }

    /**
     * @param $webhookPost
     * @return bool
     * @throws FacebookSDKException
     */
    private function autoReply(array $webhookPost)
    {
        $this->filterText = $webhookPost['changes'][0]['value']['message'];
        $this->page = Page::where('facebook_page_id', $webhookPost['id'])->get()->first();
        $this->post = Post::where('facebook_post_id', $webhookPost['changes'][0]['value']['post_id'])->first();
        $this->senderId = $webhookPost['changes'][0]['value']['from']['id'];
        $this->commentId = $webhookPost['changes'][0]['value']['comment_id'];
        $this->fb = LoginController::getFacebookGraphApi();

        if (
            $webhookPost['id'] != $webhookPost['changes'][0]['value']['from']['id'] &&
            $this->page &&
            $this->post
        ) {
            if (
                $this->page->do_broadcast_message &&
                $this->page->do_like_fan &&
                $this->post->do_like_fan
            ) {
                $this->likeComment();
            }

            if (! $this->reply($this->post)) {
                return $this->reply($this->page);
            }
        }
    }

    /**
     * @param $replyable
     * @return bool
     * @throws FacebookSDKException
     */
    private function reply($replyable)
    {
        if ($comment = $this->getReplyText($replyable->autoComments)) {
            if (
                $this->page->do_broadcast_message &&
                $this->page->do_reply_comment_fan &&
                $this->post->do_reply_comment_fan
            ) {
                $this->replyAsComment($comment->text);
                $this->updatePostCommentReport($comment);
            }

            return true;

        } else if ($message = $this->getReplyText($replyable->autoMessages)) {
            if (
                $this->page->do_broadcast_message &&
                $this->page->do_reply_message &&
                $this->post->do_reply_message
            ) {
                $this->replyAsMessage($message->text);
                $this->updatePostMessageReport($message);
            }

            return true;
        }

        return false;
    }

    /**
     * @param $filterables
     * @return array|bool
     * @throws FacebookSDKException
     */
    private function getReplyText($filterables)
    {
        foreach ($filterables as $filterable) {
            $filter = $filterable->filters->where("filter_text", $this->filterText)->first();

            if ($filter) {
                $filterable->text = $this->pageSubscriberController->replaceFacebookName($this->senderId, $this->page->page_access_token, $filterable->text);
                return  $filterable;
            }
        }

        return false;
    }

    /**
     * @param string $text
     * @return bool
     * @throws FacebookSDKException
     */
    private function replyAsComment($text)
    {
        $comment['message'] = $text;
        $this->fb->post('/' . $this->commentId . '/comments', $comment, $this->page->page_access_token);

        return true;
    }

    /**
     * @return bool
     * @throws FacebookSDKException
     */
    private function replyAsMessage($text)
    {
        $message = [
            "recipient" => [
                "comment_id" => $this->commentId
            ],
            "message" => [
                "text" => $text
            ]
        ];

        $this->fb->post('/' . $this->page->facebook_page_id . '/messages', $message, $this->page->page_access_token);
        return true;
    }

    /**
     * Likes every Comments
     * @throws FacebookSDKException
     */
    private function likeComment()
    {
        $this->fb->post('/' . $this->commentId . '/likes', [], $this->page->page_access_token);
        $this->updatePostLikeReport();
    }

    /**
     * updates the count of todays like for the post
     */
    private function updatePostLikeReport()
    {
        $postLikesReport = $this->post->postLikesReports->where('liked_at', Carbon::now()->startOfDay())->first();
        if ($postLikesReport) {
            $postLikesReport->date = Carbon::now()->startOfDay();
            $postLikesReport->likes_count ++;
        } else {
            $postLikesReport = PostLikesReport::getInstance();

            $postLikesReport->date = Carbon::now()->startOfDay();
            $postLikesReport->likes_count = 1;
            $postLikesReport->user_id = $this->page->user_id;
            $postLikesReport->page_id = $this->page->id;
        }

        $this->post->postLikesReports()->save($postLikesReport);
    }

    /**
     * @param $comment
     */
    private function updatePostCommentReport($comment)
    {
        $postCommentReport = $this->post->postCommentReports->where('date', Carbon::now()->startOfDay())->first();
        if ($postCommentReport) {
            $postCommentReport->date = Carbon::now()->startOfDay();
            $postCommentReport->comments_count ++;
        } else {
            $postCommentReport = PostCommentsReport::getInstance();

            $postCommentReport->date = Carbon::now()->startOfDay();
            $postCommentReport->comments_count = 1;
            $postCommentReport->user_id = $this->page->user_id;
            $postCommentReport->page_id = $this->page->id;
            $postCommentReport->fb_auto_comment_id = $comment->id;
        }

        $this->post->postCommentReports()->save($postCommentReport);
    }

    /**
     * @param $comment
     */
    private function updatePostMessageReport($message)
    {
        $postMessageReport = $this->post->postMessageReports->where('date', Carbon::now()->startOfDay())->first();
        if ($postMessageReport) {
            $postMessageReport->date = Carbon::now()->startOfDay();
            $postMessageReport->messages_count ++;
        } else {
            $postMessageReport = PostMessagesReport::getInstance();

            $postMessageReport->date = Carbon::now()->startOfDay();
            $postMessageReport->messages_count = 1;
            $postMessageReport->user_id = $this->page->user_id;
            $postMessageReport->page_id = $this->page->id;
            $postMessageReport->fb_auto_message_id = $message->id;
        }

        $this->post->postCommentReports()->save($postMessageReport);
    }
}

