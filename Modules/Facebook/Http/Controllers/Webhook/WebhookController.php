<?php

namespace Modules\Facebook\Http\Controllers\Webhook;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

/**
 * Class WebhookController
 * @package Modules\Facebook\Http\Controllers\Webhook
 */
class WebhookController extends Controller
{
    use DispatchesJobs;

    private $bindJob = [
        'post' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Post',
            'queue' => 'post'
        ],
        'status' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Post',
            'queue' => 'post'
        ],
        'photo' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Post',
            'queue' => 'post'
        ],
        'video' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Post',
            'queue' => 'post'
        ],
        'comment' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Comment',
            'queue' => 'comment'
        ],
        'message' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Message',
            'queue' => 'message'
        ],
        'like' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Fan',
            'queue' => 'fan'
        ],
        'default' => [
            'job' => 'Modules\\Facebook\\Jobs\\Webhook\\Post',
            'queue' => 'default'
        ],
    ];

    /**
     * Verify Webhook from facebook
     *
     * @param Request $request
     * @return int
     */
    public function verify(Request $request)
    {
        return $request->hub_challenge;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function handle(Request $request)
    {
        try {
            if(! empty($request->entry)) {
                foreach ($request->entry as $entry) {
                    if (isset($entry['changes'][0]['value']['item']) && isset($this->bindJob[$entry['changes'][0]['value']['item']])) {
                        $job = $this->bindJob[$entry['changes'][0]['value']['item']];
                    } else if (isset($entry['messaging'])) {
                        $job = $this->bindJob['message'];
                    } else {
                        $job = $this->bindJob['default'];
                    }

                    $newJob = new $job['job'];
                    $newJob = $newJob->setData($entry)->onQueue($job['queue']);
                    $this->dispatch($newJob, true);
                }
            }

            return Response::json('', 200);
        } catch (\Exception $e) {
            return Response::json('', 200);
        }
    }
}
