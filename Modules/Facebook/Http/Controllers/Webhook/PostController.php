<?php

namespace Modules\Facebook\Http\Controllers\Webhook;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Facebook\Entities\Post;
use Modules\Facebook\Repositories\FacebookPagesRepository;
use Modules\Facebook\Repositories\FacebookPostRepository;

/**
 * Class PostController
 * @package Modules\Facebook\Http\Controllers\Webhook
 */
class PostController extends Controller
{
    /**
     * @var array
     */
    private $bindPostAction = [
        'remove' => 'destroy',
        'add' => 'store'
    ];

    /**
     * @return PostController
     */
    public static function getInstance()
    {
        return new PostController();
    }

    /**
     * @param array $data
     */
    public function worker($data)
    {
        try {
            $action = $this->bindPostAction[$data['changes'][0]['value']['verb']];
            $this->$action($data);
        } catch (\Exception $e) {
            Log::info(print_r($e, true));
        }
    }

    /**
     * @param $webhookPost
     */
    private function destroy($webhookPost)
    {
        FacebookPostRepository::deletePostByFbPostId($webhookPost['changes'][0]['value']['post_id']);
    }

    /**
     * @param $webhookPost
     */
    private function store($webhookPost)
    {
        $post = $this->hydratePost($webhookPost);
        FacebookPostRepository::insertPost($post);
    }

    /**
     * @param $webhookPost
     * @param Post $post
     * @return Post
     */
    private function hydratePost($webhookPost)
    {
        $post =  new Post();
        $page = FacebookPagesRepository::getPageByFbId($webhookPost['changes'][0]['value']['from']['id']);
        $post->page_id = $page[0]->id;
        $post->facebook_post_id = $webhookPost['changes'][0]['value']['post_id'];
        $post->facebook_post_date = new \DateTime(date("Y-m-d h:i:sa", $webhookPost['time']));
        if (isset($webhookPost['changes'][0]['value']['link'])) {
            $post->feed_picture = $webhookPost['changes'][0]['value']['link'];
        }

        if (isset($webhookPost['changes'][0]['value']['message'])) {
            $post->feed_message = $webhookPost['changes'][0]['value']['message'];
        }

        return $post;
    }
}

