<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSequenceMessageTextButtonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sequence_message_text_buttons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('link');
            $table->unsignedInteger('sequence_message_text_id');

            $table->foreign('sequence_message_text_id')->references('id')->on('sequence_message_texts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sequence_message_text_buttons');
    }
}
