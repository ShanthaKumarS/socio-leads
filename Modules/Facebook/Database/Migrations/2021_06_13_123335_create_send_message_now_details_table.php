<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendMessageNowDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_message_now_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
            $table->integer('sent_count')->default(0);
            $table->integer('delivered_count')->default(0);
            $table->unsignedInteger('page_id');

            $table->foreign('page_id')->references('id')->on('facebook_pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('send_message_now_details');
    }
}
