<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeforeFbAutoMessageDeleteTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER `before_fb_auto_messages_delete` BEFORE DELETE ON `fb_auto_messages` FOR EACH ROW
                BEGIN
                    DELETE FROM `fb_auto_filters` WHERE `filterable_id` = OLD.id AND `filterable_type` = 'Modules\\\Facebook\\\Entities\\\AutoReply\\\Message';
                END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `after_facebook_page_insert`");
    }
}
