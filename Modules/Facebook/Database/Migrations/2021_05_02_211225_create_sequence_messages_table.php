<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSequenceMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sequence_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('time_zone');
            $table->time('send_time');
            $table->string('tag');
            $table->unsignedInteger('sequence_id')->nullable();
            $table->string('sequence_type')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sequence_messages');
    }
}
