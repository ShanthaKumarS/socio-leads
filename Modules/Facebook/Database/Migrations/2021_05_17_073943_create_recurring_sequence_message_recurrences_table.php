<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurringSequenceMessageRecurrencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurring_sequence_message_recurrences', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('week_day_id');
            $table->timestamp('send_at');
            $table->unsignedInteger('sent_count')->default(0);
            $table->unsignedInteger('delivered_count')->default(0);
            $table->unsignedInteger('sequence_message_id');

            $table->foreign('sequence_message_id', 'sequence_message_foreign')->references('id')->on('sequence_messages')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurring_sequence_message_recurrences');
    }
}
