<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookPageSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_page_subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('facebook_page_subscriber_id')->unique();
            $table->boolean('is_subscriber')->nullable();
            $table->timestamp('subscribed_at')->nullable();
            $table->unsignedInteger('page_id');

            $table->foreign('page_id')->references('id')->on('facebook_pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_page_subscribers');
    }
}
