<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facebook_post_id')->unique();
            $table->timestamp('facebook_post_date');
            $table->string('feed_picture', 510)->nullable();
            $table->text('feed_message')->nullable();
            $table->text('feed_caption')->nullable();
            $table->text('feed_title',510)->nullable();
            $table->text('feed_story')->nullable();
            $table->text('feed_description')->nullable();
            $table->text('feed_source')->nullable();
            $table->boolean('do_reply_message')->default(true);
            $table->boolean('do_like_comment')->default(true);
            $table->boolean('do_reply_comment')->default(true);
            $table->unsignedInteger('page_id');

            $table->foreign('page_id')->references('id')->on('facebook_pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
