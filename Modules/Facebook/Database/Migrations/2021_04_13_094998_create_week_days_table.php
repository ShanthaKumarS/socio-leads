<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeekDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('week_days', function (Blueprint $table) {
            $table->increments('id');
            $table->string('week_day');
            $table->timestamps();
        });

        $weekDays = [
            ['week_day' => 'sunday'],
            ['week_day' => 'monday'],
            ['week_day' => 'tuesday'],
            ['week_day' => 'wednesday'],
            ['week_day' => 'thursday'],
            ['week_day' => 'friday'],
            ['week_day' => 'saturday'],
        ];

        DB::table('week_days')->insert($weekDays);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('week_days');
    }
}
