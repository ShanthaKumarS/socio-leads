<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSequenceMessageTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sequence_message_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text');
            $table->unsignedInteger('sequence_message_id');

            $table->foreign('sequence_message_id')->references('id')->on('sequence_messages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sequence_message_texts');
    }
}
