<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAfterFacebookPageInsertTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER `after_facebook_page_insert`AFTER INSERT ON `facebook_pages` FOR EACh ROW
                BEGIN
                    INSERT INTO `bot_message_filters` (`filter`, `page_id`, `created_at`, `updated_at`) VALUES ('SUBSCRIBE', NEW.id, CURTIME(), CURTIME());
                    INSERT INTO `bot_message_filters` (`filter`, `page_id`, `created_at`, `updated_at`) VALUES ('UNSUBSCRIBE', NEW.id, CURTIME(), CURTIME());

                    INSERT INTO `bot_text_messages` (`text`, `bot_message_filter_id`, `created_at`, `updated_at`)
                    SELECT 'Hi @{{fb_first_name}}! Nice to see you. This is the default welcome message for your bot. Messages from this block are shown to your users when they first start to chat with your bot. Make sure to never leave this block empty (write something helpful to your users)', `id`, CURTIME(), CURTIME() FROM `bot_message_filters` where `page_id` = NEW.id and `filter` = 'SUBSCRIBE';

                    INSERT INTO `bot_text_messages` (`text`, `bot_message_filter_id`, `created_at`, `updated_at`)
                    SELECT 'Unsubscribed.......', `id`, CURTIME(), CURTIME() FROM `bot_message_filters` where `page_id` = NEW.id and `filter` = 'UNSUBSCRIBE';
                END
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `after_facebook_page_insert`");
    }
}
