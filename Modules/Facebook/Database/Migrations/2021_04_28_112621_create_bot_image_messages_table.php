<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotImageMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bot_image_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('path');
            $table->unsignedInteger('bot_message_filter_id');

            $table->foreign('bot_message_filter_id')->references('id')->on('bot_message_filters')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bot_image_messages');
    }
}
