<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeforePostsDeleteTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER `before_posts_delete` BEFORE DELETE ON `posts` FOR EACH ROW
                BEGIN
                    DELETE FROM `fb_auto_comments` WHERE `commentable_id` = OLD.id AND `commentable_type` = 'Modules\\\Facebook\\\Entities\\\Post';
                    DELETE FROM `fb_auto_messages` WHERE `messageable_id` = OLD.id AND `messageable_type` = 'Modules\\\Facebook\\\Entities\\\Post';
                END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
