<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeforeStepSequencesDeleteTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER `before_step_sequences_delete` BEFORE DELETE ON `step_sequences` FOR EACH ROW
                BEGIN
                    DELETE FROM `sequence_messages` WHERE `sequence_id` = OLD.id AND `sequence_type` = 'Modules\\\Facebook\\\Entities\\\SequenceMessage\\\StepSequence';
                END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
