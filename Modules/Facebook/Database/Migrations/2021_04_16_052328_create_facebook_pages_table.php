<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_access_token', 510);
            $table->string('facebook_page_id')->unique();
            $table->string('page_name');
            $table->text('subscription_message')->default('Hi @{{fb_first_name}} , ! I will keep you updated on great activities as well as important information about our page. Do you want to receive occasional notifications from me ?');
            $table->string('subscribe_button_name')->default('Subscribe');
            $table->string('unsubscribe_button_name')->default('Unsubscribe');
            $table->unsignedInteger('like_count');
            $table->string('cover_picture',510)->nullable();
            $table->string('profile_picture',510)->nullable();
            $table->boolean('do_reply_message_fan')->default(true);
            $table->boolean('do_reply_message_non_fan')->default(true);
            $table->boolean('do_like_fan')->default(true);
            $table->boolean('do_like_non_fan')->default(true);
            $table->boolean('do_reply_comment_fan')->default(true);
            $table->boolean('do_reply_comment_non_fan')->default(true);
            $table->boolean('do_broadcast_message')->default(true);
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_pages');
    }
}
