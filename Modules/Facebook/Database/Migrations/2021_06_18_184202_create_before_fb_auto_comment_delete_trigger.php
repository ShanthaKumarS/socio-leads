<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeforeFbAutoCommentDeleteTrigger extends Migration
{
    private $modelName = 'Modules\\Facebook\\Entities\\AutoReply\\Message';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER `before_fb_auto_comments_delete` AFTER DELETE ON `fb_auto_comments` FOR EACH ROW
                BEGIN
                    DELETE FROM `fb_auto_filters` WHERE `filterable_id` = OLD.id AND `filterable_type` = 'Modules\\\Facebook\\\Entities\\\AutoReply\\\Comment';
                END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
