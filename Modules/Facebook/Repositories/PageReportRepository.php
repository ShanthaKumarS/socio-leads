<?php
namespace Modules\Facebook\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class PageReportRepository
 * @package Modules\Facebook\Repositories
 */
class PageReportRepository extends ReportRepository
{
    /**
     * @var int|boolean
     */
    protected $pageId;

    /**
     * @param int $pageId
     * @return PageReportRepository
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
        return $this;
    }

    /**
     * @param $pageId
     * @param $lastMonths
     * @return Collection
     */
    public static function getPageMessagesReportByMonth($pageId, $lastMonths)
    {
        return DB::table('post_messages_reports')
            ->select(DB::raw('sum(messages_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('page_id', $pageId)
            ->get();
    }

    /**
     * @param $pageId
     * @param $lastMonths
     * @return Collection
     */
    public static function getPageCommentsReportByMonth($pageId, $lastMonths)
    {
        return DB::table('post_comments_reports')
            ->select(DB::raw('sum(comments_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('page_id', $pageId)
            ->get();
    }

    /**
     * @param $pageId
     * @param $lastMonths
     * @return Collection
     */
    public static function getPageLikesReportByMonth($pageId, $lastMonths)
    {
        return DB::table('post_likes_reports')
            ->select(DB::raw('sum(likes_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('page_id', $pageId)
            ->get();
    }

    /**
     * @param $pageId
     * @param $lastMonths
     * @return Collection
     */
    public static function getGainedPageFansByMonth($pageId, $lastMonths)
    {
        return DB::table('page_fans_reports')
            ->select(DB::raw('sum(fans_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('page_id', $pageId)
            ->get();
    }

    /**
     * @param $query
     * @return int
     */
    protected function bindCondition($query)
    {
        if ($this->lastDays) {
            $query->where('date', '>=', Carbon::now()->subDay($this->lastDays));
        }
        $query->where('page_id', $this->pageId);

        $count = $query->get()->first()->count;
        return $count ? $count : 0;
    }
}
