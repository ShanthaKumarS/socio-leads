<?php
namespace Modules\Facebook\Repositories;

use Exception;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\Post;
use Modules\Facebook\Entities\UserCredential;

/**
 * Class FacebookPostRepository
 * @package Modules\Facebook\Repositories
 */
class FacebookPostRepository
{

    /**
     * @param $pageId
     * @param $from
     * @param $count
     * @return Post[]
     * @throws Exception
     */
    public static function getLatestPosts($pageId, $from, $count)
    {
        return Post::where('page_id', $pageId)->latest()->where('id', '>', $from)->limit($count)->get();
    }

    /**
     * @param $post_id
     */
    public static function deletePostByFbPostId($post_id)
    {
        Post::where('facebook_post_id', $post_id)->first()->delete();
    }
}
