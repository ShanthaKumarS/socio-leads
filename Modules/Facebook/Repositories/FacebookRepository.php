<?php


namespace Modules\Facebook\Repositories;
use Modules\Facebook\Entities\UserCredential;

/**
 * Class FacebookRepository
 * @package Modules\Facebook\Repositories
 */
class FacebookRepository
{
    /**
     * @param $facebookUserId
     * @return mixed
     * @throws Exception
     */
    public static function getFacebookCredentials($facebookUserId)
    {
        return UserCredential::find($facebookUserId);
    }
}
