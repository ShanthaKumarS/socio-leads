<?php
namespace Modules\Facebook\Repositories;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Config;
use Modules\Facebook\Entities\Fans;
use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\PageSubscriber;

/**
 * Class FacebookPagesRepository
 * @package Modules\Facebook\Repositories
 */
class FacebookPagesRepository
{
    const TODAY = 1;

    /**
     * @param $userId
     * @return Page[]
     * @throws Exception
     */
    public static function getAllPages($userId)
    {
        try {
            return Page::where('user_id',$userId)->get();
        } catch (Exception $e) {
            throw new Exception('Unable To retrieve data :' . $e);
        }
    }

    /**
     * @param $pageId
     * @return Page
     * @throws Exception
     */
    public static function getPageById($pageId)
    {
        return Page::find($pageId);
    }

    /**
     * @param Page $page
     * @return bool|null
     * @throws Exception
     */
    public static function removePage($page)
    {
        return $page->delete();
    }

    /**
     * @param Page $page
     */
    public static function storePage(Page $page)
    {
        $page->save();
    }

    /**
     * Get the Welcome Message associated with the Page.
     * @return string
     */
    public static function getPageWelcomeMessage($page)
    {
        return $page->botMessageFilter
            ->where('filter', Config::get('constants.SUBSCRIBE'))
            ->first()
            ->textMessage->text;
    }

    /**
     * @param int $pageId
     * @return PageSubscriber[]
     */
    public static function fetchSubscribersOfTheDay(int $pageId)
    {
        $timeStampToday = strtotime(date('Y-m-d', Carbon::now()->subDay(self::TODAY)->timestamp).'-01 00:00:00');
        return PageSubscriber::where('page_id', $pageId)->where('is_subscriber', 1)->where('subscribed_at', '>=', $timeStampToday)->get();
    }

    /**
     * @param int $pageId
     * @return PageSubscriber[]
     */
    public static function fetchSubscribersBeforeToday(int $pageId)
    {
        $timeStampToday = strtotime(date('Y-m-d', Carbon::now()->subDay(self::TODAY)->timestamp).'-01 00:00:00');
        return PageSubscriber::where('page_id', $pageId)->where('is_subscriber', 1)->where('subscribed_at', '<', $timeStampToday)->get();
    }

    /**
     * @param $facebookPageId
     * @return Page
     */
    public static function getPageByFbId($facebookPageId)
    {
        return Page::where('facebook_page_id', $facebookPageId)->get()->first();
    }

    /**
     * @param Page $page
     * @return mixed
     */
    public static function getPageUnsubscribedMessage(Page $page)
    {
        return $page->botMessageFilter
            ->where('filter', Config::get('constants.UNSUBSCRIBE'))
            ->first()
            ->textMessage->text;
    }

    /**
     * @param Page $page
     * @return PageSubscriber[]
     */
    public static function fetchSubscribers(Page $page)
    {
        return $page->pageSubscribers->where('is_subscriber', 1);
    }
}
