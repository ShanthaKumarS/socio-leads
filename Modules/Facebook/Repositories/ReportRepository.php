<?php
namespace Modules\Facebook\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/**
 * Class PageReportRepository
 * @package Modules\Facebook\Repositories
 */
abstract class ReportRepository
{
    /**
     * @var int
     */
    protected $lastDays;

    /**
     * @param int $lastDays
     * @return ReportRepository
     */
    public function setLastDays($lastDays)
    {
        $this->lastDays = $lastDays;
        return $this;
    }

    /**
     * @return int
     */
    public function getMessagesCount()
    {
        $query = DB::table('post_messages_reports')
            ->select(DB::raw('sum(messages_count) as `count`'));

        return $this->bindCondition($query);
    }

    /**
     * @return int
     */
    public function getCommentsCount()
    {
        $query =  DB::table('post_comments_reports')
            ->select(DB::raw('sum(comments_count) as `count`'));

        return $this->bindCondition($query);
    }

    /**
     * @return int
     */
    public function getLikesCount()
    {
        $query = DB::table('post_likes_reports')
            ->select(DB::raw('sum(Likes_count) as `count`'));

        return $this->bindCondition($query);
    }

    /**
     * @return int
     */
    public function getFansCount()
    {
        $query = DB::table('page_fans_reports')
            ->select(DB::raw('sum(fans_count) as `count`'));

        return $this->bindCondition($query);
    }

    /**
     * @param $query
     */
    protected abstract function bindCondition($query);
}
