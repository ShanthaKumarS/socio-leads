<?php
namespace Modules\Facebook\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/**
 * Class PageReportRepository
 * @package Modules\Facebook\Repositories
 */
class FacebookReportRepository extends ReportRepository
{
    /**
     * @param $lastMonths
     * @return Collection
     */
    public static function getFacebookMessagesReportByMonth($lastMonths)
    {
        return DB::table('post_messages_reports')
            ->select(DB::raw('sum(messages_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('user_id', Session::get('user')['user_id'])->get();
    }

    /**
     * @param $lastMonths
     * @return Collection
     */
    public static function getFacebookCommentsReportByMonth($lastMonths)
    {
        return DB::table('post_comments_reports')
            ->select(DB::raw('sum(comments_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('user_id', Session::get('user')['user_id'])->get();
    }

    /**
     * @param $lastMonths
     * @return Collection
     */
    public static function getFacebookLikesReportByMonth($lastMonths)
    {
        return DB::table('post_likes_reports')
            ->select(DB::raw('sum(likes_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('user_id', Session::get('user')['user_id'])->get();
    }

    /**
     * @param $lastMonths
     * @return Collection
     */
    public static function getGainedFacebookFansByMonth($lastMonths)
    {
        return DB::table('page_fans_reports')
            ->select(DB::raw('sum(fans_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('user_id', Session::get('user')['user_id'])->get();
    }

    /**
     * @param $query
     * @return int
     */
    protected function bindCondition($query)
    {
        if ($this->lastDays) {
            $query->where('date', '>=', Carbon::now()->subDay($this->lastDays));
        }

        $query->where('user_id', Session::get('user')['user_id']);
        $count = $query->get()->first()->count;
        return $count ? $count : 0;
    }
}
