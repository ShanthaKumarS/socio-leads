<?php
namespace Modules\Facebook\Repositories;

use Modules\Facebook\Entities\Page;
use Modules\Facebook\Entities\SequenceMessage\DateSequence;
use Modules\Facebook\Entities\SequenceMessage\Image;
use Modules\Facebook\Entities\SequenceMessage\RecurringSequence;
use Modules\Facebook\Entities\SequenceMessage\SequenceMessage;
use Modules\Facebook\Entities\SequenceMessage\StepSequence;

/**
 * Class SequenceMessageRepository
 * @package Modules\Facebook\Repositories
 */
class SequenceMessageRepository
{
    /**
     * @param Page $page
     * @return StepSequence[]
     */
    public static function getStepSequences(Page $page)
    {
        return $page->stepSequences;
    }

    /**
     * @param Page $page
     * @return DateSequence[]
     */
    public static function getDateSequences(Page $page)
    {
        return $page->dateSequences;
    }

    /**
     * @param Page $page
     * @return RecurringSequence[]
     */
    public static function getRecurringSequences(Page $page)
    {
        return $page->recurringSequences;
    }

    /**
     * @param $sequenceId
     * @return StepSequence
     */
    public static function getStepSequenceById($sequenceId)
    {
        return StepSequence::find($sequenceId);
    }

    /**
     * @param $sequenceId
     * @return DateSequence[]
     */
    public static function getDateSequencesById($sequenceId)
    {
        return DateSequence::find($sequenceId);
    }

    /**
     * @param $sequenceId
     * @return RecurringSequence
     */
    public static function getRecurringSequenceById($sequenceId)
    {
        return RecurringSequence::find($sequenceId);
    }

    /**
     * @param array $images
     */
    public static function insertImages(array $images)
    {
        Image::insert($images);
    }

    /**
     * @param $sequenceMessageId
     */
    public static function deleteSequenceMessage($sequenceMessageId)
    {
        SequenceMessage::destroy($sequenceMessageId);
    }
}
