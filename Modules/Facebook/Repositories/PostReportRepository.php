<?php
namespace Modules\Facebook\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

/**
 * Class PageReportRepository
 * @package Modules\Facebook\Repositories
 */
class PostReportRepository extends ReportRepository
{
    /**
     * @var int
     */
    protected $postId;

    /**
     * @param int $postId
     * @return PostReportRepository
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;
        return $this;
    }

    /**
     * @param $postId
     * @param $lastMonths
     * @return Collection
     */
    public static function getPostMessagesReportByMonth($postId, $lastMonths)
    {
        return DB::table('post_messages_reports')
            ->select(DB::raw('sum(messages_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('post_id', $postId)
            ->get();
    }

    /**
     * @param $postId
     * @param $lastMonths
     * @return Collection
     */
    public static function getPostCommentsReportByMonth($postId, $lastMonths)
    {
        return DB::table('post_comments_reports')
            ->select(DB::raw('sum(comments_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('post_id', $postId)
            ->get();
    }

    /**
     * @param $postId
     * @param $lastMonths
     * @return Collection
     */
    public static function getPostLikesReportByMonth($postId, $lastMonths)
    {
        return DB::table('post_likes_reports')
            ->select(DB::raw('sum(likes_count) as `count`'), DB::raw("DATE_FORMAT(date, '%Y-%m') month"))
            ->groupby('month')
            ->where('date', '>=', Carbon::now()->subMonth($lastMonths))
            ->where('post_id', $postId)
            ->get();
    }

    /**
     * @param $query
     * @return int
     */
    protected function bindCondition($query)
    {
        if ($this->lastDays) {
            $query->where('date', '>=', Carbon::now()->subDay($this->lastDays));
        }
        $query->where('post_id', $this->postId);

        $count = $query->get()->first()->count;
        return $count ? $count : 0;
    }
}
