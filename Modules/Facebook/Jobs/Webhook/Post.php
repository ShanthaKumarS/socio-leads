<?php

namespace Modules\Facebook\Jobs\Webhook;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Facebook\Http\Controllers\Webhook\PostController;

/**
 * Class Post
 * @package Modules\Facebook\Jobs\Webhook
 */
class Post extends Job implements ShouldQueue
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $postController = PostController::getInstance();
        $postController->worker($this->data);
    }
}
