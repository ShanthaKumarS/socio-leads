<?php

namespace Modules\Facebook\Jobs\Webhook;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Facebook\Http\Controllers\Webhook\MessageController;

/**
 * Class Message
 * @package Modules\Facebook\Jobs\Webhook
 */
class Message extends Job implements ShouldQueue
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $messageController = MessageController::getInstance();
        $messageController->worker($this->data);
    }
}
