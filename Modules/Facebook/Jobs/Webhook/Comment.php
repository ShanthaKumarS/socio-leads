<?php

namespace Modules\Facebook\Jobs\Webhook;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Facebook\Http\Controllers\PageSubscriberController;
use Modules\Facebook\Http\Controllers\Webhook\CommentController;

/**
 * Class Comment
 * @package Modules\Facebook\Jobs\Webhook
 */
class Comment extends Job implements ShouldQueue
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pageSubscriber = PageSubscriberController::getInstance();
        $commentController = new CommentController($pageSubscriber);
        $commentController->worker($this->data);
    }
}
