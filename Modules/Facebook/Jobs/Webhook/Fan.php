<?php

namespace Modules\Facebook\Jobs\Webhook;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Facebook\Http\Controllers\Webhook\FanController;

class Fan extends Job implements ShouldQueue
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pageSubscriber = FanController::getInstance();
        $pageSubscriber->worker($this->data);
    }
}
