<?php
namespace Modules\Facebook\Jobs\Webhook;

use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class Like
 * @package Modules\Facebook\Jobs\Webhook
 */
class Like extends Job implements ShouldQueue
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $obj = LikeController::getInstance();
        $obj->worker($this->data);
    }
}
