<?php

namespace Modules\Facebook\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Facebook\Http\Controllers\SendMessageNow\SendMessageNowJobController;

/**
 * Class SendMessageNowJob
 * @package Modules\Facebook\Jobs
 */
class SendMessageNowJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sendMessageNowJobController = new SendMessageNowJobController();
        $sendMessageNowJobController->handleSendNowMessage($this->data);
    }
}
