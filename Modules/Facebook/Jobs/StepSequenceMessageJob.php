<?php

namespace Modules\Facebook\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Facebook\Http\Controllers\SequenceMessage\StepSequenceJobController;

/**
 * Class StepSequenceMessageJob
 * @package Modules\Facebook\Jobs
 */
class StepSequenceMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $steps;

    /**
     * @param $steps
     */
    public function __construct($steps)
    {
        $this->steps = $steps;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sendMessageNowJobController = new StepSequenceJobController();
        $sendMessageNowJobController->handleSequenceMessage($this->steps);
    }
}
