<?php

namespace Modules\Facebook\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Facebook\Http\Controllers\SequenceMessage\DateSequenceJobController;

/**
 * Class DateSequenceMessageJob
 * @package Modules\Facebook\Jobs
 */
class DateSequenceMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $dates;

    /**
     * @param $dates
     */
    public function __construct($dates)
    {
        $this->dates = $dates;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sendMessageNowJobController = new DateSequenceJobController();
        $sendMessageNowJobController->handleSequenceMessage($this->dates);
    }
}
