<?php

namespace Modules\Facebook\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Facebook\Http\Controllers\SequenceMessage\RecurringSequenceJobController;

/**
 * Class RecurringSequenceMessageJob
 * @package Modules\Facebook\Jobs
 */
class RecurringSequenceMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $recurrences;

    /**
     * @param $recurrences
     */
    public function __construct($recurrences)
    {
        $this->recurrences = $recurrences;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sendMessageNowJobController = new RecurringSequenceJobController();
        $sendMessageNowJobController->handleSequenceMessage($this->recurrences);
    }
}
