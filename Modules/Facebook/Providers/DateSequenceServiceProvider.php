<?php

namespace Modules\Facebook\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class DateSequenceServiceProvider
 * @package Modules\Facebook\Providers
 */
class DateSequenceServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
