<?php

namespace Modules\Facebook\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RecurringSequenceServiceProvider
 * @package Modules\Facebook\Providers
 */
class RecurringSequenceServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
