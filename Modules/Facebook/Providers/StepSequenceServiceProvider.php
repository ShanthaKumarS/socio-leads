<?php

namespace Modules\Facebook\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Modules\Facebook\Http\Controllers\StepSequenceController;

/**
 * Class StepSequenceServiceProvider
 * @package Modules\Facebook\Providers
 */
class StepSequenceServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('StepSequenceController', function (){
            return $this->app->resolved(StepSequenceServiceProvider::class);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
