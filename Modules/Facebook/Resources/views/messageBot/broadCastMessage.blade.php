@extends('layouts.master')
@section('title','SocioLeads | Broadcast Messages')
@section('title1',isset($pageDetails->pagename)?'Broadcast Messages:-'.$pageDetails->pagename:'Broadcast Messages:-')
@section('style')
    <link rel="stylesheet" type="text/css" href="/assets/plugins/dropify/dist/css/dropify.css">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/DataTables/media/css/jquery.dataTables.css">

    <style type="text/css">
        .activeButton {
            background-color: #3677bc !important;
        }

        .browser-mockup {
            border-top: 2em solid rgba(230, 230, 230, 0.7);
            box-shadow: 0 0.1em 1em 0 rgba(0, 0, 0, 0.4);
            position: relative;
            border-radius: 3px 3px 0 0
        }

        .browser-mockup:before {
            display: block;
            position: absolute;
            content: '';
            top: -1.25em;
            left: 1em;
            width: 0.5em;
            height: 0.5em;
            border-radius: 50%;
            background-color: #f44;
            box-shadow: 0 0 0 2px #f44, 1.5em 0 0 2px #9b3, 3em 0 0 2px #fb5;
        }

        .margin-top-5 {
            margin-top: 5px !important;
        }

        .grey {
            background-color: #F1F0F0 !important;
        }

        mark {
            background-color: #3399FF !important;
            color: #fff !important;
        }

        .configMessage {
            border: 1px solid #ddd;
            min-height: 115px;
            padding: 12px;
            border-radius: 0.42rem;
            margin-bottom: 0;
        }
        .default-msg-count{
            color: #ffffff;
            background-color: #F64E60;
            border-radius: 3px;
            padding: 2px;
        }

        .subs-msg-count{
            color: #ffffff;
            background-color: #F64E60;
            border-radius: 3px;
            padding: 2px;
        }
        #addBuildDivImg{
            width: 48%;
            float: left;
        }
        #addBuildDivTxt{
            width: 48%;
            float: right;
        }

        .row-new{
            margin-bottom: 20px;
            margin: auto;
            width: 50%;
        }
        .row-new:after {
            content: "";
            display: table;
            clear: both;
        }

        .row-new .col {
            float: left;
            box-sizing: border-box;
            padding: 0.75rem;
            min-height: 1px;
        }
        .card-panel {
            transition: box-shadow .25s;
            padding: 20px;
            margin: 0.5rem 0 1rem 0;
            border-radius: 2px;
            background-color: #fff;
            border: 1px solid #EBEDF3;
        }

        .img_card_div {
            margin-right: 40px !important;
            margin-bottom: 0px !important;
            background-color: #fff;
        }

        .radius_10 {
            border-radius: 10px !important;
        }

        #kt_datatable_paginate  .pagination li.active {
             background-color: #fff;
        }

        .dataTables_wrapper  .dataTables_paginate .paginate_button:hover {
             color: #7E8299 !important;
             border: 1px solid #fff;
             background-color: #fff;
             background: #fff;

        }

        .dataTables_wrapper .dataTables_paginate .pagination .page-item.active>.page-link {
            background-color: #E4E6EF;
            color: #7E8299;
        }

        .dataTables_wrapper .dataTables_paginate .pagination .page-item:hover:not(.disabled)>.page-link {
            background-color: #E4E6EF;
            color:  #7E8299;
        }
        .dataTables_wrapper .dataTables_paginate .pagination .page-item>.page-link {
            background-color: #F3F6F9;
        }
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {
            color: #181C32 !important;
        }
        .dataTables_wrapper .dataTables_paginate .pagination .page-item {
            margin-left: 1rem;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
             padding: 0px;
        }

        #addBuildDivImg-sequences {

            float: left;
        }

        #addBuildDivTxt-sequences {
            float: right;
        }


    </style>
@endsection
@section('content')

    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">

        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                    Message Broadcast </h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <a href="/facebook/page-settings/{{$page->id}}" class="btn btn-light-warning font-weight-bolder btn-sm">Page Settings</a>
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->

    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <div class="card card-custom gutter-bs">
                    <!--begin::Header-->
                    <div class="card-header card-header-tabs-line">
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#SubscribePermission">
                                        <i class="fas fa-cogs mr-3"></i>
                                        <span class="nav-text font-weight-bold">Configuration</span>
                                    </a>
                                </li>
                                <li class="nav-item mr-3">
                                    <a class="nav-link" data-toggle="tab" href="#SendMessageNow">
                                        <i class="fas fa-envelope mr-3"></i>
                                        <span class="nav-text font-weight-bold">Send Message</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#BroadcastStats">
                                        <i class="fas fa-chart-bar mr-3"></i>
                                        <span class="nav-text font-weight-bold">Broadcast Stats</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#Sequences">
                                        <i class="fas fa-th-large mr-3"></i>
                                        <span class="nav-text font-weight-bold">Sequences</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="ml-20">
                                <div class="d-flex justify-content-between align-items-center">
                                    <span class="text-dark-75 mr-2">Status :</span>
                                    <span class="switch switch-outline switch-icon switch-primary">
                                        <label>
                                            <input type="checkbox" onclick="changeStatus(this.id);" id="broadcast_messaging_status" {{($page->do_broadcast_message)?'checked':''}}>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Header-->

                    <!--begin::Body-->
                    <div class="card-body px-0">
                        <div class="tab-content pt-5">
                            <!--begin::Tab Content-->
                            <div class="tab-pane active" id="SubscribePermission" role="tabpanel">
                                <div class="col-lg-10 offset-xl-1">
                                    <h3 class="font-size-h6 mb-5">Subscribe Permission:</h3>
                                    <div class="row">
                                        <div class="card col-sm">
                                            <!--begin::Card-->
                                            <div class="card-body">
                                                <!--begin::Top-->
                                                <div class="d-flex align-items-center">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-40 symbol-light-success mr-5">
                                                        <span class="symbol-label">
                                                            <img src="{{file_exists(ltrim($page->profile_picture, '/'))? $page->profile_picture : 'https://graph.facebook.com/'.$page->facebook_page_id.'/picture'}}" class="h-100 align-self-end" alt="">
                                                        </span>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Info-->
                                                    <div class="d-flex flex-column flex-grow-1">
                                                        <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">{{$page->page_name}}</a>
                                                        <span class="text-muted font-weight-bold">Created at {{date('d M Y', strtotime($page->created_at))}}</span>
                                                    </div>
                                                    <!--end::Info-->

                                                </div>
                                                <!--end::Top-->
                                                <!--begin::Bottom-->
                                                <div class="pt-4">
                                                    <!--begin::Image-->
                                                    <div class="bgi-no-repeat bgi-size-cover rounded h-150px" style="background-image: url({{file_exists(ltrim($page->cover_picture, '/'))?$page->cover_picture:'/assets/images/default.jpg'}})"></div>
                                                    <!--end::Image-->

                                                    <!--begin::Action-->
                                                    <div class="btn-group btn-block mt-10" role="group">
                                                        <a href="javascript:;" class="btn btn-hover-text-primary btn-icon-primary btn-sm btn-text-dark-50 bg-hover-light-primary rounded font-weight-bolder font-size-sm p-2">
                                                            <i class="fas fa-envelope-open-text"></i>{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getMessagesCount()}}</a>

                                                        <a href="javascript:;" class="btn btn-icon-danger btn-sm btn-text-dark-50 bg-hover-light-danger btn-hover-text-danger rounded font-weight-bolder font-size-sm p-2">
                                                            <i class="fas fa-heart"></i>{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getLikesCount()}}</a>

                                                        <a href="javascript:;" class="btn btn-icon-info btn-sm btn-text-dark-50 bg-hover-light-info btn-hover-text-info rounded font-weight-bolder font-size-sm p-2">
                                                            <i class="far fa-comments"></i>{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getCommentsCount()}}</a>
                                                    </div>
                                                    <!--end::Action-->
                                                </div>
                                                <!--end::Bottom-->
                                            </div>
                                            <!--end::Card-->
                                        </div>
                                        <div class="col-sm">
                                            <div class="SubscribeMessage1">
                                                <textarea class="form-control configMessage  mb-4" rows="5" placeholder='Enter Subcription message' contenteditable="true" id="subs-msg">{{$page->subscription_message}}</textarea>
                                                <span class="right subs-msg-count" style="font-size:12px;">{{($n=strlen($page->subscription_message))?$n : 0}}/640</span>
                                            </div>

                                            <div class="mt-10">
                                                <a id="subs-btn" class="btn btn-primary btn-sm" contenteditable="true">
                                                    {{$page->subscribe_button_name ? $page->subscribe_button_name : 'Subscribe'}}
                                                </a>

                                                <a id="unsubs-btn" class="btn btn-primary btn-sm float-right" contenteditable="true">
                                                    {{$page->unsubscribe_button_name ? $page->unsubscribe_button_name : 'Unsubscribe'}}
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="separator separator-dashed my-10"></div>
                                    <div class="msg_scroll_default_message">
                                        <h3 class="font-size-h6 mb-5">Welcome Message:</h3>
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="form-group">
                                                    <label for="exampleTextarea">Message Format</label>
                                                    <textarea class="form-control" rows="5" readonly>{{ \Illuminate\Support\Facades\Config::get('constants.DEFAULT_PAGE_WELCOME_MESSAGE') }}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm">
                                                    <span class="default-msg-count">{{strlen($pageWelcomeMessage)}}/640</span>
                                                    <button class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right" data-msgid="{{$page->botMessageFilter[0]->id}}" data-toggle="tooltip" title="" data-original-title="Delete" id="deleteDefaultMessage">

                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                <div class="form-group">
                                                    <div>
                                                        <textarea class="form-control configMessage  mb-4" rows="5" placeholder='Enter Welcome Message' contenteditable="true" id="default-msg" onclick="document.getElementsByClassName('pholder')[0].style.display='none';">{{$pageWelcomeMessage ? $pageWelcomeMessage : \Illuminate\Support\Facades\Config::get('constants.DEFAULT_PAGE_WELCOME_MESSAGE')}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="save-settings btn btn-primary btn-block btn-sm mt-10" id="save-settings">Save Configuration</button>
                                </div>
                            </div>
                            <!--end::Tab Content-->
                            <!--begin::Tab Content-->
                            <div class="tab-pane" id="SendMessageNow" role="tabpanel">
                                    <!--begin::Heading-->
                                    <div class="row">
                                        <div class="col-lg-9 col-xl-6 offset-xl-3">
                                            <h3 class="font-size-h6 mb-5">Deliver Your Message Now:</h3>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-sm" id="send-now" data-users-id="[]">Send Now</button>
                                    </div>
                                    <!--end::Heading-->

                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Outside 24 Hour</label>
                                        <div class="col-lg-2 col-xl-2">
                                            <span class="switch">
                                                <label>
                                                    <input id="outside-24-hour" type="checkbox" name="email_notification_1">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                        <label class="col-sm">Match This Filter : <span id="no-of-users-outside" class="blue-text">0 users</span></label>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right">Within 24 Hour</label>
                                        <div class="col-lg-2 col-xl-2">
                                            <span class="switch">
                                                <label>
                                                    <input id="within-24-hour" type="checkbox" name="email_notification_2">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                        <label class="col-sm">Match This Filter : <span id="no-of-users-within" class="blue-text">0 users</span></label>
                                    </div>


                                <div class="select-tag-box">
                                     <div class="separator separator-dashed my-10"></div>
                                    <!--begin::Heading-->
                                     <div class="row">
                                        <div class="col-lg-9 col-xl-6 offset-xl-3">
                                            <h3 class="font-size-h6 mb-5">Select Tag:</h3>
                                        </div>
                                     </div>
                                    <!--end::Heading-->
                                     <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label text-right"></label>
                                        <div class="col-9 col-form-label">


                                            <div class="radio-list">

                                                <label class="radio" for="confirm_event_update">
                                                    <input type="radio" name="msg_tags" id="confirm_event_update" value="CONFIRMED_EVENT_UPDATE" checked="true">
                                                    <span></span>CONFIRMED EVENT UPDATE
                                                </label>
                                                <label class="radio" for="post_purchase_update">
                                                    <input type="radio" name="msg_tags" id="post_purchase_update" value="POST_PURCHASE_UPDATE">
                                                    <span></span>POST PURCHASE UPDATE
                                                </label>
                                                <label class="radio" for="account_update" >
                                                    <input type="radio" name="msg_tags" id="account_update" value="ACCOUNT_UPDATE">
                                                    <span></span>ACCOUNT UPDATE</label>
                                            </div>

                                         </div>
                                       </div>
                                </div>

                                    <div class="separator separator-dashed my-10"></div>
                                    <!--begin::Heading-->
                                    <div class="row">
                                        <div class="col-lg-9 col-xl-6 offset-xl-3">
                                            <h3 class="font-size-h6 mb-5">Updates Images and Text :</h3>
                                        </div>
                                    </div>
                                    <!--end::Heading-->
                                    <div class="col-lg-9 col-xl-6 offset-xl-3">
                                        <div class="form-group row">
                                            <div class="col-lg-9 col-xl-6">
                                                <div data-repeater-create="" class="img_card_add_btn btn font-weight-bold btn-warning mb-0">
                                                    <i class="la la-plus"></i> Add Image
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-xl-6" id="kt_repeater_2">
                                                <div class="form-group">
                                                    <div data-repeater-create="" class="text_card_add_btn btn font-weight-bold btn-warning mb-0">
                                                        <i class="la la-plus"></i> Add Text
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="items-div" style="display:none;">
                                        <div class="col m12">
                                            <!-- Extra card -->
                                            <div class="row-new">
                                                    <div class="msg_scroll">
                                                        <div class="col l5 m5"  id="addBuildDiv"  style="margin-top:10px;margin-left:2px;">
                                                            <div id="addBuildDivImg"></div>
                                                            <div id="addBuildDivTxt"></div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container sequences_div" style="display: none;">
                                        <div class="row no-padding">
                                            <div class="msg_scroll">
                                                <ul class="no-space">
                                                    <div id="sequence_div">
                                                        @if(isset($page->sequenceData) && !empty($page->sequenceData))
                                                            @foreach($pageDetails->sequenceData as $sequence)
                                                                <li class="no-space">
                                                                    <table>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="no-space" style="width: 100%;">
                                                                                <a href="/user/sequence-messages/{{$sequence->id ?? 0}}" class="collapsible-header">
                                                                                    <h5>
                                                                                        <i class="fa fa-circle fa-fw push_dark_text" aria-hidden="true" style=" line-height: 2rem; font-size: 16px;"></i>
                                                                                        @if($sequence->type=="S"){{"Step Sequence"}}
                                                                                        @elseif($sequence->type=="D"){{"Date Sequence"}}
                                                                                        @elseif($sequence->type=="R"){{"Recurring Sequence"}}
                                                                                        @endif :-
                                                                                        <span class="grey-text text-darken-3">{{$sequence->sequence_name ?? ""}}</span>
                                                                                    </h5>
                                                                                </a>
                                                                            </td>
                                                                            <td class="no-space center" style="width: 40%;">
                                                                                <a href="javascript:void(0);" data-msgId="{{$sequence->id ?? 0}}" class="delete-sequence btn btn_style red darken-3 right" style="margin-right: 5px;">
                                                                                    <i class="fa fa-trash"></i>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </li>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--end::Tab Content-->

                            <!--begin::Tab Content-->
                            <div class="tab-pane" id="BroadcastStats" role="tabpanel">
                                <div class="container">
                                    <div class="card-body">

                                        <!--begin: Datatable-->

                                        <table id="kt_datatable" class="display" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Message Send Date Time</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Message Send Date Time</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!--end::Tab Content-->
                            <!--begin::Tab Content-->
                            <div class="tab-pane" id="Sequences" role="tabpanel">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form class="form">
                                                <div class="form-group row">
                                                    <label class="col-form-label text-right col-lg-8">Step Sequences</label>
                                                    <div class="col-lg-4">
                                                        <a href="#" class="btn btn-light-primary font-weight-bold btn-sm" data-toggle="modal" data-target="#step_seq_modal">Add</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label text-right col-lg-8">Date Sequences</label>
                                                    <div class="col-lg-4">
                                                        <a href="#" class="btn btn-light-primary font-weight-bold btn-sm" data-toggle="modal" data-target="#date_seq_modal">Add</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label text-right col-lg-8">Recurring Sequences</label>
                                                    <div class="col-lg-4">
                                                        <a href="#" class="btn btn-light-primary font-weight-bold btn-sm" data-toggle="modal" data-target="#rec_seq_modal">Add</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="col-md-8">
                                            <ul id="sequences_ul">
                                                @if(isset($stepSequences) && !empty($stepSequences))
                                                    @foreach($stepSequences as $stepSequence)
                                                       <li>
                                                           <a  data-id="{{$stepSequence->id ?? 0}}" class="cursor-pointer step-sequence-messages-link">
                                                               <span class="text-dark">
                                                                       {{"Step Sequence"}}
                                                               </span>
                                                               <span class="text-primary">{{$stepSequence->name }}</span>
                                                           </a>
                                                         <button data-msgId="{{$stepSequence->id ?? 0}}" class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right delete-step-sequence kt_sweetalert_demo_8" data-toggle="tooltip" title="" data-original-title="Delete" >
                                                           <i class="fa fa-trash"></i>
                                                         </button>
                                                       </li>
                                                    <div class="separator separator-dashed my-5"></div>
                                                    @endforeach
                                                @endif

                                                @if(isset($dateSequences) && !empty($dateSequences))
                                                    @foreach($dateSequences as $dateSequence)
                                                        <li>
                                                            <a  data-id="{{$dateSequence->id ?? 0}}" class="cursor-pointer date-sequence-messages-link">
                                                           <span class="text-dark">
                                                                   {{"Date Sequence"}}
                                                           </span>
                                                                <span class="text-primary">{{$dateSequence->name }}</span>
                                                            </a>
                                                            <button data-msgId="{{$dateSequence->id ?? 0}}" class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right delete-date-sequence kt_sweetalert_demo_8" data-toggle="tooltip" title="" data-original-title="Delete" >
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </li>
                                                        <div class="separator separator-dashed my-5"></div>
                                                    @endforeach
                                                @endif

                                                @if(isset($recurringSequences) && !empty($recurringSequences))
                                                    @foreach($recurringSequences as $recurringSequence)
                                                        <li>
                                                            <a  data-id="{{$recurringSequence->id}}" class="cursor-pointer recurring-sequence-messages-link">
                                                           <span class="text-dark">
                                                                   {{"Recurring Sequence"}}
                                                           </span>
                                                                <span class="text-primary">{{$recurringSequence->name }}</span>
                                                            </a>
                                                            <button data-msgId="{{$recurringSequence->id}}" class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right delete-recurring-sequence kt_sweetalert_demo_8" data-toggle="tooltip" title="" data-original-title="Delete" >
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </li>
                                                        <div class="separator separator-dashed my-5"></div>
                                                    @endforeach
                                                @endif

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="separator separator-dashed my-5"></div>
                                    <!--begin: Datatable-->
                                    <div class="card-body">
                                        <div class="row mb-10">
                                            <div class="col-md"></div>
                                            <div class=" btn-sequences-div"></div>
                                        </div>

                                        <h3 class="text-center" id="sequence-name"></h3>
                                        <table id="kt_datatable_sequences_steps" class="display kt_datatable_sequences" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Steps</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Steps</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>


                                        <table id="kt_datatable_sequences_date" class="display kt_datatable_sequences" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Sequence Date</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Sequence Date</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                        <table id="kt_datatable_sequences_recurring" class="display kt_datatable_sequences" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Recurrence Days</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Recurrence Days</th>
                                                <th>Sent</th>
                                                <th>Delivered</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                </div>
                                <!--end::Tab Content-->
                            </div>
                        </div>
                        </div>
                    <!--end::Body-->
                </div>
                </div>
            <!--end::Container-->
            </div>
        <!--end::Entry-->
        </div>
    </div>
    <!--begin::Footer-->
    @include('layouts.page-footer')
    <!--end::Footer-->
{{--    //my--}}
    <!--end::Content-->
    <!--begin::Modal Add button name & link-->
    <div class="modal fade" style="z-index: 10000;" id="btn_link_name" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Button And URL</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Button Name</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="">
                                    <input class="form-control form-control-lg form-control-solid btn_name" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-right">Button Link</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="">
                                    <input class="form-control form-control-lg form-control-solid url_link" type="text" placeholder="Write URL">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2 close-modal" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary doneBtnOption">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->

    <!--    my    StepSequence -->
    <!--begin::Modal Step Sequence-->
    <div class="modal fade" id="step_seq_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Step Sequence</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Step Sequence</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="kt_typeahead_1_modal" dir="ltr" type="text" placeholder="Step Sequence" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2 close-modal" data-dismiss="modal">Close</button>
                        <button type="button" id="stepsubmit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal DateSequence-->
    <div class="modal fade" id="date_seq_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Date Sequence</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Date Sequence</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="kt_typeahead_1_modal" dir="ltr" type="text" placeholder="Date Sequence" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2 close-modal" data-dismiss="modal">Close</button>
                        <button type="button" id="datesubmit"  class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->
    <!--begin::Modal Recurring Sequences-->
    <div class="modal fade" id="rec_seq_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Recurring Sequences</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Recurring Sequences</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" id="kt_typeahead_1_modal" dir="ltr" type="text" placeholder="Recurring Sequence" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2 close-modal" data-dismiss="modal">Close</button>
                        <button type="button" id="recsubmit" class="btn  btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->

    <!--begin::Modal Addnew Message-->
    <div class="modal fade" id="addSequencesMsg_S" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" id="step-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Time Zone</label>
                            <div class="col-lg-9 col-sm-12">
                                <select class="form-control form-control-lg form-control-solid" id="timezone" name="time_zone">
                                    <option value="UTC">(GMT) UTC</option>
                                    <option value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa</option>
                                    <option value="America/Adak">(GMT-10:00) Hawaii-Aleutian</option>
                                    <option value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
                                    <option value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands</option>
                                    <option value="Pacific/Gambier">(GMT-09:00) Gambier Islands</option>
                                    <option value="America/Anchorage">(GMT-09:00) Alaska</option>
                                    <option value="America/Ensenada">(GMT-08:00) Tijuana, Baja California</option>
                                    <option value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
                                    <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US & Canada)</option>
                                    <option value="America/Denver">(GMT-07:00) Mountain Time (US & Canada)</option>
                                    <option value="America/Chihuahua">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                    <option value="America/Dawson_Creek">(GMT-07:00) Arizona</option>
                                    <option value="America/Belize">(GMT-06:00) Saskatchewan, Central America</option>
                                    <option value="America/Cancun">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                    <option value="Chile/EasterIsland">(GMT-06:00) Easter Island</option>
                                    <option value="America/Chicago">(GMT-06:00) Central Time (US & Canada)</option>
                                    <option value="America/New_York">(GMT-05:00) Eastern Time (US & Canada)</option>
                                    <option value="America/Havana">(GMT-05:00) Cuba</option>
                                    <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                    <option value="America/Caracas">(GMT-04:30) Caracas</option>
                                    <option value="America/Santiago">(GMT-04:00) Santiago</option>
                                    <option value="America/La_Paz">(GMT-04:00) La Paz</option>
                                    <option value="Atlantic/Stanley">(GMT-04:00) Faukland Islands</option>
                                    <option value="America/Campo_Grande">(GMT-04:00) Brazil</option>
                                    <option value="America/Goose_Bay">(GMT-04:00) Atlantic Time (Goose Bay)</option>
                                    <option value="America/Glace_Bay">(GMT-04:00) Atlantic Time (Canada)</option>
                                    <option value="America/St_Johns">(GMT-03:30) Newfoundland</option>
                                    <option value="America/Araguaina">(GMT-03:00) UTC-3</option>
                                    <option value="America/Montevideo">(GMT-03:00) Montevideo</option>
                                    <option value="America/Miquelon">(GMT-03:00) Miquelon, St. Pierre</option>
                                    <option value="America/Godthab">(GMT-03:00) Greenland</option>
                                    <option value="America/Argentina/Buenos_Aires">(GMT-03:00) Buenos Aires</option>
                                    <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                    <option value="America/Noronha">(GMT-02:00) Mid-Atlantic</option>
                                    <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.</option>
                                    <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                    <option value="Europe/Belfast">(GMT) Greenwich Mean Time : Belfast</option>
                                    <option value="Europe/Dublin">(GMT) Greenwich Mean Time : Dublin</option>
                                    <option value="Europe/Lisbon">(GMT) Greenwich Mean Time : Lisbon</option>
                                    <option value="Europe/London">(GMT) Greenwich Mean Time : London</option>
                                    <option value="Africa/Abidjan">(GMT) Monrovia, Reykjavik</option>
                                    <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                    <option value="Europe/Belgrade">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                    <option value="Europe/Brussels">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                    <option value="Africa/Algiers">(GMT+01:00) West Central Africa</option>
                                    <option value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
                                    <option value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                    <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                    <option value="Asia/Gaza">(GMT+02:00) Gaza</option>
                                    <option value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria</option>
                                    <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                    <option value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                    <option value="Asia/Damascus">(GMT+02:00) Syria</option>
                                    <option value="Europe/Moscow">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                    <option value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
                                    <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                    <option value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat</option>
                                    <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                    <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                    <option value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg</option>
                                    <option value="Asia/Tashkent">(GMT+05:00) Tashkent</option>
                                    <option value="Asia/Kolkata">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                    <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                    <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                    <option value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk</option>
                                    <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)</option>
                                    <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                    <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
                                    <option value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                    <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                    <option value="Australia/Perth">(GMT+08:00) Perth</option>
                                    <option value="Australia/Eucla">(GMT+08:45) Eucla</option>
                                    <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                    <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                    <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                    <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
                                    <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                    <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
                                    <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                    <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok</option>
                                    <option value="Australia/Lord_Howe">(GMT+10:30) Lord Howe Island</option>
                                    <option value="Etc/GMT-11">(GMT+11:00) Solomon Is., New Caledonia</option>
                                    <option value="Asia/Magadan">(GMT+11:00) Magadan</option>
                                    <option value="Pacific/Norfolk">(GMT+11:30) Norfolk Island</option>
                                    <option value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka</option>
                                    <option value="Pacific/Auckland">(GMT+12:00) Auckland, Wellington</option>
                                    <option value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                    <option value="Pacific/Chatham">(GMT+12:45) Chatham Islands</option>
                                    <option value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa</option>
                                    <option value="Pacific/Kiritimati">(GMT+14:00) Kiritimati</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">How Many Days</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input class="form-control" name="step" id="days" type="number" placeholder="No of days" />
                                </div>
                            </div>
                        </div>`
                     <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Send Time</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead scheduleTime_class" id="scheduleTime_step">
                                    <input class="form-control" name="send_time" type="time" placeholder="Time" />
                                </div>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Select Tag</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="radio-list">
                                    <label class="radio">
                                        <input type="radio" name="msg_tag" value="CONFIRMED_EVENT_UPDATE" checked="checked" >
                                        <span></span>
                                        CONFIRMED EVENT UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="msg_tag" value="POST_PURCHASE_UPDATE">
                                        <span></span>
                                        POST PURCHASE UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio"  name="msg_tag" value="ACCOUNT_UPDATE">
                                        <span></span>
                                        ACCOUNT UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio"  name="msg_tag" value="OTHER">
                                        <span></span>
                                        OTHER
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-10"></div>
                        <div class="form-group row">
                            <div class="col-lg-9 col-xl-9 offset-xl-3">
                                <div class="form-group row">
                                    <div class="col-lg-9 col-xl-6">
                                        <div data-repeater-create="" class="img_card_add_btn-sequences  btn font-weight-bold btn-warning mb-0">
                                            <i class="la la-plus"></i> Add Image
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-6" id="kt_repeater_2">
                                        <div class="form-group">
                                            <div data-repeater-create="" class="text_card_add_btn-sequences btn font-weight-bold btn-warning mb-0">
                                                <i class="la la-plus"></i> Add Text
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100 msg_scroll-sequences">
                                        <div class="row" id="addBuildDiv-sequences_S">
                                            <div class="col-6"  id="addBuildDivImg-sequences_S"></div>
                                            <div class="col-6" id="addBuildDivTxt-sequences_S"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2 close-modal" data-dismiss="modal">Close</button>
                        <button  type="button" class="btn btn-primary save-step-sequence-message">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->


    <!--begin::Modal Addnew Message-->
    <div class="modal fade" id="addSequencesMsg_D" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" id="date-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Time Zone</label>
                            <div class="col-lg-9 col-sm-12">
                                <select class="form-control form-control-lg form-control-solid" id="timezone" name="time_zone">
                                    <option value="UTC">(GMT) UTC</option>
                                    <option value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa</option>
                                    <option value="America/Adak">(GMT-10:00) Hawaii-Aleutian</option>
                                    <option value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
                                    <option value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands</option>
                                    <option value="Pacific/Gambier">(GMT-09:00) Gambier Islands</option>
                                    <option value="America/Anchorage">(GMT-09:00) Alaska</option>
                                    <option value="America/Ensenada">(GMT-08:00) Tijuana, Baja California</option>
                                    <option value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
                                    <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US & Canada)</option>
                                    <option value="America/Denver">(GMT-07:00) Mountain Time (US & Canada)</option>
                                    <option value="America/Chihuahua">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                    <option value="America/Dawson_Creek">(GMT-07:00) Arizona</option>
                                    <option value="America/Belize">(GMT-06:00) Saskatchewan, Central America</option>
                                    <option value="America/Cancun">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                    <option value="Chile/EasterIsland">(GMT-06:00) Easter Island</option>
                                    <option value="America/Chicago">(GMT-06:00) Central Time (US & Canada)</option>
                                    <option value="America/New_York">(GMT-05:00) Eastern Time (US & Canada)</option>
                                    <option value="America/Havana">(GMT-05:00) Cuba</option>
                                    <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                    <option value="America/Caracas">(GMT-04:30) Caracas</option>
                                    <option value="America/Santiago">(GMT-04:00) Santiago</option>
                                    <option value="America/La_Paz">(GMT-04:00) La Paz</option>
                                    <option value="Atlantic/Stanley">(GMT-04:00) Faukland Islands</option>
                                    <option value="America/Campo_Grande">(GMT-04:00) Brazil</option>
                                    <option value="America/Goose_Bay">(GMT-04:00) Atlantic Time (Goose Bay)</option>
                                    <option value="America/Glace_Bay">(GMT-04:00) Atlantic Time (Canada)</option>
                                    <option value="America/St_Johns">(GMT-03:30) Newfoundland</option>
                                    <option value="America/Araguaina">(GMT-03:00) UTC-3</option>
                                    <option value="America/Montevideo">(GMT-03:00) Montevideo</option>
                                    <option value="America/Miquelon">(GMT-03:00) Miquelon, St. Pierre</option>
                                    <option value="America/Godthab">(GMT-03:00) Greenland</option>
                                    <option value="America/Argentina/Buenos_Aires">(GMT-03:00) Buenos Aires</option>
                                    <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                    <option value="America/Noronha">(GMT-02:00) Mid-Atlantic</option>
                                    <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.</option>
                                    <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                    <option value="Europe/Belfast">(GMT) Greenwich Mean Time : Belfast</option>
                                    <option value="Europe/Dublin">(GMT) Greenwich Mean Time : Dublin</option>
                                    <option value="Europe/Lisbon">(GMT) Greenwich Mean Time : Lisbon</option>
                                    <option value="Europe/London">(GMT) Greenwich Mean Time : London</option>
                                    <option value="Africa/Abidjan">(GMT) Monrovia, Reykjavik</option>
                                    <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                    <option value="Europe/Belgrade">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                    <option value="Europe/Brussels">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                    <option value="Africa/Algiers">(GMT+01:00) West Central Africa</option>
                                    <option value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
                                    <option value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                    <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                    <option value="Asia/Gaza">(GMT+02:00) Gaza</option>
                                    <option value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria</option>
                                    <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                    <option value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                    <option value="Asia/Damascus">(GMT+02:00) Syria</option>
                                    <option value="Europe/Moscow">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                    <option value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
                                    <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                    <option value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat</option>
                                    <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                    <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                    <option value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg</option>
                                    <option value="Asia/Tashkent">(GMT+05:00) Tashkent</option>
                                    <option value="Asia/Kolkata">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                    <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                    <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                    <option value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk</option>
                                    <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)</option>
                                    <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                    <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
                                    <option value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                    <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                    <option value="Australia/Perth">(GMT+08:00) Perth</option>
                                    <option value="Australia/Eucla">(GMT+08:45) Eucla</option>
                                    <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                    <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                    <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                    <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
                                    <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                    <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
                                    <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                    <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok</option>
                                    <option value="Australia/Lord_Howe">(GMT+10:30) Lord Howe Island</option>
                                    <option value="Etc/GMT-11">(GMT+11:00) Solomon Is., New Caledonia</option>
                                    <option value="Asia/Magadan">(GMT+11:00) Magadan</option>
                                    <option value="Pacific/Norfolk">(GMT+11:30) Norfolk Island</option>
                                    <option value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka</option>
                                    <option value="Pacific/Auckland">(GMT+12:00) Auckland, Wellington</option>
                                    <option value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                    <option value="Pacific/Chatham">(GMT+12:45) Chatham Islands</option>
                                    <option value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa</option>
                                    <option value="Pacific/Kiritimati">(GMT+14:00) Kiritimati</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Select Date</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead">
                                    <input type="text" name="date" class="form-control" id="kt_datepicker_D" readonly placeholder="Select date" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Send Time</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead scheduleTime_class" id="scheduleTime_D">
                                    <input class="form-control" name="send_time" type="time" placeholder="Time" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Select Tag</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="radio-list">
                                    <label class="radio">
                                        <input type="radio" name="msg_tag" value="CONFIRMED_EVENT_UPDATE" checked="checked" >
                                        <span></span>
                                        CONFIRMED EVENT UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="msg_tag" value="POST_PURCHASE_UPDATE">
                                        <span></span>
                                        POST PURCHASE UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio"  name="msg_tag" value="ACCOUNT_UPDATE">
                                        <span></span>
                                        ACCOUNT UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio"  name="msg_tag" value="OTHER">
                                        <span></span>
                                        OTHER
                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-10"></div>
                        <div class="form-group row">
                            <div class="col-lg-9 col-xl-9 offset-xl-3">
                                <div class="form-group row">
                                    <div class="col-lg-9 col-xl-6">
                                        <div data-repeater-create="" class="img_card_add_btn-sequences  btn font-weight-bold btn-warning mb-0">
                                            <i class="la la-plus"></i> Add Image
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-6" id="kt_repeater_2">
                                        <div class="form-group">
                                            <div data-repeater-create="" class="text_card_add_btn-sequences btn font-weight-bold btn-warning mb-0">
                                                <i class="la la-plus"></i> Add Text
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100 msg_scroll-sequences">
                                        <div class="row" id="addBuildDiv-sequences_D">
                                            <div class="col-6"  id="addBuildDivImg-sequences_D"></div>
                                            <div class="col-6" id="addBuildDivTxt-sequences_D"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2 close-modal" data-dismiss="modal">Close</button>
                        <button  type="button" class="btn btn-primary save-date-sequence-message">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Modal-->


    <!--begin::Modal Addnew Message-->
    <div class="modal fade" id="addSequencesMsg_R" tabindex="-1" role="dialog" aria-hidden="true" style="overflow-y: auto;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" id="recurring-form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 text-right col-form-label">Time Zone</label>
                            <div class="col-lg-9 col-sm-12">
                                <select class="form-control form-control-lg form-control-solid" id="timezone" name="time_zone">
                                    <option value="UTC">(GMT) UTC</option>
                                    <option value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa</option>
                                    <option value="America/Adak">(GMT-10:00) Hawaii-Aleutian</option>
                                    <option value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
                                    <option value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands</option>
                                    <option value="Pacific/Gambier">(GMT-09:00) Gambier Islands</option>
                                    <option value="America/Anchorage">(GMT-09:00) Alaska</option>
                                    <option value="America/Ensenada">(GMT-08:00) Tijuana, Baja California</option>
                                    <option value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
                                    <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US & Canada)</option>
                                    <option value="America/Denver">(GMT-07:00) Mountain Time (US & Canada)</option>
                                    <option value="America/Chihuahua">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                    <option value="America/Dawson_Creek">(GMT-07:00) Arizona</option>
                                    <option value="America/Belize">(GMT-06:00) Saskatchewan, Central America</option>
                                    <option value="America/Cancun">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                    <option value="Chile/EasterIsland">(GMT-06:00) Easter Island</option>
                                    <option value="America/Chicago">(GMT-06:00) Central Time (US & Canada)</option>
                                    <option value="America/New_York">(GMT-05:00) Eastern Time (US & Canada)</option>
                                    <option value="America/Havana">(GMT-05:00) Cuba</option>
                                    <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                    <option value="America/Caracas">(GMT-04:30) Caracas</option>
                                    <option value="America/Santiago">(GMT-04:00) Santiago</option>
                                    <option value="America/La_Paz">(GMT-04:00) La Paz</option>
                                    <option value="Atlantic/Stanley">(GMT-04:00) Faukland Islands</option>
                                    <option value="America/Campo_Grande">(GMT-04:00) Brazil</option>
                                    <option value="America/Goose_Bay">(GMT-04:00) Atlantic Time (Goose Bay)</option>
                                    <option value="America/Glace_Bay">(GMT-04:00) Atlantic Time (Canada)</option>
                                    <option value="America/St_Johns">(GMT-03:30) Newfoundland</option>
                                    <option value="America/Araguaina">(GMT-03:00) UTC-3</option>
                                    <option value="America/Montevideo">(GMT-03:00) Montevideo</option>
                                    <option value="America/Miquelon">(GMT-03:00) Miquelon, St. Pierre</option>
                                    <option value="America/Godthab">(GMT-03:00) Greenland</option>
                                    <option value="America/Argentina/Buenos_Aires">(GMT-03:00) Buenos Aires</option>
                                    <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                    <option value="America/Noronha">(GMT-02:00) Mid-Atlantic</option>
                                    <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.</option>
                                    <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                    <option value="Europe/Belfast">(GMT) Greenwich Mean Time : Belfast</option>
                                    <option value="Europe/Dublin">(GMT) Greenwich Mean Time : Dublin</option>
                                    <option value="Europe/Lisbon">(GMT) Greenwich Mean Time : Lisbon</option>
                                    <option value="Europe/London">(GMT) Greenwich Mean Time : London</option>
                                    <option value="Africa/Abidjan">(GMT) Monrovia, Reykjavik</option>
                                    <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                    <option value="Europe/Belgrade">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                    <option value="Europe/Brussels">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                    <option value="Africa/Algiers">(GMT+01:00) West Central Africa</option>
                                    <option value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
                                    <option value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                    <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                    <option value="Asia/Gaza">(GMT+02:00) Gaza</option>
                                    <option value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria</option>
                                    <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                    <option value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                    <option value="Asia/Damascus">(GMT+02:00) Syria</option>
                                    <option value="Europe/Moscow">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                    <option value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
                                    <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                    <option value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat</option>
                                    <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                    <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                    <option value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg</option>
                                    <option value="Asia/Tashkent">(GMT+05:00) Tashkent</option>
                                    <option value="Asia/Kolkata">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                    <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                    <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                    <option value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk</option>
                                    <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)</option>
                                    <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                    <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
                                    <option value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                    <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                    <option value="Australia/Perth">(GMT+08:00) Perth</option>
                                    <option value="Australia/Eucla">(GMT+08:45) Eucla</option>
                                    <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                    <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                    <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                    <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
                                    <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                    <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
                                    <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                    <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok</option>
                                    <option value="Australia/Lord_Howe">(GMT+10:30) Lord Howe Island</option>
                                    <option value="Etc/GMT-11">(GMT+11:00) Solomon Is., New Caledonia</option>
                                    <option value="Asia/Magadan">(GMT+11:00) Magadan</option>
                                    <option value="Pacific/Norfolk">(GMT+11:30) Norfolk Island</option>
                                    <option value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka</option>
                                    <option value="Pacific/Auckland">(GMT+12:00) Auckland, Wellington</option>
                                    <option value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                    <option value="Pacific/Chatham">(GMT+12:45) Chatham Islands</option>
                                    <option value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa</option>
                                    <option value="Pacific/Kiritimati">(GMT+14:00) Kiritimati</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Select Days</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <select class="form-control selectpicker" id="weekdays" name="week_days[]" multiple>
                                    <option value="1">Monday</option>
                                    <option value="2">Tuesday</option>
                                    <option value="3">Wednesday</option>
                                    <option value="4">Thursday</option>
                                    <option value="5">Friday</option>
                                    <option value="6">Saturday</option>
                                    <option value="7">Sunday</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Send Time</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="typeahead" id="scheduleTime_R">
                                    <input class="form-control" type="time" name="send_time" placeholder="Time" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label text-right col-lg-3 col-sm-12">Select Tag</label>
                            <div class="col-lg-9 col-md-9 col-sm-12">
                                <div class="radio-list">
                                    <label class="radio">
                                        <input type="radio" name="msg_tag" value="CONFIRMED_EVENT_UPDATE" checked="checked" >
                                        <span></span>
                                        CONFIRMED EVENT UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="msg_tag" value="POST_PURCHASE_UPDATE">
                                        <span></span>
                                        POST PURCHASE UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio"  name="msg_tag" value="ACCOUNT_UPDATE">
                                        <span></span>
                                        ACCOUNT UPDATE
                                    </label>
                                    <label class="radio">
                                        <input type="radio"  name="msg_tag" value="OTHER">
                                        <span></span>
                                        OTHER
                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="separator separator-dashed my-10"></div>
                        <div class="form-group row">
                            <div class="col-lg-9 col-xl-9 offset-xl-3">
                                <div class="form-group row">
                                    <div class="col-lg-9 col-xl-6">
                                        <div data-repeater-create="" class="img_card_add_btn-sequences btn font-weight-bold btn-warning mb-0">
                                            <i class="la la-plus"></i> Add Image
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xl-6" id="kt_repeater_2">
                                        <div class="form-group">
                                            <div data-repeater-create="" class="text_card_add_btn-sequences btn font-weight-bold btn-warning mb-0">
                                                <i class="la la-plus"></i> Add Text
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100 msg_scroll-sequences">
                                        <div class="row" id="addBuildDiv-sequences_R">
                                            <div class="col-6"  id="addBuildDivImg-sequences_R"></div>
                                            <div class="col-6" id="addBuildDivTxt-sequences_R"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary mr-2 close-modal" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-secondary save-recurring-sequence-message">Submit</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!--end::Modal-->


@endsection

@section('script_function')
   <!--begin::Page Scripts(used by this page)-->
   <script type="text/javascript" src="{{ asset('assets/js/pages/widgets.js') }}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/pages/features/miscellaneous/sweetalert2.js') }}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-maxlength.js') }}"></script>
   <script type="text/javascript" src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
   <script src="/assets/plugins/dropify/dist/js/dropify.js"></script>
   <!--end::Page Scripts-->

    <script>
        var sequence_id = '{{isset($pageDetails->sequenceData) && !empty($pageDetails->sequenceData[0]->id) ? $pageDetails->sequenceData[0]->id : ''}}';
        var row_id = null;

        //Count charters of subscription message
        $('#subs-msg').keyup(function (e) {
            check_charcount('subs-msg', 640, e);
        });

        //Count charters of welcome message
        $('#default-msg').keyup(function (e) {
            check_charcount('default-msg', 640, e);
        });


        function check_charcount(id, max, e)
        {

            var msgLength = $.trim($('#' + id).val()).length;

            $('.' + id + '-count').html(msgLength + '/640');
            if (e.which != 8 && msgLength >= max) {
                e.preventDefault();
            }
        }

        $('#save-settings').on('click', function (e) {
            e.preventDefault();
            var formData = new FormData();

            formData.append('pageId', '{{$page->id}}');
            formData.append('status', $('#broadcast_messaging_status').prop('checked'));
            formData.append('subsMsg', $.trim($('#subs-msg').val()));
            formData.append('welcomeMessage', $.trim($('#default-msg').val()));
            formData.append('subsBtnText', $.trim($('#subs-btn').text()));
            formData.append('unsubsBtnText', $.trim($('#unsubs-btn').text()));
            formData.append('_token', '{{csrf_token()}}');
            console.log(validateBroadcastConfigure(formData));
            if (validateBroadcastConfigure(formData)) {
                $.ajax({
                    url: '/facebook/broadcast-message-configure',
                    method: 'post',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if (response.code == 200) {
                            Swal.fire({
                                title: 'Inserted!',
                                text: "Your message has inserted successfully",
                                icon: 'success',
                                showConfirmButton: false
                            });
                        } else {
                            Swal.fire('Error!', "Woops!.. Something went wrong", "error");
                        }
                    },
                    error: function () {
                        Swal.fire('Error!', "Woops!.. Something went wrong", "error");
                    },
                });
            }

        });

        function validateBroadcastConfigure(formData)
        {
            var validate = true;
            const rules = [
                {
                    name : 'subsMsg',
                    min : 1,
                    max : 640,
                    error : 'Please make sure that your subscription message length must be with in 1 and 640'
                },
                {
                    name : 'welcomeMessage',
                    min : 1,
                    max : 640,
                    error : 'Please make sure that your welcome message length must be with in 1 and 640'
                },
                {
                    name : 'subsBtnText',
                    min : 1,
                    max : 20,
                    error : 'Please make sure that your Subscription button length must be with in 1 and 20'
                },
                {
                    name : 'unsubsBtnText',
                    min : 1,
                    max : 20,
                    error : 'Please make sure that your Unsubscription button length must be with in 1 and 20'
                },
            ];

            $(rules).each(function(index, value) {
                if (
                    formData.get(value.name).length < value.min ||
                    formData.get(value.name).length > value.max
                ) {
                    validate = false;
                    Swal.fire('Info !', value.error, 'error');
                }
            });
            return validate;
        }

        $('#within-24-hour').on('change', function () {
            $('#outside-24-hour')[0].checked = ! $('#within-24-hour').is(':checked');
            getSubscribers();
        });

        $('#outside-24-hour').on('change', function () {
            $('#within-24-hour')[0].checked = ! $('#outside-24-hour').is(':checked');
            getSubscribers();
        });

        function getSubscribers() {
            $.ajax({
                url: '/facebook/get-subscribed-users',
                method: 'post',
                dataType: 'json',
                data: {
                    withInADay : $('#within-24-hour').is(':checked'),
                    outSideADay : $('#outside-24-hour').is(':checked'),
                    pageId: '{{$page->id}}',
                    _token : '{{csrf_token()}}'
                },

                success: function (response) {
                    if (response.type == 'withIn24hrs') {
                        $('#no-of-users-outside').text("0 users");
                        $('#no-of-users-within').text(response.subscribers.length + " users");
                    } else {
                        $('#no-of-users-within').text("0 users");
                        $('#no-of-users-outside').text(response.subscribers.length + " users");
                    }
                    subscribers = response.subscribers;
                }
            });
        }

        $(document.body).off("click", ".img_card_add_btn").on("click", ".img_card_add_btn", function () {
            let domElement = $('<form class="img_card_div row">' +
                '                   <div class="col m12 img-card margin-bottom-10">' +
                '                       <input type="file" class="img_card_image dropify" data-max-file-size="300K"/>' +
                '                       <a class="float-right" style="z-index: 99;border-radius: 15px;margin-bottom: 1px;float: right;margin-top: -12px; margin-right: -12px;position: relative;" onclick="deleteOption(this)">' +
                '                           <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" title="Remove avatar">' +
                '                           <i class="ki ki-bold-close icon-xs text-muted"></i></span></a>' +
                '                   </div>' +
                '               </form>');
            $("#addBuildDivImg").append(domElement);
            $('.dropify').dropify({
                messages: {
                    'error': 'The file size is too big ({{ 300 }}KB max).'
                },
                error: {
                    'fileSize': 'Ooops, something wrong happended.'
                }
            });
            $('#items-div').css('display', 'block');
        });

        function deleteOption(element) {
            Swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                icon: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel pls!",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $(element).parents('form:first').remove();
                    if (($('#addBuildDivImg').children().length == 0)&&($('#addBuildDivTxt').children().length == 0)) {
                        $('#items-div').css('display', 'none');
                    }
                    Swal.fire("Deleted!", "Your imaginary file has been deleted.", "success");
                }
                else {
                    Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }

        $(document.body).on("click", ".text_card_add_btn", function () {
            var domElement = $('<form class="text_card_div row">' +
                '                   <div>' +
                '                        <textarea class="form-control mb-2 text_card_text" id="kt_maxlength_5" maxlength="640" placeholder="" rows="6"></textarea>' +
                '                           <div class="buttonsDiv margin-bottom-10">' +
                '                                   <button type="button" class="btn btn-primary btn-sm open-btn-modal" data-toggle="modal" data-target="#btn_link_name">Add Button</button>' +
                '                               <a class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right" style="z-index: 99;border-radius: 15px;margin-bottom: 1px;float: right;margin-top: -8px;" onclick="deleteOption(this)">' +
                '                                   <i class="fa fa-trash"></i></a>' +
                '                           </div>' +
                '                       </div>' +
                '</form>');
            $("#addBuildDivTxt").append(domElement);
            $('#items-div').css('display', 'block');
        });

        let div_btn;

        $(document).on("click", ".open-btn-modal", function () {
            div_btn = $(this);
            $('#btn_link_name').find('.btn_name').val('');
            $('#btn_link_name').find('.url_link').val('');
        });

        $(document).on("click", ".doneBtnOption", function () {

            var valid = true;
            $(this).parents('#btn_link_name').find('input').each(function (i, v) {
                if ($(this).val() === "" || $(this).hasClass('invalid')) {
                    valid = false;
                }
            });
            if (valid) {
                var parentDiv = $(this).parents('#btn_link_name');
                var buttonText = $(parentDiv).find('.btn_name').val();
                var buttonUrl = $(parentDiv).find('.url_link').val();
                $(parentDiv).siblings('.addBtnOption').remove();
                $('<a class="custom_btn btn btn-primary btn-sm" data-url="' + buttonUrl + '">' + buttonText + '</a>').insertBefore(div_btn);
                $(div_btn).hide();

                $('#btn_link_name').modal({
                    dismissible: true
                });
                $('#btn_link_name').find('.close-modal').click();
            }
        });

        $('#send-now').on('click', function () {
            var formData = new FormData();

            $('.img_card_image').each(function (index, element) {
                if (element.files[0] != undefined &&  element.files[0] != '') {
                    formData.append('images[]', element.files[0]);
                }
            });

            $('.text_card_div').each(function(index, element) {
                var textElement = $(this).find('.text_card_text');

                if (textElement.val() != undefined &&  textElement.val() != '') {
                    var text = {text : textElement.val()}
                    if ($(this).find('.custom_btn').text() != undefined && $(this).find('.custom_btn').text() != '') {
                        text.buttons  =  [{
                            title : $(this).find('.custom_btn').text(),
                            url : $(this).find('.custom_btn').attr('data-url')
                        }]
                    }

                    formData.append('texts[]', JSON.stringify(text));
                }
            });

            formData.append('tag', $('[name=msg_tags]').val());
            formData.append('pageId', {{$page->id}});
            formData.append('withInADay', $('#within-24-hour').is(':checked'));
            formData.append('outSideADay', $('#outside-24-hour').is(':checked'));
            formData.append('_token', '{{csrf_token()}}');


            $.ajax({
                url: '/facebook/broadcast-msg-now',
                method: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    $('#addBuildDivTxt').html(' ');
                    $('#addBuildDivImg').html(' ');
                    $('#items-div').hide();
                    $('#send-now').attr('data-users-id', '[]');
                    $('#no-of-users').text('0');
                    $('#subscribtion-type').val('');
                    Swal.fire({
                        title: "Sent!",
                        text: "Your message has been successfully sent to " + response.count + " users",
                        timer: 2500,
                        icon: 'success',
                        showConfirmButton: false
                    });
                },
                error: function (response) {
                    Swal.fire('Error!', response.responseJSON.errors.texts[0], 'error');
                }

            });
        });

        function getMessageStats() {

            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '/facebook/broadcast-msg-now/stats/{{$page->id}}',
                success: function (data) {
                    addMessagesTable(data);
                },
            });

        }
        getMessageStats();
        $('#kt_datatable tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        });

        // DataTable
        var kt_datatable = $('#kt_datatable').DataTable({
            "ordering": false,
            initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                    var that = this;

                    $( 'input', this.footer() ).on( 'keyup change clear', function () {
                        if ( that.search() !== this.value ) {
                            that
                                .search( this.value )
                                .draw();
                        }
                    } );
                } );
            }
        });

        function addMessagesTable(data){
            if(data.length){
                kt_datatable.clear();
                for(let i = 0 ; i<data.length; i++){
                    var date = new Date(data[i].sentAt);
                    var sentAt = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes()
                    kt_datatable.row.add( [
                        sentAt,
                        data[i].sentCount,
                        data[i].deliveredCount,
                    ] ).draw(  );

                }
            }
        }

        $(document).ready(function(){
            var sequence_type = "S";

            // Setup - add a text input to each footer cell
            $('#kt_datatable_sequences_steps tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            // DataTable
            kt_datatable_sequences_steps = $('#kt_datatable_sequences_steps').DataTable({
                "ordering": false,
                initComplete: function () {
                    // Apply the search
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change clear', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    });
                }
            });

            // Setup - add a text input to each footer cell
            $('#kt_datatable_sequences_date tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            // DataTable
            kt_datatable_sequences_date = $('#kt_datatable_sequences_date').DataTable({
                "ordering": false,
                initComplete: function () {
                    // Apply the search
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change clear', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    });
                }
            });

            // Setup - add a text input to each footer cell
            $('#kt_datatable_sequences_recurring tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            // DataTable
            kt_datatable_sequences_recurring = $('#kt_datatable_sequences_recurring').DataTable({
                "ordering": false,
                initComplete: function () {
                    // Apply the search
                    this.api().columns().every(function () {
                        var that = this;

                        $('input', this.footer()).on('keyup change clear', function () {
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    });
                }
            });
            $('#kt_datatable_sequences_steps_wrapper, #kt_datatable_sequences_date_wrapper , #kt_datatable_sequences_recurring_wrapper').hide();
            var sequenceMessages = [];
            @if(isset($stepSequences) && count($stepSequences))
                @foreach($stepSequences[0]->sequenceMessages as $sequenceMessage)
                    sequenceMessages.push({
                        messageId : '{{$sequenceMessage->id}}',
                        step : '{{$sequenceMessage->step->count}}',
                        totalSend : 0, /** @todo update count*/
                        deliveryCount : 0 /** @todo update count*/
                    });
                @endforeach
                addStepSequencesTable({
                    id : '{{$stepSequences[0]->id}}',
                    name : '{{$stepSequences[0]->name}}',
                    sequenceMessages : sequenceMessages
                });
            @elseif(isset($dateSequences) && count($dateSequences))
                @foreach($dateSequences[0]->sequenceMessages as $sequenceMessage)
                    sequenceMessages.push({
                        messageId : '{{$sequenceMessage->id}}',
                        date : '{{$sequenceMessage->date->date}}',
                        totalSend : 0, /** @todo update count*/
                        deliveryCount : 0 /** @todo update count*/
                    });
                @endforeach
                addDateSequencesTable({
                    id : '{{$dateSequences[0]->id}}',
                    name : '{{$dateSequences[0]->name}}',
                    sequenceMessages : sequenceMessages,
                });
            @elseif(isset($recurringSequences) && count ($recurringSequences))
                @foreach($recurringSequences[0]->sequenceMessages as $sequenceMessage)
                    var days = ['Mon', 'Tue', 'Wed', 'Thu', 'fri', 'Sat'];
                    var weekDays = [];
                    @foreach($sequenceMessage->recurrences as $recurrence)
                        weekDays.push(days['{{$recurrence->week_day_id}}']);
                    @endforeach
                    sequenceMessages.push({
                        messageId : '{{$sequenceMessage->id}}',
                        days : weekDays.join(),
                        totalSend : 0, /** @todo update count*/
                        deliveryCount : 0 /** @todo update count*/
                    });
                @endforeach
                addRecurringSequenceTable({
                    id : '{{$recurringSequences[0]->id}}',
                    name : '{{$recurringSequences[0]->name}}',
                    sequenceMessages : sequenceMessages,
                });
            @endif

            $(document).off('click','.step-sequence-messages-link').on('click','.step-sequence-messages-link', function () {
                hydrateStepSequenceMessage($(this).attr('data-id'));
            });

            $(document).off('click','.date-sequence-messages-link').on('click','.date-sequence-messages-link', function () {
                hydrateDateSequenceMessage($(this).attr('data-id'));
            });

            $(document).off('click','.recurring-sequence-messages-link').on('click','.recurring-sequence-messages-link', function () {
                hydrateRecurringSequenceMessage($(this).attr('data-id'));
            });
        });

        function hydrateStepSequenceMessage(stepSequenceId) {
            $.ajax({
                url : "/facebook/step-sequence-messages-stats/" + stepSequenceId,
                method : 'get',
                success : function(response) {
                    addStepSequencesTable(response);
                }
            });
        }

        function hydrateDateSequenceMessage(dateSequenceId) {
            $.ajax({
                url : "/facebook/date-sequence-messages-stats/" + dateSequenceId,
                method : 'get',
                success : function(response) {
                    addDateSequencesTable(response);
                }
            });
        }

        function hydrateRecurringSequenceMessage(dateSequenceId) {
            $.ajax({
                url : "/facebook/recurring-sequence-messages-stats/" + dateSequenceId,
                method : 'get',
                success : function(response) {
                    addRecurringSequenceTable(response);
                }
            });
        }

        function addStepSequencesTable(data) {
            sequence_type = "S";

            $('#kt_datatable_sequences_steps_wrapper, #kt_datatable_sequences_date_wrapper , #kt_datatable_sequences_recurring_wrapper').hide();
            $('#kt_datatable_sequences_steps_wrapper').show();
            $('#sequence-name').html("Step: " + data.name);
            $('.btn-sequences-div').html(`<button type="button" data-id="${data.id}" class="btn btn-primary btn-block  create-message-sequences" data-toggle="modal" data-target="#addSequencesMsg_S">Step : New Message</button>`);
            kt_datatable_sequences_steps.clear();
            if(data.sequenceMessages.length > 0) {
                for (let i = 0; i < data.sequenceMessages.length; i++) {
                    kt_datatable_sequences_steps.row.add([
                        data.sequenceMessages[i].step,
                        data.sequenceMessages[i].totalSend,
                        data.sequenceMessages[i].deliveryCount,

                        `<button class="btn btn-icon btn-outline-danger  btn-circle btn-sm mr-2 delete-sequencesMsg" data-id="${data.sequenceMessages[i].messageId}"   data-toggle="tooltip" title="" data-original-title="Delete" >
                              <i class="fa fa-trash"></i>
                         </button>
                         <button class="btn btn-icon btn-outline-success btn-circle btn-sm edit-sequencesMsg" title="" data-id="${data.sequenceMessages[i].messageId}" data-toggle="modal" data-target="#addSequencesMsg_S">
                              <i class="far fa-edit"></i>
                         </button>`,
                    ]);
                }
            }
            kt_datatable_sequences_steps.draw();
        }

        function addDateSequencesTable(data) {
            sequence_type = "D";

            $('#kt_datatable_sequences_steps_wrapper, #kt_datatable_sequences_date_wrapper , #kt_datatable_sequences_recurring_wrapper').hide();
            $('#kt_datatable_sequences_date_wrapper').show();
            $('#sequence-name').html("Date: " + data.name);
            $('.btn-sequences-div').html(`<button type="button" data-id="${data.id}" class="btn btn-primary btn-block  create-message-sequences" data-toggle="modal" data-target="#addSequencesMsg_D">Date : New Message</button>`);
            kt_datatable_sequences_date.clear();
            if(data.sequenceMessages.length > 0) {
                for (let i = 0; i < data.sequenceMessages.length; i++) {
                    kt_datatable_sequences_date.row.add([
                        data.sequenceMessages[i].date,
                        data.sequenceMessages[i].totalSend,
                        data.sequenceMessages[i].deliveryCount,
                            `<button class="btn btn-icon btn-outline-danger  btn-circle btn-sm mr-2 delete-sequencesMsg" data-id="${data.sequenceMessages[i].messageId}"   data-toggle="tooltip" title="" data-original-title="Delete" >
                                  <i class="fa fa-trash"></i>
                             </button>
                             <button class="btn btn-icon btn-outline-success btn-circle btn-sm edit-sequencesMsg" title="" data-id="${data.sequenceMessages[i].messageId}" data-toggle="modal" data-target="#addSequencesMsg_D">
                                  <i class="far fa-edit"></i>
                             </button>`,
                    ]);
                }
            }
            kt_datatable_sequences_date.draw();
        }

        function addRecurringSequenceTable(data){
            sequence_type = "R";
            $('#kt_datatable_sequences_steps_wrapper, #kt_datatable_sequences_date_wrapper , #kt_datatable_sequences_recurring_wrapper').hide();
            $('#kt_datatable_sequences_date_wrapper').show();
            $('#sequence-name').html("Recurring: " + data.name);
            $('.btn-sequences-div').html(`<button type="button" data-id="${data.id}" class="btn btn-primary btn-block  create-message-sequences" data-toggle="modal" data-target="#addSequencesMsg_R">Recurring : New Message</button>`);
            kt_datatable_sequences_date.clear();
            if(data.sequenceMessages.length > 0) {
                for (let i = 0; i < data.sequenceMessages.length; i++) {
                    kt_datatable_sequences_date.row.add([
                        data.sequenceMessages[i].days,
                        data.sequenceMessages[i].totalSend,
                        data.sequenceMessages[i].deliveryCount,
                        `<button class="btn btn-icon btn-outline-danger  btn-circle btn-sm mr-2 delete-sequencesMsg" data-id="${data.sequenceMessages[i].messageId}"   data-toggle="tooltip" title="" data-original-title="Delete" >
                              <i class="fa fa-trash"></i>
                         </button>
                         <button class="btn btn-icon btn-outline-success btn-circle btn-sm edit-sequencesMsg" title="" data-id="${data.sequenceMessages[i].messageId}" data-toggle="modal" data-target="#addSequencesMsg_D">
                              <i class="far fa-edit"></i>
                         </button>`,
                    ]);
                }
            }
            kt_datatable_sequences_date.draw();
        }

        $(document).ready(function () {

            $(document.body).on("click", "#stepsubmit,#datesubmit,#recsubmit", function (e) {
                $('.modal').find('.error-text').addClass('hide');

                var name = $('#step_seq_modal').find('input').val();
                var elementId = $(this).attr('id');

                var divName = 'Step Sequence';
                var schedule = 'StepSequence';
                var append = 'step'

                $('#step_seq_modal').find('input').val('');
                if (elementId === 'datesubmit') {
                    name = $('#date_seq_modal').find('input').val();
                    $('#date_seq_modal').find('input').val('');

                    sequence_type = "D";
                    divName = 'Date Sequence';
                    schedule = 'DateSequence';
                    append = 'date'

                } else if (elementId === 'recsubmit') {
                    name = $('#rec_seq_modal').find('input').val();
                    $('#rec_seq_modal').find('input').val('');

                    sequence_type = "R"
                    divName = 'Recurring Sequence';
                    schedule = 'RecurringSequence';
                    append = 'recurring'

                }
                if ($.trim(name) !== '') {
                    $.ajax({
                        url: '/facebook/' + append + '-sequence-store',
                        method: 'post',
                        dataType: 'json',
                        data: {
                            schedule : schedule,
                            pageId : '{{$page->id}}',
                            name: name,
                            _token : '{{csrf_token()}}'
                        },
                        success: function (response) {
                            Swal.fire({
                                title: "Congratulations!",
                                text: "Your message has been successfully inserted!",
                                icon: "success"
                            });

                            if (schedule == 'StepSequence') {
                                divName = 'Step Sequence';
                                $('#step_seq_modal').find('.close-modal').click();
                                addStepSequencesTable({
                                    id : response.id,
                                    name : response.name,
                                    sequenceMessages : []
                                });
                            } else if (schedule == 'DateSequence') {
                                divName = 'Date Sequence';
                                $('#date_seq_modal').find('.close-modal').click();
                                addDateSequencesTable({
                                    id : response.id,
                                    name : response.name,
                                    sequenceMessages : []
                                });
                            } else if (schedule == 'RecurringSequence') {
                                divName = 'Recurring Sequence';
                                $('#rec_seq_modal').find('.close-modal').click();
                                addRecurringSequenceTable({
                                    id : response.id,
                                    name : response.name,
                                    sequenceMessages : []
                                });
                            }

                            var domElement = $(`<li>
                                                    <a  data-id="${response.id}" class="cursor-pointer ${append}-sequence-messages-link">
                                                        <span class="text-dark">${divName}:</span>
                                                        <span class="text-primary">${name}<span>
                                                    </a>
                                                    <button data-msgId="${response.id}" class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right delete-${append}-sequence kt_sweetalert_demo_8" data-toggle="tooltip" title="" data-original-title="Delete">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </li>
                                                <div class="separator separator-dashed my-5"></div>
                                            `);
                            $("#sequences_ul").append(domElement);
                        },
                    });
                } else {
                    $('.modal').find('.error-text').removeClass('hide');
                }
            });

            $(document).on('click', '.delete-step-sequence', function () {
                var id = $(this).attr('data-msgId');
                var element = $(this);
                var type = 'step';

                deleteSequence(id, type, element);
            });

            $(document).on('click', '.delete-date-sequence', function () {
                var id = $(this).attr('data-msgId');
                var element = $(this);
                var type = 'date';

                deleteSequence(id, type, element);
            });

            $(document).on('click', '.delete-recurring-sequence', function () {
                var id = $(this).attr('data-msgId');
                var element = $(this);
                var type = 'recurring';

                deleteSequence(id, type, element);
            });

            var deleteSequence = function (id, type, element) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You will not be able to recover this imaginary file!",
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!'
                }).then(function(result) {
                   if(result.value) {
                       $.ajax({
                           url: '/facebook/'+ type +'-sequence-delete',
                           method: 'delete',
                           dataType: 'json',
                           data: {
                               id : id,
                               _token : '{{csrf_token()}}'
                           },
                           success: function (response) {
                               element.parents('li:first').remove();
                               Swal.fire("Deleted!", "Your imaginary file has been deleted.", "success");

                               $('.create-message-sequences').hide();
                               $('#sequence-name').hide();
                               $('#kt_datatable_sequences_steps_wrapper, #kt_datatable_sequences_date_wrapper , #kt_datatable_sequences_recurring_wrapper').hide();

                           },
                           error : function(response) {
                               Swal.fire('Error!', "Error in deleting the sequence", 'error');
                           }
                       });
                   }
                });
            }

            $(document).on('click', '.save-step-sequence-message', function() {
                saveSequenceMessage('step');
            });

            $(document).on('click', '.save-date-sequence-message', function() {
                saveSequenceMessage('date');
            });

            $(document).on('click', '.save-recurring-sequence-message', function() {
                saveSequenceMessage('recurring');
            });

            var saveSequenceMessage = function(type) {
                var formData = new FormData($('#'+type+'-form')[0]);
                formData.append('sequence_id', $('.create-message-sequences').attr('data-id'));

                $(`#${type}-form`).find('.img_card_image').each(function (index, element) {
                    if (element.files[0] != undefined &&  element.files[0] != '') {
                        formData.append('images[]', element.files[0]);
                    }
                });

                $(`#${type}-form`).find('.text_card_div').each(function(index, element) {
                    var textElement = $(this).find('.text_card_text');
                    var buttons = [];

                    if (
                        typeof textElement.val() != undefined &&
                        textElement.val() != ''
                    ) {
                        if (
                            typeof $(this).find('.custom_btn').text() != 'undefined' &&
                            $(this).find('.custom_btn').text() != ''
                        ) {
                            buttons.push({
                                title : $(this).find('.custom_btn').text(),
                                url : $(this).find('.custom_btn').attr('data-url')
                            });
                        }
                        var text = {
                            text : textElement.val(),
                            buttons : buttons
                        }

                        formData.append('texts[]', JSON.stringify(text));
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "/facebook/save-"+ type +"-sequence-messages",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success : function (response) {
                        Swal.fire({
                            title: "Congratulations!",
                            text: "Your message has been successfully inserted!",
                            icon: "success"
                        });
                        if (type == 'step') {
                            hydrateStepSequenceMessage($('.create-message-sequences').attr('data-id'));
                        } else if (type == 'date') {
                            hydrateDateSequenceMessage($('.create-message-sequences').attr('data-id'));
                        } else if (type == 'recurring') {
                            hydrateRecurringSequenceMessage($('.create-message-sequences').attr('data-id'));
                        }

                        $(`#addSequencesMsg_${type.charAt(0).toUpperCase()}`).find('.close-modal').click();
                    },
                    error : function (response) {
                        var first = Object.keys(response.responseJSON.errors)[0];

                        Swal.fire({
                            title: "Failed to insert message",
                            text: response.responseJSON.errors[first],
                            icon: "error"
                        });
                    }
                });
            }


            $(document.body).off("click", ".img_card_add_btn-sequences").on("click", ".img_card_add_btn-sequences", function () {

                var domElement = $('<form class="img_card_div row">' +
                    '                   <div class="col m12 img-card margin-bottom-10">' +
                    '                       <input type="file" class="img_card_image dropify" data-max-file-size="300K" data-allowed-file-extensions=\'["jpg", "jpeg", "png"]\' />' +
                    '                       <a class="float-right" style="z-index: 99;border-radius: 15px;margin-bottom: 1px;float: right;margin-top: -12px; margin-right: -12px;position: relative;" onclick="deleteOption(this)">' +
                    '                           <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" title="Remove avatar">' +
                    '                           <i class="ki ki-bold-close icon-xs text-muted"></i></span></a>' +
                    '                   </div>' +
                    '               </form>');
                $(`#addBuildDivImg-sequences_${sequence_type}`).append(domElement);
                $('.dropify').dropify({
                    messages: {
                        'error': 'The file size is too big ({{ 300 }}KB max).'
                    },
                    error: {
                        'fileSize': 'Ooops, something wrong happended.'
                    }
                });
            });

            $(document.body).on("click", ".text_card_add_btn-sequences", function () {
                var domElement = $('<form class="text_card_div row">' +
                    '                   <div>' +
                    '                        <textarea class="form-control mb-2 text_card_text" id="kt_maxlength_5" maxlength="640" placeholder="" rows="6"></textarea>' +
                    '                           <div class="buttonsDiv margin-bottom-10">' +
                    '                                   <button type="button" class="btn btn-primary btn-sm open-btn-modal" data-toggle="modal" data-target="#btn_link_name">Add Button</button>' +
                    '                               <a class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right" style="z-index: 99;border-radius: 15px;margin-bottom: 1px;float: right;margin-top: -8px;" onclick="deleteOption(this)">' +
                    '                                   <i class="fa fa-trash"></i></a>' +
                    '                           </div>' +
                    '                       </div>' +
                    '                   </form>');
                $(`#addBuildDivTxt-sequences_${sequence_type}`).append(domElement);
            });
        });

        $(document.body).on("click", ".delete-sequencesMsg", function(){
            var sequenceMessageId = $(this).attr('data-id');
            var row = $(this)[0].closest('tr');
            Swal.fire({
                title: 'Are you sure?',
                text: "You will not be able to recover this imaginary file!",
                icon: 'info',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(function(result) {
                if(result.value) {
                    $.ajax({
                        url : "/facebook/sequence-message-delete",
                        type : "delete",
                        data : {
                            sequenceMessageId : sequenceMessageId,
                            _token : '{{csrf_token()}}',
                        },

                        success : function (response) {
                            Swal.fire({
                                title: "Congratulations!",
                                text: "Your message has been successfully inserted!",
                                icon: "success"
                            });
                            row.remove();
                        },
                        error : function (response) {
                            Swal.fire({
                                title: "Failed to insert message",
                                text: "Unable to delete Message",
                                icon: "error"
                            });
                        }
                    });
                }
            });

        });

        $(document.body).on("click", ".edit-sequencesMsg", function() {
            var sequenceMessageId = $(this).attr('data-id');
            $.ajax({
                url : "/facebook/edit-sequence-message/" + sequenceMessageId,
                type : "get",
                success : function (response) {
                    console.log(response);
                    editMessagesSequencesModal(response);
                },
                error : function (response) {
                    Swal.fire({
                        title: "Failed to insert message",
                        text: "Unable to delete Message",
                        icon: "error"
                    });
                }
            });
        });

        function  editMessagesSequencesModal(data)
        {
            var type;

            cleanModalefields();
            if(data) {
                var type, form;

                if (data.sequence_type == 'Modules\\Facebook\\Entities\\SequenceMessage\\StepSequence') {
                    type = "S";
                    form = "step-form";
                    $(`input[name=step]`).val(data.step.count);
                } else if (data.sequence_type == 'Modules\\Facebook\\Entities\\SequenceMessage\\DateSequence') {
                    type="D";
                    form = "date-form";

                    $(`input[name=date]`).val(data.date.date.replace('/\-/g', '/'));
                    $(`input[name=date]`).datepicker();
                } else {
                    type="R";
                    form = "recurring-form";
                    $(`input[name=step]`).val(data.recurrence.day);
                }

                $('#' + form + ' #timezone').val(data.time_zone);
                $(`input:radio[name=msg_tag]`).filter(`[value=${data.tag}]`).prop('checked', true);
                $(`input[name=send_time]`).val(data.send_time);

                // if(type == 'S'){
                //     let period = parseInt(data.sequenceData.period);
                //     $('#days').val(period);
                // }

                // if(type == 'D'){
                //     if (data.sequenceData.hasOwnProperty("time")) {
                //         let time_date_D = data.sequenceData.time.split(' ');
                //         let date_D = time_date_D[0];
                //         let time_D = time_date_D[1]
                //         $(`#scheduleTime_${type}`).find('input').val(time_D);
                //         date_D = date_D.split("-")
                //         let datepicker_date =  date_D[1] + '/' + date_D[2] + '/' + date_D[0];
                //         $('#kt_datepicker_D').val(datepicker_date);
                //         $('#kt_datepicker_D').datepicker();
                //     }
                //
                // }
                //
                // if(type == 'R'){
                //     if(data.sequenceData.hasOwnProperty("recurrence_days")) {
                //         let recuringDays_R = data.sequenceData.recurrence_days;
                //         var days_week = ['','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday'];
                //         //multiple  select weekdays
                //         if (recuringDays_R !== "undefined" && recuringDays_R !== "") {
                //             recuringDays_R = recuringDays_R.split(',');
                //             var dayName = '';
                //             recuringDays_R.forEach(getWeekDaysName);
                //
                //             function getWeekDaysName(item, index) {
                //                 console.log(item,'item');
                //                 console.log(index, 'index');
                //                 (index == 0)? dayName = days_week[item] : dayName += ", "+ days_week[item];
                //             }
                //
                //             $('.filter-option-inner-inner').text(dayName);
                //             $('#weekdays').val(recuringDays_R);
                //         }
                //     }
                // }

                // if (data.texts) {
                //
                // }
                //
                // let  str_html_modal='';
                // $(`#addSequencesMsg_${type} .modal-title`).html('Edit Message');
                // str_html_modal = `<div class="col-6"  id="addBuildDivImg-sequences_${type}">`;
                //
                // if (data.messageDetails) {
                //     for (let i = 0; i < data.messageDetails.length; i++) {
                //
                //         if (data.messageDetails[i].type == "image") {
                //             str_html_modal += '  <form class="img_card_div row"> ' +
                //                 ' <div class="col m12 img-card margin-bottom-10"> ' +
                //                 '    <input type="file" class="img_card_image dropify" data-max-file-size="300K"    data-default-file="' + data.messageDetails[i].data.url + '"    /> ' +
                //                 '<a class="float-right" style="z-index: 99;border-radius: 15px;margin-bottom: 1px;float: right;margin-top: -12px; margin-right: -12px;position: relative;" onclick="deleteOption(this)">' +
                //                 '<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" title="Remove avatar"> ' +
                //                 '<i class="ki ki-bold-close icon-xs text-muted"></i></span></a> ' +
                //                 '</div>' +
                //                 '</form>';
                //         }
                //     }
                // }

                $(`#addSequencesMsg_${type} .modal-title`).html('Edit Message');
                let  str_html_modal='';
                str_html_modal = `<div class="col-6"  id="addBuildDivImg-sequences_${type}">`;
                $.each(data.images, function(index, image) {
                    str_html_modal += '  <form class="img_card_div row"> ' +
                        ' <div class="col m12 img-card margin-bottom-10"> ' +
                        '    <input type="file" class="img_card_image dropify" data-max-file-size="300K"    data-default-file="' + image.path + '"    /> ' +
                        '<a class="float-right" style="z-index: 99;border-radius: 15px;margin-bottom: 1px;float: right;margin-top: -12px; margin-right: -12px;position: relative;" onclick="deleteOption(this)">' +
                        '<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" title="Remove avatar"> ' +
                        '<i class="ki ki-bold-close icon-xs text-muted"></i></span></a> ' +
                        '</div>' +
                        '</form>';
                });
                str_html_modal += `</div>`;

                str_html_modal += `</div><div class="col-6" id="addBuildDivTxt-sequences_${type}">`;

                $.each(data.texts, function(index, text) {
                    str_html_modal += `<form class="text_card_div row">
                                                   <div>
                                                       <textarea class="form-control mb-2 text_card_text" id="kt_maxlength_5" maxlength="640" placeholder="" rows="6">${text.text}</textarea>
                                                              <div class="buttonsDiv margin-bottom-10">`;
                   if (text.buttons.length == 0) {
                       str_html_modal += `
                                               <button type="button" class="btn btn-primary btn-sm open-btn-modal" data-toggle="modal" data-target="#btn_link_name">Add Button</button>
                                                `;
                   } else {
                       str_html_modal += `
                                                <a class="custom_btn btn btn-primary btn-sm"
                                                style="margin-bottom:5px;text-transform: none;"
                                                data-url="${text.buttons[0].link}" >${text.buttons[0].name}</a>
                                          `;
                   }
                    str_html_modal += ` <a class="btn btn-icon btn-outline-danger btn-circle btn-sm float-right" style="z-index: 99;border-radius: 15px;margin-bottom: 1px;float: right;margin-top: -8px;" onclick="deleteOption(this)">
                                                     <i class="fa fa-trash"></i></a>
                                                               </div>
                                                              </div>
                                                            </form>`;

                });
                str_html_modal += `</div>`;


                $(`#addBuildDiv-sequences_${type}`).html('');
                $(`#addBuildDiv-sequences_${type}`).append(str_html_modal);

                $('.dropify').dropify({
                    messages: {
                        'error': 'The file size is too big ({{ 300 }}KB max).'
                    },
                    error: {
                        'fileSize': 'Ooops, something wrong happended.'
                    }
                });
            }
        };

        function  cleanModalefields()
        {
            if(sequence_type == "R"){
                $("#weekdays").val("");
                $("#weekdays").trigger("change");
            } else if(sequence_type == "D"){
                let now = new Date();
                let nextMonth = new Date();
                nextMonth.setDate(1);
                nextMonth.setMonth(now.getMonth() + 1);
                let prettyDate =  nextMonth.getMonth() + '/' + nextMonth.getDate() + '/' + nextMonth.getFullYear();
                $('#kt_datepicker_D').val('');  // clear the date filter
                $('#kt_datepicker_D').click( function() {
                    $('#kt_datepicker_D').val(prettyDate);
                    $('#kt_datepicker_D').datepicker();
                });

            } else if(sequence_type == "S"){
                $('#days').val('');
            }

            $(`#timezone_${sequence_type}`).val('UTC');
            $(`#scheduleTime_${sequence_type}`).find('input').val('');

            $(`input:radio[name=msgseq_${sequence_type}]:first`).prop("checked", true);

            $(`#addBuildDivImg-sequences_${sequence_type}`).html('');
            $(`#addBuildDivTxt-sequences_${sequence_type}`).html('');

        }
    </script>
@endsection
