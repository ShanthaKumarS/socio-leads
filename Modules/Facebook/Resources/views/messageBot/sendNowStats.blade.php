@extends('User::layouts.master')
@section('title','SocioLeads | Sequence Messages')
@section('title1','send now stats')
@section('style')
@endsection
@section('content')
    <main>
        <div class="container">
            <div class="row margin-top-10">
                <div class="col s12 m12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title push_dark_text">Broadcast Stats:-<span
                                        class="grey-text">{{$pageName ?? 'Page'}}</span></span>
                            <a href="/facebook/page-settings/{{$pageId}}" class="btn right btn_style push_dark">Back To Page
                                Settings</a>
                            <a href="/facebook/broadcast-message/{{$pageId}}" class="btn right btn_style push_dark">Back To
                                Message Broadcast</a>

                            <table id="statsTable" class="cell-border display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Message</th>
                                    <th>Sent</th>
                                    <th>Delivered</th>
                                    <th>Read Count</th>
                                    <th>Read Rate</th>
                                    <th>Click Count</th>
                                    <th>Click Rate</th>
                                    <th>Send Date Time</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Message Send Date Time</th>
                                    <th>Sent</th>
                                    <th>Delivered</th>
                                    <th>Read Count</th>
                                    <th>Read Rate</th>
                                    <th>Click Count</th>
                                    <th>Click Rate</th>
                                    <th>Send Date Time</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @if(isset($messages) && !empty($messages) && is_array($messages))
                                    @foreach($messages as $message)
                                        <tr>
                                            <td><img src="/assets/images/broadcast-icon.png" class="responsive-img">
                                            </td>
                                            <td class="row messageTd">{!! $message->message ?? 'Some message' !!}</td>
                                            <td class="row  center">{{$message->total_send ?? 00}}</td>
                                            <td class="row center">{{$message->delivery_count ?? 00}}</td>
                                            <td class="row center">{{$message->read_count ?? 00}}</td>
                                            <td class="row center">{{$message->read_rate ?? 00}} %</td>
                                            <td class="row center">{{$message->click_count ?? 00}}</td>
                                            <td class="row center">{{$message->click_rate ?? 00}} %</td>
                                            <td>{{$message->time}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

@section('script_function')
    <script type="text/javascript">
        $(document).ready(function () {
            //Setup - add a text input to each footer cell
            $('#statsTable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            // DataTable
            var table = $('#statsTable').DataTable({
                "columnDefs": [
                    {"width": "4%", "targets": 0}
                ]
            });

            // Apply the search
            table.columns().every(function () {
                var that = this;

                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        });
    </script>
@endsection
