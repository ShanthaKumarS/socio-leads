@extends('User::layouts.master')
@section('title','AutoEngage | '.$sequenceData['sequence_name'])
@section('title1',$sequenceData['sequence_name'])
@section('style')
    <link rel="stylesheet" type="text/css" href="/assets/plugins/dropify/dist/css/dropify.css">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/plugins/mdPickers/dist/mdPickers.css">
    <style>
        .layout-row {
            margin-top: 6px;
        }
    </style>
@endsection
@section('content')
    <main ng-app="app" ng-controller="MainCtrl as ctrl" ng-cloak>
        <div class="container">
            <h5 class="card-title push_dark_text">@if($sequenceData['type']=='S'){{"Step Sequence"}}@elseif($sequenceData['type']=='D'){{"Date Sequence"}}@elseif($sequenceData['type']=='R'){{"Recurring Sequence"}}@endif
                :- <span class="grey-text">{{$sequenceData['sequence_name']}}</span></h5>
            <div class="row margin-top-10">
                <div class="col s12 m12">
                    <div class="container">
                        <div class="row no-space">
                            <div class="col m12">
                                <div class="row">
                                    <div class="col l12">
                                        <div class="card-panel">
                                            <div class="row">
                                                <span class="grey-text" style="font-size: 25px;">Sequence Message</span>
                                                <a id="save-messages" href="javascript:void(0);" class="btn push_dark right text_style_none" style="margin-left: 5px !important;">Save</a>
                                                <a id="cancel-messages" href="/user/sequence-messages/{{$sequenceData['id']}}" class="btn push_dark right text_style_none" style="margin-left: 5px !important;">Cancel</a>
                                            </div>
                                            <div class="row send_msg_div">
                                                <div class="col l8 center">
                                                    <ul style="margin: 0px 5px;display: inline-flex;padding: 19px 0px;">
                                                        <li class="text_card_add_btn" style="margin-right: 10px;">
                                                            <div class="card_list_option">
                                                                <h6 class="no-margin"><i class="fa fa-font" style="font-size: 40px;"></i></h6>
                                                                <a class="card_list_option_text">Text Card</a>
                                                            </div>
                                                        </li>
                                                        <li class="img_card_add_btn" style="margin-right: 10px;">
                                                            <div class="card_list_option">
                                                                <h6 class="no-margin"><i class="icon-picture" style="font-size: 40px;"></i></h6>
                                                                <a class="card_list_option_text">Image</a>
                                                            </div>
                                                        </li>
                                                        <li style="margin-right: 10px;">
                                                            <div class="input-field col l12 dropdown-content-small">
                                                                <select id="timezone" name="timezone">
                                                                    <option value="UTC">(GMT) UTC</option>
                                                                    <option value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa</option>
                                                                    <option value="America/Adak">(GMT-10:00) Hawaii-Aleutian</option>
                                                                    <option value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
                                                                    <option value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands</option>
                                                                    <option value="Pacific/Gambier">(GMT-09:00) Gambier Islands</option>
                                                                    <option value="America/Anchorage">(GMT-09:00) Alaska</option>
                                                                    <option value="America/Ensenada">(GMT-08:00) Tijuana, Baja California</option>
                                                                    <option value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
                                                                    <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US & Canada)</option>
                                                                    <option value="America/Denver">(GMT-07:00) Mountain Time (US & Canada)</option>
                                                                    <option value="America/Chihuahua">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
                                                                    <option value="America/Dawson_Creek">(GMT-07:00) Arizona</option>
                                                                    <option value="America/Belize">(GMT-06:00) Saskatchewan, Central America</option>
                                                                    <option value="America/Cancun">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
                                                                    <option value="Chile/EasterIsland">(GMT-06:00) Easter Island</option>
                                                                    <option value="America/Chicago">(GMT-06:00) Central Time (US & Canada)</option>
                                                                    <option value="America/New_York">(GMT-05:00) Eastern Time (US & Canada)</option>
                                                                    <option value="America/Havana">(GMT-05:00) Cuba</option>
                                                                    <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
                                                                    <option value="America/Caracas">(GMT-04:30) Caracas</option>
                                                                    <option value="America/Santiago">(GMT-04:00) Santiago</option>
                                                                    <option value="America/La_Paz">(GMT-04:00) La Paz</option>
                                                                    <option value="Atlantic/Stanley">(GMT-04:00) Faukland Islands</option>
                                                                    <option value="America/Campo_Grande">(GMT-04:00) Brazil</option>
                                                                    <option value="America/Goose_Bay">(GMT-04:00) Atlantic Time (Goose Bay)</option>
                                                                    <option value="America/Glace_Bay">(GMT-04:00) Atlantic Time (Canada)</option>
                                                                    <option value="America/St_Johns">(GMT-03:30) Newfoundland</option>
                                                                    <option value="America/Araguaina">(GMT-03:00) UTC-3</option>
                                                                    <option value="America/Montevideo">(GMT-03:00) Montevideo</option>
                                                                    <option value="America/Miquelon">(GMT-03:00) Miquelon, St. Pierre</option>
                                                                    <option value="America/Godthab">(GMT-03:00) Greenland</option>
                                                                    <option value="America/Argentina/Buenos_Aires">(GMT-03:00) Buenos Aires</option>
                                                                    <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
                                                                    <option value="America/Noronha">(GMT-02:00) Mid-Atlantic</option>
                                                                    <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.</option>
                                                                    <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
                                                                    <option value="Europe/Belfast">(GMT) Greenwich Mean Time : Belfast</option>
                                                                    <option value="Europe/Dublin">(GMT) Greenwich Mean Time : Dublin</option>
                                                                    <option value="Europe/Lisbon">(GMT) Greenwich Mean Time : Lisbon</option>
                                                                    <option value="Europe/London">(GMT) Greenwich Mean Time : London</option>
                                                                    <option value="Africa/Abidjan">(GMT) Monrovia, Reykjavik</option>
                                                                    <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
                                                                    <option value="Europe/Belgrade">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
                                                                    <option value="Europe/Brussels">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
                                                                    <option value="Africa/Algiers">(GMT+01:00) West Central Africa</option>
                                                                    <option value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
                                                                    <option value="Asia/Beirut">(GMT+02:00) Beirut</option>
                                                                    <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
                                                                    <option value="Asia/Gaza">(GMT+02:00) Gaza</option>
                                                                    <option value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria</option>
                                                                    <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
                                                                    <option value="Europe/Minsk">(GMT+02:00) Minsk</option>
                                                                    <option value="Asia/Damascus">(GMT+02:00) Syria</option>
                                                                    <option value="Europe/Moscow">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
                                                                    <option value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
                                                                    <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
                                                                    <option value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat</option>
                                                                    <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
                                                                    <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
                                                                    <option value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg</option>
                                                                    <option value="Asia/Tashkent">(GMT+05:00) Tashkent</option>
                                                                    <option value="Asia/Kolkata">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
                                                                    <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
                                                                    <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
                                                                    <option value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk</option>
                                                                    <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)</option>
                                                                    <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
                                                                    <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
                                                                    <option value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
                                                                    <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
                                                                    <option value="Australia/Perth">(GMT+08:00) Perth</option>
                                                                    <option value="Australia/Eucla">(GMT+08:45) Eucla</option>
                                                                    <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
                                                                    <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
                                                                    <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
                                                                    <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
                                                                    <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
                                                                    <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
                                                                    <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
                                                                    <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok</option>
                                                                    <option value="Australia/Lord_Howe">(GMT+10:30) Lord Howe Island</option>
                                                                    <option value="Etc/GMT-11">(GMT+11:00) Solomon Is., New Caledonia</option>
                                                                    <option value="Asia/Magadan">(GMT+11:00) Magadan</option>
                                                                    <option value="Pacific/Norfolk">(GMT+11:30) Norfolk Island</option>
                                                                    <option value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka</option>
                                                                    <option value="Pacific/Auckland">(GMT+12:00) Auckland, Wellington</option>
                                                                    <option value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
                                                                    <option value="Pacific/Chatham">(GMT+12:45) Chatham Islands</option>
                                                                    <option value="Pacific/Tongatapu">(GMT+13:00) Nuku'alofa</option>
                                                                    <option value="Pacific/Kiritimati">(GMT+14:00) Kiritimati</option>
                                                                </select>
                                                                <label for="timezone">Select Your Timezone</label>
                                                            </div>
                                                        </li>
                                                        <li style="margin-right: 10px;">
                                                            @if($sequenceData["type"]=="R")
                                                                <div class="input-field dropdown-content-small">
                                                                    <select id="weekdays" name="weekdays" multiple>
                                                                        <option value="" disabled selected>Choose your option</option>
                                                                        <option value="1">Monday</option>
                                                                        <option value="2">Tuesday</option>
                                                                        <option value="3">Wednesday</option>
                                                                        <option value="4">Thursday</option>
                                                                        <option value="5">Friday</option>
                                                                        <option value="6">Saturday</option>
                                                                        <option value="7">Sunday</option>
                                                                    </select>
                                                                    <label for="weekdays">Select Days</label>
                                                                </div>
                                                            @endif
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col l4">
                                                    <md-content layout="column" layout-align="center center">
                                                        <div layout="row" ng-form="demoForm">
                                                            @if($sequenceData["type"]=="S")
                                                                <div class="input-field col l12" ng-form="daysForm" style="margin-top:27px;">
                                                                    <i class="icon-calendar prefix"
                                                                       style="line-height: 36px; margin-left: 10px; margin-top: 19px; font-size: 23px; color: rgba(0,0,0,0.54);"></i>
                                                                    <input id="days" type="number" name="noOfDays"
                                                                           ng-model="noOfDays" class="validate" required>
                                                                    <label style="font-size:11px;" for="days">Wait How Many Days*</label>
                                                                    <div ng-show="daysForm.noOfDays.$touched" ng-messages="daysForm.noOfDays.$error"
                                                                         style="color:red" role="alert">
                                                                        <div ng-message="required">Please enter days to wait</div>
                                                                        <div ng-show="daysForm.noOfDays.$error.number">Only numbers are allowed</div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($sequenceData["type"]=="D")
                                                                <div layout-padding>
                                                                    <div>
                                                                        <!-- <h4 class="md-subhead">Pick a date</h4> -->
                                                                        <mdp-date-picker id="scheduleDate" mdp-open-on-click required
                                                                                         name="dateFormat"
                                                                                         mdp-placeholder="(dd/mm/yyyy)"
                                                                                         mdp-format="DD/MM/YYYY"
                                                                                         ng-model="currentDate"
                                                                                         mdp-min-date="minDate">
                                                                            <div ng-messages="demoForm.dateFormat.$error">
                                                                                <div ng-message="required">Date is required</div>
                                                                                <div ng-message="format">Invalid format</div>
                                                                            </div>
                                                                        </mdp-date-picker>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($sequenceData["type"]=="S" || $sequenceData["type"]=="D" || $sequenceData["type"]=="R")
                                                                <div layout-padding>
                                                                    <div>
                                                                        <!-- <h4 class="md-subhead">Pick a time</h4> -->
                                                                        <mdp-time-picker id="scheduleTime" mdp-open-on-click
                                                                                         name="timeFormat" required
                                                                                         ng-model="currentTime"
                                                                                         mdp-format="HH:mm"
                                                                                         mdp-placeholder="(hh/mm)"
                                                                                         ng-change="checkTime(this)">
                                                                            <div ng-messages="demoForm.timeFormat.$error">
                                                                                <div ng-message="required">This is
                                                                                    required
                                                                                </div>
                                                                                <div ng-message="format">Invalid
                                                                                    format
                                                                                </div>
                                                                            </div>
                                                                        </mdp-time-picker>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </md-content>
                                                </div>
                                            </div>

                                            <!-- Message Tag -->
                                            <div class="row message_tags" style="margin-left: 290px;margin-top: -21px;">
                                                <h5 class="no-space">
                                                    <span style="margin-left: 35%;float: left;">Select Tag:</span>
                                                </h5>
                                                <div class="col">
                                                    <div class="form-check" style="width: 122%;">
                                                        <input class="form-check-input" type="radio" name="msg_tags" id="confirm_event_update" value="CONFIRMED_EVENT_UPDATE" @if((isset($sequenceData) && !empty($sequenceData['tag_type']) && $sequenceData['tag_type']=="CONFIRMED_EVENT_UPDATE")) {{"checked"}} @endif>
                                                        <label class="form-check-label" for="confirm_event_update" style="color:#171717;">
                                                            CONFIRMED EVENT UPDATE
                                                        </label>
                                                    </div>
                                                    <div class="form-check" style="width: 122%;">
                                                        <input class="form-check-input" type="radio" name="msg_tags" id="post_purchase_update" value="POST_PURCHASE_UPDATE" @if((isset($sequenceData) && !empty($sequenceData['tag_type']) && $sequenceData['tag_type']=="POST_PURCHASE_UPDATE")) {{"checked"}} @endif>
                                                        <label class="form-check-label" for="post_purchase_update" style="color:#171717;">
                                                            POST PURCHASE UPDATE
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="msg_tags" id="account_update" value="ACCOUNT_UPDATE" @if((isset($sequenceData) && !empty($sequenceData['tag_type']) && $sequenceData['tag_type']=="ACCOUNT_UPDATE")) {{"checked"}} @endif>
                                                        <label class="form-check-label" for="account_update" style="color:#171717;">
                                                            ACCOUNT UPDATE
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="msg_tags" id="other" value="OTHER" @if((isset($sequenceData) && !empty($sequenceData['tag_type']) && $sequenceData['tag_type']=="OTHER")) {{"checked"}} @endif>
                                                        <label class="form-check-label" for="other" style="color:#171717;">
                                                            OTHER
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="items-div" class="col m12" style="<?php if (isset($messageDetails) && !empty($messageDetails)) {
                                echo "display:block;";
                            } else {
                                echo "display:none;";
                            }?>">
                                <div class="card-panel no-space">
                                    <div class="row no-space">
                                        <div class="col l12 m12">
                                            <div class="msg_scroll">
                                                <div class="col l5 m5" style="margin-top: 10px; margin-left: 2px;">
                                                    <div id="addBuildDiv">
                                                        @if(isset($messageDetails) && !empty($messageDetails))
                                                            @foreach($messageDetails as $message)
                                                                @if($message['type']=="text" || $message['type']=="buttonTemplate")
                                                                    <form class="text_card_div row">
                                                                        <div class="col m12 card-panel radius_10">
                                                                            <a class="btn btn_style clsBtnOption" style="z-index: 99;border-radius: 15px;color: #3b5998;background:#ffffff;float: right;margin-top: -15px;border: 1px solid #3b5998;" onclick="deleteOption(this)">
                                                                                <i class="icon-trash"></i>
                                                                            </a>
                                                                            <div style="min-height:65px;">
                                                                                <h6 class="text_card_text" contenteditable="true" onkeydown="lengthCount(this,640)">{{$message['data']['text'] or ""}}</h6>
                                                                            </div>
                                                                            <hr class="hrstyle">
                                                                            <span class="right char-count" style="color:gray;font-size:12px;">{{strlen($message['data']['text'])}}/640</span>
                                                                            <div class="buttonsDiv center margin-bottom-10">
                                                                                @if($message['type']=="text")
                                                                                    <div class="addBtnOption btn btn_style push_dark white-text">
                                                                                        <span><i class="icon-plus"></i> Add Button</span>
                                                                                    </div>
                                                                                @elseif($message['type']=="buttonTemplate")
                                                                                    @foreach($message['data']['button'] as $button)
                                                                                        <a class="custom_btn col s12 btn btn_style push_dark white-text" style="margin-bottom:5px;text-transform: none;" data-url="{{$button['url'] or ""}}">{{$button['title'] or ""}}</a>
                                                                                    @endforeach
                                                                                @endif
                                                                                <div class="add_re_btn_div card-panel radius_10" id="add_re_btn_div" style="display:none; z-index: 99; width: 333px; position: absolute; margin-left: 347px;margin-top: -126px; padding: 3px;border: 1px solid;">
                                                                                    <div class="input-field col s12 no-space">
                                                                                        <input id="btn_name" type="text" class="btn_name validate" size="20" maxlength="20">
                                                                                        <label for="btn_name">Enter Button Name</label>
                                                                                    </div>
                                                                                    <div class="input-field col s12 no-space">
                                                                                        <input id="url_link" type="url" class="url_link validate" placeholder="http://autoengage.io">
                                                                                        <label for="url_link">Enter URL</label>
                                                                                    </div>
                                                                                    <div class="center col s12">
                                                                                        <a href="javascript:void(0)" class="btn btn_style red doneBtnOption">DONE</a>
                                                                                    </div>
                                                                                    <a class="btn btn_style clsTextBtnDiv" style="border-radius: 15px; color: #3b5998; background:#ffffff; position: absolute; right: -9px;top: -10px; border: 1px solid #3b5998;">
                                                                                        <i class="icon-trash"></i>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                @elseif($message['type']=="image")
                                                                    <form class="img_card_div row">
                                                                        <div class="col m12 card-panel radius_10 margin-bottom-10">
                                                                            <a class="btn btn_style clsBtnOption" style="z-index: 99;border-radius: 15px;color: #3b5998;background:#ffffff;float: right;margin-top: -15px;border: 1px solid #3b5998;" onclick="deleteOption(this)">
                                                                                <i class="icon-trash"></i>
                                                                            </a>
                                                                            <input type="file" class="img_card_image dropify" data-max-file-size="300K" data-default-file="{{$message['data']['url'] or ""}}"/>
                                                                        </div>
                                                                    </form>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="blockDiv" class="hide">
                <div class=""
                     style="padding: 0px; margin: 0px; text-align: center; color: rgb(0, 0, 0); border: 3px solid rgb(170, 170, 170); width: 100%; height: 100%; position: fixed; top: 0%; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.5; z-index: 1004; cursor: wait;"></div>
                <div class="blockUI blockMsg blockPage "
                     style="padding: 0px; margin: 0px; top: 50%; color: rgb(0, 0, 0); border: 3px solid rgb(170, 170, 170); font-weight: normal; background-color: rgb(255, 255, 255); font-size: 20px; left: 35%; text-align: center; z-index: 999999 ! important; position: fixed; width: 30%;">
                    <img src="/assets/images/facebook.gif" style="height:25px;">Just A Moment
                </div>
            </div>
        </div>
    </main>
@endsection

@section('script_function')
    <script src="/assets/plugins/dropify/dist/js/dropify.js"></script>
    <script type="text/javascript" src="/assets/plugins/moment.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js" type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js"
            type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"
            type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"
            type="text/javascript"></script>

    <script src="//ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.js"
            type="text/javascript"></script>
    <script type="text/javascript" src="/assets/plugins/mdPickers/dist/mdPickers.js"></script>
    <script type="text/javascript">


        // datetime picker
        (function () {
            var module = angular.module("app", ["ngMaterial", "ngAnimate", "ngAria", "ngMessages", "mdPickers"]);
            module.controller("MainCtrl", ['$scope', '$mdpDatePicker', '$mdpTimePicker', function ($scope, $mdpDatePicker, $mdpTimePicker) {

                $scope.currentDate = new Date();
                $scope.currentTime = new Date();

//                //validation for date picker
//                $scope.minDate = new Date(
//                        $scope.currentDate.getFullYear(),
//                        $scope.currentDate.getMonth(),
//                        $scope.currentDate.getDate()
//                );

                //validation for time picker
                $scope.checkTime = function (element) {
                    var sequenceType = '<?php if (isset($sequenceData["type"])) {
                        echo $sequenceData["type"];
                    }?>';
//                    if(sequenceType=="D") {
//                        if (($scope.currentDate.getDate() === new Date().getDate()) && $scope.currentDate.getMonth() === new Date().getMonth() && $scope.currentDate.getYear() === new Date().getYear()) {
//                            if ($scope.currentTime.getTime() < new Date().getTime()) {
//                                $scope.currentTime = new Date();
//                            }
//                        }
//                    }
                };

                //for editing time
                var period = '<?php if (isset($sequenceData["period"])) {
                    echo $sequenceData["period"];
                }?>';
                if (period !== "undefined" && period !== "") {
                    $scope.noOfDays = parseInt(period);
                }

                var time = '<?php if (isset($sequenceData["time"])) {
                    echo $sequenceData["time"];
                }?>';
                if (time !== "undefined" && time !== "") {
                    var sequenceType = '<?php if (isset($sequenceData["type"])) {
                        echo $sequenceData["type"];
                    }?>';
                    if (sequenceType == "S" || sequenceType == "R") {
                        var scheduleDate = new Date();
                        time = time.split(':');
                        scheduleDate.setHours(time[0]);
                        scheduleDate.setMinutes(time[1]);
                        $scope.currentTime = new Date(scheduleDate);
                    }
                    if (sequenceType == "D") {
                        var currentDate = new Date();
                        time = time.split(' ');
                        var scheduleD = time[0];
                        var scheduleT = time[1];
                        scheduleD = scheduleD.split("-");
                        scheduleT = scheduleT.split(":");
                        currentDate.setFullYear((scheduleD[0]));
                        currentDate.setMonth((scheduleD[1]));
                        currentDate.setDate((scheduleD[2]));
                        currentDate.setHours(scheduleT[0]);
                        currentDate.setMinutes(scheduleT[1]);
                        $scope.currentDate = new Date(currentDate);
                        $scope.currentTime = new Date(currentDate);
                    }
                }

                var timezone = '<?php if (isset($sequenceData["timezone"])) {
                    echo $sequenceData["timezone"];
                }?>';
                if (recuringDays !== "undefined" && recuringDays !== "") {
                    $('#timezone').val(timezone);
                    $('#timezone').material_select();
                }

                var recuringDays = '<?php if (isset($sequenceData["recurrence_days"])) {
                    echo $sequenceData["recurrence_days"];
                }?>';
                if (recuringDays !== "undefined" && recuringDays !== "") {
                    recuringDays = recuringDays.split(',');
                    var i = 0, size = recuringDays.length, $options = $('#weekdays option');
                    for (i; i < size; i++) {
                        $options.filter('[value="' + recuringDays[i] + '"]').prop('selected', true);
                    }
                    $(document).find('select').material_select();
                }
            }]);


            // Basic drop down photo
            $('.dropify').dropify({
                messages: {'error': 'The file size is too big ({{ 300 }}KB max).'},
                error: {'fileSize': 'Ooops, something wrong happended.'}
            });

            $('.dropdown-button').dropdown({
                inDuration: 300,
                outDuration: 225,
                constrain_width: false, // Does not change width of dropdown to that of the activator
                hover: true, // Activate on hover
                gutter: 0, // Spacing from edge
                belowOrigin: false, // Displays dropdown below the button
                alignment: 'left' // Displays dropdown with edge aligned to the left of button
            });

            $('.msg_scroll').slimScroll({
                color: '#bbdefb ',
                size: '10px',
                height: '400px',
                alwaysVisible: true,
                allowPageScroll: true
            });
        })();

        //To show length count
        function lengthCount(e,max) {
            var msgLength=$.trim($(e).text()).length;
            $(e).parents('.text_card_div').find('.char-count').html(msgLength+'/'+max);
        }

        //appened text card
        $(document.body).on("click", ".text_card_add_btn", function () {
//            var domElement = $('<form class="text_card_div row"><div class="col m12 card-panel radius_10"><a class="btn btn_style clsBtnOption" style="z-index: 99;border-radius: 15px;color: #3b5998;background:#ffffff;float: right;margin-top: -15px;border: 1px solid #3b5998;" onclick="deleteOption(this)"><i class="icon-trash"></i></a><div style="min-height:65px;"><h6 class="text_card_text" contenteditable="true">Hi !! Add your text !!</h6></div><hr class="hrstyle"><div class="buttonsDiv center margin-bottom-10"><div class="addBtnOption btn btn_style push_dark white-text"><span><i class="icon-plus"></i> Add Button</span></div><div class="add_re_btn_div card-panel radius_10" id="add_re_btn_div" style="display:none; z-index: 99; width: 333px; position: absolute; margin-top: -20px; padding: 10px;"> <div class="input-field col s12"><input id="btn_name" type="text" class="btn_name validate" size="20" maxlength="20"><label for="btn_name">Enter Button Name</label></div><div class="input-field col s12"><input id="url_link" type="url" class="url_link validate" placeholder="http://autoengage.io"><label for="url_link">Enter URL</label></div> <div class="center col s12"> <a href="javascript:void(0)" class="btn btn_style red doneBtnOption">DONE</a> </div> <a class="btn btn_style clsTextBtnDiv" style="border-radius: 15px; color: #3b5998; background:#ffffff; position: absolute; right: -9px;top: -10px; border: 1px solid #3b5998;"><i class="icon-trash"></i></a></div></div></div></form>');
            var domElement = $('<form class="text_card_div row"><div class="col m12 card-panel radius_10"><a class="btn btn_style clsBtnOption" style="z-index: 99;border-radius: 15px;color: #3b5998;background:#ffffff;float: right;margin-top: -15px;border: 1px solid #3b5998;" onclick="deleteOption(this)"><i class="icon-trash"></i></a><div style="min-height:65px;"><h6 class="text_card_text" contenteditable="true" onkeydown="lengthCount(this,640)">Hi !! Add your text !!</h6></div><hr class="hrstyle"><span class="right char-count" style="color:gray;font-size:12px;">22/640</span><div class="buttonsDiv center margin-bottom-10"><div class="addBtnOption btn btn_style push_dark white-text"><span><i class="icon-plus"></i> Add Button</span></div><div class="add_re_btn_div card-panel radius_10" id="add_re_btn_div" style="display:none; z-index: 99; width: 333px; position: absolute; margin-left: 347px;margin-top: -126px; padding: 3px;border: 1px solid;"> <div class="input-field col s12 no-space"><input id="btn_name" type="text" class="btn_name validate" size="20" maxlength="20"><label for="btn_name">Enter Button Name</label></div><div class="input-field col s12 no-space"><input id="url_link" type="url" class="url_link validate" placeholder="http://autoengage.io"><label for="url_link">Enter URL</label></div> <div class="center col s12"> <a href="javascript:void(0)" class="btn btn_style red doneBtnOption">DONE</a> </div> <a class="btn btn_style clsTextBtnDiv" style="border-radius: 15px; color: #3b5998; background:#ffffff; position: absolute; right: -9px;top: -10px; border: 1px solid #3b5998;"><i class="icon-trash"></i></a></div></div></div></form>');
            $("#addBuildDiv").append(domElement);
            $('#items-div').css('display', 'block');
        });

        //appened Img card
        $(document.body).on("click", ".img_card_add_btn", function () {
            var domElement = $('<form class="img_card_div row"><div class="col m12 card-panel radius_10 margin-bottom-10"><a class="btn btn_style clsBtnOption" style="z-index: 99;border-radius: 15px;color: #3b5998;background:#ffffff;float: right;margin-top: -15px;border: 1px solid #3b5998;" onclick="deleteOption(this)"><i class="icon-trash"></i></a><input type="file" class="img_card_image dropify" data-max-file-size="300K"/></div></form>');
            $("#addBuildDiv").append(domElement);
            $('.dropify').dropify({
                messages: {
                    'error': 'The file size is too big ({{ 300 }}KB max).'
                },
                error: {
                    'fileSize': 'Ooops, something wrong happended.'
                }
            });
            $('#items-div').css('display', 'block');
        });

        // delete sweet alert
        function deleteOption(element) {
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel pls!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $(element).parents('form:first').remove();
                            if ($('#addBuildDiv').children().length == 0) {
                                $('#items-div').css('display', 'none');
                            }
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        }
                        else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
        }

    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            //To close the add button div for text card
            $(document).on("click", ".clsTextBtnDiv", function () {
                $(this).parents(".add_re_btn_div:first").hide();
            });

            //To display the add button div for text card
            $(document).on("click", ".addBtnOption", function () {
                $(this).parents('form:first').find(".add_re_btn_div").show();
                Materialize.updateTextFields();
            });

            //To add the button after the button creation
            $(document).on("click", ".doneBtnOption", function () {
                var valid = true;
                $(this).parents('.add_re_btn_div:first').find('input').each(function (i, v) {
                    if ($(this).val() === "" || $(this).hasClass('invalid')) {
                        valid = false;
                    }
                });
                if (valid) {
                    var parentDiv = $(this).parents('.add_re_btn_div:first');
                    var buttonText = $(parentDiv).find('.btn_name').val();
                    var buttonUrl = $(parentDiv).find('.url_link').val();
                    $(parentDiv).siblings('.addBtnOption').remove();
                    $('<a class="custom_btn col s12 btn btn_style push_dark white-text" style="margin-bottom:5px;text-transform: none;" data-url="' + buttonUrl + '">' + buttonText + '</a>').insertBefore(parentDiv);
                    $(parentDiv).hide();
                }
            });

            //Display alert message on select tag
            $('input[name=msg_tags]').click(function(){
                var message='',tag_value;
                tag_value = $(this).val();

                if (tag_value == "CONFIRMED_EVENT_UPDATE"){
                    message = 'This should be information about an event a subscriber registered for with you.If your message sent is not relevant to this your Fan Page and/or account may be banned by Facebook';
                }else if(tag_value == "POST_PURCHASE_UPDATE"){
                    message = 'This should be Transaction confirmations,shipment status or order changes.If your message sent is not relevant to this your Fan Page and/or account may be banned by Facebook';
                }else if(tag_value == "ACCOUNT_UPDATE"){
                    message = 'This should be Application status, approvals, suspicious activity, fraud alerts.If your message sent is not relevant to this your Fan Page and/or account may be banned by Facebook';
                }else if(tag_value == "OTHER"){
                    message = 'It will send the message to anyone who\'s interacated within the 24 hour window.';
                }

                if(message != ''){
                    sweetAlert('Info!', message, 'info');
                }
            });

            //ajax to send the message to users
            $('#save-messages').on('click', function () {
                var createdItems = {};
                var rowId = '<?php if (isset($sequenceData["msgId"])) {
                    echo $sequenceData["msgId"];
                }?>';
                var url = '/user/save-message';
                var tag_type = $('#tag_type').val();
                if (typeof rowId !== "undefined" && rowId !== "") {
                    url = '/user/save-message/' + rowId;
                }
                var sequenceType = '<?php if (isset($sequenceData["type"])) {
                    echo $sequenceData["type"];
                }?>';
                var pageId = '<?php if (isset($sequenceData["for_page_id"])) {
                    echo $sequenceData["for_page_id"];
                }?>';
                var sequenceId = '<?php if (isset($sequenceData["id"])) {
                    echo $sequenceData["id"];
                }?>';

                <!-- Message Tag Field -->
                var selectedTags='';
                if($(':radio[name=msg_tags]').is(':visible')){
                    if(!$("input[name=msg_tags]").is(":checked")) {
                        sweetAlert('Info!', "Please select a tag", 'info');
                        return false;
                    }else{
                        selectedTags = $("input[name=msg_tags]:checked").val();
                    }
                }
                <!-- End -->

                var scheduleData = {};
                if (sequenceType == 'S') {
                    if ($.trim($('#days').val()) !== "" && $.trim($('#scheduleTime').find('input').val() !== "")) {
                        scheduleData['schedulePeriod'] = $('#days').val();
                        scheduleData['scheduleTime'] = $('#scheduleTime').find('input').val();
                    } else {
                        alert("Please select a schedule");
                        return false;
                    }
                } else if (sequenceType == 'R') {
                    if ($.trim($('#weekdays').val()) !== "" && $.trim($('#scheduleTime').find('input').val() !== "")) {
                        scheduleData['scheduleWeekDay'] = $('#weekdays').val();
                        scheduleData['scheduleTime'] = $('#scheduleTime').find('input').val();
                    } else {
                        alert("Please select a schedule");
                        return false;
                    }
                } else if (sequenceType == 'D') {
                    if ($.trim($('#scheduleDate').find('input').val()) !== "" && $.trim($('#scheduleTime').find('input').val() !== "")) {
                        scheduleData['scheduleDate'] = $('#scheduleDate').find('input').val();
                        scheduleData['scheduleTime'] = $('#scheduleTime').find('input').val();
                    } else {
                        alert("Please select a schedule");
                        return false;
                    }
                }

                $('#addBuildDiv').children('form').each(function (i, v) {
                    var currentElement = '';
                    var item = {};
                    var data = {};
                    if ($(this).hasClass('text_card_div')) {
                        currentElement = $(this);
                        item = {};
                        data = {};
                        item['type'] = 'text';
                        var buttonExist = $(currentElement).find('.custom_btn');
                        if (typeof buttonExist !== "undefined" && buttonExist.length > 0) {
                            item['type'] = 'buttonTemplate';
                            data['text'] = $(currentElement).find('.text_card_text:first').text();
                            var buttonsData = [];
                            var button = {};
                            button['title'] = $(currentElement).find('.custom_btn').text();
                            button['url'] = $(currentElement).find('.custom_btn').attr('data-url');
                            buttonsData.push(button);
                            data['button'] = buttonsData;
                            item['data'] = data;
                            createdItems[i] = item;
                        } else {
                            data['text'] = $(currentElement).find('.text_card_text:first').text();
                            item['data'] = data;
                            createdItems[i] = item;
                        }
                    } else if ($(this).hasClass('img_card_div')) {
                        currentElement = $(this);
                        item = {};
                        data = {};
                        item['type'] = 'image';
                        data['url'] = $(currentElement).find('img').attr('src');
                        item['data'] = data;
                        createdItems[i] = item;
                    }
                });
                if ($('#timezone').val() !== "") {
                    scheduleData['timezone'] = $('#timezone').val();
                    if ($('#addBuildDiv').children().length !== 0) {
                        $.ajax({
                            url: url,
                            method: 'post',
                            dataType: 'json',
                            data: {
                                scheduleData: scheduleData,
                                messageDetails: createdItems,
                                filterType: 'all',
                                sequenceType: sequenceType,
                                pageId: pageId,
                                sequenceId: sequenceId,
                                messageTag:selectedTags
                            },
                            beforeSend: function () {
                                $('#blockDiv').removeClass('hide');
                            },
                            success: function (response) {
                                $('#blockDiv').addClass('hide');
                                if (response.code == 200) {
                                    $('#addBuildDiv').html(' ');
                                    $('#items-div').hide();
                                    var successMessage = "Your message has been successfully inserted!";
                                    if (typeof rowId !== "undefined" && rowId !== "") {
                                        successMessage = "Your message has been successfully updated!";
                                    }
                                    swal({
                                        title: "Congratulations!",
                                        text: successMessage,
                                        type: "success"
                                    }, function () {
                                        window.location = response.redirectUrl;
                                    });
                                } else {
                                    alert("Error in inserting the message");
                                }
                            },
                            error: function () {
                                $('#blockDiv').addClass('hide');
                            },
                            complete: function () {
                                $('#blockDiv').addClass('hide');
                            }
                        });
                    } else {
                        if ($('#addBuildDiv').children().length == 0) {
                            alert("Please add an item to send");
//                            swal({
//                                title: "Please add an item to send",
//                                timer: 1500,
//                                showConfirmButton: false
//                            });
                        } else {
                            alert("Please select a filteration type")
                        }
                    }
                } else {
                    alert("Please select a timezone");
                }
            });
        });
    </script>
@endsection