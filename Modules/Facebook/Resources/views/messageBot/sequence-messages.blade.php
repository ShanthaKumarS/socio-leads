@extends('User::layouts.master')
@section('title','SocioLeads | Sequence Messages')
@section('title1',$sequenceData[0]['sequence_name'])
@section('content')
    <main>
        <div class="container">
            <div class="row margin-top-10">
                <div class="col s12 m12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title push_dark_text">@if($sequenceData[0]['type']=='S'){{"Step Sequence"}}@elseif($sequenceData[0]['type']=='D'){{"Date Sequence"}}@elseif($sequenceData[0]['type']=='R'){{"Recurring Sequence"}}@endif
                                :- <span class="grey-text">{{$sequenceData[0]['sequence_name']}}</span></span>
                            <a href="/user/create-message/{{$sequenceData[0]['id']}}" class="btn right btn_style push_dark">Create New Message</a>
                            <a href="/facebook/page-settings/{{$sequenceData[0]['pageId']}}" class="btn right btn_style push_dark">Back To Page Settings</a>
                            <a href="/facebook/broadcast-message/{{$sequenceData[0]['pageId']}}" class="btn right btn_style push_dark">Back To Message Broadcast</a>
                            <table id="sequenceTable" class="cell-border display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th style="display:@if($sequenceData[0]['type']!=='D'){{"none"}}@endif">Sequence Date</th>
                                    <th style="display:@if($sequenceData[0]['type']!=='R'){{"none"}}@endif">Recurrence Days</th>
                                    <th style="display:@if($sequenceData[0]['type']!=='S'){{"none"}}@endif">Steps<br><span style="font-size: 10px;">(Days since user subscribed)</span></th>
                                    <th>Current Subscriber</th>
                                    <th>Sent</th>
                                    <th>Delivered</th>
                                    <th>Read Count</th>
                                    <th>Click Count</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th style="display:@if($sequenceData[0]['type']!=='D'){{"none"}}@endif">Sequence Date</th>
                                    <th style="display:@if($sequenceData[0]['type']!=='R'){{"none"}}@endif">Recurrence Days</th>
                                    <th style="display:@if($sequenceData[0]['type']!=='S'){{"none"}}@endif">Steps</th>
                                    <th>Current Subscriber</th>
                                    <th>Sent</th>
                                    <th>Delivered</th>
                                    <th>Read Count</th>
                                    <th>Click Count</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @if(is_array($sequenceData) && isset($sequenceData[0]['msgId']))
                                    <?php $i = 1; ?>
                                    @foreach($sequenceData as $message)
                                        <tr>
                                            <td><img src="/assets/images/broadcast-icon.png" class="responsive-img"></td>
                                            <td style="display:@if($sequenceData[0]['type']!=='D'){{"none"}}@endif">{{$message['time'] ?? NULL}}</td>
                                            <td class="row" style="display:@if($sequenceData[0]['type']!=='R'){{"none"}}@endif">
                                                <div class="left"></div>
                                                <div class="right">{{$message['recurrence_days'] ?? NULL}}</div>
                                            </td>
                                            <td class="row" style="display:@if($sequenceData[0]['type']!=='S'){{"none"}}@endif">
                                                <div class="left"></div>
                                                <div class="right">{{$message['period'] ?? NULL}}</div>
                                            </td>
                                            <td class="center">{{$message['user_count'] ?? 0}}</td>
                                            <td class="center">{{$message['total_send'] ?? 0}}</td>
                                            <td class="center">{{$message['delivery_count'] ?? 0}}</td>
                                            <td class="center">{{$message['read_count'] ?? 0}}</td>
                                            <td class="center">{{$message['click_count'] ?? 0}}</td>
                                            <td class="row">
                                                <div class="center">
                                                    <a class='dropdown-button' href='#' data-activates='pushLetters_dropdown{{$i}}'><img src="/assets/images/actions-icon.png" class="responsive-img"></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <ul id='pushLetters_dropdown{{$i}}' class='dropdown-content'>
                                            <li>
                                                {{--<a href="javascript:void(0);"><i class="fa fa-bar-chart-o"></i> Edit</a>--}}
                                                <a href="/user/edit-message/{{$message['msgId']}}"><i class="icon-pencil"></i> Edit</a>
                                                <a href="/user/sequence-message-stats/{{$message['msgId']}}?seq={{$sequenceData[0]['id']}}"><i class="fa fa-bar-chart-o"></i> Stats</a>
                                                <a onclick="deleteOption('/user/delete-message/{{$message['msgId']}}')"><i class="icon-trash"></i> Delete</a>
                                            </li>
                                        </ul>
                                        <?php $i = $i + 1; ?>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection

@section('script_function')
    <script type="text/javascript">
        $(document).ready(function () {
            //Setup - add a text input to each footer cell
            $('#sequenceTable tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            // DataTable
            var table = $('#sequenceTable').DataTable({
                "columnDefs": [
                    { "width": "4%", "targets": 0 }
                ]
            });

            // Apply the search
            table.columns().every(function () {
                var that = this;

                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                                .search(this.value)
                                .draw();
                    }
                });
            });
        });
    </script>
    <script>
        //To delete the sequence
        function deleteOption(url) {
            swal({
                        title: "Are you sure?",
                        text: "Once deleted you will need to authenticate again.",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Disconnect !",
                        cancelButtonText: "No, It was a mistake!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your Autoresponders has been deleted.", "success");
                            window.location.href = url;
                        }
                        else {
                            swal("Cancelled", "Your Autoresponders are safe :)", "error");
                        }
                    });
        }
    </script>
@endsection
