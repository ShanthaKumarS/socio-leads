@extends('layouts.master')
@section('title','SocioLeads | PageSettings')
@section('title1','Page Settings')
@section('style')
    <style>
        .icon-star {
            cursor: pointer;
            float: right;
            font-size: 16px;
            line-height: 32px;
            padding-left: 8px;
        }
        .icon-close {
            cursor: pointer;
        }
        .h6New{
            font-size: 0.9rem;!important;
        }

        .mam, .mac{
            border: 4px double #3677bc;
        }

        .activeButton{
            border: 4px double rgba(255, 255, 255, 0.52);
            background-color: #3677bc !important;
        }

        @media screen and (min-width: 480px) {
            .card_inline_off{
                display: inline-flex;
            }

            .l20, .m20 {
                width: 20% !important;
            }
        }
        @media only screen
        and (min-device-width: 320px)
        and (max-device-width: 480px)
        and (-webkit-min-device-pixel-ratio: 2) {
            .card_inline_off{
                display: block;
            }
        }

        .settings-text {
            font-size: 1.3rem;
            line-height: 140%;
            margin: 0.5rem 0 0.4rem;
        }

        .switch-text {
            font-size: 1rem;
        }

        .fixed-center {
            display: inline-flex;
            text-align: center;
            width: 100%;
        }

        .tt .toltip{display: none;}
        .tt:hover .toltip {
            display: block;
            margin: -6px 0 0 -37px;
            position: absolute;
            width: 90px;
        }


        .arrow_box {
            position: relative;
            background: #73b4f9;
            border: 2px solid #61a2e7;
            color: #fff;
            padding: 4px 7px;font-size: 12px;
            border-radius: 10px;
            margin: 10px 0 0 0;
        }

        .arrow_box a{ color: #fff; text-decoration: underline;}
        .arrow_box:after, .arrow_box:before {
            bottom: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(136, 183, 213, 0) rgba(136, 183, 213, 0) #73b4f9;
            border-width: 11px;
            margin-left: -11px;
        }
        .arrow_box:before {
            border-color:  rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #61a2e7;
            border-width: 13px;
            margin-left: -13px;
        }

        .modal {
            overflow-y: visible !important;
            max-height: 80% !important;
        }

        @media screen and (max-width:799px) {

            .fixed-center {
                display: block;
                text-align: center;
                width: 100%;
            }
        }
        .collection li {
            list-style-type: none;
        }

        .collection .RopaSans_font {
            margin-left: 25px;
        }

    </style>
@endsection
@section('content')

        <!--begin::Header-->
        <div id="kt_header" class="header header-fixed">
            <!--begin::Container-->
            <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
                <!--begin::Header Menu Wrapper-->
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> Currently Viewing & Editing </h5>
                    <!--end::Page Title-->
                    <!--begin::Actions-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    <!--end::Actions-->
{{--                    <a href="/user/update-status/{{$page->id}}" class="btn btn-light-success font-weight-bolder btn-sm mr-2">Update Status</a>--}}
                    <!--a href="#global_filter_modal" class="btn btn-light-primary font-weight-bolder btn-sm mr-2 modal-trigger">Global Filter</a-->
{{--                    <button class="btn btn-light-primary font-weight-bolder btn-sm mr-2" data-toggle="modal" data-target="#GlobalFilter">--}}
{{--                        Global Filter--}}
{{--                    </button>--}}
                    <a href="/facebook/broadcast-message/{{$page->id}}" class="btn btn-light-info font-weight-bolder btn-sm mr-2">Broadcast</a>
                    <a href="/facebook/posts/{{$page->id}}" class="btn btn-light-warning font-weight-bolder btn-sm mr-2">All Posts</a>
                    <a href="/facebook/report/page/{{$page->id}}" class="btn btn-light-danger font-weight-bolder btn-sm">Page Report</a>
                </div>
                <!--end::Info-->
                <!--end::Header Menu Wrapper-->
                @include('layouts.top-toolbar')
            </div>
            <!--end::Container-->
        </div>
        <!--end::Header-->

        <!--begin::Content-->
        <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
            <!--begin::Entry-->
            <div class="d-flex flex-column-fluid">
                <!--begin::Container-->
                <div class=" container ">
                    <!--begin::Row-->
                    <div class="row">
                        <div class="col-lg-12">
                            <!--begin::Accordion-->
                            <div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample8">
                                <div class="card">
                                    <div class="card-header" id="headingOne8">
                                        <div class="card-title" data-toggle="collapse" data-target="#collapseOne8">
                                            <div class="card-label">Currently Viewing</div>
                                            <span class="svg-icon">
                                                <!--begin::Svg Icon | path:../assets/media/svg/icons/Navigation/Angle-double-right.svg-->
                                                <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                    </g>
                                                </svg><!--end::Svg Icon-->
                                            </span>
                                        </div>
                                    </div>
                                    <div id="collapseOne8" class="collapse show" data-parent="#accordionExample8">
                                        <div class="card-body">
                                            <div class="d-flex">
                                                <!--begin: Pic-->
                                                <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                                                    <div class="symbol symbol-50 symbol-lg-120">
                                                        <img src="{{file_exists(ltrim($page->picture, '/'))? $page->picture : 'https://graph.facebook.com/'.$page->facebook_page_id.'/picture?type=large'}}" alt="Pic">
                                                    </div>
                                                    <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                                                        <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                                                    </div>
                                                </div>
                                                <!--end: Pic-->

                                                <!--begin: Info-->
                                                <div class="flex-grow-1">
                                                    <!--begin: Title-->
                                                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                                                        <div class="mr-3">
                                                            <!--begin::Name-->
                                                            <a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">
                                                                {{$page->pagename ?? 'Page'}}<i class="flaticon2-correct text-success icon-md ml-2"></i>
                                                            </a>
                                                            <!--end::Name-->
                                                        </div>
                                                    </div>
                                                    <!--end: Title-->
                                                    <div class="d-flex align-items-center flex-wrap">
                                                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                            <div class="">
                                                                <div class="font-weight-bold mb-2">Current Fan Count</div>
                                                                <span class="btn btn-sm btn-text btn-light-primary text-uppercase font-weight-bold"><i class="fas fa-users mr-2"></i>{{$page->like_count}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                            <div class="">
                                                                <div class="font-weight-bold mb-2">Fans Gained</div>
                                                                <span class="btn btn-sm btn-text btn-light-success text-uppercase font-weight-bold"><i class="fas fa-user-friends mr-2"></i>{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getFansCount()}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                            <div class="">
                                                                <div class="font-weight-bold mb-2">Fans Per Day</div>
                                                                <span class="btn btn-sm btn-text btn-light-info text-uppercase font-weight-bold"><i class="fas fa-user-plus mr-2"></i>{{$pageReportRepository->setPageId($page->id)->setLastDays(1)->getFansCount()}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end: Info-->
                                            </div>

                                            <div class="separator separator-solid my-7"></div>

                                            <!--begin: Items-->
                                            <div class="d-flex align-items-center flex-wrap">
                                                <!--begin: Item-->
                                                <div class="d-flex align-items-center flex-lg-fill">
                                                    <span class="switch switch-outline switch-icon switch-primary">
                                                        <label>
                                                            <input type="checkbox" id="message_fan_status" {{($page->do_reply_message_fan)?'checked':''}}>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                    <div class="d-flex flex-column text-dark-75">
                                                        <span class="font-weight-bolder font-size-h5">
                                                            Message Replies<i class="text-primary far fa-envelope-open ml-3">{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getMessagesCount()}}</i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end: Item-->
                                                <!--begin: Item-->
                                                <div class="d-flex align-items-center flex-lg-fill">
                                                    <span class="switch switch-outline switch-icon switch-danger">
                                                        <label>
                                                            <input type="checkbox" id="like_non_fan_status" {{($page->do_like_fan)?'checked':''}}>
                                                            <span></span>
                                                        </label>
                                                    </span>
                                                    <div class="d-flex flex-column text-dark-75">
                                                        <span class="font-weight-bolder font-size-h5">
                                                            Like Comments<i class="text-danger far fa-heart ml-3"> {{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getLikesCount()}}</i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end: Item-->
                                                <!--begin: Item-->
                                                <div class="d-flex align-items-center flex-lg-fill">
                                                    <span class="switch switch-outline switch-icon switch-info">
                                                        <label>
                                                        <input type="checkbox" id="comment_fan_status" {{($page->do_reply_comment_fan)?'checked':''}}>
                                                        <span></span>
                                                    </label>
                                                    </span>
                                                    <div class="d-flex flex-column text-dark-75">
                                                        <span class="font-weight-bolder font-size-h5">
                                                            Comment Replies<i class="far fa-comments text-info ml-3">{{$pageReportRepository->setPageId($page->id)->setLastDays(null)->getCommentsCount()}}</i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end: Item-->
                                            </div>
                                            <!--begin: Items-->
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingTwo8">
                                        <div class="card-title">
                                            <div class="card-label">
                                                <a href="/facebook/posts/{{$page->id}}">Manage Auto Messages</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingThree8">
                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree8">
                                            <div class="card-label">Manage Auto Comments </div>

                                            <span class="svg-icon"><!--begin::Svg Icon | path:../assets/media/svg/icons/Navigation/Angle-double-right.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                </g>
                                            </svg><!--end::Svg Icon--></span></div>

                                    </div>

                                    <div id="collapseThree8" class="collapse" data-parent="#accordionExample8">
                                        <div class="card-body">
                                            <!--begin::Details-->
                                            <button type="button" class="btn btn-light-success btn-sm mb-3" onclick='addNewComment()' data-toggle="modal" data-target="#edit_Comments_modal">Add New</button>
                                            <div id="addfanscmt">
                                            @isset($page->autoComments)
                                                @foreach($page->autoComments as $autoComment)
                                            <div id='commentDiv-{{$autoComment->id}}'>
                                                <div class="d-flex mb-9">
                                                    <!--begin: Pic-->
                                                    <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                                                        <div class="symbol symbol-50 symbol-lg-120">
                                                            <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=large" alt="image">
                                                        </div>
                                                        <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                                                            <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                                                        </div>
                                                    </div>
                                                    <!--end::Pic-->

                                                    <!--begin::Info-->
                                                    <div class="flex-grow-1">
                                                        <!--begin::Title-->
                                                        <div class="d-flex justify-content-between flex-wrap mt-1">
                                                            <div class="d-flex mr-3">
                                                                <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$page->pag_ename ?? 'page'}}</a>
                                                                <a href="#"><i class="flaticon2-correct text-success font-size-h5"></i></a>
                                                            </div>

                                                            <div class="my-lg-0 my-3">
                                                                <button type="button" class="btn btn-sm btn-light-danger font-weight-bolder text-uppercase" id='{{$autoComment->id}}' onclick='deleteComment({{$autoComment->id}})'>Delete</button>
                                                                <button  id="comments-fans-{{$autoComment->id}}" onclick='editComment("{{$autoComment->id}}");' class="btn btn-sm btn-info font-weight-bolder text-uppercase" data-toggle="modal" data-target="#edit_Comments_modal">Edit</button>
                                                            </div>
                                                        </div>
                                                        <!--end::Title-->

                                                        <!--begin::Content-->
                                                        <div class="d-flex flex-wrap justify-content-between mt-1">
                                                            <div class="d-flex flex-column flex-grow-1 pr-8">
                                                                <span class="font-weight-bold text-dark-50" id="comm-fan-{{$autoComment->id}}">{{$autoComment->text ?? 'comment'}}</span>
                                                            </div>
                                                        </div>
                                                        <!--end::Content-->
                                                    </div>
                                                    <!--end::Info-->
                                                </div>

                                                Filters:
                                                <div id="FanCmtFilter{{$autoComment->id}}" class="text-dark-75 font-size-lg font-weight-normal mb-2">
                                                    @if($autoComment->filters && !empty($autoComment->filters))
                                                        @foreach($autoComment->filters as $filter)
                                                            @if(trim($filter->filter_text))<div class="label label-info label-inline">{{$filter->filter_text}} </div>@endif
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <div class="separator separator-solid mb-4"></div>
                                            </div>
                                            <!--end::Details-->
                                                @endforeach
                                            @endisset
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Accordion-->
                        </div>
                    </div>
                    <!--end::Row-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Entry-->
        </div>
        <!--end::Content-->
        <!-- Add new comment Modal-->
        <div class="modal fade" id="GlobalFilter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Global No Activity Filter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="">Global Filter Words</label>
                                        <div class="campaigns_scroll filter-list gbl-list hide">
                                            <?php
                                            $gblFilters=(trim($page->global_filter))?explode(',',$page->global_filter):[];
                                            $gblFilters = array_map('trim', $gblFilters);
                                            $gblFiltersString = implode(',', $gblFilters);
                                            ?>
                                        </div>
                                        <input id="gblFiltersData" class="form-control" name='tags-outside' class='form-control tagify tagify--outside' value='{{$gblFiltersString}}' placeholder='Write some tags' />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="">Post Filter Ids</label>
                                        <div class="campaigns_scroll filter-list post-list hide">
                                            <?php
                                            $postFilters=(trim($page->post_filter))?explode(',',$page->post_filter):[];
                                            $postFilters = array_map('trim', $postFilters);
                                            $postFiltersString = implode(',', $postFilters);
                                            ?>
                                        </div>
                                        <input id="postFiltersData" class="form-control" name='tags-outside' class='form-control tagify tagify--outside' value='{{$postFiltersString}}' placeholder='Write some tags' />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary font-weight-bold caller">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /End Global Filter -->

        <!-- Add new comment Modal-->
        <div class="modal fade" id="edit_Comments_modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Draft Your Auto Comment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="page-auto-comment-form">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="exampleTextarea">Auto Commend send by SL</label>
                                        <textarea id="comment" name="comment" class="form-control" rows="5"></textarea>
                                    </div>

                                    <ul class="collection">
                                        <li class="collection-item avatar">
                                            <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=small"
                                                 alt="Letschill" class="circle">
                                            <span class="title"><a href="#" class="push_dark_text RopaSans_font">{{$page->pagename ?? 'page'}}</a></span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="">Filter Words</label>
                                        <input id="kt_tagify_1" class="form-control tagify filter-list" name='filters' placeholder='type...' value='' autofocus data-blacklist='.NET,PHP' />

                                        <div class="mt-3">
                                            <a href="javascript:;" id="kt_tagify_1_remove" class="btn btn-sm btn-primary font-weight-bold">Remove tags</a>
                                        </div>

                                        <div class="mt-3 text-muted">
                                            Simply use below short codes and full name or first name will be dynamically added to your reply. FacebookName or fb_first_name Copy paste in your message to display persons full name on your comment.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input id="comment-type" type="hidden" value="fans">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold close-modal" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary font-weight-bold" onclick="saveComments()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /End comment -->

        <!--begin::Footer-->
        @include('layouts.page-footer')
        <!--end::Footer-->


        @if($page->status == 'P')
            <div class="container">
                <div class="col s12 m12 l12">
                    <div class="card-panel grey lighten-5 z-depth-2 ">
                        <div class="row valign-wrapper  red">
                            <div class="col s2 red white-text center">
                                <i class="fa fa-times-circle-o fa-4x"></i>
                            </div>
                            <div class="col s10 white grey-text text-darken-3">
                              <span class="black-text">
                                <h5>Alert - This page is in “cool down” mode right now.</h5>
                                <p> This is as a result of notifications from Facebook stating the activity is to high.
                                    We do this to prevent your page from getting banned from SocioLeads completely. The
                                    cool down period will last for 3 day per Facebooks requirements and then SocioLeads
                                    activity should resume on this page. This will not affect any other pages you have
                                    added to your account. If you have any questions please use the support button and
                                    let us know.</p>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($page->status == 'I')
            <div class="container">
                <div class="col s12 m12 l12">
                    <div class="card-panel grey lighten-5 z-depth-2 ">
                        <div class="row valign-wrapper  red">
                            <div class="col s2 red white-text center">
                                <i class="fa fa-times-circle-o fa-4x"></i>
                            </div>
                            <div class="col s10 white grey-text text-darken-3">
                              <span class="black-text">
                                <h5>Alert - This page is in “inactive” mode right now.</h5>
                                <p>This page has been deactivated for violating Facebook policies and/or complaints. Please contact us for further information.</p>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($page->status == 'W')
            <div class="container">
                <div class="col s12 m12 l12">
                    <div class="card-panel grey lighten-5 z-depth-2 ">
                        <div class="row valign-wrapper ">
                            <div class="col s2 blue white-text center">
                                <img src="/assets/images/info.png" class="responsive-img">
                            </div>
                            <div class="col s10 white grey-text text-darken-3">
                              <span class="black-text">
                                <h5> This page is "Pending" for approval.</h5>
                                <p>Thanks for adding your page to SocioLeads! In an effort to prevent spam and continue quality control while working
                                   with Facebook all pages with over 100,000 fans require manual approval. Don't worry! An alert has been sent to
                                   Anthony and he will review your page and approve it within 24 hours.</p>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($page->password_changed == 'Y')
            <div class="container password_changed_div">
                <div class="col s12 m12 l12">
                    <div class="card-panel grey lighten-5 z-depth-2 ">
                        <div class="row valign-wrapper  red">
                            <div class="col s2 red white-text center">
                                <i class="fa fa-times-circle-o fa-4x"></i>
                            </div>
                            <div class="col s10 white grey-text text-darken-3">
                              <span class="black-text">
                                <h5>Re-Authenticate Page</h5>
                                <p>We got notification from Facebook that the previous access token is expired as you
                                    have changed the password of your account, therefore SocioLeads is not able to reply
                                    to your comments.
                                    Please click below button and Re-authentication so that SocioLeads can start working
                                    again for your page.</p>
                             <div class="center col m12 l12">
                                 <a class="btn blue" onclick="reAuthFacebookLogin()">Re-Authenticate Now</a>
                             </div>
                              </span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($page->messaging == 'D')
            <div class="container password_changed_div">
                <div class="col s12 m12 l12">
                    <div class="card-panel grey lighten-5 z-depth-2 ">
                        <div class="row valign-wrapper  deep-orange lighten-1">
                            <div class="col s2 deep-orange lighten-1 white-text center">
                                <i class="fa fa-times-circle-o fa-4x"></i>
                            </div>
                            <div class="col s10 white grey-text text-darken-3">
                              <span class="black-text">
                                <h5>Re-Authenticate Page</h5>
                                <p>Now you can subscribe users to SocioLeads and send them a Facebook broadcast message once a day.
                                To activate this service you would need re-authenticate your Facebook page to SocioLeads.
                                Please click the button below to get started.
                                </p>
                             <div class="center col m12 l12">
                                 <a class="btn push_dark" onclick="reAuthFacebookLogin()" style="text-transform:none;">Re-Authenticate Now</a>
                             </div>
                              </span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="container">
        <!-- edit Messages fan modal Structure -->
        <div id="edit_Messages_modal" class="modal">
            <div class="modal-content">
                <a href="javascript:void(0);"
                   class="edit_Messages_modal modal-action modal-close waves-effect waves-green btn-flat right"><i
                            class="fa fa-close"></i></a>
                <h4 class="RopaSans_font push_dark_text"> Draft Your Auto Message</h4>
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="row card no-space lighten-5">
                            <div class="input-field col s12 m12 l12">
                                <textarea id="message" name="message" class="materialize-textarea"></textarea>
                                <label for="message" class=""> <i class="icon-pencil right"></i>Enter Your Message here
                                </label>
                            </div>
                            <div class="col s12 m12 l12 margin-bottom-10">
                                <span id="spnError1"
                                      style="color: Red; display: none">Please Enter your message.*</span>
                                <a href="#" id="save" class=" btn push_dark text_style_none col s12"
                                   onclick="sendmessage();"><strong class="strong_700">Save</strong> Message</a>
                            </div>
                        </div>
                        <ul class="collection">
                            <li class="collection-item avatar">
                                <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=small"
                                     alt="Letschill" class="circle">
                                <span class="title"><a href=""
                                                       class="push_dark_text RopaSans_font">{{$page->pagename ?? 'page'}}</a></span>
                                <p id="message_text">Hello,<?php echo '{{}}';?> thanks for your comment,I glad to have a
                                    fan like you.</p>
                                <a href="javascript:void(0);" class="secondary-content"><i
                                            class="icon-star blue-text text-darken-3"></i></a>
                            </li>
                        </ul>
                        <input id="message-type" type="hidden" value="fans">
                    </div>
                    <div class="col m6 l6 s12">
                        <div class="card-panel padding_10 no-margin filterDiv">
                            <h5 class="center push_dark_text no-space"><strong class="strong_700">Filter Words</strong></h5>
                            <hr>
                           <div class="row no-space">
                               <div class="input-field col s12" style="margin-top: .5rem; padding: 0;">
                                   <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                       <i class="icon-question push_dark_text" ></i>
                                       <div class="toltip">
                                           <div class="arrow_box">
                                               <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                               <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>
                                           </div>
                                       </div>
                                   </div>
                                   <i class="icon-plus push_dark_text add-filters positive" style="position: absolute;right: 35px;top: 15px;"></i>
                                   <input id="msg_filter_word-po" type="text" class="validate" style="margin: 0 0 7px 0;">
                                   <label for="msg_filter_word-po" style="left: 0;">Positive Filter Words</label>
                               </div>
                           </div>
                            <div class="row no-space">
                                <div class="input-field col s12" style="margin-top: .5rem; padding: 0;">
                                    <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                        <i class="icon-question push_dark_text" ></i>
                                        <div class="toltip">
                                            <div class="arrow_box">
                                                <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                                <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the filter box you will be telling SocioLeads to respond to people with this particular message or comment IF a person uses one of your filter words in their post.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <i class="icon-plus push_dark_text add-filters negative" style="position: absolute;right: 35px;top: 15px;"></i>
                                    <input id="msg_filter_word-ne" type="text" class="validate" style="margin: 0 0 7px 0;">
                                    <label for="msg_filter_word-ne" style="left: 0;">Negative Filter Words</label>
                                </div>
                            </div>
                            <div id="message-filters" class="filter-list"></div>
                        </div>
                        <div class="card-panel padding_10 no-margin push_dark">
                            <h5 class="center white-text no-space"><strong class="strong_700">Customize</strong> Your Message</h5>
                            <hr>
                            <div class="white-text">
                                <h6 style="font-weight: 300;">Simply use below short codes and full name or first name will be dynamically added to your reply.</h6>
                                <span><?php echo '{{FacebookName}} or {{fb_first_name}}';?></span>
                                <h6 style="font-weight: 300;"><i>Copy paste in your message to display persons full name on your Message.</i></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- global_filter modal Structure -->
        <div id="global_filter_modal" class="modal">
            <div class="modal-content padding_15">

                <a href="javascript:void(0);"
                   class="global_filter_modal modal-action modal-close waves-effect waves-green btn-flat right">
                    <i class="fa fa-close"></i></a>
                <div class="row global-div">
                <h5>Global No Activity Filter</h5>
                <div class="row filterDiv">
                    <div class="col m6 l6 s12">
                        <div class="row card-panel no-space">
                            <div class="input-field col s12">
                                <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                    <i class="icon-question push_dark_text add-filters" ></i>
                                    <div class="toltip">
                                        <div class="arrow_box">
                                            <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                            <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your page, regardless of what they type. By entering words into the global filter box you will be telling SocioLeads to NOT to respond to people with any message or comment IF a person uses one of your filter words in their post.</div>
                                        </div>
                                    </div>
                                </div>

                                <i class="icon-plus push_dark_text add-filters" style="position: absolute;right: 35px;top: 15px;"></i>
                                <input id="glb_filter_activity" type="text" class="validate">
                                <label for="glb_filter_activity">Global no activity filter</label>
                            </div>
                            <div class="col s12 margin-bottom-10">
                                <a href="javascript:;" class="btn push_dark text_style_none col s12 caller"><strong class="strong_700">Save</strong> Filters</a>
                            </div>
                        </div>
                    </div>
                    <div class="col m6 l6 s12">
                        <div class="card-panel padding_10 no-margin">
                            <h5 class="center push_dark_text no-space"><strong class="strong_700">Global Filter Words</strong></h5>
                            <hr>
                            <div class="campaigns_scroll filter-list gbl-list">
                                <?php $gblFilters=(trim($page->global_filter))?explode(',',$page->global_filter):false;?>
                                @if($gblFilters && !empty($gblFilters))
                                    @foreach($gblFilters as $gblFilter)
                                        @if(trim($gblFilter))<div class="label label-info label-inline">
                                                {{$gblFilter}}
                                                <i class="close icon-close delete-filter"></i></div>@endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                </div>


                <div class="row post-div">
                <h5>Post No Activity Filter</h5>
                <div class="row filterDiv">
                    <div class="col m6 l6 s12">
                        <div class="row card-panel no-space">
                            <div class="input-field col s12">
                                <div class="tt" style="top: 12px; position: absolute; right: 13px;">
                                    <i class="icon-question push_dark_text add-filters" ></i>
                                    <div class="toltip">
                                        <div class="arrow_box">
                                            <a id="more-info" class="more-info" href="javascript:void(0);">What’s This?</a>
                                            <div id="more-info-text" class="more-info-text hide">By default messages and comments are sent to anyone on your posts, regardless of what they type. By entering post ids into the global filter box you will be telling SocioLeads to NOT to respond to people with any message or comment on these posts.</div>
                                        </div>
                                    </div>
                                </div>

                                <i class="icon-plus push_dark_text add-filters" style="position: absolute;right: 35px;top: 15px;"></i>
                                <input id="post_filter_activity" type="text" class="validate">
                                <label for="post_filter_activity">Post no activity filter</label>
                            </div>
                            <div class="col s12 margin-bottom-10">
                                <a href="javascript:;" class="btn push_dark text_style_none col s12 caller2"><strong class="strong_700">Save</strong> Posts</a>
                            </div>
                        </div>
                    </div>
                    <div class="col m6 l6 s12">
                        <div class="card-panel padding_10 no-margin">
                            <h5 class="center push_dark_text no-space"><strong class="strong_700">Post Filter Ids</strong></h5>
                            <hr>
                            <div class="campaigns_scroll filter-list post-list">
                                <?php $postFilters=(trim($page->post_filter))?explode(',',$page->post_filter):false;?>
                                @if($postFilters && !empty($postFilters))
                                    @foreach($postFilters as $postFilter)
                                        @if(trim($postFilter))<div class="label label-info label-inline">{{$postFilter}} <i class="close icon-close delete-filter"></i></div>@endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
            <input id="fbid" type="text" value="{{env('FACEBOOK_APP_ID')}}" style="display: none;"/>

            <div id="msgAtt" class="modal ">
                <div class="modal-content">
                    <a href="javascript:;" class="right modal-action modal-close waves-effect waves-green "><i
                                class="icon-close grey-text text-darken-3"></i></a>
                    {{--<h5 class="RopaSans_font push_dark_text center">Re-Authenticate Pages</h5>--}}
                    <div class="row valign-wrapper  blue">
                        <div class="col s2 blue white-text center">
                            <img src="/assets/images/users.png" class="responsive-img">
                        </div>
                        <div class="col s10 white grey-text text-darken-3">
                      <span class="black-text">
                        <h5></h5>
                         <p>
                             You now manage your direct messages to commenters directly on the post they commented on. This will provide the most relevant messaging to your fans. Click the button below to view the posts on your page from there you can add direct messages that go out to anyone who comments on each specific post.
                         </p>
                          <div class="center col m12 l12">
                             <a class="btn blue" href="/facebook/posts/{{$page->id}}">View Page Posts</a>
                         </div>
                      </span>

                        </div>
                    </div>
                </div>
            </div>
            <div id="cmtAtt" class="modal ">
                <div class="modal-content">
                    <a href="javascript:;" class="right modal-action modal-close waves-effect waves-green "><i
                                class="icon-close grey-text text-darken-3"></i></a>
                    {{--<h5 class="RopaSans_font push_dark_text center">Re-Authenticate Pages</h5>--}}
                    <div class="row valign-wrapper  blue">
                        <div class="col s2 blue white-text center">
                            <img src="/assets/images/users.png" class="responsive-img">
                        </div>
                        <div class="col s10 white grey-text text-darken-3">
                      <span class="black-text">
                        <h5></h5>
                         <p>
                             You now manage your reply comment to commenter directly on the post they commented on. This will provide the most relevant commenting to your fans. Click the button below to view the posts on your page from there you can add auto comments that go out to anyone who comments on each specific post.
                         </p>
                          <div class="center col m12 l12">
                             <a class="btn blue" href="/facebook/posts/{{$page->id}}">View Page Posts</a>
                         </div>
                      </span>

                        </div>
                    </div>
                </div>
            </div>
    </main>
@endsection


@section('script_function')
    <!--begin::Page Scripts(used by this page)-->
    <script src="/assets/js/pages/crud/forms/widgets/tagify.js"></script>
    <script src="/assets/js/pages/features/miscellaneous/sweetalert2.js"></script>
    <!--end::Page Scripts-->
    <script>
        var fbid=$('#fbid').val();
        window.fbAsyncInit = function () {
            FB.init({
                appId: fbid,
                xfbml: true,
                cookie: true,
                version: 'v10.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function reAuthFacebookLogin() {

            //facebook login.......
            var details=[];
            FB.login(function (response) {
                //welcome message in console
                if (response.status === "connected") {
                    r_token = response.authResponse.accessToken;
                    fb_id = response.authResponse.userID;
                    exp_in = response.authResponse.expiresIn;
                    fb_name='';
                    //fetching response indivisually
                    //fetching facebook username
                    FB.api(response.authResponse.userID, function (response) {
                        if (response && !response.error) {
                            fb_name = response.name;
                        }
                    });

                    details.push({
                        r_token:r_token,
                        fb_id:fb_id,
                        exp_in:exp_in,
                        fb_name:fb_name
                    });
                    var pageId = '<?php echo $page->id; ?>';
                    $.ajax({
                        url: '/user/facebook/update',
                        method: 'post',
                        data: {
                            fb_pages_attributes: 'gggggggg',
                            pageId: pageId
                        },
                        datatype: 'json',
                        beforeSend : function() {
                            // $('#blockDiv').removeClass('hide');
                        },
                        complete: function () {
                            // $('#blockDiv').addClass('hide');
                        },
                        success: function (pages) {
                            pages = $.parseJSON(pages);
                            // $('#blockDiv').addClass('hide');
                            var tokenDiv = $('.password_changed_div');
                            if (pages['code'] == 200) {
                                tokenDiv.find('p').css('color', '#238723').html('<b>' + pages['message'] + '<b>');
                                tokenDiv.fadeOut(5000);
                            } else if (pages['code'] == 404) {
                                tokenDiv.find('p').css('color', '#ff8400').html(pages['message']);
                            }
                        }
                    });
                }
            }, {scope: 'manage_pages,publish_pages,read_page_mailboxes'})
        }

    </script>

    <script>
        var commentId;

        $('.waves-green').removeClass('waves-green');
        $('#message').on('keyup', function () {
            $('#message_text').text($(this).val());
            $("#spnError1").css("display", "none");
        });
        $('#comment').on('keyup', function () {
            $('#comment_text').text($(this).val());
            $("#spnError2").css("display", "none");
        });

        function callModal(type) {

            $('#spnError1').hide();
            $('#message').val(null);
            $('#message_text').text(null);
            $('#msg_filter').val(null);
            var model=$('#edit_Messages_modal');
            model.find('input#message-type').val(type);
            model.find('.filter-list').html('');
            model.find('.filter-list').parent().css('display','none');
        }

        function callModal2(type) {
            $('#spnError2').hide();
            $('#cmt_filter').val(null);
            $('#comment').val(null);
            $('#comment_text').text(null);
            var model=$('#edit_Comments_modal');
            model.find('input#comment-type').val(type);
            model.find('.filter-list').html('');
            model.find('.filter-list').parent().css('display','none');
        }

        $('.dropdown-button').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: false, // Does not change width of dropdown to that of the activator
                    hover: true, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: false, // Displays dropdown below the button
                    alignment: 'right' // Displays dropdown with edge aligned to the left of button
                }
        );


        // div toogle
        $(document).ready(function () {
            $(".mam").click(function () {
                $(".mac_div").hide();
                $(".mac").removeClass('activeButton');
                $(".mam_div").show();
                $(".mam").addClass('activeButton');
            });
            $(".mac").click(function () {
                $(".mam_div").hide();
                $(".mam").removeClass('activeButton');
                $(".mac_div").show();
                $(".mac").addClass('activeButton');
            });
        });

        function sendmessage() {
            var status = $('#message-type').val();
            if (status) {
                var model=$('#edit_Messages_modal');
                $.each(model.find('.add-filters'),function () {
                    $(this).click();
                });
                var id = '<?php echo $page->id ?>';
                var type = status;
                var message = $("#message").val();
                var msg_filter = '';
                var filter_details=[];
                if (model.find('.filter-list').children().length > 0) {
                    $.each(model.find('.filter-list').children(), function (i, v) {
                        var word=$(this).text().trim();
                        if(word!=''){
                            msg_filter += $(this).text().trim() + ",";
                            if($(this).find('.fa-plus-square').length === 1)
                                filter_details[word]=1;
                            else if ($(this).find('.fa-minus-square').length === 1)
                                filter_details[word]=0;
                        }
                    });
                } else {
                    msg_filter = null;
                }
                filter_details=$.extend({},filter_details);
                if (message == null || message == "") {
                    $("#spnError1").css("display", "block");
                    return false;
                } else {
                    $('.edit_Messages_modal').trigger('click');
                    $.ajax({
                        url: '/user/page-messages',
                        data: {
                            for_page_id: id,
                            fan_type: type,
                            message: message,
                            msg_filter: msg_filter,
                            filter_details: filter_details,
                            type: 'page'
                        },
                        type: 'post',
                        datatype: 'json',
                        success: function (response) {
                            response = $.parseJSON(response);
                            if (response['status'] == 200) {
                                if (type == 'fans') {
                                    var data = "'fans-update'";
                                    $('#addfanmsg').prepend('<div class="card-panel grey lighten-5 z-depth-1" id="messageDiv-' + response['id'] + '"> <div class="row valign-wrapper"> <div class="col s2 center"> <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=small" alt="LetsChill" class="circle responsive-img"> </div>' +
                                            '<div class="col s10"> <div class="black-text"><a class="push_dark_text right" href="javascript:;" id="' + response['id'] + '"  onclick="deletemessage(this.id)"><i class="icon-close"></i></a> <h6>{{$page->pagename ?? 'page'}}' +
                                            '</h6> <span id="mess-fan-' + response['id'] + '">' + response['message'] + '</span> </div> <div class="row no-space"> ' +
                                            '<a id="messages-fans-' + response['id'] + '" onclick="editComment('+ response['id'] +');" class="push_dark_text modal-trigger right" href="javascript:;"><i class="icon-pencil"></i></a> </div><div id="FanMsgFilter' + response['id'] + '">' + response['div'] + '</div></div> </div> </div>');
                                }

                                else if (type == "nonfans") {
                                    var data1 = "'nonfans-update'";
                                    $('#addnonfanmsg').prepend('<div class="card-panel grey lighten-5 z-depth-1" id="messageDiv-' + response['id'] + '"> <div class="row valign-wrapper"> <div class="col s2 center"> <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=small" alt="LetsChill" class="circle responsive-img"> </div>' +
                                            '<div class="col s10"> <div class="black-text"> <a class="push_dark_text right" href="javascript:;" id="' + response['id'] + '"  onclick="deletemessage(this.id)"><i class="icon-close"></i></a> <h6>{{$page->pagename ?? 'page'}}' +
                                            '</h6> <span id="mess-nonfan-' + response['id'] + '">' + response['message'] + '</span> </div>  <div class="row no-space"> ' +
                                            '<a id="messages-nonfans-' + response['id'] + '" onclick="editComment('+ response['id'] +');" class="push_dark_text modal-trigger right" href="javascript:;"><i class="icon-pencil"></i></a></div><div id="nonFanMsgFilter' + response['id'] + '">' + response['div'] + '</div> </div></div> </div> </div>');
                                }
                            }
                            else if (response['status'] == 198) {

                                $('#closepop').show();
                            }
                            else if (response['status'] == 201) {
                                $("#mess-fan-" + response['id']).html(response['message']);
                                $('#FanMsgFilter' + response['id']).html(response['div']);
                            }
                            else if (response['status'] == 202) {
                                $("#mess-nonfan-" + response['id']).html(response['message']);
                                $('#nonFanMsgFilter' + response['id']).html(response['div']);
                            }
                        }
                    });
                }

            }
        }

        function addNewComment()
        {
            commentId = undefined;
        }

        function editComment(id)
        {
            commentId = id;

            tagifyDemo1.removeAllTags();
            var filter=[];
            $.each($('#FanCmtFilter' + id).find('.label'), function(){
                filter.push($(this).text().trim());
            });
            $('#comment').val($('#comm-fan-' + id).text());
            tagifyDemo1.addTags(filter);
        }

        function saveComments()
        {
            var form = new FormData($('#page-auto-comment-form')[0]);

            if ( commentId != undefined && commentId != '') {
                form.append('commentId', commentId);
            }

            $.ajax({
                url : '/facebook/store-page-auto-comment/{{$page->id}}',
                type : 'post',
                data : form,
                processData : false,
                contentType : false,
                success : function (response) {
                    var tags = '';
                    $.each(response.filters, function (index, value) {
                        tags = tags + '<div class="label label-info label-inline">' + value + '</div>'
                    });
                    $('#edit_Comments_modal').find('.close-modal').click();
                    $('#commentDiv-' + response.id).remove();
                    $('#addfanscmt').prepend(
                        '<div id="commentDiv-' + response.id + '">' +
                        '    <div class="d-flex mb-9">' +
                        '        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">' +
                        '            <div class="symbol symbol-50 symbol-lg-120">' +
                        '                <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=large" alt="image">' +
                        '            </div>' +
                        '            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">' +
                        '                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>' +
                        '            </div>' +
                        '        </div>' +
                        '        <div class="flex-grow-1">' +
                        '            <div class="d-flex justify-content-between flex-wrap mt-1">' +
                        '                <div class="d-flex mr-3">' +
                        '                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$page->page_name ?? 'page'}}</a>' +
                        '                    <a href="#"><i class="flaticon2-correct text-success font-size-h5"></i></a>' +
                        '                </div>' +
                        '                <div class="my-lg-0 my-3">' +
                        '                    <button type="button" class="btn btn-sm btn-light-danger font-weight-bolder text-uppercase" id="' + response.id + '" onclick="deleteComment(' + response.id + ')">Delete</button>' +
                        '                    <button  id="comments-fans-' + response.id + '" onclick="editComment(' + response.id + ');" class="btn btn-sm btn-info font-weight-bolder text-uppercase" data-toggle="modal" data-target="#edit_Comments_modal">Edit</button>' +
                        '                </div>' +
                        '            </div>' +
                        '            <div class="d-flex flex-wrap justify-content-between mt-1">' +
                        '                <div class="d-flex flex-column flex-grow-1 pr-8">' +
                        '                    <span class="font-weight-bold text-dark-50" id="comm-fan-' + response.id + '">' + response.comment + '</span>' +
                        '                </div>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>' +
                        '    Filters:<div id="FanCmtFilter' + response.id + '" class="text-dark-75 font-size-lg font-weight-normal mb-2">' + tags + ' </div>' +
                        '    <div class="separator separator-solid mb-4"></div>' +
                        '</div>');
                },
                error : function (response) {

                }
            });
        }


        {{--function sendcomments1() {--}}
        {{--    var status = $('#comment-type').val();--}}
        {{--    if (status) {--}}
        {{--        var model=$('#edit_Comments_modal');--}}
        {{--        var id = '<?php echo $page->id ?>';--}}
        {{--        var type = status;--}}
        {{--        var message = $("#comment").val();--}}
        {{--        var filter_details={};--}}
        {{--        var cmt_filter = '';--}}
        {{--        let filterListString = '';--}}

        {{--        if($('#kt_tagify_1').val().trim()) {--}}
        {{--            let filterListStringObject = JSON.parse($('#kt_tagify_1').val());--}}
        {{--            filterListStringObject     = filterListStringObject.map(el => el.value);--}}
        {{--            filterListString       = filterListStringObject.join(',');--}}
        {{--        }--}}

        {{--        //let filterListString = model.find('.filter-list').val();--}}
        {{--        if (filterListString.trim()) {--}}
        {{--            let filterDetailsArr = filterListString.split(',');--}}
        {{--            $.each(filterDetailsArr, function(i, v) {--}}
        {{--                filter_details[v.trim()] = 1;--}}
        {{--                filterDetailsArr[i] = v.trim();--}}
        {{--            });--}}
        {{--            cmt_filter = filterDetailsArr.join(',');--}}
        {{--        } else {--}}
        {{--            cmt_filter = null;--}}
        {{--        }--}}
        {{--         filter_details = $.extend({}, filter_details);--}}
        {{--        if (message == null || message == "") {--}}
        {{--            $("#spnError2").css("display", "block");--}}
        {{--            return false;--}}
        {{--        } else {--}}
        {{--            $('.edit_Comments_modal').trigger('click');--}}
        {{--            $.ajax({--}}
        {{--                url: '/facebook/store-page-auto-comment',--}}
        {{--                data: {--}}
        {{--                    for_page_id: id,--}}
        {{--                    fan_type: type,--}}
        {{--                    comment: message,--}}
        {{--                    cmt_filter: cmt_filter,--}}
        {{--                    filter_details: filter_details,--}}
        {{--                    type: 'page',--}}
        {{--                    _token : '{{csrf_token()}}'--}}
        {{--                },--}}
        {{--                type: 'post',--}}
        {{--                dataType: 'json',--}}
        {{--                success: function (response) {--}}
        {{--                    if (response['status'] == 200) {--}}
        {{--                        if (type == 'fans') {--}}
        {{--                            var data = "'fans-update'";--}}

        {{--                            $('#addfanscmt').prepend(--}}
        {{--                            '<div id="commentDiv-' + response['id'] + '">' +--}}
        {{--                            '    <div class="d-flex mb-9">' +--}}
        {{--                            '        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">' +--}}
        {{--                            '            <div class="symbol symbol-50 symbol-lg-120">' +--}}
        {{--                            '                <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=large" alt="image">' +--}}
        {{--                            '            </div>' +--}}
        {{--                            '            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">' +--}}
        {{--                            '                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>' +--}}
        {{--                            '            </div>' +--}}
        {{--                            '        </div>' +--}}
        {{--                            '        <div class="flex-grow-1">' +--}}
        {{--                            '            <div class="d-flex justify-content-between flex-wrap mt-1">' +--}}
        {{--                            '                <div class="d-flex mr-3">' +--}}
        {{--                            '                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$page->pagename ?? 'page'}}</a>' +--}}
        {{--                            '                    <a href="#"><i class="flaticon2-correct text-success font-size-h5"></i></a>' +--}}
        {{--                            '                </div>' +--}}
        {{--                            '                <div class="my-lg-0 my-3">' +--}}
        {{--                            '                    <button type="button" class="btn btn-sm btn-light-danger font-weight-bolder text-uppercase" id="' + response['id'] + '" onclick="deletecomment(this.id)">Delete</button>' +--}}
        {{--                            '                    <button  id="comments-fans-' + response['id'] + '" onclick="select(this.id,' + data + ');" class="btn btn-sm btn-info font-weight-bolder text-uppercase" data-toggle="modal" data-target="#edit_Comments_modal">Edit</button>' +--}}
        {{--                            '                </div>' +--}}
        {{--                            '            </div>' +--}}
        {{--                            '            <div class="d-flex flex-wrap justify-content-between mt-1">' +--}}
        {{--                            '                <div class="d-flex flex-column flex-grow-1 pr-8">' +--}}
        {{--                            '                    <span class="font-weight-bold text-dark-50" id="comm-fan-' + response['id'] + '">' + response['message'] + '</span>' +--}}
        {{--                            '                </div>' +--}}
        {{--                            '            </div>' +--}}
        {{--                            '        </div>' +--}}
        {{--                            '    </div>' +--}}
        {{--                            '       <div id="FanCmtFilter' + response['id'] + '" class="text-dark-75 font-size-lg font-weight-normal mb-2">' + response['div'] + ' </div>' +--}}
        {{--                            '    <div class="separator separator-solid mb-4"></div>' +--}}
        {{--                            '</div>');--}}
        {{--                        }--}}
        {{--                        else if (type == "nonfans") {--}}
        {{--                            var data1 = "'nonfans-update'";--}}

        {{--                            $('#addfanscmt').prepend(--}}
        {{--                                '<div id="commentDiv-' + response['id'] + '">' +--}}
        {{--                                '    <div class="d-flex mb-9">' +--}}
        {{--                                '        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">' +--}}
        {{--                                '            <div class="symbol symbol-50 symbol-lg-120">' +--}}
        {{--                                '                <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=large" alt="image">' +--}}
        {{--                                '            </div>' +--}}
        {{--                                '            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">' +--}}
        {{--                                '                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>' +--}}
        {{--                                '            </div>' +--}}
        {{--                                '        </div>' +--}}
        {{--                                '        <div class="flex-grow-1">' +--}}
        {{--                                '            <div class="d-flex justify-content-between flex-wrap mt-1">' +--}}
        {{--                                '                <div class="d-flex mr-3">' +--}}
        {{--                                '                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$page->pagename ?? 'page'}}</a>' +--}}
        {{--                                '                    <a href="#"><i class="flaticon2-correct text-success font-size-h5"></i></a>' +--}}
        {{--                                '                </div>' +--}}
        {{--                                '                <div class="my-lg-0 my-3">' +--}}
        {{--                                '                    <button type="button" class="btn btn-sm btn-light-danger font-weight-bolder text-uppercase" id="' + response['id'] + '" onclick="deletecomment(this.id)">Delete</button>' +--}}
        {{--                                '                    <button  id="comments-fans-' + response['id'] + '" onclick="select(this.id,' + data1 + ');" class="btn btn-sm btn-info font-weight-bolder text-uppercase" data-toggle="modal" data-target="#edit_Comments_modal">Edit</button>' +--}}
        {{--                                '                </div>' +--}}
        {{--                                '            </div>' +--}}
        {{--                                '            <div class="d-flex flex-wrap justify-content-between mt-1">' +--}}
        {{--                                '                <div class="d-flex flex-column flex-grow-1 pr-8">' +--}}
        {{--                                '                    <span class="font-weight-bold text-dark-50" id="comm-fan-' + response['id'] + '">' + response['message'] + '</span>' +--}}
        {{--                                '                </div>' +--}}
        {{--                                '            </div>' +--}}
        {{--                                '        </div>' +--}}
        {{--                                '    </div>' +--}}
        {{--                                '       <div id="FanCmtFilter' + response['id'] + '" class="text-dark-75 font-size-lg font-weight-normal mb-2">' + response['div'] + ' </div>' +--}}
        {{--                                '    <div class="separator separator-solid mb-4"></div>' +--}}
        {{--                                '</div>');--}}
        {{--                        }--}}
        {{--                    }--}}
        {{--                    else if (response['status'] == 198) {--}}
        {{--                    }--}}
        {{--                    else if (response['status'] == 201) {--}}
        {{--                        $("#comm-fan-" + response['id']).html(response['message']);--}}
        {{--                        $('#FanCmtFilter' + response['id']).html(response['div']);--}}
        {{--                    }--}}
        {{--                    else if (response['status'] == 202) {--}}
        {{--                        $("#comm-nonfan-" + response['id']).html(response['message']);--}}
        {{--                        $('#nonFanCmtFilter' + response['id']).html(response['div']);--}}
        {{--                    }--}}
        {{--                    $("#comment").val('');--}}
        {{--                    tagifyDemo1.removeAllTags();--}}
        {{--                    model.find('#comment-type').val('fans');--}}
        {{--                    model.find('.close-modal').click();--}}
        {{--                }--}}
        {{--            });--}}
        {{--        }--}}

        {{--    }--}}
        {{--}--}}


        $('#message_fan_status').change(function() {
            var type = 'fan-message-status';
            var value = $('#message_fan_status').prop('checked');

            changePageStatus(value, type);
        });

        $('#like_non_fan_status').change(function() {
            var type = 'non-fan-like-status';
            var value = $('#like_non_fan_status').prop('checked');

            changePageStatus(value, type);
        });

        $('#comment_fan_status').change(function() {
            var type = 'fan-comment-status';
            var value = $('#comment_fan_status').prop('checked');

            changePageStatus(value, type);
        });

        function changePageStatus(value, type)
        {
            $.ajax({
                url : '/facebook/page-' + type + '/update',
                type : 'put',
                data : {
                    pageId : '{{$page->id}}',
                    status : value,
                },
                success : function (response) {
                    $("#success-comments").html('Comment added for fans').show();
                }
            });
        }

        {{--function changeStatus1(type) {--}}
        {{--    if (type) {--}}
        {{--        var idObj = $('#' + type);--}}
        {{--        var status;--}}
        {{--        if (idObj.prop("checked") == true) {--}}
        {{--            status = idObj.val();--}}
        {{--        } else {--}}
        {{--            status = 'off';--}}
        {{--        }--}}
        {{--        var id = '<?php echo $page->id ?>';--}}
        {{--        $.ajax({--}}
        {{--            url: '/user/page-status',--}}
        {{--            data: {--}}
        {{--                for_page_id: id,--}}
        {{--                glb_filter: status,--}}
        {{--                type: type--}}
        {{--            },--}}
        {{--            type: 'post',--}}
        {{--            dataType: 'json',--}}
        {{--            complete: function () {--}}
        {{--                $('#closepopcmts').hide();--}}
        {{--            },--}}
        {{--            success: function (response) {--}}
        {{--                if (response['status'] == 200 && type == 'fans') {--}}
        {{--                    $("#success-comments").html('Comment added for fans').show();--}}
        {{--                }--}}
        {{--                else if (response['status'] == 200 && type == 'nonfans') {--}}
        {{--                    $("#success-comments").html('Comment added for nonfans').show();--}}
        {{--                }--}}
        {{--                else if (response['status'] == 200 && type == 'global') {--}}
        {{--                    $("#success-comments").html('Comment added Global').show();--}}
        {{--                }--}}
        {{--            }--}}
        {{--        });--}}
        {{--    }--}}
        {{--}--}}

        function statuserror(message) {
            Swal.fire({
                title: "Completed!",
                text: message,
                icon: "info",
                buttonsStyling: false,
                confirmButtonText: "Confirm me!",
                customClass: {
                    confirmButton: "btn btn-primary"
                }
            });
        }

    </script>
    <script type="text/javascript">
        function deletemessage(id) {
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this message again!",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            var message = $('#fanmsgid').val();
                            $.ajax({
                                url: '/user/deletePageMsg',
                                data: {
                                    message: message,
                                    id: id,
                                    type: 'page'
                                },
                                type: 'post',
                                dataType: 'json',
                                success: function (data) {
                                    if (data['code'] == 200) {
                                        if (data['data'] == 1) {
                                            swal("Deleted!", "Your message has been deleted.", "success");
                                            $('#messageDiv-' + data['id']).css('display', 'none');
                                        } else
                                            swal("Error!", "The message not deleted there is some error.", "info");
                                    } else if (data['code'] == 400) {
                                        swal("Error!", "The message not deleted there is some error.", "info");
                                    }
                                }
                            });
                        }
                        else {
                            swal("Cancelled", "Your message is safe :)", "error");
                        }
                    });
        }

        function deleteComment(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(function(result) {
                if (result.value) {
                    var message = $('#fanmsgid').val();
                    $.ajax({
                        url: '/facebook/delete-page-auto-comment',
                        data: {
                            _token : '{{csrf_token()}}',
                            commentId : id
                        },
                        type: 'delete',
                        dataType: 'json',
                        success: function (data) {
                            Swal.fire({
                                title: "Success!",
                                text: "Your message has been deleted.",
                                icon: "success",
                                buttonsStyling: false,
                                confirmButtonText: "yes",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                            $('#commentDiv-' + data.id).remove();
                        },
                        error : function() {
                            Swal.fire({
                                title: "Error!",
                                text: "Error",
                                icon: "info",
                                buttonsStyling: false,
                                confirmButtonText: "Confirm me!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                        }
                    });
                }
            });
        }

        $('.caller').on('click', function () {
            var glb_filter=$('#gblFiltersData').val();
            if (glb_filter) {
                glb_filter = glb_filter.trim();
            }
            var successSave = false;
            var id = '<?php echo $page->id ?>';
            $.ajax({
                url: '/user/page-status',
                data: {
                    for_page_id: id,
                    glb_filter: glb_filter,
                    type: 'global_filter'
                },
                type: 'post',
                async: false,
                dataType: 'json',
                complete: function () {
                    $('#closepopcmts').hide();
                },
                success: function (response) {
                    if (response) {
                        successSave = true;
                        Swal.fire({
                            title: "Congratulations!",
                            text: "Your Global filter is set successfully.",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Confirm me!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    } else {
                        Swal.fire({
                            title: "Error!",
                            text: "Some thing went wrong.Your Global filter is not set.",
                            icon: "info",
                            buttonsStyling: false,
                            confirmButtonText: "Confirm me!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    }
                }
            });

            var glb_filter=$('#postFiltersData').val();
            if (glb_filter) {
                glb_filter = glb_filter.trim();
            }
            var id = '<?php echo $page->id ?>';
            $.ajax({
                url: '/user/page-status',
                data: {
                    for_page_id: id,
                    glb_filter: glb_filter,
                    type: 'post_filter'
                },
                type: 'post',
                datatype: 'json',
                complete: function () {
                    $('#closepopcmts').hide();
                },
                success: function (response) {
                    if (response) {

                        let msg = 'Post';

                        if (successSave) {
                            msg += ' and Global';
                        }

                        Swal.fire({
                            title: "Congratulations!",
                            text: "Your " + msg + " filter is set successfully.",
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Confirm me!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    } else {
                        let msg = 'Post';

                        if (!successSave) {
                            msg += ' and Global';
                        }

                        //swal("Error!", "Some thing went wrong.Your Post filter is not set.", "info");
                        Swal.fire({
                            title: "Error!",
                            text: "Some thing went wrong.Your " + msg + " filter is not set.",
                            icon: "info",
                            buttonsStyling: false,
                            confirmButtonText: "Confirm me!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        });
                    }
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.add-filters').on('click', function () {
                var grantParent = $(this).parents('.filterDiv:first');
                var parent = $(this).parents('.input-field:first');
                var filter=$(parent).find("input").val().trim();
                if(filter !==''){
                    filter=filter.split(',');
                    var type='';
                    if($(this).hasClass('positive')){
                        type='<i class="fa fa-plus-square" aria-hidden="true" style="color: #39e600; font-size: 110%"></i> ';
                    }else if($(this).hasClass('negative')){
                        type='<i class="fa fa-minus-square" aria-hidden="true" style="color: #ff8080; font-size: 110%"></i> ';
                    }
                    $(grantParent).find('.filter-list').parent().css('display','block');
                    $.each(filter,function (i,val) {
                        val=val.trim();
                            if(val !=='') {
                            $(grantParent).find('.filter-list').append(' <div class="label label-info label-inline">' +type+ val + ' <i class="close icon-close delete-filter"></i></div> ');
                        }
                    });
                    $(parent).find("input").val(null);
                }

            });

            $(document.body).on('click', '.delete-filter', function () {
                $(this).parent().remove();
            });
        });
        @if(session()->has('msg'))
                $(window).on('load', function () {
            statuserror('{{session('msg')}}');
        });
        @endif
            $('#glb_filter_activity,#msg_filter_word-po,#msg_filter_word-ne,#cmt_filter_word-po,#post_filter_activity,#cmt_filter_word-ne').keypress(function (e) {
            if (e.which == 13) {
                $(this).siblings().trigger('click');
            }
        });
    </script>

    <script>
        $(document).ready(function(){
            $('#edit_Comments_modal .ki-close, #edit_Comments_modal .close-modal').on('click', function (){
                $("#edit_Comments_modal #comment").val('');
                tagifyDemo1.removeAllTags();
                $('#edit_Comments_modal').find('#comment-type').val('fans');
            })
            $('.more-info').on('mouseover',function(){
                $(this).addClass('hide');
                $(this).parents('.toltip:first').css('width','300px').css('margin','-6px 0 0 -143px');
                $(this).siblings('.more-info-text').removeClass('hide');
            });
            $('.more-info-text').on('mouseout',function(){
                $(this).addClass('hide');
                $(this).parents('.toltip:first').css('width','90px').css('margin','-6px 0 0 -37px');
                $(this).siblings('.more-info').removeClass('hide');
            });
        });
        $('.mam1old').on('click',function () {
            $('#msgAtt').openModal();
        });
        $('.mac1').on('click',function () {
            $('#cmtAtt').openModal();
        });
    </script>

    <script>
        // function changeStatusPost(event) {
        //     var type = $(event).attr('data-type');
        //     if (type) {
        //         var idObj = $(event);
        //         var status;
        //         if (idObj.prop("checked") == true) {
        //             status = idObj.val();
        //         } else {
        //             status = 'off';
        //         }
        //         var id = idObj.attr('data-id');
        //         $.ajax({
        //             url: '/user/post-status',
        //             data: {
        //                 for_page_id: id,
        //                 status: status,
        //                 type: type
        //             },
        //             type: 'post',
        //             datatype: 'json',
        //             success: function (response) {
        //                 response = $.parseJSON(response);
        //                 if (response['status'] == 200 && type == 'fans') {
        //                     $("#success-comments").html('Comment added for fans').show();
        //                 }
        //                 else if (response['status'] == 200 && type == 'nonfans') {
        //                     $("#success-comments").html('Comment added for nonfans').show();
        //                 }
        //                 else if (response['status'] == 200 && type == 'global') {
        //                     $("#success-comments").html('Comment added Global').show();
        //                 }
        //             }
        //         });
        //     }
        // }
    </script>
@endsection
