@extends('layouts.master')
@section('title','SocioLeads | Posts')
@section('style')
    <style>
        .card p{
            margin: 6px;
        }
        .card span{
            margin: 6px;
        }
        #waterfall {
            margin: 10px;
        }
        #waterfall p{
            word-wrap: break-word;
        }
        #waterfall li.show {
            opacity: 1;
            transform: translateY(0);
            transition: all 0.3s, top 1s;
        }

        #waterfall li > div {
            color: rgba(0, 0, 0, 0.6);
            border-radius: 3px;
            margin: 10px;
            padding: 15px;
            background: rgb(255, 255, 255);
            border: 1px solid rgba(038, 191, 64, 0);
            transition: all 0.5s;
        }

        #waterfall li > div:hover {
            transform: translateY(-10px);
            border: 1px solid #d6d6d6;
            box-shadow: 0 10px 15px rgba(154, 187, 221, 0.4);
            transition: all 0.3s;
        }

        .tt .toltip{display: none;}
        .tt:hover .toltip {
            display: block;
            margin: -6px 0 0 -62px;
            position: absolute;
            width: 140px;
        }

        .arrow_box {
            position: relative;
            background: #73b4f9;
            border: 2px solid #61a2e7;
            color: #fff;
            padding: 4px 7px;font-size: 12px;
            border-radius: 10px;
            margin: 10px 0 0 0;
            z-index: 9999;
        }

        .arrow_box a{ color: #fff; text-decoration: underline;}
        .arrow_box:after, .arrow_box:before {
            bottom: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(136, 183, 213, 0) rgba(136, 183, 213, 0) #73b4f9;
            border-width: 11px;
            margin-left: -11px;
        }
        .arrow_box:before {
            border-color:  rgba(194, 225, 245, 0) rgba(194, 225, 245, 0) #61a2e7;
            border-width: 13px;
            margin-left: -13px;
        }
        #waterfall li {
            list-style-type: none;
        }
    </style>
@endsection
@section('content')
    <!--begin::Header-->
    <div id="kt_header" class="header header-fixed">
        <!--begin::Container-->
        <div class=" container-fluid  d-flex align-items-stretch justify-content-between">
            <!--begin::Header Menu Wrapper-->
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5"> All Posts </h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <!--end::Actions-->
                <a href="/facebook/page-settings/{{$page->id}}" class="btn btn-light-primary font-weight-bolder btn-sm mr-2"> Page Settings </a>
                <button class="btn btn-light-warning font-weight-bolder btn-sm mr-2 modal-trigger" data-toggle="modal" data-target="#unable_locate">
                    Are you unable to locate your post on this page?
                </button>
            </div>
            <!--end::Info-->
            <!--end::Header Menu Wrapper-->
            @include('layouts.top-toolbar')
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header-->

    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Row-->
                <ul id="waterfall">
                    @if (count($posts) > 0)
                        @foreach ($posts as $post)
                            <li class="hide" id="test' . $i . '">
                                <div class="">
                                    <!--begin::Card-->
                                    <div class="card card-custom gutter-b card-stretch">
                                        <!--begin::Body-->
                                        <div class="card-body pt-4">
                                            <!--begin::User-->
                                            <div class="d-flex align-items-center mb-7">
                                                <!--begin::Pic-->
                                                <div class="flex-shrink-0 mr-4">
                                                    <div class="symbol symbol-circle symbol-lg-75">
                                                        <img src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture?type=small" alt="" class="responsive-img">
                                                    </div>
                                                </div>
                                                <!--end::Pic-->
                                                <!--begin::Title-->
                                                <div class="d-flex flex-column">
                                                    <a href="#" class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">{{$page->page_name}}</a>
                                                    <span class="text-muted font-weight-bold">{{getTimeDiff($post->facebook_post_date)}}</span>
                                                </div>
                                                <!--end::Title-->
                                            </div>
                                            <!--end::User-->
                                            <!--begin::Desc-->
                                            <p class="mb-7">{{$post->feed_message}}</p>
                                            <!--end::Desc-->
                                            <!--begin::Info-->
                                            <div class="mb-7">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">Message Replies: {{$postReportRepository->setPostId($post->id)->setLastDays(null)->getMessagesCount()}}</span>
                                                    <span class="switch switch-outline switch-icon switch-primary">
                                                <label>
                                                    <input type="checkbox" data-type="post_message_fan_status" id="post_message_fan_status" data-id="{{$post->id}}" checked="{{$post->do_reply_message}}">
                                                    <span></span>
                                                </label>
                                            </span>
                                                </div>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">Like Comments: {{$postReportRepository->setPostId($post->id)->setLastDays(null)->getLikesCount()}}</span>
                                                    <span class="switch switch-outline switch-icon switch-danger">
                                                <label>
                                                    <input type="checkbox" data-type="post_like_status" id="post_like_status" data-id="{{$post->id}}" checked="{{$post->do_like_comment}}">
                                                    <span></span>
                                                </label>
                                            </span>
                                                </div>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <span class="text-dark-75 font-weight-bolder mr-2">Comment Replies: {{$postReportRepository->setPostId($post->id)->setLastDays(null)->getCommentsCount()}}</span>
                                                    <span class="switch switch-outline switch-icon switch-info">
                                                <label>
                                                    <input type="checkbox" data-type="post_comment_fan_status" id="post_comment_fan_status" data-id="{{$post->id}}" checked="{{$post->do_reply_comment}}">
                                                    <span></span>
                                                </label>
                                            </span>
                                                </div>
                                            </div>
                                            <!--end::Info-->
                                            <div class="btn-group btn-block" role="group">
                                                <a href="/facebook/report/post/{{$post->id}}" class="btn  btn-text-success btn-hover-light-primary font-weight-bold text-uppercase py-4">Report</a>
                                                <a href="https://www.facebook.com/{{$post->facebook_post_id}}" class="btn btn-text-primary btn-hover-light-primary font-weight-bold text-uppercase py-4">Go to  Facebook</a>
                                            </div>
                                            <a href="/facebook/post-comment-settings/{{$post->id}}" type="button" class="btn btn-info btn-lg btn-block btn-sm">Manage Auto Comments</a>
                                            <a href="/facebook/post-message-settings/{{$post->id}}" type="button" class="btn btn-primary btn-lg btn-block btn-sm">Manage Auto Messages</a>
                                        </div>

                                        <!--end::Body-->
                                    </div>
                                    <!--end:: Card-->
                                </div>
                            </li>
                        @endforeach
                    @else
                        <div class="center deep-orange-text text-accent-3">There are no post available for this page!</div>
                    @endif
                </ul>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--begin::Footer-->
    @include('layouts.page-footer')
    <!--end::Footer-->

    <!-- Add new comment Modal-->
    <div class="modal fade" id="unable_locate" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Potential Reasons Your Post Isn't Showing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>Your Facebook post is set to "private". Please make sure your post is a public post as only those posts will show up here.</li>
                        <li>If your Facebook post is public, but still does not show up here please go to your page and LIKE or COMMENT on your post. It should then show up on this page within a few minutes.</li>
                        <li>If your post has a video we do not show the video here on this page because it would affect load times. You will still see the posts here, however, we don't display the videos on this page. You can still edit the post settings
                            as usual.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script_function')
    <script>
        $('#post_message_fan_status').change(function() {
            var type = 'fan-message-status';
            var value = $('#post_message_fan_status').prop('checked');
            var id = $(this).attr('data-id');

            changePageStatus(value, type, id);
        });

        $('#post_like_status').change(function() {
            var type = 'non-fan-like-status';
            var value = $('#post_like_status').prop('checked');
            var id = $(this).attr('data-id');

            changePageStatus(value, type, id);
        });

        $('#post_comment_fan_status').change(function() {
            var type = 'fan-comment-status';
            var value = $('#post_comment_fan_status').prop('checked');
            var id = $(this).attr('data-id');

            changePageStatus(value, type, id);
        });

        function changePageStatus(value, type, id)
        {
            $.ajax({
                url : '/facebook/post-' + type + '/update',
                type : 'put',
                data : {
                    postId : id,
                    status : value,
                    _token : '{{csrf_token()}}',
                },
                success : function (response) {
                    $("#success-comments").html('Comment added for fans').show();
                }
            });
        }

        // function changeStatusPost(event) {
        //     var type = $(event).attr('data-type');
        //     if (type) {
        //         var idObj = $(event);
        //         var status;
        //         if (idObj.prop("checked") == true) {
        //             status = idObj.val();
        //         } else {
        //             status = 'off';
        //         }
        //         var id = idObj.attr('data-id');
        //         $.ajax({
        //             url: '/user/post-status',
        //             data: {
        //                 for_page_id: id,
        //                 status: status,
        //                 type: type
        //             },
        //             type: 'post',
        //             datatype: 'json',
        //             success: function (response) {
        //                 response = $.parseJSON(response);
        //                 if (response['status'] == 200 && type == 'fans') {
        //                     $("#success-comments").html('Comment added for fans').show();
        //                 }
        //                 else if (response['status'] == 200 && type == 'nonfans') {
        //                     $("#success-comments").html('Comment added for nonfans').show();
        //                 }
        //                 else if (response['status'] == 200 && type == 'global') {
        //                     $("#success-comments").html('Comment added Global').show();
        //                 }
        //             }
        //         });
        //     }
        // }

    </script>
@endsection
