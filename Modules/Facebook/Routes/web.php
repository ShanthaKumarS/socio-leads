<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Facebook\Http\Controllers\AutoReply\PageAutoCommentController;
use Modules\Facebook\Http\Controllers\AutoReply\PostAutoCommentController;
use Modules\Facebook\Http\Controllers\AutoReply\PostAutoMessageController;
use Modules\Facebook\Http\Controllers\BroadCastMessageController;
use Modules\Facebook\Http\Controllers\Page\PageSettingController;
use Modules\Facebook\Http\Controllers\JobController;
use Modules\Facebook\Http\Controllers\Reports\FacebookReportController;
use Modules\Facebook\Http\Controllers\Reports\PageReportController;
use Modules\Facebook\Http\Controllers\Reports\PostReportController;
use Modules\Facebook\Http\Controllers\SendMessageNow\SendMessageNowController;
use Modules\Facebook\Http\Controllers\SequenceMessage\DateSequenceController;
use Modules\Facebook\Http\Controllers\LoginController;
use Modules\Facebook\Http\Controllers\Page\PageDeleteController;
use Modules\Facebook\Http\Controllers\Page\PageDisplayController;
use Modules\Facebook\Http\Controllers\Page\PageStoreController;
use Modules\Facebook\Http\Controllers\PageBroadCastMessageController;
use Modules\Facebook\Http\Controllers\Post\PostController;
use Modules\Facebook\Http\Controllers\Post\PostSettingController;
use Modules\Facebook\Http\Controllers\SequenceMessage\RecurringSequenceController;
use Modules\Facebook\Http\Controllers\SequenceMessage\SequenceController;
use Modules\Facebook\Http\Controllers\SequenceMessage\StepSequenceController;
use Modules\Facebook\Http\Controllers\Webhook\WebhookController;

Route::middleware('auth')->prefix('facebook')->group(function() {
    Route::get('get-pages', [PageDisplayController::class, 'index'])
        ->name('facebook.getPages');

    Route::post('store-pages', [PageStoreController::class, 'store'])
        ->name('facebook.store-pages');

    Route::get('delete-page/{pageId}', [PageDeleteController::class, 'delete'])
        ->name('facebook.deletePages');

    Route::get('login', [LoginController::class, 'login'])
        ->name('facebook.login');


    Route::get('posts/{pageId}', [PostController::class, 'index'])
        ->name('facebook.posts');

    Route::get('broadcast-message/{pageId}', [BroadCastMessageController::class, 'index'])
        ->name('page.broadcast');

    Route::post('broadcast-message-configure', [BroadCastMessageController::class, 'configure'])
        ->name('broadcast.configure');

    Route::post('get-subscribed-users', [BroadCastMessageController::class, 'getSubscribedUsers'])
        ->name('broadcast.getUsers');

    Route::post('broadcast-msg-now', [SendMessageNowController::class, 'sendMessage'])
        ->name('broadcast.message-now');

    Route::get('broadcast-msg-now/stats/{pageId}', [SendMessageNowController::class, 'getStats'])
        ->name('broadcast.message-now.stats');

    Route::post('step-sequence-store', [StepSequenceController::class, 'store'])
        ->name('step.sequence.store');

    Route::post('date-sequence-store', [DateSequenceController::class, 'store'])
        ->name('date.sequence.store');

    Route::post('recurring-sequence-store', [RecurringSequenceController::class, 'store'])
        ->name('recurring.sequence.store');

    Route::delete('step-sequence-delete', [StepSequenceController::class, 'destroy'])
        ->name('step.sequence.destroy');

    Route::delete('date-sequence-delete', [DateSequenceController::class, 'destroy'])
        ->name('date.sequence.destroy');

    Route::delete('recurring-sequence-delete', [RecurringSequenceController::class, 'destroy'])
        ->name('recurring.sequence.destroy');

    Route::delete('sequence-message-delete', [SequenceController::class, 'destroyMessage'])
        ->name('sequence-message.destroy');

    Route::get('edit-sequence-message/{squenceId}', [SequenceController::class, 'editMessage'])
        ->name('sequence-message.edit');

    Route::get('update-sequence-message', [SequenceController::class, 'updateMessage'])
        ->name('sequence-message.delete');

    Route::get('step-sequence-messages-stats/{sequenceId}', [StepSequenceController::class, 'index'])
        ->name('step-sequence.stats');

    Route::get('date-sequence-messages-stats/{sequenceId}', [DateSequenceController::class, 'index'])
        ->name('date-sequence.stats');

    Route::get('recurring-sequence-messages-stats/{sequenceId}', [RecurringSequenceController::class, 'index'])
        ->name('recurring-sequence.stats');

    Route::post('save-step-sequence-messages', [StepSequenceController::class, 'save'])
        ->name('step-sequence-message.save');

    Route::post('save-date-sequence-messages', [DateSequenceController::class, 'save'])
        ->name('date-sequence-message.save');

    Route::post('save-recurring-sequence-messages', [RecurringSequenceController::class, 'save'])
        ->name('recurring-sequence-message.save');

    Route::get('page-settings/{pageId}', [PageSettingController::class, 'index'])
        ->name('recurring-sequence.index');

    Route::put('page-fan-message-status/update', [PageSettingController::class, 'updateFanMessageStatus'])
        ->name('page-fan-message-status.update');

    Route::put('page-non-fan-like-status/update', [PageSettingController::class, 'updateNonFanLikeStatus'])
        ->name('page-non-fan-like-status.update');

    Route::put('page-fan-comment-status/update', [PageSettingController::class, 'updateFanCommentStatus'])
        ->name('recurring-sequence.update');

    Route::put('post-fan-message-status/update', [PostSettingController::class, 'updateFanMessageStatus'])
        ->name('post-fan-message-status.update');

    Route::put('post-non-fan-like-status/update', [PostSettingController::class, 'updateNonFanLikeStatus'])
        ->name('post-non-fan-like-status.update');

    Route::put('post-fan-comment-status/update', [PostSettingController::class, 'updateFanCommentStatus'])
        ->name('post-non-fan-comment-status.update');

    Route::post('store-page-auto-comment/{pageId}', [PageAutoCommentController::class, 'storeComment'])
        ->name('page-fan-comment-status.store');

    Route::post('store-post-auto-comment/{postId}', [PostAutoCommentController::class, 'storeComment'])
        ->name('post-fan-comment-status.store');

    Route::post('store-post-auto-message/{postId}', [PostAutoMessageController::class, 'storeMessage'])
        ->name('post-fan-message-status.store');

    Route::delete('delete-page-auto-comment', [PageAutoCommentController::class, 'destroy'])
        ->name('page-auto-comment.delete');

    Route::delete('delete-post-auto-comment', [PostAutoCommentController::class, 'destroy'])
        ->name('post-auto-comment.delete');

    Route::delete('delete-post-auto-message', [PostAutoMessageController::class, 'destroy'])
        ->name('post-auto-message.delete');

    Route::get('post-comment-settings/{postId}', [PostAutoCommentController::class, 'index'])
        ->name('post.auto-comment.index');

    Route::get('post-message-settings/{postId}', [PostAutoMessageController::class, 'index'])
        ->name('post.auto-comment.index');

    Route::prefix('report')->group(function (){
        Route::get('/', [FacebookReportController::class, 'report'])
            ->name('report');

        Route::post('/graphs', [FacebookReportController::class, 'reportGraphDetails'])
            ->name('report.ajax');

        Route::get('/page/{pageId}', [PageReportController::class, 'pageReport'])
            ->name('page.report');

        Route::post('/graphs/page/{pageId}', [PageReportController::class, 'pageReportGraphDetails'])
            ->name('page.report.ajax');

        Route::get('/post/{postId}', [PostReportController::class, 'postReport'])
            ->name('page.report');

        Route::post('/graphs/post/{postId}', [PostReportController::class, 'postReportGraphDetails'])
            ->name('post.report.ajax');
    });
});

Route::prefix('webhook')->group(function() {
    Route::prefix('facebook')->group(function() {

        Route::get('/', [WebhookController::class, 'verify'])
            ->name('webhook.facebook.verify');

        Route::post('/', [WebhookController::class, 'handle'])
            ->name('webhook.facebook.handle');
    });
});
