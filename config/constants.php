<?php
return [
    'VERIFIED' => 1,
    'DEFAULT_PAGE_SUBSCRIPTION_MESSAGE' => 'Hi {{fb_first_name}} , ! I will keep you updated on great activities as well as important information about our page. Do you want to receive occasional notifications from me ?',
    'DEFAULT_PAGE_WELCOME_MESSAGE' => 'Hi @{{fb_first_name}}! Nice to see you. This is the default welcome message for your bot. Messages from this block are shown to your users when they first start to chat with your bot. Make sure to never leave this block empty (write something helpful to your users)',
    'SUBSCRIBE' => 'SUBSCRIBE',
    'UNSUBSCRIBE' => 'UNSUBSCRIBE',
    'WEEK_DAYS' => [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 =>'Thursday', 5 => 'friday', 6 =>'Saturday', 7 =>'Sunday'],
    'NO_OF_JOBS_TO_WORK' => 50,
    'ZERO' => 0,
    'CAMPAIGN' => [
        'DEFAULT_BUTTON_SIZE' => 1,
        'DEFAULT_BUTTON_COLOR' => '#fff',
        'DEFAULT_BUTTON_TEXT_COLOR' => '#187DE4',
    ],
    'AUTO_RESPONDERS' => [
        'GET_RESPONSE' => [
            'CREDENTIALS' => [
                'API_KEY' => 'apiKey'
            ]
        ],
    ],
];
